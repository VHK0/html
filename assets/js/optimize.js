!(function(e) {
    function t(i) { if (n[i]) return n[i].exports; var r = n[i] = { exports: {}, id: i, loaded: !1 }; return e[i].call(r.exports, r, r.exports, t), r.loaded = !0, r.exports } var n = {}; return t.m = e, t.c = n, t.p = "", t(0) })([(function(e, t, n) {
    function i() {
        function e(e) { var t = n(150),
                i = [t];
            i.push(n(152)), i.push(n(158)), i.push(n(161)), i.push(n(164)), i.push(n(166)), i.push(n(176)), i.push(n(179)), i.push(n(181)), i.push(n(184)), i.push(n(187)), i.push(n(191)), i.push(n(194)), i.push(n(198)), i.push(n(201)), i.push(n(205)), i.push(n(209)), i.push(n(214)), i.push(n(215)), i.push(n(218)), i.push(n(221)), i.push(n(225)), i.push(n(227)), i.push(n(232)), i.push(n(235)), i.push(n(236)), i.push(n(238)), i.push(n(239)), i.push(n(240)), h.initialize({ clientData: e, plugins: i }) }

        function t(e, t, n) { return "/dist/preview_data.js?token=__TOKEN__&preview_layer_ids=__PREVIEW_LAYER_IDS__".replace("__TOKEN__", e).replace("__PROJECT_ID__", t).replace("__PREVIEW_LAYER_IDS__", n.join(",")).replace("__GET_ONLY_PREVIEW_LAYERS__", !0) }
        window.performance && window.performance.mark && window.performance.mark("optimizely:blockBegin");
        var i = n(1);
        i.initialize();
        var r = n(84),
            a = n(23),
            o = n(16);
        n(130);
        var s = o.get("stores/directive"),
            c = n(91);
        if (!c.isCORSSupported()) throw new Error("CORS is not supported on this browser, aborting.");
        var u, l = n(133),
            d = n(118),
            f = n(134),
            p = {
                "layers": [{ "holdback": 0, "activation": {}, "integrationSettings": {}, "integrationStringVersion": 2, "viewIds": ["10799399934", "16627720029"], "experiments": [{ "weightDistributions": [{ "entityId": "10449173693", "endOfRange": 10000 }], "audienceName": "Not QA,Not in Africa", "name": "Hotfix - PAGE CHANGE", "bucketingStrategy": null, "variations": [{ "id": "10449640726", "actions": [], "name": "Original" }, { "id": "10449173693", "actions": [{ "viewId": "16627720029", "changes": [{ "dest": "https://www.mintmobile.com/refer-a-friend/", "allowAdditionalRedirect": true, "dependencies": [], "preserveParameters": true, "type": "redirect", "id": "83F4AFEF-81D2-4AF5-84D6-F828549CC87A" }] }], "name": "Variation #1" }], "audienceIds": ["and", "11216211532", "13333340160"], "changes": null, "id": "10431126053", "integrationSettings": null }], "id": "10419643426", "weightDistributions": null, "name": "Hotfix - PAGE CHANGE", "groupId": null, "commitId": "16923178151", "decisionMetadata": null, "policy": "single_experiment", "changes": null }, {
                    "holdback": 0,
                    "activation": {},
                    "integrationSettings": {},
                    "integrationStringVersion": 2,
                    "viewIds": ["9420962616"],
                    "experiments": [{
                        "weightDistributions": [{ "entityId": "11942881303", "endOfRange": 10000 }],
                        "audienceName": "Everyone else",
                        "name": "Hotfix - ECOMM-2359 - Add Sales Phone Number to FAQ Page",
                        "bucketingStrategy": null,
                        "variations": [{ "id": "11944811603", "actions": [{ "viewId": "9420962616", "changes": [] }], "name": "Original" }, {
                            "id": "11942881303",
                            "actions": [{
                                "viewId": "9420962616",
                                "changes": [{
                                    "dependencies": [],
                                    "type": "custom_code",
                                    "id": "F3B46C9F-DBD8-4F11-9F36-B19293624A67",
                                    "value": function($) {
                                        var utils = optimizely.get('utils');

                                        utils.waitForElement('.Footer').then(function() {
                                            var hLs = document.querySelector('.HelpLinks');
                                            hLs.innerHTML = '<div class="PhoneType px-md-3 my-md-3"><a href="/setup-for-android" class="d-flex justify-content-center align-items-center" data-ga="Setting Up Your Data Help Link|value|Setting Up Your Data For Android"><p><span>SET UP YOUR MMS &amp; DATA</span><span>FOR ANDROID</span></p><img class="img-fluid d-md-none mobile" src="https://cdn.mintmobile.com/wp-content/themes/mintmobile/images/help-android.jpg" alt="Set Up Your MMS &amp; Data For Android"><img class="img-fluid d-none d-md-block desktop" src="https://cdn.mintmobile.com/wp-content/themes/mintmobile/images/help-android-desktop.jpg" alt="Set Up Your MMS &amp; Data For Android"></a></div><div class="PhoneType px-md-3 my-md-3"><a href="/setup-for-iphone" class="d-flex justify-content-center align-items-center" data-ga="Setting Up Your Data Help Link|value|Setting Up Your Data For iPhone"><p><span>SET UP YOUR MMS &amp; DATA</span><span>FOR iPHONE</span></p><img class="img-fluid d-md-none mobile" src="https://cdn.mintmobile.com/wp-content/themes/mintmobile/images/help-iphone.jpg" alt="Set Up Your MMS &amp; Data For iPhone"><img class="img-fluid d-none d-md-block desktop" src="https://cdn.mintmobile.com/wp-content/themes/mintmobile/images/help-iphone-desktop.jpg" alt="Set Up Your MMS &amp; Data For iPhone"></a></div>';
                                        });
                                    }
                                }, { "name": "FAQ Sales Phone Number", "config": {}, "id": "BD5F55AD-3946-49C0-A1B6-86AED8866B47", "dependencies": [], "type": "widget", "widget_id": "11787897034" }]
                            }],
                            "name": "Variation #1"
                        }],
                        "audienceIds": null,
                        "changes": null,
                        "id": "11944672239",
                        "integrationSettings": null
                    }],
                    "id": "11976120434",
                    "weightDistributions": null,
                    "name": "Hotfix - ECOMM-2359 - Add Sales Phone Number to FAQ Page",
                    "groupId": null,
                    "commitId": "15142310561",
                    "decisionMetadata": null,
                    "policy": "single_experiment",
                    "changes": null
                }, {
                    "holdback": 0,
                    "activation": {},
                    "integrationSettings": {},
                    "integrationStringVersion": 2,
                    "viewIds": ["15505140899", "15543670327"],
                    "experiments": [{
                        "weightDistributions": [{ "entityId": "15474630671", "endOfRange": 10000 }],
                        "audienceName": "Everyone else",
                        "name": "Influencers - Free Item w Purchase",
                        "bucketingStrategy": null,
                        "variations": [{ "id": "15494780033", "actions": [], "name": "Original" }, {
                            "id": "15474630671",
                            "actions": [{ "viewId": "15505140899", "changes": [{ "name": "Free SKU w Coupon", "config": {}, "widget_id": "15502310823", "dependencies": [], "type": "widget", "id": "0D49B403-29EA-4336-A0C7-20CCE532A0DF" }] }, {
                                "viewId": "15543670327",
                                "changes": [{
                                    "dependencies": [],
                                    "type": "custom_code",
                                    "id": "A8CD3D22-BEF9-4943-8D98-4DD7CF672CB7",
                                    "value": function($) {
                                        var utils = optimizely.get('utils');

                                        utils.waitForElement('.Footer')
                                            .then(function(elem) {

                                                var grip = jQuery("a[href$='grip/']");

                                                var gripParent = grip.parent()[0];
                                                gripParent.innerHTML = "<p style='margin:0;padding:0;'>FREE Mint Loop Grip</p>";
                                            });
                                    }
                                }]
                            }],
                            "name": "Variation #1"
                        }],
                        "audienceIds": null,
                        "changes": null,
                        "id": "15494470491",
                        "integrationSettings": null
                    }],
                    "id": "15494391103",
                    "weightDistributions": null,
                    "name": "Influencers - Free Item w Purchase",
                    "groupId": null,
                    "commitId": "15531610494",
                    "decisionMetadata": null,
                    "policy": "single_experiment",
                    "changes": null
                }, { "holdback": 0, "activation": {}, "integrationSettings": {}, "integrationStringVersion": 2, "viewIds": ["10679811972", "14890110471", "9481221941"], "experiments": [{ "weightDistributions": [{ "entityId": "15885860373", "endOfRange": 10000 }], "audienceName": "Not QA", "name": "Hotfix - ECOMM-2360 -  \"Here's What You Get Section\"", "bucketingStrategy": null, "variations": [{ "id": "15901100991", "actions": [{ "viewId": "10679811972", "changes": [{ "value": "<style>.Benefits-original {\n  display: block !important;\n}\n\n.Benefits-updated {\n  display: none !important;\n}</style>", "selector": "head", "dependencies": [], "type": "append", "id": "3c2601e60aa6417aad0da421bb08d289" }] }, { "viewId": "14890110471", "changes": [{ "value": "<style>.Benefits-original {\n  display: block !important;\n}\n\n.Benefits-updated {\n  display: none;\n}</style>", "selector": "head", "dependencies": [], "type": "append", "id": "0d6280044336425fa6fab9f3518d00a4" }] }, { "viewId": "9481221941", "changes": [{ "value": "<style>.Benefits-updated {\n  display: none !important;\n}\n\n.Benefits-original {\n  display: block !important;\n}</style>", "selector": "head", "dependencies": [], "type": "append", "id": "56d9bb8ff7bb4c4a92650d2513c337f9" }] }], "name": "Original" }, { "id": "15885860373", "actions": [{ "viewId": "10679811972", "changes": [] }, { "viewId": "14890110471", "changes": [] }, { "viewId": "9481221941", "changes": [] }], "name": "Variation #1" }], "audienceIds": ["and", "11216211532"], "changes": null, "id": "15897530457", "integrationSettings": null }], "id": "15871850669", "weightDistributions": null, "name": "100% of traffic MM-23[Home/Plans/Product][Everyone] Update \"Here's What You Get Section\"", "groupId": null, "commitId": "17134710338", "decisionMetadata": null, "policy": "single_experiment", "changes": null }, { "holdback": 0, "activation": {}, "integrationSettings": {}, "integrationStringVersion": 2, "viewIds": ["16479340678"], "experiments": [{ "weightDistributions": [{ "entityId": "16454670867", "endOfRange": 10000 }], "audienceName": "Everyone else", "name": "Temporary - Kiosk Demo", "bucketingStrategy": null, "variations": [{ "id": "16489930303", "actions": [], "name": "Original" }, { "id": "16454670867", "actions": [{ "viewId": "16479340678", "changes": [{ "src": "/actions/a153e4c69d1947ff83fb1acb1c181c37e8b478876a3567999dfa88f9ce0af398.js", "dependencies": [], "id": "25BA43BC-6B7F-45B5-B8F6-32340AE3AA4A" }] }], "name": "Variation #1" }], "audienceIds": null, "changes": null, "id": "16490970705", "integrationSettings": null }], "id": "16490040256", "weightDistributions": null, "name": "Temporary - Kiosk Demo", "groupId": null, "commitId": "16507090065", "decisionMetadata": null, "policy": "single_experiment", "changes": null }, { "holdback": 0, "activation": {}, "integrationSettings": {}, "integrationStringVersion": 2, "viewIds": ["9414584294", "9481221941"], "experiments": [{ "weightDistributions": [{ "entityId": "16702790270", "endOfRange": 10000 }], "audienceName": "Not QA", "name": "100% Suppression MM-14[Plans][Everyone] Average Data Amount", "bucketingStrategy": null, "variations": [{ "id": "16702790270", "actions": [{ "viewId": "9481221941", "changes": [{ "value": "<style>#fox-banner {\n  display: none !important;\n}</style>", "selector": "head", "dependencies": [], "type": "append", "id": "6921FB78-1D15-4FC9-A1DB-C6A131D36B07" }] }, { "viewId": "9414584294", "changes": [{ "selector": "#fox-banner", "dependencies": [], "attributes": { "remove": true }, "type": "attribute", "id": "C18909CE-9826-4255-A3CB-E7F0E0393208", "css": {} }, { "name": "MM-12 : Best Seller Badge", "config": { "text": "My Butterbar" }, "id": "303343CA-80FB-4CB9-BDF9-E27387AE31A8", "dependencies": [], "type": "widget", "widget_id": "14551320006" }] }], "name": "Original" }, { "id": "16711680134", "actions": [{ "viewId": "9481221941", "changes": [] }], "name": "Variation #1" }], "audienceIds": ["and", "11216211532"], "changes": null, "id": "16711870115", "integrationSettings": null }], "id": "16713880468", "weightDistributions": null, "name": "100% Suppression MM-14[Plans][Everyone] Average Data Amount", "groupId": null, "commitId": "16867056605", "decisionMetadata": null, "policy": "single_experiment", "changes": null }, {
                    "holdback": 0,
                    "activation": {},
                    "integrationSettings": {},
                    "integrationStringVersion": 2,
                    "viewIds": ["10604744570", "14890110471", "9488460812"],
                    "experiments": [{
                        "weightDistributions": [{ "entityId": "16752740257", "endOfRange": 10000 }],
                        "audienceName": "QA Only",
                        "name": "MM-37[Product/Cart][New Visitors] Gift w/ Purchase - Ninja Loops",
                        "bucketingStrategy": null,
                        "variations": [{ "id": "16775690684", "actions": [{ "viewId": "14890110471", "changes": [] }, { "viewId": "10604744570", "changes": [] }], "name": "Original" }, {
                            "id": "16752740257",
                            "actions": [{
                                "viewId": "14890110471",
                                "changes": [{
                                    "dependencies": [],
                                    "type": "custom_code",
                                    "id": "F5C64F90-1E5B-4502-8984-03C83BFD7F67",
                                    "value": function($) {
                                        var utils = optimizely.get('utils');

                                        utils.waitForElement('.Footer')
                                            .then(function(elem) {

                                                // Prepend the extension html to the body
                                                function setCookie(cname, cvalue, exdays) {
                                                    var d = new Date();
                                                    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                                                    var expires = "expires=" + d.toUTCString();
                                                    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
                                                }

                                                function getCookie(name) {
                                                    var dc = document.cookie;
                                                    var prefix = name + "=";
                                                    var begin = dc.indexOf("; " + prefix);
                                                    if (begin == -1) {
                                                        begin = dc.indexOf(prefix);
                                                        if (begin != 0) return null;
                                                    } else {
                                                        begin += 2;
                                                        var end = document.cookie.indexOf(";", begin);
                                                        if (end == -1) {
                                                            end = dc.length;
                                                        }
                                                    }
                                                    // because unescape has been deprecated, replaced with decodeURI
                                                    return decodeURI(dc.substring(begin + prefix.length, end));
                                                }

                                                var coupon_cookie = getCookie("ultra_custom_discount");

                                                if (coupon_cookie == null) {
                                                    setCookie('ultra_custom_discount', 'mm_37_gwp_ninja', 5);
                                                }

                                            });
                                    }
                                }, { "name": "MM-37 [Product] Gift With Purchase", "config": { "img": "//cdn.optimizely.com/img/8894411182/5022342fbc274f989fed3f1449a6e920.png" }, "id": "9E1BC893-7F19-4468-99A9-62DC0E16AF86", "dependencies": [], "type": "widget", "widget_id": "16756580917" }]
                            }, { "viewId": "10604744570", "changes": [{ "value": "<style>.coupon-mm_37_gwp_ninja {\n  display: none;\n}</style>", "selector": "head", "dependencies": [], "type": "append", "id": "0BE4E993-DB0E-4C92-B6B9-2EEBA557EE02" }, { "name": "Influencer - Free Item w Coupon", "config": {}, "id": "EC40A58F-A66B-442C-BB83-9FFFA3EE0FF9", "dependencies": [], "type": "widget", "widget_id": "15502310823" }] }, { "viewId": "9488460812", "changes": [{ "value": "<style>.coupon-mm_37_gwp_ninja {\n  display: none;\n}</style>", "selector": "head", "dependencies": [], "type": "append", "id": "14944410-2A50-447D-8AB2-8E8945EC88B9" }] }],
                            "name": "Variation #1"
                        }],
                        "audienceIds": ["and", "10023895230"],
                        "changes": null,
                        "id": "16754531130",
                        "integrationSettings": null
                    }],
                    "id": "16772570017",
                    "weightDistributions": null,
                    "name": "MM-37[Product/Cart][New Visitors] Gift w/ Purchase - Ninja Loops",
                    "groupId": null,
                    "commitId": "17223310393",
                    "decisionMetadata": null,
                    "policy": "single_experiment",
                    "changes": null
                }, {
                    "holdback": 0,
                    "activation": {},
                    "integrationSettings": {},
                    "integrationStringVersion": 2,
                    "viewIds": ["9488460812"],
                    "experiments": [{
                        "weightDistributions": [{ "entityId": "16758910268", "endOfRange": 10000 }],
                        "audienceName": "QA Only",
                        "name": "MM-XX [Checkout][Everyone] Hide AR Section",
                        "bucketingStrategy": null,
                        "variations": [{ "id": "16766381039", "actions": [{ "viewId": "9488460812", "changes": [] }], "name": "Original" }, {
                            "id": "16758910268",
                            "actions": [{
                                "viewId": "9488460812",
                                "changes": [{
                                    "dependencies": [],
                                    "type": "custom_code",
                                    "id": "0C7D22BE-7D81-4014-AE4A-F6AC5DA9C37A",
                                    "value": function($) {
                                        utils = optimizely.get('utils');

                                        utils.waitForElement('#auto_recharge')
                                            .then(function(element) {
                                                var ar = document.getElementById('enable_auto_recharge');
                                                ar.checked = false;
                                            });
                                    }
                                }, { "value": "<style>#auto_recharge {\n  display: none !important;\n}</style>", "selector": "head", "dependencies": [], "type": "append", "id": "7FFA67CC-BFE0-47E0-B75F-522C45B26913" }]
                            }],
                            "name": "Variation #1"
                        }],
                        "audienceIds": ["and", "10023895230"],
                        "changes": null,
                        "id": "16752600208",
                        "integrationSettings": null
                    }],
                    "id": "16781540493",
                    "weightDistributions": null,
                    "name": "MM-XX [Checkout][Everyone] Hide AR Section",
                    "groupId": null,
                    "commitId": "16768330354",
                    "decisionMetadata": null,
                    "policy": "single_experiment",
                    "changes": null
                }, { "holdback": 0, "activation": {}, "integrationSettings": {}, "integrationStringVersion": 2, "viewIds": ["16885861792"], "experiments": [{ "weightDistributions": [{ "entityId": "16856809177", "endOfRange": 10000 }], "audienceName": "Everyone else", "name": "Hotfix - ECOMM 2534- RAF Legal Link", "bucketingStrategy": null, "variations": [{ "id": "16854707880", "actions": [], "name": "Original" }, { "id": "16856809177", "actions": [{ "viewId": "16885861792", "changes": [{ "selector": ".raf-tiered-legal", "dependencies": [], "attributes": { "html": "Referral credits limited to 10 referrals. Must have an active Mint account. Friend credits awarded for new customers only. May not be combined with other offers. Terms subject to change. See full rules at <a href=\"/referral-terms\" target=\"_blank\">www.mintmobile.com/referral-terms/</a>." }, "type": "attribute", "id": "786116B6-AEBF-4996-8133-0E86CC4E5117", "css": {} }] }], "name": "Variation #1" }], "audienceIds": null, "changes": null, "id": "16856826933", "integrationSettings": null }], "id": "16868402270", "weightDistributions": null, "name": "Hotfix - ECOMM 2534- RAF Legal Link", "groupId": null, "commitId": "16935023025", "decisionMetadata": null, "policy": "single_experiment", "changes": null }, {
                    "holdback": 0,
                    "activation": {},
                    "integrationSettings": {},
                    "integrationStringVersion": 2,
                    "viewIds": ["9481221941"],
                    "experiments": [{
                        "weightDistributions": [{ "entityId": "17029650329", "endOfRange": 10000 }],
                        "audienceName": "Everyone else",
                        "name": "Hotfix - Plans Page - Remove dupe FAQ",
                        "bucketingStrategy": null,
                        "variations": [{ "id": "17023640953", "actions": [{ "viewId": "9481221941", "changes": [] }], "name": "Original" }, {
                            "id": "17029650329",
                            "actions": [{
                                "viewId": "9481221941",
                                "changes": [{
                                    "dependencies": [],
                                    "type": "custom_code",
                                    "id": "12CA071E-1328-4E0D-AD4B-9DF17253354D",
                                    "value": function($) {
                                        var utils = optimizely.get('utils');

                                        utils.waitForElement('.Footer')
                                            .then(function(elem) {
                                                document.querySelector('.FAQ--single').style.display = "none";

                                            });
                                    }
                                }]
                            }],
                            "name": "Variation #1"
                        }],
                        "audienceIds": null,
                        "changes": null,
                        "id": "17045040177",
                        "integrationSettings": null
                    }],
                    "id": "16996500360",
                    "weightDistributions": null,
                    "name": "Hotfix - Plans Page - Remove dupe FAQ",
                    "groupId": null,
                    "commitId": "17020020927",
                    "decisionMetadata": null,
                    "policy": "single_experiment",
                    "changes": null
                }, { "holdback": 500, "activation": {}, "integrationSettings": {}, "integrationStringVersion": 2, "viewIds": ["9481221941"], "experiments": [{ "weightDistributions": null, "audienceName": "New Visitor: Landing Page: Not Plans", "name": "New Visitor: Landing Page: Not Plans", "bucketingStrategy": null, "variations": [{ "id": "17033050671", "actions": [{ "viewId": "9481221941", "changes": [{ "name": "HotJar Poll Trigger", "config": { "hotjarPollId": "smm16-notlanding" }, "widget_id": "12348583044", "dependencies": [], "type": "widget", "id": "C3E287E3-F205-43B6-9822-A00BCF47C80E" }] }], "name": "Variation #1" }], "audienceIds": ["17027440409"], "changes": null, "id": "17042951013", "integrationSettings": null }, { "weightDistributions": null, "audienceName": "New Visitor: Landing Page: Plans", "name": "New Visitor: Landing Page: Plans", "bucketingStrategy": null, "variations": [{ "id": "17023811117", "actions": [{ "viewId": "9481221941", "changes": [{ "name": "HotJar Poll Trigger", "config": { "hotjarPollId": "smm16-landing" }, "widget_id": "12348583044", "dependencies": [], "type": "widget", "id": "25C77E77-B6EA-478E-A626-52AE1776D218" }] }], "name": "Variation #1" }], "audienceIds": ["17025410970"], "changes": null, "id": "17049150146", "integrationSettings": null }], "id": "17021631103", "weightDistributions": null, "name": "Plans Page Poll - \"Was this page helpful\"", "groupId": null, "commitId": "17037500408", "decisionMetadata": { "experimentPriorities": [
                            [{ "entityId": "17049150146", "endOfRange": null }],
                            [{ "entityId": "17042951013", "endOfRange": null }]
                        ], "offerConsistency": null }, "policy": "equal_priority", "changes": null }, { "holdback": 0, "activation": {}, "integrationSettings": {}, "integrationStringVersion": 2, "viewIds": ["16992750466"], "experiments": [{ "weightDistributions": [{ "entityId": "17010620916", "endOfRange": 10000 }], "audienceName": "Not QA", "name": "Hotfix - Ryan Reynolds - Announcment Page", "bucketingStrategy": null, "variations": [{ "id": "17043190489", "actions": [], "name": "Original" }, { "id": "17010620916", "actions": [{ "viewId": "16992750466", "changes": [{ "value": "<style>.g-announce-module .quote-container.large-quote h2:after {\n  content: normal;\n}\n\n@media screen and (min-width: 1800px) {\n.g-announce-module {\n   background-position: top center !important;\n}\n\n}</style>", "selector": "head", "dependencies": [], "type": "append", "id": "A04E2575-5C51-47DE-BE99-0E98E165893E" }, { "selector": "#rel-blog img", "dependencies": [], "attributes": { "src": "https://cdn.mintmobile.com/wp-content/uploads/2019/11/blog-graphic.jpg" }, "type": "attribute", "id": "8CFFD7CC-CED2-4C26-AA98-BBE22703E8A4", "css": {} }, { "selector": "#rel-gatsby", "dependencies": [], "attributes": { "style": "display: block;", "html": "<a href=\"/about-mint-mobile\"><img src=\"https://cdn.mintmobile.com/wp-content/uploads/2019/11/about-graphic.jpg\">\n  <br>About</a>" }, "type": "attribute", "id": "C916B3A8-5AFD-43EE-853A-B47E920875F3", "css": {} }, { "selector": "h2", "dependencies": [], "attributes": { "html": "While every other tech titan is off chasing rockets, I\u2019ll corner the budget-friendly wireless sector. Like most people, I only use rockets 10-12 times a year but I use my mobile service every day.&rdquo;" }, "type": "attribute", "id": "DAD26C41-2A83-416D-AE54-412CF6C7F289", "css": {} }, { "selector": "#rel-reviews", "dependencies": [], "attributes": { "html": "<a href=\"/reviews/\"><img src=\"https://cdn.mintmobile.com/wp-content/themes/mintmobile/images/about/review-graphic.gif\">\n  <br>Reviews</a>" }, "type": "attribute", "id": "474D9159-3D53-4DD3-B1F1-C62692FF8C98", "css": {} }] }], "name": "Variation #1" }], "audienceIds": ["and", "11216211532"], "changes": null, "id": "17014310590", "integrationSettings": null }], "id": "17022090202", "weightDistributions": null, "name": "Hotfix - Ryan Reynolds - Announcment Page", "groupId": null, "commitId": "17175001353", "decisionMetadata": null, "policy": "single_experiment", "changes": null }, { "holdback": 0, "activation": {}, "integrationSettings": {}, "integrationStringVersion": 2, "viewIds": ["17008810266"], "experiments": [{ "weightDistributions": [{ "entityId": "17010500417", "endOfRange": 10000 }], "audienceName": "Not QA", "name": "Hotfix - Ryan Reynolds - About Mint Mobile", "bucketingStrategy": null, "variations": [{ "id": "16998660111", "actions": [], "name": "Original" }, { "id": "17010500417", "actions": [{ "viewId": "17008810266", "changes": [{ "selector": "#rel-blog img", "dependencies": [], "attributes": { "src": "https://cdn.mintmobile.com/wp-content/uploads/2019/11/blog-graphic.jpg" }, "type": "attribute", "id": "6A7283EB-ADF6-47FD-84B5-E2817B711712", "css": {} }, { "selector": "#rel-gatsby", "dependencies": [], "attributes": { "style": "display: block;", "html": "<a href=\"/ryan-reynolds-announcement\"><img src=\"https://cdn.mintmobile.com/wp-content/themes/mintmobile/images/about/announcement-fpo.jpg\">\n  <br>Announcement</a>" }, "type": "attribute", "id": "A0D9C68E-4B1D-456A-B7CF-5BC94872EB9D", "css": {} }, { "selector": "#rel-gatsby img", "dependencies": [], "attributes": { "src": "//cdn.optimizely.com/img/8894411182/0c1b83c2a92c4b15a35f87576dd93904.jpg" }, "type": "attribute", "id": "1C357D75-C3F6-48DD-A70A-49721FD331F6", "css": {} }, { "selector": "#rel-reviews", "dependencies": [], "attributes": { "html": "<a href=\"/reviews/\"><img src=\"https://cdn.mintmobile.com/wp-content/themes/mintmobile/images/about/review-graphic.gif\">\n  <br>Reviews</a>" }, "type": "attribute", "id": "93468E0D-ECF4-4B35-AA7F-E43E1BD415F9", "css": {} }] }], "name": "Variation #1" }], "audienceIds": ["and", "11216211532"], "changes": null, "id": "16996900260", "integrationSettings": null }], "id": "17027590358", "weightDistributions": null, "name": "Hotfix - Ryan Reynolds - About Mint Mobile", "groupId": null, "commitId": "17137892653", "decisionMetadata": null, "policy": "single_experiment", "changes": null }, { "holdback": 0, "activation": {}, "integrationSettings": {}, "integrationStringVersion": 2, "viewIds": ["17031140726"], "experiments": [{ "weightDistributions": [{ "entityId": "17013850587", "endOfRange": 10000 }], "audienceName": "Not QA", "name": "Hotfix - Ryan Reynolds - Home Page ", "bucketingStrategy": null, "variations": [{ "id": "17029430375", "actions": [], "name": "Original" }, { "id": "17013850587", "actions": [{ "viewId": "17031140726", "changes": [{ "value": "<style>@media screen and (min-width: 1800px) {\n   .g-announce-module {\n    \tbackground-position: top center !important; \n    }\n}</style>", "selector": "head", "dependencies": [], "type": "append", "id": "750C20BA-217C-4E0D-BA5E-7BCA1CF2FAA3" }, { "selector": ".no-quote", "dependencies": [], "attributes": { "html": "<div class=\"col-sm-7 col-12\">\n  <div>\n    <h2 style=\"\">\nOUR MINTY FRESH OWNER RYAN&nbsp;REYNOLDS </h2>\n    <div class=\"quotee\">\n      <p><a href=\"/ryan-reynolds-announcement\" class=\"btn btn-primary\">View Announcement</a></p>\n    </div>\n  </div>\n</div>" }, "type": "attribute", "id": "EE71553F-7BA7-4CA9-B37B-1F89B31F4C60", "css": {} }] }], "name": "Variation #1" }], "audienceIds": ["and", "11216211532"], "changes": null, "id": "17026070180", "integrationSettings": null }], "id": "17047110221", "weightDistributions": null, "name": "Hotfix - Ryan Reynolds - Home Page ", "groupId": null, "commitId": "17191620373", "decisionMetadata": null, "policy": "single_experiment", "changes": null }, { "holdback": 0, "activation": {}, "integrationSettings": {}, "integrationStringVersion": 2, "viewIds": ["9476731050"], "experiments": [{ "weightDistributions": [{ "entityId": "17122620903", "endOfRange": 10000 }], "audienceName": "Everyone else", "name": "Hotfix: MintMobile twitter Handle redirect", "bucketingStrategy": null, "variations": [{ "id": "17155061415", "actions": [{ "viewId": "9476731050", "changes": [] }], "name": "Original" }, { "id": "17122620903", "actions": [{ "viewId": "9476731050", "changes": [{ "selector": ".Footer-communication > .Share", "dependencies": [], "attributes": { "html": "<a href=\"https://www.facebook.com/MintMobileWireless/\" target=\"_blank\" data-ga=\"Footer|Menu Selection|Facebook\">\n  <svg class=\"Share-facebook\">\n    <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#icon-facebook\"></use>\n  </svg>\n</a>\n<a href=\"https://www.instagram.com/mintmobile/\" target=\"_blank\" data-ga=\"Top Navigation|Menu Selection|Instagram\">\n  <svg class=\"Share-instagram\">\n    <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#icon-instagram\"></use>\n  </svg>\n</a>\n<a href=\"https://twitter.com/mintmobile\" target=\"_blank\" data-ga=\"Footer|Menu Selection|Twitter\">\n  <svg class=\"Share-twitter\">\n    <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#icon-twitter\"></use>\n  </svg>\n</a>\n<a href=\"https://www.youtube.com/mintmobile\" target=\"_blank\" data-ga=\"Footer|Menu Selection|YouTube\">\n  <svg class=\"Share-youtube\">\n    <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#icon-youtube\"></use>\n  </svg>\n</a>" }, "type": "attribute", "id": "876FBFA1-ABBA-4F7B-9491-7B104A196168", "css": {} }, { "selector": ".col-12.col-md-4 > div:nth-of-type(2)", "dependencies": [], "attributes": { "html": "<div class=\"col-12\">\n  <div class=\"Footer-communication\">\n    <div class=\"Share\">\n      <a href=\"https://www.facebook.com/MintMobileWireless/\" target=\"_blank\" data-ga=\"Footer|Menu Selection|Facebook\">\n        <svg class=\"Share-facebook\">\n          <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#icon-facebook\"></use>\n        </svg>\n      </a>\n      <a href=\"https://www.instagram.com/mintmobile/\" target=\"_blank\" data-ga=\"Top Navigation|Menu Selection|Instagram\">\n        <svg class=\"Share-instagram\">\n          <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#icon-instagram\"></use>\n        </svg>\n      </a>\n      <a href=\"https://twitter.com/mintmobile\" target=\"_blank\" data-ga=\"Footer|Menu Selection|Twitter\">\n        <svg class=\"Share-twitter\">\n          <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#icon-twitter\"></use>\n        </svg>\n      </a>\n      <a href=\"https://www.youtube.com/mintmobile\" target=\"_blank\" data-ga=\"Footer|Menu Selection|YouTube\">\n        <svg class=\"Share-youtube\">\n          <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#icon-youtube\"></use>\n        </svg>\n      </a>\n    </div>\n    <a href=\"/\" class=\"Footer-logo\" data-ga=\"Footer|Menu Selection|Logo\">\n      <svg>\n        <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#mint-logo\"></use>\n      </svg>\n    </a>\n  </div>\n</div>" }, "type": "attribute", "id": "F2091684-1DFF-4F9E-95DB-C6323EC0370F", "css": {} }] }], "name": "Variation #1" }], "audienceIds": null, "changes": null, "id": "17165042092", "integrationSettings": null }], "id": "17124891208", "weightDistributions": null, "name": "Hotfix: MintMobile twitter Handle redirect", "groupId": null, "commitId": "17136570959", "decisionMetadata": null, "policy": "single_experiment", "changes": null }, { "holdback": 0, "activation": {}, "integrationSettings": {}, "integrationStringVersion": 2, "viewIds": ["17192490828"], "experiments": [{ "weightDistributions": [{ "entityId": "17227060618", "endOfRange": 10000 }], "audienceName": "Everyone else", "name": "Hotfix-Affiliate-Program", "bucketingStrategy": null, "variations": [{ "id": "17196370321", "actions": [], "name": "Original" }, { "id": "17227060618", "actions": [{ "viewId": "17192490828", "changes": [{ "value": "<style>.FAQ {\n    padding-bottom: 2rem!important;\n \t\tmargin-bottom: 0px!important;\n}</style>", "selector": "head", "dependencies": [], "type": "append", "id": "FEEADFB9-F10D-4617-B165-D09989CE9A03" }, { "selector": "#contact-affiliate", "dependencies": [], "attributes": { "remove": true }, "type": "attribute", "id": "7F912FFE-7859-4914-BB73-4C18296F0269", "css": {} }] }], "name": "Variation #1" }], "audienceIds": null, "changes": null, "id": "17217050351", "integrationSettings": null }], "id": "17215260683", "weightDistributions": null, "name": "Hotfix-Affiliate-Program", "groupId": null, "commitId": "17210050836", "decisionMetadata": null, "policy": "single_experiment", "changes": null }, { "holdback": 0, "activation": {}, "integrationSettings": {}, "integrationStringVersion": 2, "viewIds": ["10021952944", "9476731050"], "experiments": [{ "weightDistributions": [{ "entityId": "17221070118", "endOfRange": 10000 }], "audienceName": "Everyone else", "name": "CCPA - Popup Banner", "bucketingStrategy": null, "variations": [{ "id": "17227030128", "actions": [{ "viewId": "10021952944", "changes": [] }], "name": "Original" }, { "id": "17221070118", "actions": [{ "viewId": "10021952944", "changes": [{ "src": "/actions/4e5626649386d01e93da86b74763a69a46db19a1fc224e1db351c6d4c4cb61c6.js", "dependencies": [], "id": "9DF236CA-0B10-4E01-BF47-F92972F4613A" }] }, { "viewId": "9476731050", "changes": [{ "src": "/actions/c796926d24aa8e539dd1f9a65752451fa31dc90b1386cff7d5c32beaf7d3bf93.js", "dependencies": [], "id": "A8670C72-AE57-4D45-8B81-98B9C9B19F13" }] }], "name": "Variation #1" }], "audienceIds": null, "changes": null, "id": "17215750110", "integrationSettings": null }], "id": "17221300186", "weightDistributions": null, "name": "CCPA - Popup Banner", "groupId": null, "commitId": "17281041863", "decisionMetadata": null, "policy": "single_experiment", "changes": null }, { "holdback": 0, "activation": {}, "integrationSettings": {}, "integrationStringVersion": 2, "viewIds": ["9414584294", "9481221941"], "experiments": [{ "weightDistributions": [{ "entityId": "17295242895", "endOfRange": 5000 }, { "entityId": "17264272698", "endOfRange": 10000 }], "audienceName": "QA Only", "name": "MM-32[Plans][Everyone] Plan Card Redesign", "bucketingStrategy": null, "variations": [{ "id": "17295242895", "actions": [{ "viewId": "9481221941", "changes": [] }], "name": "Original" }, { "id": "17264272698", "actions": [{ "viewId": "9481221941", "changes": [{ "name": "Add Class to Element", "config": { "className": "MM-32", "selector": "body" }, "id": "958B688C-0831-4E97-B3AF-13D87DFA6092", "dependencies": [], "type": "widget", "widget_id": "16916282530" }] }, { "viewId": "9414584294", "changes": [{ "name": "Add Class to Element", "config": { "className": "MM-32", "selector": "body" }, "widget_id": "16916282530", "dependencies": [], "type": "widget", "id": "9D5F3309-21BF-43F1-9FA0-1AAB5350461C" }] }], "name": "Variation #1" }], "audienceIds": ["and", "10023895230"], "changes": null, "id": "17266073045", "integrationSettings": null }], "id": "17256543200", "weightDistributions": null, "name": "MM-32[Plans][Everyone] Plan Card Redesign", "groupId": null, "commitId": "17390641151", "decisionMetadata": null, "policy": "single_experiment", "changes": null }, { "holdback": 0, "activation": {}, "integrationSettings": {}, "integrationStringVersion": 2, "viewIds": ["9414584294", "9665581552"], "experiments": [{ "weightDistributions": [{ "entityId": "17266492238", "endOfRange": 10000 }], "audienceName": "Everyone else", "name": "Hotfix - ECOMM-2661 - RR Injection on Home Page", "bucketingStrategy": null, "variations": [{ "id": "17283531809", "actions": [{ "viewId": "9665581552", "changes": [] }], "name": "Original" }, { "id": "17266492238", "actions": [{ "viewId": "9665581552", "changes": [{ "value": "<style>.g-announce-module {\n        background-image: url(https://cdn.mintmobile.com/wp-content/uploads/2019/11/announcement-page-mobile.jpg);\n        background-position:bottom;\n    }\n\n    @media screen and (min-width: 576px) {\n        .g-announce-module {\n            background-image: url(https://cdn.mintmobile.com/wp-content/uploads/2019/11/announcement-page.jpg);\n            background-position: top;\n        }\n    }\n\n    @media screen and (min-width: 1800px) {\n        .g-announce-module {\n\t\t        background-size: contain !important;\n\t\t        background-color: #153e30;\n            background-position: top right;\n        }\n\t}</style>", "selector": "head", "dependencies": [], "type": "append", "id": "93EA8FD2-5F8F-4CA2-A232-71BAC9958911" }, { "value": "<div class=\"g-announce-module\">\n\n    <div class=\"container-fluid\">\n        <div class=\"row quote-container no-quote\">\n            <div class=\"col-sm-6 col-12\">\n                <div>\n                    <h2>\n                        OUR MINTY FRESH OWNER RYAN REYNOLDS                    </h2>\n                    <div class=\"quotee\">\n                        <p><a href=\"/ryan-reynolds-announcement\" class=\"btn btn-primary\">View Announcement</a></p>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n\n<style type=\"text/css\">\n@media (max-width: 575px) {\n     .g-announce-module .quote-container {\npadding-top: 20px;\n      }\n}\n</style>", "selector": ".Hero-main", "dependencies": [], "operator": "after", "type": "append", "id": "B511ECC9-26A5-46B0-94F9-8902335E5C72" }] }, { "viewId": "9414584294", "changes": [{ "value": "<style>.g-announce-module {\n        background-image: url(https://cdn.mintmobile.com/wp-content/uploads/2019/11/announcement-page-mobile.jpg);\n        background-position:bottom;\n    }\n\n    @media screen and (min-width: 576px) {\n        .g-announce-module {\n            background-image: url(https://cdn.mintmobile.com/wp-content/uploads/2019/11/announcement-page.jpg);\n            background-position: top;\n        }\n    }\n\n    @media screen and (min-width: 1800px) {\n        .g-announce-module {\n\t\t    background-size: contain !important;\n\t\t    background-color: #153e30;\n        background-position: center;\n        }\n\t}</style>", "selector": "head", "dependencies": [], "type": "append", "id": "E4B805BC-88C1-43D8-A9B5-DB9B983AE88D" }, { "value": "<div class=\"g-announce-module\">\n\n    <div class=\"container-fluid\">\n        <div class=\"row quote-container no-quote\">\n            <div class=\"col-sm-6 col-12\">\n                <div>\n                    <h2>\n                        OUR MINTY FRESH OWNER RYAN REYNOLDS                    </h2>\n                    <div class=\"quotee\">\n                        <p><a href=\"/ryan-reynolds-announcement\" class=\"btn btn-primary\">View Announcement</a></p>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n\n<style type=\"text/css\">\n@media (max-width: 575px) {\n     .g-announce-module .quote-container {\npadding-top: 20px;\n      }\n}\n</style>", "selector": ".Hero-main", "dependencies": [], "operator": "after", "type": "append", "id": "3A931E9A-8CFB-4DD2-9766-5AF45D84F923" }] }], "name": "Variation #1" }], "audienceIds": null, "changes": null, "id": "17256843055", "integrationSettings": null }], "id": "17269722710", "weightDistributions": null, "name": "Hotfix - ECOMM-2661 - RR Injection on Home Page", "groupId": null, "commitId": "17263861979", "decisionMetadata": null, "policy": "single_experiment", "changes": null }, { "holdback": 0, "activation": {}, "integrationSettings": {}, "integrationStringVersion": 2, "viewIds": ["17298732199"], "experiments": [{ "weightDistributions": [{ "entityId": "17279642262", "endOfRange": 10000 }], "audienceName": "Everyone else", "name": "Hotfix - ECOMM 2677 - iPhone Landing Page", "bucketingStrategy": null, "variations": [{ "id": "17275464248", "actions": [], "name": "Original" }, { "id": "17279642262", "actions": [{ "viewId": "17298732199", "changes": [{ "selector": ".subtitle", "dependencies": [], "attributes": { "remove": true }, "type": "attribute", "id": "FBCCA8E1-6A19-4669-8D1A-828DD7EF21EA", "css": {} }] }], "name": "Variation #1" }], "audienceIds": null, "changes": null, "id": "17298503900", "integrationSettings": null }], "id": "17274015438", "weightDistributions": null, "name": "Hotfix - ECOMM 2677 - iPhone Landing Page", "groupId": null, "commitId": "17263855988", "decisionMetadata": null, "policy": "single_experiment", "changes": null }, {
                    "holdback": 0,
                    "activation": {},
                    "integrationSettings": {},
                    "integrationStringVersion": 2,
                    "viewIds": ["10604744570"],
                    "experiments": [{
                        "weightDistributions": [{ "entityId": "17273723471", "endOfRange": 5000 }, { "entityId": "17290085227", "endOfRange": 10000 }],
                        "audienceName": "Everyone else",
                        "name": "MM-42[Cart][Everyone] Push Tax and Fees calc to checkout page",
                        "bucketingStrategy": null,
                        "variations": [{ "id": "17273723471", "actions": [], "name": "Original" }, {
                            "id": "17290085227",
                            "actions": [{
                                "viewId": "10604744570",
                                "changes": [{
                                    "dependencies": [],
                                    "type": "custom_code",
                                    "id": "9E1DFB5B-9844-4347-B9DC-BC7163EA1BBD",
                                    "value": function($) {
                                        var utils = optimizely.get('utils');

                                        utils.waitForElement('Footer').then(function(element) {
                                            if (document.querySelector('.woocommerce-shipping-methods')) {
                                                document.querySelector('.validate-cart-zip').innerText = "UPDATE";
                                            } else {
                                                document.querySelector('#mint-shipping-form p').innerText = "Please enter your ZIP code to see shipping options.";

                                            }

                                            $('.Order-total').last().css('display', 'none');
                                            document.querySelectorAll('.Order-fee')[1].style.display = "none";
                                            document.querySelectorAll('.Order-fee')[0].style.display = "none";




                                            // var orderF = jQuery('.Order-fee')[0];
                                            // orderF.setAttribute('id', 't-and-f-later');
                                            // jQuery('.Order-fee')[0].after(orderF);
                                            jQuery('.Order-fee').last().after("<tr class='Order-fee' id='taxes-fees-later-container'></tr>");
                                            document.querySelector('#taxes-fees-later-container').innerHTML = "<div id='taxes-fees-later'>Taxes & fees calculated at checkout</div>";

                                            // updateTotal();
                                            document.querySelector('.woocommerce-shipping-totals').style.borderBottom = "1px solid #5e9f7a";
                                            // document.querySelector('#taxes-fees-later-container').style.borderLeft = "1px solid #5e9f7a";
                                            // document.querySelector('#taxes-fees-later-container').style.borderRight = "1px solid #5e9f7a";
                                            document.querySelector('#taxes-fees-later-container').style.padding = ".75rem";
                                            document.querySelector('#taxes-fees-later-container').style.backgroundColor = "white";

                                            document.querySelector('#taxes-fees-later').style.width = "375px";
                                            document.querySelector('#taxes-fees-later').style.padding = ".74rem";
                                            document.querySelector('#taxes-fees-later').style.color = "#666";
                                            document.querySelector('#taxes-fees-later').style.fontSize = ".85rem";
                                            document.querySelector('#taxes-fees-later').style.fontStyle = "italic";

                                            // document.querySelector('.Order-total th').innerText = "Subtotal";
                                            $('.Order-collaterals .table').css('border', 'none');


                                            // Handle totals 

                                            var subTotals = document.querySelector('.Order-collaterals .Order-subtotal');
                                            var orderFee = document.querySelector('.Order-collaterals .Order-fee');
                                            var rowTwo = jQuery('.Order-collaterals .table-striped tr').eq(1);
                                            var rowOne = jQuery('.Order-collaterals .table-striped tr').first();

                                        });
                                    }
                                }]
                            }],
                            "name": "Variation #1"
                        }],
                        "audienceIds": null,
                        "changes": null,
                        "id": "17283482051",
                        "integrationSettings": null
                    }],
                    "id": "17275533956",
                    "weightDistributions": null,
                    "name": "MM-42[Cart][Everyone] Push Tax and Fees calc to checkout page",
                    "groupId": null,
                    "commitId": "17380090640",
                    "decisionMetadata": null,
                    "policy": "single_experiment",
                    "changes": null
                }, {
                    "holdback": 0,
                    "activation": {},
                    "integrationSettings": {},
                    "integrationStringVersion": 2,
                    "viewIds": ["9420962616"],
                    "experiments": [{
                        "weightDistributions": [{ "entityId": "17264095121", "endOfRange": 10000 }],
                        "audienceName": "Everyone else",
                        "name": "Hotfix - MM - ECOMM-2680 - Update contact numbers on FAQ page",
                        "bucketingStrategy": null,
                        "variations": [{ "id": "17256673641", "actions": [{ "viewId": "9420962616", "changes": [] }], "name": "Original" }, {
                            "id": "17264095121",
                            "actions": [{
                                "viewId": "9420962616",
                                "changes": [{
                                    "dependencies": [],
                                    "type": "custom_code",
                                    "id": "CED3750C-30BB-4F33-BC2B-B987DFA6508E",
                                    "value": function($) {
                                        var utils = optimizely.get('utils');

                                        utils.waitForElement('Footer').then(function(element) {
                                            var allSections = document.querySelectorAll('.MoreContent .row .col-lg-4');
                                            allSections[1].style.display = "none";
                                            allSections[0].classList.remove("col-lg-4");
                                            allSections[0].classList.add("col-lg-6");
                                            allSections[2].classList.remove("col-lg-4");
                                            allSections[2].classList.add("col-lg-6");



                                            allSections[2].innerHTML = '<img src="https://www.mintmobile.com/wp-content/uploads/2018/10/M_MS_Help_R7_03-2.png" alt="email" style="height: 48px;"><p class="mt-2">CUSTOMER SUPPORT <br><a href="tel:800-683-7392">800-683-7392</a><br><a href="mailto:support@mintmobile.com">SUPPORT@MINTMOBILE.COM</a></p>';
                                        });
                                    }
                                }]
                            }],
                            "name": "Variation #1"
                        }],
                        "audienceIds": null,
                        "changes": null,
                        "id": "17256445027",
                        "integrationSettings": null
                    }],
                    "id": "17285183807",
                    "weightDistributions": null,
                    "name": "Hotfix - MM - ECOMM-2680 - Update contact numbers on FAQ page",
                    "groupId": null,
                    "commitId": "17266193792",
                    "decisionMetadata": null,
                    "policy": "single_experiment",
                    "changes": null
                }, {
                    "holdback": 0,
                    "activation": {},
                    "integrationSettings": {},
                    "integrationStringVersion": 2,
                    "viewIds": ["17277703455"],
                    "experiments": [{
                        "weightDistributions": [{ "entityId": "17300943194", "endOfRange": 10000 }],
                        "audienceName": "Everyone else",
                        "name": "WURFL Alert",
                        "bucketingStrategy": null,
                        "variations": [{ "id": "17281643108", "actions": [], "name": "Original" }, {
                            "id": "17300943194",
                            "actions": [{
                                "viewId": "17277703455",
                                "changes": [{
                                    "dependencies": [],
                                    "type": "custom_code",
                                    "id": "1169EDEF-2D17-40CC-8714-483E6B0F8E50",
                                    "value": function($) {
                                        var utils = optimizely.get('utils');

                                        utils.waitForElement('Footer').then(function(element) {
                                            alert(WURFL.complete_device_name);
                                        });
                                    }
                                }]
                            }],
                            "name": "Variation #1"
                        }],
                        "audienceIds": null,
                        "changes": null,
                        "id": "17268144848",
                        "integrationSettings": null
                    }],
                    "id": "17288072700",
                    "weightDistributions": null,
                    "name": "WURFL Alert",
                    "groupId": null,
                    "commitId": "17280113256",
                    "decisionMetadata": null,
                    "policy": "single_experiment",
                    "changes": null
                }, {
                    "holdback": 0,
                    "activation": {},
                    "integrationSettings": {},
                    "integrationStringVersion": 2,
                    "viewIds": ["14890110471"],
                    "experiments": [{
                        "weightDistributions": [{ "entityId": "17275493410", "endOfRange": 10000 }],
                        "audienceName": "Everyone else",
                        "name": "Hotfix MM - Breadcrumb Fix",
                        "bucketingStrategy": null,
                        "variations": [{ "id": "17267955908", "actions": [], "name": "Original" }, {
                            "id": "17275493410",
                            "actions": [{
                                "viewId": "14890110471",
                                "changes": [{
                                    "dependencies": [],
                                    "type": "custom_code",
                                    "id": "C01BC302-3254-4548-B93F-E189F37273CC",
                                    "value": function($) {
                                        var utils = optimizely.get('utils');

                                        utils.waitForElement('Footer').then(function(element) {

                                            var planDetailsDiv = document.querySelector('.PlanDetails-breadcrumb');

                                            if (planDetailsDiv) {
                                                var planUrlArr = planDetailsDiv.children[4].href.split("/");
                                                planUrlArr.splice(0, 2);
                                                planUrlArr.splice(1, 0, 'product');
                                                planDetailsDiv.children[4].href = 'https://' + planUrlArr.join('/');
                                            }
                                        });
                                    }
                                }]
                            }],
                            "name": "Variation #1"
                        }],
                        "audienceIds": null,
                        "changes": null,
                        "id": "17268036108",
                        "integrationSettings": null
                    }],
                    "id": "17288963645",
                    "weightDistributions": null,
                    "name": "Hotfix MM - Breadcrumb Fix",
                    "groupId": null,
                    "commitId": "17367941949",
                    "decisionMetadata": null,
                    "policy": "single_experiment",
                    "changes": null
                }],
                "listTargetingKeys": [],
                "groups": [],
                "views": [{ "category": "other", "staticConditions": ["and", ["or", { "type": "url", "value": "https://www.mintmobile.com", "match": "simple" }],
                        ["not", ["or", { "type": "url", "value": "https://qa.mintmobile.com", "match": "simple" }]]
                    ], "name": "Home", "apiName": "8894411182_home", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "9414584294" }, { "category": "other", "staticConditions": ["and", ["or", { "type": "url", "value": "https://www.mintmobile.com/faq", "match": "simple" }, { "type": "url", "value": "https://qa.mintmobile.com/faq", "match": "simple" }]], "name": "FAQ", "apiName": "8894411182_faq", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "9420962616" }, { "category": "other", "staticConditions": null, "name": "Global", "apiName": "8894411182_global", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "9476731050" }, { "category": "other", "staticConditions": ["and", ["or", { "type": "url", "value": "https://www.mintmobile.com/plans", "match": "simple" }],
                        ["not", ["or", { "type": "url", "value": "https://qa.mintmobile.com/plans", "match": "simple" }, { "type": "url", "value": "https://qa.mintmobile.com/plans-v2", "match": "simple" }]]
                    ], "name": "Plans", "apiName": "8894411182_plans", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "9481221941" }, { "category": "other", "staticConditions": ["and", ["or", { "type": "url", "value": "https://www.mintmobile.com/checkout", "match": "simple" }, { "type": "url", "value": "https://qa.mintmobile.com/checkout", "match": "simple" }]], "name": "Checkout", "apiName": "8894411182_checkout", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "9488460812" }, { "category": "other", "staticConditions": ["and", ["or", { "type": "url", "value": "https://qa.mintmobile.com/", "match": "simple" }]], "name": "QA - Home", "apiName": "8894411182_qa__home_page", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "9665581552" }, { "category": "other", "staticConditions": ["or", ["or", { "type": "url", "value": "https:\\/\\/www\\.mintmobile\\.com\\/checkout\\/order-received\\/\\d+\\/\\?key=wc_order_[a-z0-9]+", "match": "regex" }],
                        ["or", { "type": "url", "value": "mode=order_message", "match": "substring" }]
                    ], "name": "Order Received", "apiName": "8894411182_purchase_complete", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "9842182253" }, { "category": "other", "staticConditions": ["and", ["or", { "type": "url", "value": "https:\\/\\/qa\\.mintmobile\\.com", "match": "regex" }]], "name": "QA - Global", "apiName": "8894411182_qamintsimcom", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "10021952944" }, { "category": "other", "staticConditions": ["and", ["or", { "type": "url", "value": "https://www.mintmobile.com/cart", "match": "simple" }]], "name": "Cart", "apiName": "8894411182_cart__production_only", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "10604744570" }, { "category": "other", "staticConditions": ["and", ["or", { "type": "url", "value": "https://www.mintmobile.com/", "match": "simple" }],
                        ["not", ["or", { "type": "url", "value": "https://www.mintmobile.com(\\/)?\\?s=", "match": "regex" }, { "type": "url", "value": "https://www.mintmobile.com/home", "match": "simple" }]]
                    ], "name": "Home - No FAQ Search Query Params", "apiName": "8894411182_home_no_query_parameters", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "10679811972" }, { "category": "other", "staticConditions": ["or", ["or", { "type": "url", "value": "https:\\/\\/www\\.mintmobile.com\\/product\\/.*", "match": "regex" }, { "type": "url", "value": "https:\\/\\/qa\\.mintmobile.com\\/product\\/.*", "match": "regex" }]], "name": "Product - All", "apiName": "8894411182_any_product_page", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "14890110471" }, {
                    "category": "other",
                    "staticConditions": ["and", ["or", { "type": "url", "value": "https://www.mintmobile.com/cart", "match": "simple" }, { "type": "url", "value": "https://qa.mintmobile.com/cart", "match": "simple" }]],
                    "activationType": "polling",
                    "name": "Cart - Includes Influencer Promo Code",
                    "apiName": "8894411182_cart__includes_influencer_promo_code",
                    "tags": [],
                    "undoOnDeactivation": false,
                    "activationCode":
                    /**
                     * Sample Polling Function
                     * Supply an expression to return a boolean inside a function.
                     * For complete documentation, see https://developers.optimizely.com/x/solutions/javascript/topics/dynamic-websites/index.html#polling
                     */

                        function pollingPredicate() {
                        var dl = window.dataLayer;
                        for (i = 0; i < dl.length; i++) {
                            if (dl[i]['event'] == 'Promo Code') {
                                if (dl[i]['Selection'].includes('GWP_') || dl[i]['Selection'].includes('gwp_')) {
                                    return true;
                                }
                            }
                        }
                        var ds = jQuery('[data-sku]');
                        for (d = 0; d < ds.length; d++) {
                            if (ds[d].getAttribute('data-sku').includes('MT-LOOPS')) {
                                return true;
                            }
                        }
                        return false;
                    },
                    "deactivationEnabled": false,
                    "id": "15505140899"
                }, {
                    "category": "other",
                    "staticConditions": ["and", ["or", { "type": "url", "value": "https://www.mintmobile.com/checkout/order-received/", "match": "substring" }, { "type": "url", "value": "https://qa.mintmobile.com/checkout/order-received/", "match": "substring" }]],
                    "activationType": "polling",
                    "name": "Order Received - Contains Promo Item",
                    "apiName": "8894411182_order_received__contains_promo_item",
                    "tags": [],
                    "undoOnDeactivation": false,
                    "activationCode":
                    /**
                     * Sample Polling Function
                     * Supply an expression to return a boolean inside a function.
                     * For complete documentation, see https://developers.optimizely.com/x/solutions/javascript/topics/dynamic-websites/index.html#polling
                     */

                        function pollingPredicate() {
                        var grip = jQuery("a[href$='grip/']");
                        if (grip.length > 0) {
                            return true;
                        }
                        return false;
                    },
                    "deactivationEnabled": false,
                    "id": "15543670327"
                }, {
                    "category": "other",
                    "staticConditions": ["and", ["or", { "type": "url", "value": "https://www.mintmobile.com/faq", "match": "simple" }]],
                    "activationType": "manual",
                    "name": "FAQ Expand",
                    "apiName": "8894411182_faq_expand",
                    "tags": [{
                        "category": "other",
                        "locator": function($) {
                            var faqQ = window.faqQuestion[window.faqQuestion.length - 1];
                            return faqQ;
                        },
                        "valueType": "string",
                        "locatorType": "javascript",
                        "apiName": "faq_question"
                    }],
                    "undoOnDeactivation": false,
                    "activationCode":
                    /**
                     * Sample Activation Function
                     * For complete documentation, see https://developers.optimizely.com/x/solutions/javascript/code-samples/index.html#page-activation
                     * @param {Function} activate - Call this function when you want to activate the page.
                     * @param {Object} options - An object containing Page information.
                     */

                        function(activate, options) {
                        // following line assumes you are including jQuery in your Optimizely snippet
                        window.oxOptions = options;
                        console.log('activate called -- options created');
                        activate();

                    },
                    "deactivationEnabled": false,
                    "id": "16336380433"
                }, { "category": "other", "staticConditions": ["or", ["or", { "type": "url", "value": "https://www.mintmobile.com/kiosk/", "match": "simple" }],
                        ["or", { "type": "url", "value": "https://www.mintmobile.com/?kiosk=1", "match": "exact" }]
                    ], "name": "URL Targeting for Temporary - Kiosk Demo", "apiName": "8894411182_url_targeting_for_temporary__kiosk_demo", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "16479340678" }, { "category": "other", "staticConditions": ["or", ["or", { "type": "url", "value": "https://www.mintmobile.com/product/share-the-love-kit/", "match": "simple" }]], "name": "Share the Love Kit - PDP", "apiName": "8894411182_share_the_love_kit__pdp", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "16627720029" }, { "category": "other", "staticConditions": ["or", ["or", { "type": "url", "value": "www.mintmobile.com/refer-a-friend", "match": "simple" }]], "name": "Refer", "apiName": "8894411182_refer", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "16885861792" }, { "category": "other", "staticConditions": ["and", ["or", { "type": "url", "value": "https://www.mintmobile.com/ryan-reynolds-announcement/", "match": "simple" }]], "name": "URL Targeting for Hotfix - Ryan Reynolds - Announcment Page", "apiName": "8894411182_url_targeting_for_hotfix__bf__announce", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "16992750466" }, { "category": "other", "staticConditions": ["and", ["or", { "type": "url", "value": "https://www.mintmobile.com/about-mint-mobile/", "match": "simple" }]], "name": "URL Targeting for Hotfix - Ryan Reynolds - About Mint Mobile", "apiName": "8894411182_url_targeting_for_hotfix__bf_v2", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "17008810266" }, { "category": "other", "staticConditions": ["and", ["or", { "type": "url", "value": "https://www.mintmobile.com/", "match": "simple" }]], "name": "URL Targeting for Hotfix - Ryan Reynolds - Home Page ", "apiName": "8894411182_url_targeting_for_hotfix__bf_v2__home", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "17031140726" }, { "category": "other", "staticConditions": ["and", ["or", { "type": "url", "value": "https://www.mintmobile.com/affiliate-program", "match": "simple" }]], "name": "URL Targeting for Hotfix-Affiliate-Program", "apiName": "8894411182_url_targeting_for_hotfixaffiliateprogram", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "17192490828" }, { "category": "other", "staticConditions": ["and", ["or", { "type": "url", "value": "https://qa.mintmobile.com?WURFL", "match": "exact" }]], "name": "URL Targeting for WURFL Alert", "apiName": "8894411182_url_targeting_for_wurfl_alert", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "17277703455" }, { "category": "other", "staticConditions": ["and", ["or", { "type": "url", "value": "https://www.mintmobile.com/apple-iphone-offers", "match": "simple" }]], "name": "URL Targeting for Hotfix - ECOMM 2677 - iPhone Landing Page", "apiName": "8894411182_url_targeting_for_hotfix__ecomm_2677__iphone_landing_", "tags": [], "undoOnDeactivation": false, "deactivationEnabled": false, "id": "17298732199" }],
                "projectId": "8894411182",
                "plugins": [function(PluginManager) {
                    var Hogan = function(t) {
                        function r(e) { if (n[e]) return n[e].exports; var i = n[e] = { exports: {}, id: e, loaded: !1 }; return t[e].call(i.exports, i, i.exports, r), i.loaded = !0, i.exports } var n = {}; return r.m = t, r.c = n, r.p = "", r(0) }([function(t, r) {
                        function n(t) { this.r = t, this.buf = "" }

                        function e(t, r) { var n; if (r && "object" == typeof r)
                                if (void 0 !== r[t]) n = r[t];
                            return n }

                        function i(t) { return String(null === t || void 0 === t ? "" : t) }

                        function o(t) { return t = i(t), p.test(t) ? t.replace(u, "&amp;").replace(f, "&lt;").replace(c, "&gt;").replace(l, "&#39;").replace(a, "&quot;") : t }
                        t.exports = n, n.prototype = { r: function(t, r, n) { return "" }, v: o, t: i, render: function(t, r, n) { return this.ri([t], r || {}, n) }, ri: function(t, r, n) { return this.r(t, r, n) }, rs: function(t, r, n) { var e = t[t.length - 1]; if (!s(e)) return void n(t, r, this); for (var i = 0; i < e.length; i++) t.push(e[i]), n(t, r, this), t.pop() }, s: function(t, r, n, e, i, o, u) { var f; if (s(t) && 0 === t.length) return !1; if (f = !!t, !e && f && r) r.push("object" == typeof t ? t : r[r.length - 1]); return f }, d: function(t, r, n, i) { var o, u = t.split("."),
                                    f = this.f(u[0], r, n, i),
                                    c = null; if ("." === t && s(r[r.length - 2])) f = r[r.length - 1];
                                else
                                    for (var l = 1; l < u.length; l++)
                                        if (o = e(u[l], f), void 0 !== o) c = f, f = o;
                                        else f = ""; if (i && !f) return !1; if (!i && "function" == typeof f) r.push(c), f = this.mv(f, r, n), r.pop(); return f }, f: function(t, r, n, i) { for (var o = !1, u = null, f = !1, c = r.length - 1; c >= 0; c--)
                                    if (u = r[c], o = e(t, u), void 0 !== o) { f = !0; break }
                                if (!f) return i ? !1 : ""; if (!i && "function" == typeof o) o = this.mv(o, r, n); return o }, b: function(t) { this.buf += t }, fl: function() { var t = this.buf; return this.buf = "", t }, mv: function(t, r, n) { var e = r[r.length - 1],
                                    o = t.call(e); if ("function" == typeof o) return this.ct(i(o.call(e)), e, n);
                                else return o } }; var u = /&/g,
                            f = /</g,
                            c = />/g,
                            l = /'/g,
                            a = /"/g,
                            p = /[&<>"']/,
                            s = Array.isArray || function(t) { return "[object Array]" === Object.prototype.toString.call(t) } }]);

                    PluginManager.registerWidget({
                        widgetId: '11787897034',
                        showFn: function(event) {
                            var $ = window.optimizely.get('jquery');
                            var widget = event.data.config;
                            var _template = new Hogan(function(c, p, i) { var t = this;
                                t.b(i = i || "");
                                t.b("<div id=\"optimizely-extension-");
                                t.b(t.v(t.d("extension.$instance", c, p, 0)));
                                t.b("\" class=\"faq-sales-phone-number\">");
                                t.b("\n" + i);
                                t.b("  ");
                                t.b(t.v(t.d("extension.text", c, p, 0)));
                                t.b("\n" + i);
                                t.b("</div>"); return t.fl(); })
                            widget.$id = "11787897034";
                            widget.$instance = event.data.id;
                            widget.$render = _template.render.bind(_template)
                            widget.$fieldDefaults = [];
                            (function(widg) {
                                var i = 0;
                                var field;
                                for (; i < widg.$fieldDefaults.length; ++i) {
                                    field = widg.$fieldDefaults[i];
                                    if (!widg.hasOwnProperty(field.name)) {
                                        widg[field.name] = field.default_value;
                                    }
                                }
                            })(widget);
                            widget.$html = _template.render({ widget: widget, extension: widget })
                            var extension = widget;
                            widget._styleTag = document.createElement('style');
                            widget._styleTag.id = 'widget-css-11787897034';
                            widget._styleTag.innerHTML = '.faq-sales-phone-number {  background-color: #fff575;  border-bottom: 1px solid #e0d769;  color: #555;  padding: 10px;  font-weight: bold;  text-align: center;  font-size: 20px;}';
                            document.getElementsByTagName('head')[0].appendChild(widget._styleTag);
                            var utils = optimizely.get('utils');

                            utils.waitForElement('.Footer')
                                .then(function(elem) {
                                    // Prepend the extension html to the body
                                    document.querySelector('body > section.MoreContent > div:nth-child(1) > div.row.mt-5 > div:nth-child(2) > img').setAttribute('src', 'https://www.mintmobile.com/wp-content/uploads/2018/10/M_MS_Help_R7_03-4.png');
                                    document.querySelector('body > section.MoreContent > div:nth-child(1) > div.row.mt-5 > div:nth-child(2) > img').style.height = '48px';

                                    document.querySelector('body > section.MoreContent > div:nth-child(1) > div.row.mt-5 > div:nth-child(2) > p').innerHTML = "TALK TO A SALES REP<br /> <a href='tel:929-276-6011'>929-276-6011</a>";


                                    document.querySelector('body > section.MoreContent > div:nth-child(1) > div.row.mt-5 > div:nth-child(3) > img').setAttribute('src', 'https://www.mintmobile.com/wp-content/uploads/2018/10/M_MS_Help_R7_03-2.png');
                                    document.querySelector('body > section.MoreContent > div:nth-child(1) > div.row.mt-5 > div:nth-child(3) > img').style.height = '48px';

                                    document.querySelector('body > section.MoreContent > div:nth-child(1) > div.row.mt-5 > div:nth-child(3) > p').innerHTML = "CUSTOMER SUPPORT <br /><a href='tel:213-372-7777'>213-372-7777</a><br /><a href='mailto:support@mintmobile.com'>SUPPORT@MINTMOBILE.COM</a>";
                                });
                        },
                        hideFn: function(event) {
                            var $ = window.optimizely.get('jquery');
                            var widget = event.data.config;
                            widget.$id = "11787897034";
                            widget.$instance = event.data.id;
                            widget.$fieldDefaults = [];
                            (function(widg) {
                                var i = 0;
                                var field;
                                for (; i < widg.$fieldDefaults.length; ++i) {
                                    field = widg.$fieldDefaults[i];
                                    if (!widg.hasOwnProperty(field.name)) {
                                        widg[field.name] = field.default_value;
                                    }
                                }
                            })(widget);
                            var extension = widget;
                            widget._styleTag = document.getElementById('widget-css-11787897034');
                            if (widget._styleTag) widget._styleTag.parentNode.removeChild(widget._styleTag);
                            var extensionElement = document.getElementById('optimizely-extension-' + extension.$instance);
                            if (extensionElement) {
                                extensionElement.parentElement.removeChild(extensionElement);
                            }

                        },
                    });

                    PluginManager.registerWidget({
                        widgetId: '12348583044',
                        showFn: function(event) {
                            var $ = window.optimizely.get('jquery');
                            var widget = event.data.config;
                            var _template = new Hogan(function(c, p, i) { var t = this;
                                t.b(i = i || ""); return t.fl(); })
                            widget.$id = "12348583044";
                            widget.$instance = event.data.id;
                            widget.$render = _template.render.bind(_template)
                            widget.$fieldDefaults = [{ "name": "hotjarPollId", "default_value": "" }];
                            (function(widg) {
                                var i = 0;
                                var field;
                                for (; i < widg.$fieldDefaults.length; ++i) {
                                    field = widg.$fieldDefaults[i];
                                    if (!widg.hasOwnProperty(field.name)) {
                                        widg[field.name] = field.default_value;
                                    }
                                }
                            })(widget);
                            widget.$html = _template.render({ widget: widget, extension: widget })
                            var extension = widget;

                            var utils = window["optimizely"].get("utils");

                            var cancelPolling = utils.poll(function() {
                                if (typeof hj === 'function') {
                                    console.log('HJ FOUND');
                                    hj('trigger', extension.hotjarPollId);
                                    cancelPolling();
                                }
                            }, 1000);
                        },
                        hideFn: function(event) {
                            var $ = window.optimizely.get('jquery');
                            var widget = event.data.config;
                            widget.$id = "12348583044";
                            widget.$instance = event.data.id;
                            widget.$fieldDefaults = [{ "name": "hotjarPollId", "default_value": "" }];
                            (function(widg) {
                                var i = 0;
                                var field;
                                for (; i < widg.$fieldDefaults.length; ++i) {
                                    field = widg.$fieldDefaults[i];
                                    if (!widg.hasOwnProperty(field.name)) {
                                        widg[field.name] = field.default_value;
                                    }
                                }
                            })(widget);
                            var extension = widget;

                            var extensionElement = document.getElementById('optimizely-extension-' + extension.$instance);
                            if (extensionElement) {
                                extensionElement.parentElement.removeChild(extensionElement);
                            }

                        },
                    });

                    PluginManager.registerWidget({
                        widgetId: '15502310823',
                        showFn: function(event) {
                            var $ = window.optimizely.get('jquery');
                            var widget = event.data.config;
                            var _template = new Hogan(function(c, p, i) { var t = this;
                                t.b(i = i || ""); return t.fl(); })
                            widget.$id = "15502310823";
                            widget.$instance = event.data.id;
                            widget.$render = _template.render.bind(_template)
                            widget.$fieldDefaults = [];
                            (function(widg) {
                                var i = 0;
                                var field;
                                for (; i < widg.$fieldDefaults.length; ++i) {
                                    field = widg.$fieldDefaults[i];
                                    if (!widg.hasOwnProperty(field.name)) {
                                        widg[field.name] = field.default_value;
                                    }
                                }
                            })(widget);
                            widget.$html = _template.render({ widget: widget, extension: widget })
                            var extension = widget;
                            widget._styleTag = document.createElement('style');
                            widget._styleTag.id = 'widget-css-15502310823';
                            widget._styleTag.innerHTML = '.coupon-mm_37_gwp_ninja {  display: none;}';
                            document.getElementsByTagName('head')[0].appendChild(widget._styleTag);
                            var utils = optimizely.get('utils');

                            utils.waitForElement('.Footer')
                                .then(function(elem) {

                                    window.planInCart = false;
                                    window.promoCode = false;
                                    window.promoProductSku = 'LOOPS';
                                    window.promoProductId = '334721';
                                    var dl = window.dataLayer;

                                    function checkPromoCodes() {
                                        var dl = window.dataLayer;
                                        for (i = 0; i < dl.length; i++) {
                                            if (dl[i]['event'] == 'Promo Code') {
                                                if (dl[i]['Selection'].includes('GWP_') || dl[i]['Selection'].includes('gwp_')) {
                                                    return true;
                                                }
                                            }
                                        }
                                        return false;
                                    }

                                    function promoCode(promoCode) {
                                        if (promoCode) {
                                            // Make sure product is in cart. If not, make sure there is a plan in cart. If so, add product
                                            if (checkPlans()) {
                                                addFreeItem();
                                            } else {
                                                removeFreeItem();
                                            }
                                        } else {
                                            // Make sure produdct is not in cart, remove if not

                                        }
                                    }

                                    function checkPlans() {
                                        // Returns true if there is a plan in the cart; false if not.
                                        var ds = jQuery('[data-sku]');
                                        for (d = 0; d < ds.length; d++) {
                                            if (ds[d].getAttribute('data-sku').includes('MINT-')) {
                                                return true;
                                            }
                                        }
                                        return false;
                                    }

                                    function addFreeItem() {
                                        if (!checkFreeItem()) {
                                            var url = new URL(document.location.href);
                                            // If your expected result is "http://foo.bar/?x=1&y=2&x=42"
                                            url.searchParams.append('add-to-cart', window.promoProductId);
                                            document.location = url;
                                        }
                                    }

                                    function checkFreeItem() {
                                        var ds = jQuery('[data-sku]');
                                        for (d = 0; d < ds.length; d++) {
                                            if (ds[d].getAttribute('data-sku').includes(window.promoProductSku)) {
                                                return true;
                                            }
                                        }
                                        return false;
                                    }

                                    function removeFreeItem() {
                                        if (checkFreeItem()) {
                                            // remove promo item
                                            jQuery('[data-product_id="' + window.promoProductId + '"]')[0].click();
                                        }
                                    }

                                    function freeItemLinkRemove() {
                                        var grip = jQuery("a[href$='grip/']");
                                        var gripParent = grip.parent()[0];
                                        gripParent.innerHTML = "<p style='margin:0;padding:0;'>FREE Mint Loop Grip</p>";
                                    }

                                    // If free item in cart, but no plan in cart, remove the free item and the coupon code
                                    if (checkFreeItem() && !checkPlans()) {
                                        removeFreeItem();
                                    }

                                    if (checkFreeItem() && !checkPromoCodes()) {
                                        removeFreeItem();
                                    }

                                    if (!checkFreeItem() && checkPlans() && checkPromoCodes()) {
                                        addFreeItem();
                                    }

                                    if (checkFreeItem()) {
                                        freeItemLinkRemove();
                                    }
                                });

                        },
                        hideFn: function(event) {
                            var $ = window.optimizely.get('jquery');
                            var widget = event.data.config;
                            widget.$id = "15502310823";
                            widget.$instance = event.data.id;
                            widget.$fieldDefaults = [];
                            (function(widg) {
                                var i = 0;
                                var field;
                                for (; i < widg.$fieldDefaults.length; ++i) {
                                    field = widg.$fieldDefaults[i];
                                    if (!widg.hasOwnProperty(field.name)) {
                                        widg[field.name] = field.default_value;
                                    }
                                }
                            })(widget);
                            var extension = widget;
                            widget._styleTag = document.getElementById('widget-css-15502310823');
                            if (widget._styleTag) widget._styleTag.parentNode.removeChild(widget._styleTag);
                            var extensionElement = document.getElementById('optimizely-extension-' + extension.$instance);
                            if (extensionElement) {
                                extensionElement.parentElement.removeChild(extensionElement);
                            }

                        },
                    });

                    PluginManager.registerWidget({
                        widgetId: '16756580917',
                        showFn: function(event) {
                            var $ = window.optimizely.get('jquery');
                            var widget = event.data.config;
                            var _template = new Hogan(function(c, p, i) { var t = this;
                                t.b(i = i || "");
                                t.b("<div id=\"optimizely-extension-");
                                t.b(t.v(t.d("extension.$instance", c, p, 0)));
                                t.b("\" class=\"mm-37-gwp d-flex justify-content-start align-items-center\">");
                                t.b("\n" + i);
                                t.b("    <div>");
                                t.b("\n" + i);
                                t.b("      <img src=\"");
                                t.b(t.v(t.d("extension.img", c, p, 0)));
                                t.b("\" class=\"gift-icon\" />");
                                t.b("\n" + i);
                                t.b("	  </div>");
                                t.b("\n" + i);
                                t.b("    <div>");
                                t.b("\n" + i);
                                t.b("      <p><strong>Free gift with purchase!</strong> Mint Mobile Ninja Loop <label class=\"gwp-learn-more\" for=\"modal-1\">Learn More</label></p>");
                                t.b("\n" + i);
                                t.b("    </div>");
                                t.b("\n" + i);
                                t.b("</div>");
                                t.b("\n");
                                t.b("\n" + i);
                                t.b("<input class=\"modal-state\" id=\"modal-1\" type=\"checkbox\" />");
                                t.b("\n" + i);
                                t.b("<div class=\"modally\">");
                                t.b("\n" + i);
                                t.b("  <label class=\"modal__bg\" for=\"modal-1\"></label>");
                                t.b("\n" + i);
                                t.b("  <div class=\"modal__inner\">");
                                t.b("\n" + i);
                                t.b("      <label class=\"modal__close\" for=\"modal-1\"></label>");
                                t.b("\n" + i);
                                t.b("    	<img src=\"https://s3-us-west-2.amazonaws.com/s.cdpn.io/2869328/hand-mobile_popup%402x.png\" class=\"mobile-ninja-loop\" />");
                                t.b("\n" + i);
                                t.b("      <div class=\"copy\">");
                                t.b("\n" + i);
                                t.b("        <h1><span>FREE GIFT</span><br>WITH PURCHASE</h1>");
                                t.b("\n" + i);
                                t.b("        <h2>Mint Mobile Ninja Loop ($14.99 value)</h2>");
                                t.b("\n" + i);
                                t.b("        <p>");
                                t.b("\n" + i);
                                t.b("          • Secures your phone in style <br>");
                                t.b("\n" + i);
                                t.b("          • Protects it from costly drops <br>");
                                t.b("\n" + i);
                                t.b("          • One-handed device operation <br>");
                                t.b("\n" + i);
                                t.b("          • Fits any mobile device with a case <br>");
                                t.b("\n" + i);
                                t.b("<br>");
                                t.b("\n" + i);
                                t.b("          <span>*Limit one per purchase, while supplies last</span>");
                                t.b("\n" + i);
                                t.b("        </p>");
                                t.b("\n" + i);
                                t.b("      </div>");
                                t.b("\n" + i);
                                t.b("  </div>");
                                t.b("\n" + i);
                                t.b("</div>"); return t.fl(); })
                            widget.$id = "16756580917";
                            widget.$instance = event.data.id;
                            widget.$render = _template.render.bind(_template)
                            widget.$fieldDefaults = [{ "name": "img", "default_value": "//cdn.optimizely.com/img/8894411182/5022342fbc274f989fed3f1449a6e920.png" }];
                            (function(widg) {
                                var i = 0;
                                var field;
                                for (; i < widg.$fieldDefaults.length; ++i) {
                                    field = widg.$fieldDefaults[i];
                                    if (!widg.hasOwnProperty(field.name)) {
                                        widg[field.name] = field.default_value;
                                    }
                                }
                            })(widget);
                            widget.$html = _template.render({ widget: widget, extension: widget })
                            var extension = widget;
                            widget._styleTag = document.createElement('style');
                            widget._styleTag.id = 'widget-css-16756580917';
                            widget._styleTag.innerHTML = '.mm-37-gwp {  margin: 10px;  margin-right: 0px;}.mm-37 div p { font-size: 14px;   color: #373737;}.gift-icon {  height: 26px;  vertical-align:middle;  position: relative;  margin-right: 10px;}.gwp-learn-more {  color: #549c74;  transition: color .2s;  text-decoration: underline;  cursor: pointer;}/* [Object] Modal * =============================== */.modally {  opacity: 0;  visibility: hidden;  position: fixed;  top: 0;  right: 0;  bottom: 0;  left: 0;  text-align: left;  background: rgba(0,0,0, .9);  transition: opacity .25s ease;  z-index: 2147483647;}.modal__bg {  position: absolute;  top: 0;  right: 0;  bottom: 0;  left: 0;  cursor: pointer;}.modal-state {  display: none;}.modal-state:checked + .modally {  opacity: 1;  visibility: visible;}.modal-state:checked + .modally .modal__inner {  top: 0;}.modal__inner {  transition: top .25s ease;  position: absolute;  top: -20%;  right: 0;  bottom: 0;  left: 0;  width: 858px;  margin: auto;  overflow: hidden;  background-image: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/2869328/hand-gift%402x.png");  background-size: 858px 457px;  background-repeat: no-repeat;  border-radius: 5px;  height: 457px;  display:flex;   -webkit-box-pack: end!important;  -ms-flex-pack: end!important;  justify-content: flex-end!important;  -webkit-box-align: center!important;  -ms-flex-align: center!important;  align-items: center!important;}.modal__inner .copy {    width: 50%;    text-align: center;}.modal__inner h1 {  font-size: 38px;	  font-weight: 500;	  line-height: 42px;  color: #5c9e79;}.modal__inner h1 span {  color: #f05a28;}.modal__inner h2 {  color: #666666;  font-family: proxima-nova,sans-serif;  font-size: 18px;  font-weight: 800;  line-height: 22px;  text-align: center;}    .modal__inner p {  color: #636363;  font-family: proxima-nova,sans-serif;  font-size: 18px;  font-weight: 600;  line-height: 23px;  text-align: center;}.modal__inner span {  color: #999;}.modal__close {  position: absolute;  right: 1em;  top: 1em;  width: 1.1em;  height: 1.1em;  cursor: pointer;}.modal__close:after,.modal__close:before {  content: "";  position: absolute;  width: 2px;  height: 1.5em;  background: #fff;  display: block;  transform: rotate(45deg);  left: 50%;  margin: -3px 0 0 -1px;  top: 0;  cursor: pointer;}.modal__close:hover:after,.modal__close:hover:before {  background: #fff;}.modal__close:before {  transform: rotate(-45deg);}.mobile-ninja-loop {  display:none;}@media screen and (max-width: 858px) {	  .modal__inner {    background-image: url("");    width: 90%;    height: auto;    max-height: 820px;    max-width: 500px;    background-size: contain;    box-sizing: border-box;    -ms-flex-align: start!important;    align-items: flex-start!important;    -ms-flex-direction: column!important;    flex-direction: column!important;    -webkit-box-pack: start !important;    -ms-flex-pack: start !important;    justify-content: flex-start !important;  }    .mobile-ninja-loop {     display: block;    width: 100%;  }   .modal__inner .copy {    width: 100%;    text-align: center;    background-color: white;  }}@media screen and (max-width: 400px) {  .modal__inner h1 {    font-size: 30px;    line-height: 30px;  }  .modal__inner h2, .modal__inner p {    font-size: 14px;  }}';
                            document.getElementsByTagName('head')[0].appendChild(widget._styleTag);
                            var utils = optimizely.get('utils');

                            utils.waitForElement('.product-description ul.list')
                                .then(function(elem) {
                                    // Prepend the extension html to the body
                                    elem.insertAdjacentHTML('afterend', extension.$html);
                                });

                        },
                        hideFn: function(event) {
                            var $ = window.optimizely.get('jquery');
                            var widget = event.data.config;
                            widget.$id = "16756580917";
                            widget.$instance = event.data.id;
                            widget.$fieldDefaults = [{ "name": "img", "default_value": "//cdn.optimizely.com/img/8894411182/5022342fbc274f989fed3f1449a6e920.png" }];
                            (function(widg) {
                                var i = 0;
                                var field;
                                for (; i < widg.$fieldDefaults.length; ++i) {
                                    field = widg.$fieldDefaults[i];
                                    if (!widg.hasOwnProperty(field.name)) {
                                        widg[field.name] = field.default_value;
                                    }
                                }
                            })(widget);
                            var extension = widget;
                            widget._styleTag = document.getElementById('widget-css-16756580917');
                            if (widget._styleTag) widget._styleTag.parentNode.removeChild(widget._styleTag);
                            var extensionElement = document.getElementById('optimizely-extension-' + extension.$instance);
                            if (extensionElement) {
                                extensionElement.parentElement.removeChild(extensionElement);
                            }

                        },
                    });

                    PluginManager.registerWidget({
                        widgetId: '16916282530',
                        showFn: function(event) {
                            var $ = window.optimizely.get('jquery');
                            var widget = event.data.config;
                            var _template = new Hogan(function(c, p, i) { var t = this;
                                t.b(i = i || ""); return t.fl(); })
                            widget.$id = "16916282530";
                            widget.$instance = event.data.id;
                            widget.$render = _template.render.bind(_template)
                            widget.$fieldDefaults = [{ "name": "selector", "default_value": "body" }, { "name": "className", "default_value": "new-class" }];
                            (function(widg) {
                                var i = 0;
                                var field;
                                for (; i < widg.$fieldDefaults.length; ++i) {
                                    field = widg.$fieldDefaults[i];
                                    if (!widg.hasOwnProperty(field.name)) {
                                        widg[field.name] = field.default_value;
                                    }
                                }
                            })(widget);
                            widget.$html = _template.render({ widget: widget, extension: widget })
                            var extension = widget;

                            var utils = optimizely.get('utils');
                            var selector = extension.selector;
                            var className = extension.className;

                            utils.waitForElement(selector).then(function(element) {
                                $(selector).addClass(className);
                            });
                        },
                        hideFn: function(event) {
                            var $ = window.optimizely.get('jquery');
                            var widget = event.data.config;
                            widget.$id = "16916282530";
                            widget.$instance = event.data.id;
                            widget.$fieldDefaults = [{ "name": "selector", "default_value": "body" }, { "name": "className", "default_value": "new-class" }];
                            (function(widg) {
                                var i = 0;
                                var field;
                                for (; i < widg.$fieldDefaults.length; ++i) {
                                    field = widg.$fieldDefaults[i];
                                    if (!widg.hasOwnProperty(field.name)) {
                                        widg[field.name] = field.default_value;
                                    }
                                }
                            })(widget);
                            var extension = widget;

                            var extensionHtml = document.querySelector('.optly_butter_bar');
                            if (extensionHtml) extensionHtml.remove();

                        },
                    });

                    PluginManager.registerWidget({
                        widgetId: '17194870906',
                        showFn: function(event) {
                            var $ = window.optimizely.get('jquery');
                            var widget = event.data.config;
                            var _template = new Hogan(function(c, p, i) { var t = this;
                                t.b(i = i || "");
                                t.b("<div class=\"bottom-floating-slim-banner\">");
                                t.b("\n" + i);
                                t.b("  <div class=\"slim-banner-text\">");
                                t.b("\n" + i);
                                t.b("    <h1>");
                                t.b(t.v(t.d("extension.title", c, p, 0)));
                                t.b("</h1>");
                                t.b("\n" + i);
                                t.b("	  <p>");
                                t.b(t.t(t.d("extension.content", c, p, 0)));
                                t.b("</p>");
                                t.b("\n" + i);
                                t.b("    <button class=\"close-slim-banner\"> Dismiss </button>");
                                t.b("\n" + i);
                                t.b("  </div>");
                                t.b("\n" + i);
                                t.b("</div>"); return t.fl(); })
                            widget.$id = "17194870906";
                            widget.$instance = event.data.id;
                            widget.$render = _template.render.bind(_template)
                            widget.$fieldDefaults = [{ "name": "title", "default_value": "Important update to our Privacy Policy" }, { "name": "content", "default_value": "We have added some new information to our Privacy Policy to provide your rights under the California Consumer Privacy Act of 2018 (the “CCPA”). Click <a id=\"banner-link\" href=\"/privacy-policy\">here</a> to read a description of your rights and disclosures about how we process your personal information." }, { "name": "cookie", "default_value": "ccpa-popup3" }];
                            (function(widg) {
                                var i = 0;
                                var field;
                                for (; i < widg.$fieldDefaults.length; ++i) {
                                    field = widg.$fieldDefaults[i];
                                    if (!widg.hasOwnProperty(field.name)) {
                                        widg[field.name] = field.default_value;
                                    }
                                }
                            })(widget);
                            widget.$html = _template.render({ widget: widget, extension: widget })
                            var extension = widget;
                            widget._styleTag = document.createElement('style');
                            widget._styleTag.id = 'widget-css-17194870906';
                            widget._styleTag.innerHTML = '.bottom-floating-slim-banner {  background-color: #fdfdf3;  border-top: 1px solid #549c74;  color: #549c74;  text-align: center;  line-height: 18px;  font-size: 10px;  transition: 1s;  height: auto;  width: 100%;  position: fixed;  bottom: 0;  left: 0;	padding: 6px;  z-index: 2147483646;}@media screen and (max-width: 350px) {	.coupon-applied-slim-banner {    	font-size: 10px;     line-height: 14px;  }}.opti-hide-banner {  display:none;  height: 0px;}.opti-body-no-margin {  margin-top: 0; }.slim-banner-text h1 {  font-size: 16px;  padding-top: 5px;}.slim-banner-text p {  font-size: 12px;  margin-bottom: 6px;}#banner-link {  color: #f05a28;}.slim-banner-text button {  background-color: #f05a28;  border-color: #f05a28;  margin-bottom: 5px;  border-radius: 0;  padding: 0.1rem .4rem;  color: #FFF;  font-size: 14px;}';
                            document.getElementsByTagName('head')[0].appendChild(widget._styleTag);
                            var utils = window.optimizely.get('utils');

                            // helper functions from developer documentation: https://developers.optimizely.com/classic/javascript/code-samples/index.html
                            var setCookie = function(c_name, value, exdays, c_domain) {
                                c_domain = (typeof c_domain === "undefined") ? "" : "domain=" + c_domain + ";";
                                var exdate = new Date();
                                exdate.setDate(exdate.getDate() + exdays);
                                var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
                                document.cookie = c_name + "=" + c_value + ";" + c_domain + "path=/";
                            };

                            var getCookie = function(name) {
                                var match = document.cookie.match(name + '=([^;]*)');
                                return match ? match[1] : undefined;
                            };

                            utils.waitForElement('body').then(function(element) {
                                var html = widget.$html;
                                var cookieName = extension.cookie + '_' + widget.$id + '_dismissed';
                                var cookiePresent = getCookie(cookieName);

                                // check to make sure cookie is undefined
                                if (!cookiePresent) {
                                    element.insertAdjacentHTML('beforebegin', html);

                                    // Handle close action 
                                    utils.waitUntil(function() {
                                        var closerOnPage = document.querySelectorAll('.close-slim-banner');
                                        return closerOnPage.length > 0;
                                    }).then(function() {
                                        jQuery('.close-slim-banner').click(function() {
                                            jQuery('.bottom-floating-slim-banner').addClass("opti-hide-banner");
                                        });
                                        document.getElementByClassName("close-slim-banner").addEventListener('click', function(event) {
                                            document.getElementsByClassName('bottom-floating-slim-banner').addClass('opti-hide-banner');
                                            // document.getElementsByTagName('body')[0].classList.add('opti-body-no-margin');
                                            setCookie(cookieName, 'true', 1);
                                        });
                                    });

                                    // always trigger set cookie after first view
                                    setCookie(cookieName, 'true', 1);
                                }

                            });

                        },
                        hideFn: function(event) {
                            var $ = window.optimizely.get('jquery');
                            var widget = event.data.config;
                            widget.$id = "17194870906";
                            widget.$instance = event.data.id;
                            widget.$fieldDefaults = [{ "name": "title", "default_value": "Important update to our Privacy Policy" }, { "name": "content", "default_value": "We have added some new information to our Privacy Policy to provide your rights under the California Consumer Privacy Act of 2018 (the “CCPA”). Click <a id=\"banner-link\" href=\"/privacy-policy\">here</a> to read a description of your rights and disclosures about how we process your personal information." }, { "name": "cookie", "default_value": "ccpa-popup3" }];
                            (function(widg) {
                                var i = 0;
                                var field;
                                for (; i < widg.$fieldDefaults.length; ++i) {
                                    field = widg.$fieldDefaults[i];
                                    if (!widg.hasOwnProperty(field.name)) {
                                        widg[field.name] = field.default_value;
                                    }
                                }
                            })(widget);
                            var extension = widget;
                            widget._styleTag = document.getElementById('widget-css-17194870906');
                            if (widget._styleTag) widget._styleTag.parentNode.removeChild(widget._styleTag);
                            var extensionElement = document.getElementById('optimizely-extension-' + extension.$instance);
                            if (extensionElement) {
                                extensionElement.parentElement.removeChild(extensionElement);
                            }

                        },
                    });
                }],
                "namespace": "8894411182",
                "tagGroups": [{ "id": "273", "tags": [{ "viewId": "16336380433", "tagApiName": "faq_question" }] }],
                "integrationSettings": [],
                "interestGroups": [{ "keywords": ["data"], "tagGroupId": "273", "available": true, "id": "423" }, { "keywords": ["device", "my", "own"], "tagGroupId": "273", "available": true, "id": "430" }],
                "dimensions": [{ "segmentId": null, "id": "9724110997", "apiName": "pjs_chat_clicker", "name": "Chat Clicker" }, { "segmentId": null, "id": "9728110844", "apiName": "pjs_utm_medium", "name": "utm_medium" }, { "segmentId": null, "id": "9728972276", "apiName": "pjs_utm_source", "name": "utm_source" }, { "segmentId": null, "id": "9731151141", "apiName": "pjs_referral_url", "name": "referral_url" }, { "segmentId": null, "id": "9734230552", "apiName": "pjs_utm_content", "name": "utm_content" }, { "segmentId": null, "id": "9737252521", "apiName": "pjs_utm_campaign", "name": "utm_campaign" }, { "segmentId": null, "id": "9745441203", "apiName": "pjs_utm_term", "name": "utm_term" }, { "segmentId": null, "id": "10449614147", "apiName": "gtm_source_medium", "name": "Source / Medium" }, { "segmentId": null, "id": "10827983108", "apiName": "ni_propensity_score", "name": "NI Propensity Score" }, { "segmentId": null, "id": "11165152687", "apiName": "risk_free_trial_kit", "name": "Risk Free Trial Kit Adders" }, { "segmentId": null, "id": "11726140801", "apiName": "Onsite_Click", "name": "Onsite Click" }, { "segmentId": null, "id": "13151330506", "apiName": "validated", "name": "Has Validated" }, { "segmentId": null, "id": "13167040282", "apiName": "placed_order", "name": "Placed Order" }, { "segmentId": null, "id": "13173720349", "apiName": "login_clicked_past", "name": "Clicked Log In In The Past" }, { "segmentId": null, "id": "15059511120", "apiName": "vf_entry_point", "name": "VF Entry Point" }, { "segmentId": null, "id": "15615210672", "apiName": "vf_entry_page", "name": "Validation Flow Entry Page" }],
                "audiences": [{ "conditions": ["and", ["or", ["or", { "value": "yes", "type": "query", "name": "qa_only", "match": "exact" }],
                        ["or", { "value": "yes", "type": "cookies", "name": "qa_only", "match": "exact" }],
                        ["or", { "value": "localStorage.getItem('qaOnly') === 'yes';", "type": "code", "name": null, "match": null }]
                    ]], "id": "10023895230", "name": "QA Only" }, { "conditions": ["and", ["or", ["not", ["or", { "value": "", "type": "query", "name": "qa_only", "match": "exists" }]]],
                        ["or", ["not", ["or", { "value": null, "type": "cookies", "name": "qa_only", "match": "exists" }]]]
                    ], "id": "11216211532", "name": "Not QA" }, { "conditions": ["and", ["or", ["not", ["or", { "value": "LR||AFRICANJORDAN", "type": "location", "name": null, "match": null }]]]], "id": "13333340160", "name": "Not in Africa" }, { "conditions": ["and", ["or", ["or", { "value": "typeof(Storage) != \"undefined\" && (localStorage.getItem(\"_landing-page-path\") == \"/plans/\" || localStorage.getItem(\"_landing-page-path\") == null)\n", "type": "code", "name": null, "match": null }]],
                        ["or", ["or", { "value": null, "type": "first_session", "name": null, "match": null }]]
                    ], "id": "17025410970", "name": "New Visitor: Landing Page: Plans" }, { "conditions": ["and", ["or", ["or", { "value": null, "type": "first_session", "name": null, "match": null }]],
                        ["or", ["or", { "value": "typeof(Storage) != \"undefined\" && localStorage.getItem(\"_landing-page-path\") != \"/plans/\"", "type": "code", "name": null, "match": null }]]
                    ], "id": "17027440409", "name": "New Visitor: Landing Page: Not Plans" }],
                "anonymizeIP": true,
                "projectJS": function() {
                    function validationStatus() {

                        var coverage = localStorage.getItem("coverage");
                        var compatibility = localStorage.getItem("compatibility");
                        var validated = null;

                        if (coverage === null || compatibility === null) {
                            validated = "false";
                        } else {
                            validated = "true";
                        }

                        console.info("Optimizely validation status: %s", validated);
                        return validated;
                    }

                    function infoinClickedStatus() {
                        var infoin_clicked_past = localStorage.getItem('infoin_clicked');
                        if (infoin_clicked_past === null || infoin_clicked_past === "false") {
                            infoin_clicked_past = "false";
                        } else {
                            infoin_clicked_past = "true";
                        }

                        console.info("Optimizely infoin_clicked_past status: %s", infoin_clicked_past);
                        return infoin_clicked_past;
                    }

                    var validated = validationStatus();
                    var infoin_clicked_past = infoinClickedStatus();


                    function onInitialized() {
                        window["optimizely"].push({
                            "type": "user",
                            "attributes": {
                                "validated": validated,
                                "infoin_clicked_past": infoin_clicked_past
                            }
                        });
                    }

                    function onCampaignDecided(event) {
                        var vId = event.data.decision.variationId;
                        var cId = event.data.decision.experimentId;
                        var experiments = event.data.campaign.experiments;
                        if (event.data.campaign.name.includes('MM-')) {
                            var campaignName = event.data.campaign.name.split('[')[0].trim();
                            var varName = null;
                            if (event.data.decision.variationId != null) {
                                for (var i = 0; i < experiments.length; i++) {
                                    var variations = experiments[i].variations;
                                    for (var v = 0; v < variations.length; v++) {
                                        if (variations[v].id == vId) {
                                            varName = variations[v].name;
                                        }
                                    }
                                }
                                window.dataLayer = window.dataLayer || [];
                                window.dataLayer.push({ 'event': 'oxCampaignDecidedEvent', 'campaignName': campaignName + '|' + varName + '(' + cId + '|' + vId + ')' });
                            }
                        }
                    }

                    window["optimizely"] = window["optimizely"] || [];
                    window["optimizely"].push({
                        type: "addListener",
                        filter: {
                            type: "lifecycle",
                            name: "campaignDecided"
                        },
                        handler: onCampaignDecided
                    });

                    window["optimizely"].push({
                        type: "addListener",
                        filter: {
                            type: "lifecycle",
                            name: "initialized"
                        },
                        handler: onInitialized
                    });
                },
                "visitorAttributes": [],
                "enableForceParameters": true,
                "accountId": "8894411182",
                "events": [{ "category": "other", "name": "Click BYOP", "eventType": "click", "viewId": "9414584294", "apiName": "8894411182_hp_byop_click", "id": "9416604820", "eventFilter": { "filterType": "target_selector", "selector": "nav.nav > a:nth-of-type(2)" } }, { "category": "other", "name": "Click Shop Now - Hero", "eventType": "click", "viewId": "9414584294", "apiName": "8894411182_hp_shop_now_hero_click", "id": "9424604862", "eventFilter": { "filterType": "target_selector", "selector": ".Hero-imgLarge" } }, { "category": "other", "name": "Click Shop Plans", "eventType": "click", "viewId": "9414584294", "apiName": "8894411182_hp_shop_plans_click", "id": "9429688059", "eventFilter": { "filterType": "target_selector", "selector": "nav.nav > a:nth-of-type(1)" } }, { "category": "other", "name": "Click Nav", "eventType": "click", "viewId": "9476731050", "apiName": "8894411182_global_nav_click", "id": "9432043624", "eventFilter": { "filterType": "target_selector", "selector": ".navbar-nav" } }, { "category": "other", "name": "Click Shop Add-ons", "eventType": "click", "viewId": "9414584294", "apiName": "8894411182_hp_shop_addons_click", "id": "9463732162", "eventFilter": { "filterType": "target_selector", "selector": "nav.nav > a:nth-of-type(3)" } }, { "category": "other", "name": "Click Add to Cart", "eventType": "click", "viewId": "9476731050", "apiName": "8894411182_add_to_cart_clickers", "id": "9734601508", "eventFilter": { "filterType": "target_selector", "selector": ".js-calc-add-to-cart, .js-add-plan" } }, { "category": "other", "name": "Click Cart Modal", "eventType": "click", "viewId": "9476731050", "apiName": "8894411182_cart_modal_clickers", "id": "9736432599", "eventFilter": { "filterType": "target_selector", "selector": ".Cart" } }, { "category": "other", "name": "Click Need Help - Chat Bot", "eventType": "click", "viewId": "9476731050", "apiName": "8894411182_chat_clickers", "id": "9737302430", "eventFilter": { "filterType": "target_selector", "selector": "#js-ada-chat-button" } }, { "category": "other", "name": "Click Place Order", "eventType": "click", "viewId": "9488460812", "apiName": "8894411182_place_order_clickers_1", "id": "9824971706", "eventFilter": { "filterType": "target_selector", "selector": ".btn-purchase" } }, { "category": "other", "name": "Click Apply Promo", "eventType": "click", "viewId": "9476731050", "apiName": "8894411182_apply_promo_clickers_1", "id": "9833671644", "eventFilter": { "filterType": "target_selector", "selector": "input[name=apply_coupon]" } }, { "category": "other", "name": "Click Shop Plans", "eventType": "click", "viewId": "9665581552", "apiName": "8894411182_shop_plans", "id": "9867259345", "eventFilter": { "filterType": "target_selector", "selector": "nav.nav > a:nth-of-type(1) img" } }, { "category": "other", "name": "View Video", "eventType": "custom", "viewId": null, "apiName": "videoView", "id": "10514751563", "eventFilter": null }, { "category": "other", "name": "Scroll Depth 50", "eventType": "custom", "viewId": null, "apiName": "scroll50", "id": "10516890199", "eventFilter": null }, { "category": "other", "name": "Scroll Depth 75", "eventType": "custom", "viewId": null, "apiName": "scroll75", "id": "10517420336", "eventFilter": null }, { "category": "other", "name": "Scroll Depth 25", "eventType": "custom", "viewId": null, "apiName": "scroll25", "id": "10522190356", "eventFilter": null }, { "category": "other", "name": "Scroll Depth 100", "eventType": "custom", "viewId": null, "apiName": "scroll100", "id": "10522710169", "eventFilter": null }, { "category": "other", "name": "Add to Cart", "eventType": "custom", "viewId": null, "apiName": "add_to_cart", "id": "11093593234", "eventFilter": null }, { "category": "other", "name": "Click Secondary Plan CTA", "eventType": "click", "viewId": "9481221941", "apiName": "8894411182_secondary_plans_cta", "id": "11210370414", "eventFilter": { "filterType": "target_selector", "selector": ".card-footer > a" } }, { "category": "other", "name": "Click Primary Plan CTA", "eventType": "click", "viewId": "9481221941", "apiName": "8894411182_primary_cta", "id": "11216330346", "eventFilter": { "filterType": "target_selector", "selector": ".card-text > .btn-primary" } }, { "category": "other", "name": "Click iPhone Sales CTA - Hero ", "eventType": "click", "viewId": "10679811972", "apiName": "8894411182_iphone_sales__hero_cta_clicks", "id": "11469852726", "eventFilter": { "filterType": "target_selector", "selector": "#iphone-sales-hero, #buy-now-cta, #learn-more-cta" } }, { "category": "other", "name": "Purchase 3MO/3GB", "eventType": "custom", "viewId": null, "apiName": "Purchase_MINT-SMALL-03", "id": "11697834129", "eventFilter": null }, { "category": "other", "name": "Add To Cart - PDP", "eventType": "custom", "viewId": null, "apiName": "add_to_cart_pdp", "id": "11925451030", "eventFilter": null }, { "category": "other", "name": "Add To Cart - Plans", "eventType": "custom", "viewId": null, "apiName": "add_to_cart_plans", "id": "11942830995", "eventFilter": null }, { "category": "other", "name": "Submit Newsletter Sign Up", "eventType": "custom", "viewId": null, "apiName": "email_acquisition", "id": "13142100415", "eventFilter": null }, { "category": "other", "name": "Begin VF - Homepage Hero", "eventType": "custom", "viewId": null, "apiName": "homepage_hero_vf_start", "id": "13148380294", "eventFilter": null }, { "category": "other", "name": "View Slide Down Nav", "eventType": "custom", "viewId": null, "apiName": "slide-down-nav-seen", "id": "13199650407", "eventFilter": null }, { "category": "other", "name": "Click Slide Down Nav", "eventType": "custom", "viewId": null, "apiName": "slide-down-nav-clicked", "id": "13201810110", "eventFilter": null }, { "category": "other", "name": "Click iPhone XR Banner", "eventType": "click", "viewId": "10679811972", "apiName": "8894411182_iphone_xr_banner_click__homepage", "id": "14095800317", "eventFilter": { "filterType": "target_selector", "selector": "#iphone-xr-banner" } }, { "category": "other", "name": "Click Customer Service Info - Footer", "eventType": "click", "viewId": "9476731050", "apiName": "8894411182_customer_service_footer", "id": "14471920456", "eventFilter": { "filterType": "target_selector", "selector": "#js-footer-subscribe > div.Footer-help" } }, { "category": "other", "name": "Complete Billing Method - Checkout", "eventType": "custom", "viewId": null, "apiName": "billing_method_complete", "id": "14476390923", "eventFilter": null }, { "category": "other", "name": "Click Customer Service Info - Top Nav", "eventType": "click", "viewId": "9476731050", "apiName": "8894411182_customer_service_info_top_nav", "id": "14481890716", "eventFilter": { "filterType": "target_selector", "selector": "#menu-item-271829, #menu-item-271831, #menu-item-271832, #menu-item-271833, #menu-item-271835, #menu-item-271836" } }, { "category": "other", "name": "Complete Billing Address - Checkout", "eventType": "custom", "viewId": null, "apiName": "billing_address_complete", "id": "14498420455", "eventFilter": null }, { "category": "other", "name": "Complete Shipping Address - Checkout", "eventType": "custom", "viewId": null, "apiName": "shipping_address_complete", "id": "14502240419", "eventFilter": null }, { "category": "other", "name": "Purchase 3MO/8GB", "eventType": "custom", "viewId": null, "apiName": "Purchase_MINT-MEDIUM-03", "id": "14616360957", "eventFilter": null }, { "category": "other", "name": "Click Any Plan But 3MO/8GB", "eventType": "click", "viewId": "9481221941", "apiName": "8894411182_any_plan_but_3mo8gb", "id": "14625170233", "eventFilter": { "filterType": "target_selector", "selector": "#plan_3month_3GB .js-add-plan, #plan_3month_3GB a.btn, #plan_3month_12GB .js-add-plan, #plan_3month_12GB a.btn, #plan_6month_3GB .js-add-plan, #plan_6month_3GB a.btn, #plan_6month_8GB .js-add-plan, #plan_6month_8GB a.btn, #plan_6month_12GB .js-add-plan, #plan_6month_12GB a.btn, #plan_12month_3GB .js-add-plan, #plan_12month_3GB a.btn, #plan_12month_8GB .js-add-plan, #plan_12month_8GB a.btn, #plan_12month_12GB .js-add-plan, #plan_12month_12GB a.btn" } }, { "category": "other", "name": "Click 3MO/8GB Plan", "eventType": "click", "viewId": "9481221941", "apiName": "8894411182_3mo8gb_plan_click", "id": "14625190701", "eventFilter": { "filterType": "target_selector", "selector": "#plan_3month_8GB > div > div.card-text > a, #plan_3month_8GB > div > div.card-footer > a, #plan_3month_5GB .js-add-plan, #plan_3month_5GB a.btn, .cta-container.form-group button.btn-primary, .hero-copy button" } }, { "category": "other", "name": "Click Buy a New One - Nav", "eventType": "click", "viewId": "9476731050", "apiName": "8894411182_clicked_phones_in_nav", "id": "14838610215", "eventFilter": { "filterType": "target_selector", "selector": "#menu-item-83018 > .dropdown-item" } }, { "category": "other", "name": "Click Phones Dropdown - Nav", "eventType": "click", "viewId": "9476731050", "apiName": "8894411182_click_phones_dropdown_in_nav", "id": "14840640608", "eventFilter": { "filterType": "target_selector", "selector": "#menu-item-64743" } }, { "category": "other", "name": "Click Plans - Nav", "eventType": "click", "viewId": "9476731050", "apiName": "8894411182_clicked_plans_in_nav", "id": "14854280220", "eventFilter": { "filterType": "target_selector", "selector": "#menu-item-651 > a, #menu-item-651 > a.nav-link" } }, { "category": "other", "name": "Click BYOP - Nav", "eventType": "click", "viewId": "9476731050", "apiName": "8894411182_clicked_byop_in_nav", "id": "14861960908", "eventFilter": { "filterType": "target_selector", "selector": "#menu-item-654 > a.nav-link, #menu-item-654 > .dropdown-item" } }, { "category": "other", "name": "VF Entry", "eventType": "custom", "viewId": null, "apiName": "vf_entry", "id": "15056230783", "eventFilter": null }, { "category": "other", "name": "Switch Plans - Multi-Month", "eventType": "custom", "viewId": null, "apiName": "switch_to_multi", "id": "15201660455", "eventFilter": null }, { "category": "other", "name": "Switch Plans - Single Month", "eventType": "custom", "viewId": null, "apiName": "switch_to_single", "id": "15215360382", "eventFilter": null }, { "category": "other", "name": "VF Complete", "eventType": "custom", "viewId": null, "apiName": "vf_complete", "id": "15527240638", "eventFilter": null }, { "category": "other", "name": "Here's What You Get", "eventType": "custom", "viewId": null, "apiName": "heres_what_you_get", "id": "15686730630", "eventFilter": null }, { "category": "other", "name": "Add to Cart -  3 Month Plans PDP", "eventType": "custom", "viewId": null, "apiName": "add_to_cart_3_mo_PDP", "id": "16574940131", "eventFilter": null }, { "category": "other", "name": "Purchase 3MO/12GB", "eventType": "custom", "viewId": null, "apiName": "Purchase_MINT-LARGE-03", "id": "16577110184", "eventFilter": null }, { "category": "other", "name": "Viewed Medium Plan", "eventType": "custom", "viewId": null, "apiName": "viewed_medium_plan", "id": "16710870533", "eventFilter": null }, { "category": "other", "name": "Purchased Medium Plan", "eventType": "custom", "viewId": null, "apiName": "purchased_medium_plan", "id": "16720320970", "eventFilter": null }, { "category": "other", "name": "MM-14 Avg Data Visible", "eventType": "custom", "viewId": null, "apiName": "avg_data_visibility", "id": "16855440055", "eventFilter": null }, { "category": "other", "name": "Purchase 3MO Plan", "eventType": "custom", "viewId": null, "apiName": "purchase_3MO_plan", "id": "16870421768", "eventFilter": null }, { "category": "other", "name": "Purchase Complete", "eventType": "custom", "viewId": null, "apiName": "purchase_complete", "id": "16933881680", "eventFilter": null }, { "category": "other", "name": "Click CTA on Check Coverage Module", "eventType": "custom", "viewId": null, "apiName": "click_check_coverage", "id": "17170460392", "eventFilter": null }, { "category": "other", "name": "Click CTA in Secondary Nav", "eventType": "custom", "viewId": null, "apiName": "click_CTA_secondary_nav", "id": "17258793976", "eventFilter": null }, { "category": "other", "name": "View Checkout, Phones", "eventType": "custom", "viewId": null, "apiName": "view_checkout_phones", "id": "17266073041", "eventFilter": null }, { "category": "other", "name": "View Cart, Phones", "eventType": "custom", "viewId": null, "apiName": "view_cart_phones", "id": "17271701782", "eventFilter": null }, { "category": "other", "name": "Add to Cart, Phones", "eventType": "custom", "viewId": null, "apiName": "atc_phones", "id": "17273531781", "eventFilter": null }, { "category": "other", "name": "View iPhone PDP, Phones", "eventType": "custom", "viewId": null, "apiName": "view_iphone_pdp", "id": "17277253090", "eventFilter": null }, { "category": "other", "name": "Click iPhone Grid CTA", "eventType": "custom", "viewId": null, "apiName": "iphone_grid_cta", "id": "17277452356", "eventFilter": null }, { "category": "other", "name": "Purchase Complete, Phones Site", "eventType": "custom", "viewId": null, "apiName": "purchase_complete_phones", "id": "17283606297", "eventFilter": null }, { "category": "other", "name": "Enter ZIP on Cart", "eventType": "custom", "viewId": null, "apiName": "enter_zip_on_cart", "id": "17287174202", "eventFilter": null }],
                "experimental": { "trimPages": true },
                "revision": "12840"
            },
            h = n(135),
            g = "initializeOptimizelyPreview";
        if (d.populateDirectiveData(), s.clientHasAlreadyInitialized()) return void a.warn("Main / Disabling because Optimizely has already initialized on this page load. Are there multiple snippets on the page?");
        if (s.shouldBailForDesktopApp()) return void a.log("Main / Disabling because of desktop app.");
        if (s.conflictInObservingChanges()) return void a.log("Main / Disabling: Observe Changes Indefinitely is on, but browser does not support it.");
        if (s.shouldLoadInnie()) l.registerFunction("getProjectId", (function() { return p.projectId })), l.registerFunction("getAccountId", (function() { return p.accountId })), f.addScriptAsync("https://app.optimizely.com/js/innie.js"), a.log("Main / Disabling in favor of the editor client.");
        else if (s.shouldLoadPreview()) { var v;
            v = s.isSlave() ? window.optimizely : window.optimizely = window.optimizely || [], v.push({ type: "load", data: p }), a.log("Main / Disabling in favor of the preview client."), n(149).setupPreviewGlobal(), n(149).pushToPreviewGlobal({ type: "pushPreviewData", name: "liveCommitData", data: p }), s.isSlave() || (l.registerFunction("getProjectId", (function() { return p.projectId })), f.addScriptSync("https://cdn-assets-prod.s3.amazonaws.com/js/preview2/8894411182.js")) } else if (s.shouldBootstrapDataForPreview()) { l.registerFunction(g, (function(t) { e(t), l.unregisterFunction(g) })); var m = s.isSlave() ? PROJECT_ID_FOR_SLAVE_PREVIEW : l.getFunction("getProjectId")();
            u = t(s.getProjectToken(), m, s.getPreviewLayerIds()), f.addScriptSync(u), n(149).setupPreviewGlobal(), f.addScriptAsync("/dist/js/preview_ui.js") } else s.shouldBootstrapDataForEditor() ? (l.registerFunction(g, (function(t) { e(t), l.unregisterFunction(g) })), f.addScriptAsync(window.optimizely_editor_data_endpoint)) : s.shouldInitialize() && e(p);
        r.timeEnd("block")
    }
    try { i() } catch (e) { try { n(121).handleError(e) } catch (e) { console.log(e) } }
}), (function(e, t, n) {
    function i() { s(); var e = F.getRumData(); return e.extras = e.extras || {}, e.extras.beacon = { cjsTimeout: !0 }, e = v.pickBy(e, (function(e) { return !v.isUndefined(e) })), a(e) }

    function r(e) { var t = L.getPromise("RUM_FIRST_BEACON"); return t ? t.then(e) : _.makeAsyncRequest("RUM_FIRST_BEACON", e) }

    function a(e) { return v.isEmpty(e) ? w.resolve() : r((function() { return N.request({ url: B, method: "POST", data: e, withCredentials: !0 }).then((function(e) { return _.resolveRequest("RUM_FIRST_BEACON", e), e }))["catch"]((function(e) { throw A.error("POST to client-rum failed:", e), _.rejectRequest("RUM_FIRST_BEACON", e), e })) })) }

    function o() { var e = I.getCurrentScript(); if (e) return e.src }

    function s() { var e = { id: F.getRumId(), v: H, account: k.getAccountId(), project: k.getSnippetId() || k.getProjectId(), snippet: k.getSnippetId(), revision: k.getRevision(), clientVersion: P.getClientVersion(), hasSlave: !1, wxhr: !0 }; try { e["numBehaviorEvents"] = E.getEvents().length } catch (e) { A.debug("Unable to get behavior events for RUM:", e) }
        v.assign(e, c(), d()), T.dispatch(C.SET_RUM_DATA, { data: e }) }

    function c() { var e = R.getGlobal("performance"); if (e) { var t, n = F.getScriptSrc(); try { if (n) { A.debug("Using derived script src: ", n); var i = e.getEntriesByName(n);
                    i.length > 0 && (t = i[0]) } if (!t) { var r = /\/\/[^.]+\.optimizely\.(com|test)\/(js|api\/client)\/[\d]+\.js/gi;
                    A.debug("Scanning resource timing entries with regex"); var a = e.getEntriesByType("resource");
                    t = v.find(a, (function(e) { return r.test(e.name) })) } if (t) return v.mapValues(O.ResourceTimingAttributes, (function(e, n) { var i = t[n]; return "number" == typeof i ? Math.round(1e3 * (i || 0)) / 1e3 : "serverTiming" === n ? i || [] : void 0 })) } catch (e) { return } } }

    function u() { try { return !I.querySelector("body") } catch (e) { return null } }

    function l() { try { R.getGlobal("requestAnimationFrame")((function() { var e = F.getRumData().timebase;
                T.dispatch(C.SET_RUM_DATA, { data: { render: y.now() - (e || 0) } }) })) } catch (e) { return } }

    function d() { return M.getDurationsFor(v.values(O.RUMPerformanceTimingAttributes)) }

    function f() { var e = S.keys(),
            t = v.filter(v.map(e, (function(e) { var t = D.getStorageKeyFromKey(e); return t ? { key: e, isForeign: D.isForeignKey(e), category: t, size: e.length + S.getItem(e).length } : null }))),
            n = v.reduce(t, (function(e, t) { var n = t.key,
                    i = D.getIdFromKey(n); if (!i) return e; var r = t.isForeign ? e.foreign : e.local; return r[i] = !0, e }), { local: {}, foreign: {} }),
            i = v.chain(t).filter({ isForeign: !0 }).reduce((function(e, t) { var n = t.key.split("_")[0]; return e[n] = !0, e }), {}).value(),
            r = { local: 0, foreign: 0 },
            a = { local: {}, foreign: {} };
        v.forEach(t, (function(e) { var t = e.isForeign ? "foreign" : "local";
            r[t] += e.size, a[t][e.category] || (a[t][e.category] = 0), a[t][e.category] += e.size })); var o = { numKeys: S.allKeys().length, sizeKeys: S.allKeys().toString().length, sizeValues: S.allValues().toString().length, idCounts: { local: v.keys(n.local).length, foreign: v.keys(n.foreign).length }, foreignOriginCount: v.keys(i).length, byteTotals: r, byteTotalsByCategory: a },
            s = b.estimateStorage(); return s.then((function(e) { return v.assign(o, { storageEstimate: e }) })) }

    function p() { var e = R.getGlobal("performance"),
            t = e ? e.timing : {},
            n = M.getMarks() || {},
            i = F.getApiData(),
            r = F.getDOMObservationData(),
            o = G.get("state").getActiveExperimentIds(),
            s = F.getFeaturesNeededData(),
            c = I.parseUri(F.getScriptSrc()),
            u = F.getRumData() || {},
            l = u.extras || {};
        v.assign(l, { apiCalls: i, DOMObservationData: r, paintTimings: g(), activeExperimentIds: o, numPages: U.getNumberOfPages(), snippet: { scheme: c.protocol.slice(0, -1), host: c.host, path: c.pathname }, networkInfo: h(), experimental: k.getExperimental(), featuresNeeded: s, beacon: { cjsOnload: !0 } }); var d = R.getGlobal("Prototype");
        d && !v.isUndefined(d.Version) && (l.prototypeJS = d.Version); var p = !1;
        p = !0; var m = V.getFrames();
        m.length && (l.xdFramesLoaded = m.length); var _ = { id: F.getRumId(), v: H, project: k.getSnippetId() || k.getProjectId(), navigationTimings: t, userTimings: n, xd: p, apis: v.keys(i), extras: l };
        f().then((function(e) { var t = v.assign(_, { lsMetrics: e });
            a(t) })) }

    function h() { var e = R.getGlobal("navigator"); if (e && e.connection) return v.pick(e.connection, ["downlink", "rtt", "effectiveType"]) }

    function g() { var e = R.getGlobal("performance"); if (e) try { var t = e.getEntriesByType("paint"); if (v.isEmpty(t)) return; return v.reduce(t, (function(e, t) { return e[t.name] = Math.round(t.startTime), e }), {}) } catch (e) { return } } var v = n(2),
        m = n(5),
        _ = n(6),
        E = n(72),
        y = n(24),
        I = n(81),
        T = n(9),
        S = n(82).LocalStorage,
        A = n(23),
        b = n(90),
        w = n(12).Promise,
        D = n(75),
        R = n(41),
        N = n(91),
        C = n(7),
        O = n(25),
        x = n(16),
        L = x.get("stores/async_request"),
        P = x.get("stores/client_metadata"),
        k = x.get("stores/global"),
        F = x.get("stores/rum"),
        M = x.get("stores/performance"),
        V = x.get("stores/xdomain"),
        U = x.get("stores/view_data"),
        G = n(93),
        B = "https://rum.optimizely.com/rum",
        j = 3e3,
        H = "1.0",
        z = .01;
    t.initialize = function() { var e, t = m.generate().replace(/-/g, "");
        e = Math.random() < z; var n = o();
        T.dispatch(C.SET_RUM_DATA, { id: t, RumHost: B, inRumSample: e, src: n, data: { id: t, sync: u(), timebase: y.now(), sampleRate: z, url: n } }) }, t.queueBeacons = function() { return F.getSampleRum() ? (l(), I.isLoaded() ? R.setTimeout(p, j) : R.addEventListener("load", p), new w(function(e, t) { R.setTimeout((function() { i().then(e, t) }), j) }).catch((function(e) { A.warn("RUM / Error sending data:", e) }))) : w.resolve() } }), (function(e, t, n) { e.exports = n(3)._.noConflict() }), (function(e, t, n) {
    (function(e, n) {
        (function() {
            function i(e, t) { return e.set(t[0], t[1]), e }

            function r(e, t) { return e.add(t), e }

            function a(e, t) { return c(De(e), pn) }

            function o(e, t) { return !!e.length && f(e, t, 0) > -1 }

            function s(e, t, n) { for (var i = -1, r = e.length; ++i < r;)
                    if (n(t, e[i])) return !0;
                return !1 }

            function c(e, t) { for (var n = -1, i = t.length, r = e.length; ++n < i;) e[r + n] = t[n]; return e }

            function u(e, t, n) { for (var i = -1, r = e.length; ++i < r;) { var a = e[i],
                        o = t(a); if (null != o && (s === An ? o === o : n(o, s))) var s = o,
                        c = a } return c }

            function l(e, t, n, i) { var r; return n(e, (function(e, n, a) { if (t(e, n, a)) return r = i ? n : e, !1 })), r }

            function d(e, t, n) { for (var i = e.length, r = n ? i : -1; n ? r-- : ++r < i;)
                    if (t(e[r], r, e)) return r;
                return -1 }

            function f(e, t, n) { if (t !== t) return y(e, n); for (var i = n - 1, r = e.length; ++i < r;)
                    if (e[i] === t) return i;
                return -1 }

            function p(e, t, n, i, r) { return r(e, (function(e, r, a) { n = i ? (i = !1, e) : t(n, e, r, a) })), n }

            function h(e, t) { for (var n = -1, i = Array(e); ++n < e;) i[n] = t(n); return i }

            function g(e) { return function(t) { return e(t) } }

            function v(e, t) { return Ee(t, (function(t) { return e[t] })) }

            function m(e) { return e && e.Object === Object ? e : null }

            function _(e, t) { if (e !== t) { var n = null === e,
                        i = e === An,
                        r = e === e,
                        a = null === t,
                        o = t === An,
                        s = t === t; if (e > t && !a || !r || n && !o && s || i && s) return 1; if (e < t && !n || !s || a && !i && r || o && r) return -1 } return 0 }

            function E(e) { return hi[e] }

            function y(e, t, n) { for (var i = e.length, r = t + (n ? 0 : -1); n ? r-- : ++r < i;) { var a = e[r]; if (a !== a) return r } return -1 }

            function I(e) { var t = !1; if (null != e && "function" != typeof e.toString) try { t = !!(e + "") } catch (e) {}
                return t }

            function T(e, t) { return e = "number" == typeof e || di.test(e) ? +e : -1, t = null == t ? Pn : t, e > -1 && e % 1 == 0 && e < t }

            function S(e) { for (var t, n = []; !(t = e.next()).done;) n.push(t.value); return n }

            function A(e) { var t = -1,
                    n = Array(e.size); return e.forEach((function(e, i) { n[++t] = [i, e] })), n }

            function b(e) { var t = -1,
                    n = Array(e.size); return e.forEach((function(e) { n[++t] = e })), n }

            function w(e) { if (Ht(e) && !dr(e)) { if (e instanceof D) return e; if (Di.call(e, "__wrapped__")) return tt(e) } return new D(e) }

            function D(e, t) { this.e = e, this.u = [], this.l = !!t }

            function R() {}

            function N(e, t) { return O(e, t) && delete e[t] }

            function C(e, t) { if (Xi) { var n = e[t]; return n === Rn ? An : n } return Di.call(e, t) ? e[t] : An }

            function O(e, t) { return Xi ? e[t] !== An : Di.call(e, t) }

            function x(e, t, n) { e[t] = Xi && n === An ? Rn : n }

            function L(e) { var t = -1,
                    n = e ? e.length : 0; for (this.clear(); ++t < n;) { var i = e[t];
                    this.set(i[0], i[1]) } }

            function P() { this.d = { hash: new R, map: Yi ? new Yi : [], string: new R } }

            function k(e) { var t = this.d; return Ze(e) ? N("string" == typeof e ? t.string : t.hash, e) : Yi ? t.map["delete"](e) : W(t.map, e) }

            function F(e) { var t = this.d; return Ze(e) ? C("string" == typeof e ? t.string : t.hash, e) : Yi ? t.map.get(e) : X(t.map, e) }

            function M(e) { var t = this.d; return Ze(e) ? O("string" == typeof e ? t.string : t.hash, e) : Yi ? t.map.has(e) : $(t.map, e) }

            function V(e, t) { var n = this.d; return Ze(e) ? x("string" == typeof e ? n.string : n.hash, e, t) : Yi ? n.map.set(e, t) : J(n.map, e, t), this }

            function U(e) { var t = -1,
                    n = e ? e.length : 0; for (this.d = new L; ++t < n;) this.push(e[t]) }

            function G(e, t) { var n = e.d; if (Ze(t)) { var i = n.d,
                        r = "string" == typeof t ? i.string : i.hash; return r[t] === Rn } return n.has(t) }

            function B(e) { var t = this.d; if (Ze(e)) { var n = t.d,
                        i = "string" == typeof e ? n.string : n.hash;
                    i[e] = Rn } else t.set(e, Rn) }

            function j(e) { var t = -1,
                    n = e ? e.length : 0; for (this.clear(); ++t < n;) { var i = e[t];
                    this.set(i[0], i[1]) } }

            function H() { this.d = { array: [], map: null } }

            function z(e) { var t = this.d,
                    n = t.array; return n ? W(n, e) : t.map["delete"](e) }

            function q(e) { var t = this.d,
                    n = t.array; return n ? X(n, e) : t.map.get(e) }

            function Y(e) { var t = this.d,
                    n = t.array; return n ? $(n, e) : t.map.has(e) }

            function K(e, t) { var n = this.d,
                    i = n.array;
                i && (i.length < wn - 1 ? J(i, e, t) : (n.array = null, n.map = new L(i))); var r = n.map; return r && r.set(e, t), this }

            function W(e, t) { var n = Q(e, t); if (n < 0) return !1; var i = e.length - 1; return n == i ? e.pop() : ji.call(e, n, 1), !0 }

            function X(e, t) { var n = Q(e, t); return n < 0 ? An : e[n][1] }

            function $(e, t) { return Q(e, t) > -1 }

            function Q(e, t) { for (var n = e.length; n--;)
                    if (Ct(e[n][0], t)) return n;
                return -1 }

            function J(e, t, n) { var i = Q(e, t);
                i < 0 ? e.push([t, n]) : e[i][1] = n }

            function Z(e, t, n, i) { return e === An || Ct(e, bi[n]) && !Di.call(i, n) ? t : e }

            function ee(e, t, n) {
                (n === An || Ct(e[t], n)) && ("number" != typeof t || n !== An || t in e) || (e[t] = n) }

            function te(e, t, n) { var i = e[t];
                Di.call(e, t) && Ct(i, n) && (n !== An || t in e) || (e[t] = n) }

            function ne(e, t) { return e && ir(t, sn(t), e) }

            function ie(e) { return "function" == typeof e ? e : vn }

            function re(e, t, n, i, r, a, o) { var s; if (i && (s = a ? i(e, r, a, o) : i(e)), s !== An) return s; if (!jt(e)) return e; var c = dr(e); if (c) { if (s = Xe(e), !t) return De(e, s) } else { var u = We(e),
                        l = u == Gn || u == Bn; if (fr(e)) return Ce(e, t); if (u == zn || u == kn || l && !a) { if (I(e)) return a ? e : {}; if (s = $e(l ? {} : e), !t) return s = ne(s, e), n ? Ve(e, s) : s } else { if (!pi[u]) return a ? e : {};
                        s = Qe(e, u, t) } }
                o || (o = new j); var d = o.get(e); return d ? d : (o.set(e, s), (c ? tr : fe)(e, (function(r, a) { te(s, a, re(r, t, n, i, a, e, o)) })), n && !c ? Ve(e, s) : s) }

            function ae(e) { return jt(e) ? Gi(e) : {} }

            function oe(e, t, n) { if ("function" != typeof e) throw new TypeError(Dn); return setTimeout((function() { e.apply(An, n) }), t) }

            function se(e, t, n, i) { var r = -1,
                    a = o,
                    c = !0,
                    u = e.length,
                    l = [],
                    d = t.length; if (!u) return l;
                n && (t = Ee(t, g(n))), i ? (a = s, c = !1) : t.length >= wn && (a = G, c = !1, t = new U(t));
                e: for (; ++r < u;) { var f = e[r],
                        p = n ? n(f) : f; if (c && p === p) { for (var h = d; h--;)
                            if (t[h] === p) continue e;
                        l.push(f) } else a(t, p, i) || l.push(f) }
                return l }

            function ce(e, t) { var n = !0; return tr(e, (function(e, i, r) { return n = !!t(e, i, r) })), n }

            function ue(e, t) { var n = []; return tr(e, (function(e, i, r) { t(e, i, r) && n.push(e) })), n }

            function le(e, t, n, i) { i || (i = []); for (var r = -1, a = e.length; ++r < a;) { var o = e[r];
                    t > 0 && Pt(o) && (n || dr(o) || xt(o)) ? t > 1 ? le(o, t - 1, n, i) : c(i, o) : n || (i[i.length] = o) } return i }

            function de(e, t) { return null == e ? e : nr(e, t, cn) }

            function fe(e, t) { return e && nr(e, t, sn) }

            function pe(e, t) { return ue(t, (function(t) { return Gt(e[t]) })) }

            function he(e, t, n, i, r) { return e === t || (null == e || null == t || !jt(e) && !Ht(t) ? e !== e && t !== t : ge(e, t, he, n, i, r)) }

            function ge(e, t, n, i, r, a) { var o = dr(e),
                    s = dr(t),
                    c = Fn,
                    u = Fn;
                o || (c = Ci.call(e), c = c == kn ? zn : c), s || (u = Ci.call(t), u = u == kn ? zn : u); var l = c == zn && !I(e),
                    d = u == zn && !I(t),
                    f = c == u;
                a || (a = []); var p = vt(a, (function(t) { return t[0] === e })); if (p && p[1]) return p[1] == t; if (a.push([e, t]), f && !l) { var h = o || Qt(e) ? ze(e, t, n, i, r, a) : qe(e, t, c, n, i, r, a); return a.pop(), h } if (!(r & xn)) { var g = l && Di.call(e, "__wrapped__"),
                        v = d && Di.call(t, "__wrapped__"); if (g || v) { var h = n(g ? e.value() : e, v ? t.value() : t, i, r, a); return a.pop(), h } } if (!f) return !1; var h = Ye(e, t, n, i, r, a); return a.pop(), h }

            function ve(e) { var t = typeof e; return "function" == t ? e : null == e ? vn : ("object" == t ? ye : be)(e) }

            function me(e) { return zi(Object(e)) }

            function _e(e) { e = null == e ? e : Object(e); var t = []; for (var n in e) t.push(n); return t }

            function Ee(e, t) { var n = -1,
                    i = Lt(e) ? Array(e.length) : []; return tr(e, (function(e, r, a) { i[++n] = t(e, r, a) })), i }

            function ye(e) { var t = sn(e); return function(n) { var i = t.length; if (null == n) return !i; for (n = Object(n); i--;) { var r = t[i]; if (!(r in n && he(e[r], n[r], An, On | xn))) return !1 } return !0 } }

            function Ie(e, t, n, i, r) { if (e !== t) { var a = dr(t) || Qt(t) ? An : cn(t);
                    tr(a || t, (function(o, s) { if (a && (s = o, o = t[s]), jt(o)) r || (r = new j), Te(e, t, s, n, Ie, i, r);
                        else { var c = i ? i(e[s], o, s + "", e, t, r) : An;
                            c === An && (c = o), ee(e, s, c) } })) } }

            function Te(e, t, n, i, r, a, o) { var s = e[n],
                    c = t[n],
                    u = o.get(c); if (u) return void ee(e, n, u); var l = a ? a(s, c, n + "", e, t, o) : An,
                    d = l === An;
                d && (l = c, dr(c) || Qt(c) ? dr(s) ? l = s : Pt(s) ? l = De(s) : (d = !1, l = re(c, !a)) : Wt(c) || xt(c) ? xt(s) ? l = tn(s) : !jt(s) || i && Gt(s) ? (d = !1, l = re(c, !a)) : l = s : d = !1), o.set(c, l), d && r(l, c, i, a, o), o["delete"](c), ee(e, n, l) }

            function Se(e, t) { return e = Object(e), yt(t, (function(t, n) { return n in e && (t[n] = e[n]), t }), {}) }

            function Ae(e, t) { var n = {}; return de(e, (function(e, i) { t(e, i) && (n[i] = e) })), n }

            function be(e) { return function(t) { return null == t ? An : t[e] } }

            function we(e, t, n) { var i = -1,
                    r = e.length;
                t < 0 && (t = -t > r ? 0 : r + t), n = n > r ? r : n, n < 0 && (n += r), r = t > n ? 0 : n - t >>> 0, t >>>= 0; for (var a = Array(r); ++i < r;) a[i] = e[i + t]; return a }

            function De(e) { return we(e, 0, e.length) }

            function Re(e, t) { var n; return tr(e, (function(e, i, r) { return n = t(e, i, r), !n })), !!n }

            function Ne(e, t) { var n = e; return yt(t, (function(e, t) { return t.func.apply(t.thisArg, c([e], t.args)) }), n) }

            function Ce(e, t) { if (t) return e.slice(); var n = new e.constructor(e.length); return e.copy(n), n }

            function Oe(e) { var t = new e.constructor(e.byteLength); return new Fi(t).set(new Fi(e)), t }

            function xe(e) { return yt(A(e), i, new e.constructor) }

            function Le(e) { var t = new e.constructor(e.source, ui.exec(e)); return t.lastIndex = e.lastIndex, t }

            function Pe(e) { return yt(b(e), r, new e.constructor) }

            function ke(e) { return er ? Object(er.call(e)) : {} }

            function Fe(e, t) { var n = t ? Oe(e.buffer) : e.buffer; return new e.constructor(n, e.byteOffset, e.length) }

            function Me(e, t, n, i) { n || (n = {}); for (var r = -1, a = t.length; ++r < a;) { var o = t[r],
                        s = i ? i(n[o], e[o], o, n, e) : e[o];
                    te(n, o, s) } return n }

            function Ve(e, t) { return ir(e, ar(e), t) }

            function Ue(e) { return Dt((function(t, n) { var i = -1,
                        r = n.length,
                        a = r > 1 ? n[r - 1] : An; for (a = "function" == typeof a ? (r--, a) : An, t = Object(t); ++i < r;) { var o = n[i];
                        o && e(t, o, i, a) } return t })) }

            function Ge(e, t) { return function(n, i) { if (null == n) return n; if (!Lt(n)) return e(n, i); for (var r = n.length, a = t ? r : -1, o = Object(n);
                        (t ? a-- : ++a < r) && i(o[a], a, o) !== !1;); return n } }

            function Be(e) { return function(t, n, i) { for (var r = -1, a = Object(t), o = i(t), s = o.length; s--;) { var c = o[e ? s : ++r]; if (n(a[c], c, a) === !1) break } return t } }

            function je(e) { return function() { var t = arguments,
                        n = ae(e.prototype),
                        i = e.apply(n, t); return jt(i) ? i : n } }

            function He(e, t, n, i) {
                function r() { for (var t = -1, s = arguments.length, c = -1, u = i.length, l = Array(u + s), d = this && this !== Si && this instanceof r ? o : e; ++c < u;) l[c] = i[c]; for (; s--;) l[c++] = arguments[++t]; return d.apply(a ? n : this, l) } if ("function" != typeof e) throw new TypeError(Dn); var a = t & Nn,
                    o = je(e); return r }

            function ze(e, t, n, i, r, a) { var o = -1,
                    s = r & xn,
                    c = r & On,
                    u = e.length,
                    l = t.length; if (u != l && !(s && l > u)) return !1; for (var d = !0; ++o < u;) { var f, p = e[o],
                        h = t[o]; if (f !== An) { if (f) continue;
                        d = !1; break } if (c) { if (!Re(t, (function(e) { return p === e || n(p, e, i, r, a) }))) { d = !1; break } } else if (p !== h && !n(p, h, i, r, a)) { d = !1; break } } return d }

            function qe(e, t, n, i, r, a, o) { switch (n) {
                    case Mn:
                    case Vn:
                        return +e == +t;
                    case Un:
                        return e.name == t.name && e.message == t.message;
                    case Hn:
                        return e != +e ? t != +t : e == +t;
                    case qn:
                    case Kn:
                        return e == t + "" } return !1 }

            function Ye(e, t, n, i, r, a) { var o = r & xn,
                    s = sn(e),
                    c = s.length,
                    u = sn(t),
                    l = u.length; if (c != l && !o) return !1; for (var d = c; d--;) { var f = s[d]; if (!(o ? f in t : Di.call(t, f))) return !1 } for (var p = !0, h = o; ++d < c;) { f = s[d]; var g, v = e[f],
                        m = t[f]; if (!(g === An ? v === m || n(v, m, i, r, a) : g)) { p = !1; break }
                    h || (h = "constructor" == f) } if (p && !h) { var _ = e.constructor,
                        E = t.constructor;
                    _ != E && "constructor" in e && "constructor" in t && !("function" == typeof _ && _ instanceof _ && "function" == typeof E && E instanceof E) && (p = !1) } return p }

            function Ke(e, t) { var n = e[t]; return qt(n) ? n : An }

            function We(e) { return Ci.call(e) }

            function Xe(e) { var t = e.length,
                    n = e.constructor(t); return t && "string" == typeof e[0] && Di.call(e, "index") && (n.index = e.index, n.input = e.input), n }

            function $e(e) { return "function" != typeof e.constructor || et(e) ? {} : ae(Vi(e)) }

            function Qe(e, t, n) { var i = e.constructor; switch (t) {
                    case $n:
                        return Oe(e);
                    case Mn:
                    case Vn:
                        return new i(+e);
                    case Qn:
                    case Jn:
                    case Zn:
                    case ei:
                    case ti:
                    case ni:
                    case ii:
                    case ri:
                    case ai:
                        return Fe(e, n);
                    case jn:
                        return xe(e);
                    case Hn:
                    case Kn:
                        return new i(e);
                    case qn:
                        return Le(e);
                    case Yn:
                        return Pe(e);
                    case Wn:
                        return ke(e) } }

            function Je(e) { var t = e ? e.length : An; return Bt(t) && (dr(e) || $t(e) || xt(e)) ? h(t, String) : null }

            function Ze(e) { var t = typeof e; return "number" == t || "boolean" == t || "string" == t && "__proto__" != e || null == e }

            function et(e) { var t = e && e.constructor,
                    n = "function" == typeof t && t.prototype || bi; return e === n }

            function tt(e) { var t = new D(e.e, e.l); return t.u = De(e.u), t }

            function nt(e) { return ue(e, Boolean) }

            function it(e, t) { return e && e.length ? d(e, ve(t, 3)) : -1 }

            function rt(e) { var t = e ? e.length : 0; return t ? le(e, 1) : [] }

            function at(e) { var t = e ? e.length : 0; return t ? le(e, Ln) : [] }

            function ot(e) { return e ? e[0] : An }

            function st(e, t, n) { var i = e ? e.length : 0;
                n = "number" == typeof n ? n < 0 ? qi(i + n, 0) : n : 0; for (var r = (n || 0) - 1, a = t === t; ++r < i;) { var o = e[r]; if (a ? o === t : o !== o) return r } return -1 }

            function ct(e) { var t = e ? e.length : 0; return t ? e[t - 1] : An }

            function ut(e, t, n) { var i = e ? e.length : 0; return t = null == t ? 0 : +t, n = n === An ? i : +n, i ? we(e, t, n) : [] }

            function lt(e) { var t = w(e); return t.l = !0, t }

            function dt(e, t) { return t(e), e }

            function ft(e, t) { return t(e) }

            function pt() { return Ne(this.e, this.u) }

            function ht(e, t, n) { return t = n ? An : t, ce(e, ve(t)) }

            function gt(e, t) { return ue(e, ve(t)) }

            function vt(e, t) { return l(e, ve(t), tr) }

            function mt(e, t) { return tr(e, ie(t)) }

            function _t(e, t, n, i) { e = Lt(e) ? e : pn(e), n = n && !i ? pr(n) : 0; var r = e.length; return n < 0 && (n = qi(r + n, 0)), $t(e) ? n <= r && e.indexOf(t, n) > -1 : !!r && f(e, t, n) > -1 }

            function Et(e, t) { return Ee(e, ve(t)) }

            function yt(e, t, n) { return p(e, ve(t), n, arguments.length < 3, tr) }

            function It(e) { return null == e ? 0 : (e = Lt(e) ? e : sn(e), e.length) }

            function Tt(e, t, n) { return t = n ? An : t, Re(e, ve(t)) }

            function St(e, t) { var n = 0; return t = ve(t), Ee(Ee(e, (function(e, i, r) { return { value: e, index: n++, criteria: t(e, i, r) } })).sort((function(e, t) { return _(e.criteria, t.criteria) || e.index - t.index })), be("value")) }

            function At(e, t) { var n; if ("function" != typeof t) throw new TypeError(Dn); return e = pr(e),
                    function() { return --e > 0 && (n = t.apply(this, arguments)), e <= 1 && (t = An), n } }

            function bt(e) { if ("function" != typeof e) throw new TypeError(Dn); return function() { return !e.apply(this, arguments) } }

            function wt(e) { return At(2, e) }

            function Dt(e, t) { if ("function" != typeof e) throw new TypeError(Dn); return t = qi(t === An ? e.length - 1 : pr(t), 0),
                    function() { for (var n = arguments, i = -1, r = qi(n.length - t, 0), a = Array(r); ++i < r;) a[i] = n[t + i]; var o = Array(t + 1); for (i = -1; ++i < t;) o[i] = n[i]; return o[t] = a, e.apply(this, o) } }

            function Rt(e) { return jt(e) ? dr(e) ? De(e) : ir(e, sn(e)) : e }

            function Nt(e) { return re(e, !0, !0) }

            function Ct(e, t) { return e === t || e !== e && t !== t }

            function Ot(e, t) { return e > t }

            function xt(e) { return Pt(e) && Di.call(e, "callee") && (!Bi.call(e, "callee") || Ci.call(e) == kn) }

            function Lt(e) { return null != e && Bt(rr(e)) && !Gt(e) }

            function Pt(e) { return Ht(e) && Lt(e) }

            function kt(e) { return e === !0 || e === !1 || Ht(e) && Ci.call(e) == Mn }

            function Ft(e) { return Ht(e) && Ci.call(e) == Vn }

            function Mt(e) { if (Lt(e) && (dr(e) || $t(e) || Gt(e.splice) || xt(e))) return !e.length; for (var t in e)
                    if (Di.call(e, t)) return !1;
                return !0 }

            function Vt(e, t) { return he(e, t) }

            function Ut(e) { return "number" == typeof e && Hi(e) }

            function Gt(e) { var t = jt(e) ? Ci.call(e) : ""; return t == Gn || t == Bn }

            function Bt(e) { return "number" == typeof e && e > -1 && e % 1 == 0 && e <= Pn }

            function jt(e) { var t = typeof e; return !!e && ("object" == t || "function" == t) }

            function Ht(e) { return !!e && "object" == typeof e }

            function zt(e) { return Kt(e) && e != +e }

            function qt(e) { return null != e && (Gt(e) ? xi.test(wi.call(e)) : Ht(e) && (I(e) ? xi : li).test(e)) }

            function Yt(e) { return null === e }

            function Kt(e) { return "number" == typeof e || Ht(e) && Ci.call(e) == Hn }

            function Wt(e) { if (!Ht(e) || Ci.call(e) != zn || I(e)) return !1; var t = Vi(e); if (null === t) return !0; var n = t.constructor; return "function" == typeof n && n instanceof n && wi.call(n) == Ni }

            function Xt(e) { return jt(e) && Ci.call(e) == qn }

            function $t(e) { return "string" == typeof e || !dr(e) && Ht(e) && Ci.call(e) == Kn }

            function Qt(e) { return Ht(e) && Bt(e.length) && !!fi[Ci.call(e)] }

            function Jt(e) { return e === An }

            function Zt(e, t) { return e < t }

            function en(e) { return Lt(e) ? e.length ? De(e) : [] : pn(e) }

            function tn(e) { return ir(e, cn(e)) }

            function nn(e) { return "string" == typeof e ? e : null == e ? "" : e + "" }

            function rn(e, t) { var n = ae(e); return t ? gr(n, t) : n }

            function an(e, t) { return e && fe(e, ie(t)) }

            function on(e, t) { return null != e && Di.call(e, t) }

            function sn(e) { var t = et(e); if (!t && !Lt(e)) return me(e); var n = Je(e),
                    i = !!n,
                    r = n || [],
                    a = r.length; for (var o in e) !Di.call(e, o) || i && ("length" == o || T(o, a)) || t && "constructor" == o || r.push(o); return r }

            function cn(e) { for (var t = -1, n = et(e), i = _e(e), r = i.length, a = Je(e), o = !!a, s = a || [], c = s.length; ++t < r;) { var u = i[t];
                    o && ("length" == u || T(u, c)) || "constructor" == u && (n || !Di.call(e, u)) || s.push(u) } return s }

            function un(e, t) { var n = {}; return t = ve(t, 3), fe(e, (function(e, i, r) { n[i] = t(e, i, r) })), n }

            function ln(e, t) { return t = ve(t), Ae(e, (function(e, n) { return !t(e, n) })) }

            function dn(e, t) { return null == e ? {} : Ae(e, ve(t)) }

            function fn(e, t, n) { var i = null == e ? An : e[t]; return i === An && (i = n), Gt(i) ? i.call(e) : i }

            function pn(e) { return e ? v(e, sn(e)) : [] }

            function hn(e) { return e = nn(e), e && si.test(e) ? e.replace(oi, E) : e }

            function gn(e) { return function() { return e } }

            function vn(e) { return e }

            function mn(e) { return ye(gr({}, e)) }

            function _n(e, t, n) { var i = sn(t),
                    r = pe(t, i);
                null != n || jt(t) && (r.length || !i.length) || (n = t, t = e, e = this, r = pe(t, sn(t))); var a = !(jt(n) && "chain" in n) || n.chain,
                    o = Gt(e); return tr(r, (function(n) { var i = t[n];
                    e[n] = i, o && (e.prototype[n] = function() { var t = this.l; if (a || t) { var n = e(this.e),
                                r = n.u = De(this.u); return r.push({ func: i, args: arguments, thisArg: e }), n.l = t, n } return i.apply(e, c([this.value()], arguments)) }) })), e }

            function En() { return Si._ === this && (Si._ = Oi), this }

            function yn() {}

            function In(e) { var t = ++Ri; return nn(e) + t }

            function Tn(e) { return e && e.length ? u(e, vn, Ot) : An }

            function Sn(e) { return e && e.length ? u(e, vn, Zt) : An } var An, bn = "4.6.1",
                wn = 200,
                Dn = "Expected a function",
                Rn = "__lodash_hash_undefined__",
                Nn = 1,
                Cn = 32,
                On = 1,
                xn = 2,
                Ln = 1 / 0,
                Pn = 9007199254740991,
                kn = "[object Arguments]",
                Fn = "[object Array]",
                Mn = "[object Boolean]",
                Vn = "[object Date]",
                Un = "[object Error]",
                Gn = "[object Function]",
                Bn = "[object GeneratorFunction]",
                jn = "[object Map]",
                Hn = "[object Number]",
                zn = "[object Object]",
                qn = "[object RegExp]",
                Yn = "[object Set]",
                Kn = "[object String]",
                Wn = "[object Symbol]",
                Xn = "[object WeakMap]",
                $n = "[object ArrayBuffer]",
                Qn = "[object Float32Array]",
                Jn = "[object Float64Array]",
                Zn = "[object Int8Array]",
                ei = "[object Int16Array]",
                ti = "[object Int32Array]",
                ni = "[object Uint8Array]",
                ii = "[object Uint8ClampedArray]",
                ri = "[object Uint16Array]",
                ai = "[object Uint32Array]",
                oi = /[&<>"'`]/g,
                si = RegExp(oi.source),
                ci = /[\\^$.*+?()[\]{}|]/g,
                ui = /\w*$/,
                li = /^\[object .+?Constructor\]$/,
                di = /^(?:0|[1-9]\d*)$/,
                fi = {};
            fi[Qn] = fi[Jn] = fi[Zn] = fi[ei] = fi[ti] = fi[ni] = fi[ii] = fi[ri] = fi[ai] = !0, fi[kn] = fi[Fn] = fi[$n] = fi[Mn] = fi[Vn] = fi[Un] = fi[Gn] = fi[jn] = fi[Hn] = fi[zn] = fi[qn] = fi[Yn] = fi[Kn] = fi[Xn] = !1; var pi = {};
            pi[kn] = pi[Fn] = pi[$n] = pi[Mn] = pi[Vn] = pi[Qn] = pi[Jn] = pi[Zn] = pi[ei] = pi[ti] = pi[jn] = pi[Hn] = pi[zn] = pi[qn] = pi[Yn] = pi[Kn] = pi[Wn] = pi[ni] = pi[ii] = pi[ri] = pi[ai] = !0, pi[Un] = pi[Gn] = pi[Xn] = !1; var hi = { "&": "&amp;", "<": "&lt;", ">": "&gt;", '"': "&quot;", "'": "&#39;", "`": "&#96;" },
                gi = { "function": !0, object: !0 },
                vi = gi[typeof t] && t && !t.nodeType ? t : An,
                mi = gi[typeof e] && e && !e.nodeType ? e : An,
                _i = mi && mi.exports === vi ? vi : An,
                Ei = m(vi && mi && "object" == typeof n && n),
                yi = m(gi[typeof self] && self),
                Ii = m(gi[typeof window] && window),
                Ti = m(gi[typeof this] && this),
                Si = Ei || Ii !== (Ti && Ti.window) && Ii || yi || Ti || Function("return this")(),
                Ai = Array.prototype,
                bi = Object.prototype,
                wi = Function.prototype.toString,
                Di = bi.hasOwnProperty,
                Ri = 0,
                Ni = wi.call(Object),
                Ci = bi.toString,
                Oi = Si._,
                xi = RegExp("^" + wi.call(Di).replace(ci, "\\$&").replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, "$1.*?") + "$"),
                Li = _i ? An : An,
                Pi = Si.Reflect,
                ki = Si.Symbol,
                Fi = Si.Uint8Array,
                Mi = Pi ? Pi.enumerate : An,
                Vi = Object.getPrototypeOf,
                Ui = Object.getOwnPropertySymbols,
                Gi = Object.create,
                Bi = bi.propertyIsEnumerable,
                ji = Ai.splice,
                Hi = Si.isFinite,
                zi = Object.keys,
                qi = Math.max,
                Yi = Ke(Si, "Map"),
                Ki = Ke(Si, "Set"),
                Wi = Ke(Si, "WeakMap"),
                Xi = Ke(Object, "create"),
                $i = Yi ? wi.call(Yi) : "",
                Qi = Ki ? wi.call(Ki) : "",
                Ji = Wi ? wi.call(Wi) : "",
                Zi = ki ? ki.prototype : An,
                er = Zi ? Zi.valueOf : An,
                tr = Ge(fe),
                nr = Be();
            Mi && !Bi.call({ valueOf: 1 }, "valueOf") && (_e = function(e) { return S(Mi(e)) }); var ir = Me,
                rr = be("length"),
                ar = Ui || function() { return [] };
            (Yi && We(new Yi) != jn || Ki && We(new Ki) != Yn || Wi && We(new Wi) != Xn) && (We = function(e) { var t = Ci.call(e),
                    n = t == zn ? e.constructor : null,
                    i = "function" == typeof n ? wi.call(n) : ""; if (i) switch (i) {
                    case $i:
                        return jn;
                    case Qi:
                        return Yn;
                    case Ji:
                        return Xn }
                return t }); var or = Dt((function(e, t) { return dr(e) || (e = null == e ? [] : [Object(e)]), t = le(t, 1), a(e, t) })),
                sr = Dt((function(e, t, n) { return He(e, Nn | Cn, t, n) })),
                cr = Dt((function(e, t) { return oe(e, 1, t) })),
                ur = Dt((function(e, t, n) { return oe(e, hr(t) || 0, n) })),
                lr = Dt((function(e, t) { return He(e, Cn, An, t) })),
                dr = Array.isArray,
                fr = Li ? function(e) { return e instanceof Li } : gn(!1),
                pr = Number,
                hr = Number,
                gr = Ue((function(e, t) { ir(t, sn(t), e) })),
                vr = Ue((function(e, t) { ir(t, cn(t), e) })),
                mr = Ue((function(e, t, n, i) { Me(t, cn(t), e, i) })),
                _r = Dt((function(e) { return e.push(An, Z), mr.apply(An, e) })),
                Er = Ue((function(e, t, n) { Ie(e, t, n) })),
                yr = Dt((function(e, t) { return null == e ? {} : (t = Ee(le(t, 1), String), Se(e, se(cn(e), t))) })),
                Ir = Dt((function(e, t) { return null == e ? {} : Se(e, le(t, 1)) })),
                Tr = ve;
            D.prototype = ae(w.prototype), D.prototype.constructor = D, R.prototype = Xi ? Xi(null) : bi, L.prototype.clear = P, L.prototype["delete"] = k, L.prototype.get = F, L.prototype.has = M, L.prototype.set = V, U.prototype.push = B, j.prototype.clear = H, j.prototype["delete"] = z, j.prototype.get = q, j.prototype.has = Y, j.prototype.set = K, w.assign = gr, w.assignIn = vr, w.before = At, w.bind = sr, w.chain = lt, w.compact = nt, w.concat = or, w.create = rn, w.defaults = _r, w.defer = cr, w.delay = ur, w.filter = gt, w.flatten = rt, w.flattenDeep = at, w.iteratee = Tr, w.keys = sn, w.map = Et, w.mapValues = un, w.matches = mn, w.merge = Er, w.mixin = _n, w.negate = bt, w.omit = yr, w.omitBy = ln, w.once = wt, w.partial = lr, w.pick = Ir, w.pickBy = dn, w.slice = ut, w.sortBy = St, w.tap = dt, w.thru = ft, w.toArray = en, w.values = pn, w.extend = vr, _n(w, w), w.clone = Rt, w.cloneDeep = Nt, w.escape = hn, w.every = ht, w.find = vt, w.findIndex = it, w.forEach = mt, w.forOwn = an, w.has = on, w.head = ot, w.identity = vn, w.includes = _t, w.indexOf = st, w.isArguments = xt, w.isArray = dr, w.isBoolean = kt, w.isDate = Ft, w.isEmpty = Mt, w.isEqual = Vt, w.isFinite = Ut, w.isFunction = Gt, w.isNaN = zt, w.isNull = Yt, w.isNumber = Kt, w.isObject = jt, w.isRegExp = Xt, w.isString = $t, w.isUndefined = Jt, w.last = ct, w.max = Tn, w.min = Sn, w.noConflict = En, w.noop = yn, w.reduce = yt, w.result = fn, w.size = It, w.some = Tt, w.uniqueId = In, w.each = mt, w.first = ot, _n(w, (function() { var e = {}; return fe(w, (function(t, n) { Di.call(w.prototype, n) || (e[n] = t) })), e })(), { chain: !1 }), w.VERSION = bn, tr(["pop", "join", "replace", "reverse", "split", "push", "shift", "sort", "splice", "unshift"], (function(e) { var t = (/^(?:replace|split)$/.test(e) ? String.prototype : Ai)[e],
                    n = /^(?:push|sort|unshift)$/.test(e) ? "tap" : "thru",
                    i = /^(?:pop|join|replace|shift)$/.test(e);
                w.prototype[e] = function() { var e = arguments; return i && !this.l ? t.apply(this.value(), e) : this[n]((function(n) { return t.apply(n, e) })) } })), w.prototype.toJSON = w.prototype.valueOf = w.prototype.value = pt, (Ii || yi || {})._ = w, vi && mi && (_i && ((mi.exports = w)._ = w), vi._ = w) }).call(this) }).call(t, n(4)(e), (function() { return this })()) }), (function(e, t) { e.exports = function(e) { return e.webpackPolyfill || (e.deprecate = function() {}, e.paths = [], e.children = [], e.webpackPolyfill = 1), e } }), (function(e, t) { t.generate = function e(t) { return t ? (t ^ 16 * Math.random() >> t / 4).toString(16) : ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, e) } }), (function(e, t, n) { var i = n(7),
        r = n(9),
        a = n(12).Promise,
        o = n(16),
        s = o.get("stores/async_request");
    t.makeAsyncRequest = function(e, t) { var n = s.getPromise(e); if (n) return n; var o, c, u = new a(function(e, t) { o = e, c = t }); return r.dispatch(i.REGISTER_ASYNC_DEFERRED, { source: e, promise: u, resolver: o, rejecter: c }), t && t(), u }, t.resolveRequest = function(e, t) { r.dispatch(i.RESOLVE_DEFERRED, { source: e, resolveWith: t }) }, t.rejectRequest = function(e, t) { r.dispatch(i.REJECT_DEFERRED, { source: e, rejectWith: t }) } }), (function(e, t, n) {
    var i = n(8);
    e.exports = i({
        LOG: null,
        SET_LOGLEVEL: null,
        INITIALIZE_STATE: null,
        SET_DOMCONTENTLOADED: null,
        ACTIVATE: null,
        UPDATE_BEHAVIOR_STORE: null,
        DATA_LOADED: null,
        SET_CLIENT_NAME: null,
        SET_CLIENT_VERSION: null,
        LOAD_PERSISTED_LAYER_STATES: null,
        RECORD_GLOBAL_DECISION: null,
        RECORD_LAYER_DECISION: null,
        ENSURE_ORIGINAL_PUSHSTATE: null,
        ENSURE_ORIGINAL_REPLACESTATE: null,
        SET_VISITOR_ATTRIBUTES: null,
        SET_VISITOR_ATTRIBUTE_PENDING: null,
        LOAD_EXISTING_VISITOR_PROFILE: null,
        SET_VISITOR_EVENTS: null,
        SET_FOREIGN_VISITOR_EVENTS: null,
        SET_FOREIGN_VISITOR_EVENT_QUEUE: null,
        SET_VISITOR_ID: null,
        SET_VISITOR_ID_VIA_API: null,
        REFRESH_SESSION: null,
        LOAD_SESSION_STATE: null,
        UPDATE_VARIATION_ID_MAP: null,
        MERGE_VARIATION_ID_MAP: null,
        UPDATE_PREFERRED_LAYER_MAP: null,
        MERGE_PREFERRED_LAYER_MAP: null,
        RECORD_LAYER_DECISION_EVENT_ID: null,
        TRACK_VIEW_ACTIVATED_EVENT: null,
        REGISTER_ASYNC_DEFERRED: null,
        RESOLVE_DEFERRED: null,
        REJECT_DEFERRED: null,
        REGISTER_PLUGIN: null,
        ADD_CLEANUP_FN: null,
        CLEAR_CLEANUP_FN: null,
        ACTION_EXECUTED: null,
        REGISTER_ACTION: null,
        SET_VIEW_ACTIVE_STATE: null,
        UPDATE_PARSED_VIEW_METADATA: null,
        UPDATE_USER_SUPPLIED_METADATA: null,
        REGISTER_VIEWS: null,
        SET_GLOBAL_TAGS: null,
        SET_VIEW_BATCHING: null,
        ATTACH_EVENT_STREAM_PUBLISHERS: null,
        DETACH_EVENT_STREAM_PUBLISHERS: null,
        LOAD_DIRECTIVE: null,
        SET_COOKIE_AGE: null,
        SET_COOKIE_DOMAIN: null,
        SET_COOKIE_AUTO_REFRESH: null,
        XDOMAIN_SET_DEFAULT_FRAME: null,
        XDOMAIN_ADD_FRAME: null,
        XDOMAIN_SET_MESSAGE: null,
        XDOMAIN_ADD_SUBSCRIBER: null,
        XDOMAIN_SET_CANONICAL_ORIGINS: null,
        XDOMAIN_SET_DISABLED: null,
        ADD_EMITTER_HANDLER: null,
        REMOVE_EMITTER_HANDLER: null,
        SET_INTEGRATION_SETTINGS: null,
        ADD_CHANGE: null,
        SET_CHANGE_APPLIER: null,
        REMOVE_ACTION_STATE: null,
        ANNOUNCE_PENDING_REDIRECT: null,
        LOAD_REDIRECT_DATA: null,
        REGISTER_TRACKED_REDIRECT_DATA: null,
        SET_PENDING_EVENT: null,
        REMOVE_PENDING_EVENT: null,
        LOAD_PENDING_EVENTS: null,
        SANDBOXED_FUNCTIONS_ADDED: null,
        SET_RUM_DATA: null,
        RECORD_API_USAGE: null,
        INITIALIZE_CHANGE_METRICS: null,
        RECORD_ACTIVATION_TYPE_USAGE: null,
        RECORD_AUDIENCE_USAGE: null,
        RECORD_CHANGE_MACROTASK_RATE: null,
        RECORD_CHANGE_OVERHEATED: null,
        RECORD_CHANGE_TYPE_USAGE: null,
        RECORD_DOM_OBSERVATION_OCCURENCE: null,
        RECORD_INTEGRATION_USAGE: null,
        RECORD_LAYER_FEATURE_USAGE: null,
        RECORD_LAYER_POLICY_USAGE: null,
        RECORD_RECOMMENDATIONS_USAGE: null,
        RECORD_VIEW_FEATURE_USAGE: null,
        SET_PERFORMANCE_MARKS_DATA: null,
        FINALIZE_BATCH_SNAPSHOT: null,
        REGISTER_PREVIOUS_BATCH: null,
        REGISTER_TRACKER_VISITOR: null,
        REGISTER_TRACKER_EVENT: null,
        REGISTER_TRACKER_DECISION: null,
        RESET_TRACKER_EVENTS: null,
        RESET_TRACKER_PREVIOUS_BATCHES: null,
        RESET_TRACKER_STORE: null,
        SET_TRACKER_POLLING: null,
        SET_TRACKER_BATCHING: null,
        SET_TRACKER_SEND_EVENTS: null,
        SET_TRACKER_PERSISTABLE_STATE: null,
        SET_TRACKER_DIRTY: null,
        UPDATE_TRACKER_VISITOR_ATTRIBUTES: null,
        SET_UA_DATA: null
    })
}), (function(e, t) { "use strict"; var n = function(e) { var t, n = {}; if (!(e instanceof Object) || Array.isArray(e)) throw new Error("keyMirror(...): Argument must be an object."); for (t in e) e.hasOwnProperty(t) && (n[t] = t); return n };
    e.exports = n }), (function(e, t, n) { var i = n(10);
    e.exports = i.create() }), (function(e, t, n) {
    function i(e) { e = e || {}, this.f = {}, this.g = {}, this.I = 0, this.T = [], this.S = [] }

    function r(e, t) { return function() { var n = e.indexOf(t);
            n !== -1 && e.splice(n, 1) } } var a = n(2),
        o = n(11);
    i.prototype.registerStores = function(e) { a.forOwn(e, a.bind((function(e, t) { this.f[t] = new o(t, this, e) }), this)) }, i.prototype.getStore = function(e) { return this.f[e] }, i.prototype.dispatch = function(e, t) { this.dispatchId++, a.each(this.T, a.bind((function(n) { n.call(this, e, t) }), this)), a.forOwn(this.f, (function(n) { n.A(e, t) })), a.each(this.S, a.bind((function(n) { n.call(this, e, t) }), this)), a.forOwn(this.f, a.bind((function(e, t) { e.hasChanges() && this.g[t] && (e.resetChange(), a.each(this.g[t], (function(t) { t(e) }))) }), this)) }, i.prototype.reset = function() { this.g = {}, a.forOwn(this.f, (function(e, t) { e.b() })) }, i.prototype.getState = function() { var e = {}; return a.forOwn(this.f, (function(t, n) { e[n] = t.w() })), e }, i.prototype.onPreAction = function(e) { var t = this.T; return t.push(e), r(t, e) }, i.prototype.onPostAction = function(e) { var t = this.S; return t.push(e), r(t, e) }, i.prototype.D = function(e, t) { this.g[e] || (this.g[e] = []), this.g[e].push(t); var n = this.g[e]; return r(n, t) }, e.exports = { create: function(e) { return new i(e) } } }), (function(e, t, n) {
    function i(e, t, n) { this.R = e, this.N = t, this.C = 0, this.O = !1, this.L = {}, r.extend(this, n), this.P = {}, this.initialize && this.initialize() } var r = n(2);
    i.prototype.A = function(e, t) { var n = this.L[e];
        n && "function" == typeof n && n.call(this, t, e) }, i.prototype.w = function() { return r.cloneDeep(this.P) }, i.prototype.on = function(e, t) { this.L[e] = r.bind(t, this) }, i.prototype.observe = function(e) { return this.N.D(this.R, e) }, i.prototype.emitChange = function() { this.O = !0, this.C++ }, i.prototype.hasChanges = function() { return this.O }, i.prototype.resetChange = function() { this.O = !1 }, i.prototype.getStateId = function() { return this.C }, i.prototype.b = function() { this.reset && "function" == typeof this.reset && this.reset(), this.initialize() }, e.exports = i }), (function(e, t, n) { e.exports = n(13) }), (function(e, t, n) {
    (function(t, i) {
        /*!
         * @overview es6-promise - a tiny implementation of Promises/A+.
         * @copyright Copyright (c) 2014 Yehuda Katz, Tom Dale, Stefan Penner and contributors (Conversion to ES6 API by Jake Archibald)
         * @license   Licensed under MIT license
         *            See https://raw.githubusercontent.com/stefanpenner/es6-promise/master/LICENSE
         * @version   4.1.0
         */
        !(function(t, n) { e.exports = n() })(this, (function() { "use strict";

            function e(e) { return "function" == typeof e || "object" == typeof e && null !== e }

            function r(e) { return "function" == typeof e }

            function a(e) { X = e }

            function o(e) { $ = e }

            function s() { return function() { return t.nextTick(f) } }

            function c() { return "undefined" != typeof W ? function() { W(f) } : d() }

            function u() { var e = 0,
                    t = new Z(f),
                    n = document.createTextNode(""); return t.observe(n, { characterData: !0 }),
                    function() { n.data = e = ++e % 2 } }

            function l() { var e = new MessageChannel; return e.port1.onmessage = f,
                    function() { return e.port2.postMessage(0) } }

            function d() { var e = setTimeout; return function() { return e(f, 1) } }

            function f() { for (var e = 0; e < K; e += 2) { var t = ne[e],
                        n = ne[e + 1];
                    t(n), ne[e] = void 0, ne[e + 1] = void 0 }
                K = 0 }

            function p() { try { var e = n(15); return W = e.runOnLoop || e.runOnContext, c() } catch (e) { return d() } }

            function h(e, t) { var n = arguments,
                    i = this,
                    r = new this.constructor(v);
                void 0 === r[re] && k(r); var a = i._state; return a ? !(function() { var e = n[a - 1];
                    $((function() { return x(a, r, e, i._result) })) })() : R(i, r, e, t), r }

            function g(e) { var t = this; if (e && "object" == typeof e && e.constructor === t) return e; var n = new t(v); return A(n, e), n }

            function v() {}

            function m() { return new TypeError("You cannot resolve a promise with itself") }

            function _() { return new TypeError("A promises callback cannot return that same promise.") }

            function E(e) { try { return e.then } catch (e) { return ce.error = e, ce } }

            function y(e, t, n, i) { try { e.call(t, n, i) } catch (e) { return e } }

            function I(e, t, n) { $((function(e) { var i = !1,
                        r = y(n, t, (function(n) { i || (i = !0, t !== n ? A(e, n) : w(e, n)) }), (function(t) { i || (i = !0, D(e, t)) }), "Settle: " + (e._label || " unknown promise"));!i && r && (i = !0, D(e, r)) }), e) }

            function T(e, t) { t._state === oe ? w(e, t._result) : t._state === se ? D(e, t._result) : R(t, void 0, (function(t) { return A(e, t) }), (function(t) { return D(e, t) })) }

            function S(e, t, n) { t.constructor === e.constructor && n === h && t.constructor.resolve === g ? T(e, t) : n === ce ? (D(e, ce.error), ce.error = null) : void 0 === n ? w(e, t) : r(n) ? I(e, t, n) : w(e, t) }

            function A(t, n) { t === n ? D(t, m()) : e(n) ? S(t, n, E(n)) : w(t, n) }

            function b(e) { e._onerror && e._onerror(e._result), N(e) }

            function w(e, t) { e._state === ae && (e._result = t, e._state = oe, 0 !== e._subscribers.length && $(N, e)) }

            function D(e, t) { e._state === ae && (e._state = se, e._result = t, $(b, e)) }

            function R(e, t, n, i) { var r = e._subscribers,
                    a = r.length;
                e._onerror = null, r[a] = t, r[a + oe] = n, r[a + se] = i, 0 === a && e._state && $(N, e) }

            function N(e) { var t = e._subscribers,
                    n = e._state; if (0 !== t.length) { for (var i = void 0, r = void 0, a = e._result, o = 0; o < t.length; o += 3) i = t[o], r = t[o + n], i ? x(n, i, r, a) : r(a);
                    e._subscribers.length = 0 } }

            function C() { this.error = null }

            function O(e, t) { try { return e(t) } catch (e) { return ue.error = e, ue } }

            function x(e, t, n, i) { var a = r(n),
                    o = void 0,
                    s = void 0,
                    c = void 0,
                    u = void 0; if (a) { if (o = O(n, i), o === ue ? (u = !0, s = o.error, o.error = null) : c = !0, t === o) return void D(t, _()) } else o = i, c = !0;
                t._state !== ae || (a && c ? A(t, o) : u ? D(t, s) : e === oe ? w(t, o) : e === se && D(t, o)) }

            function L(e, t) { try { t((function(t) { A(e, t) }), (function(t) { D(e, t) })) } catch (t) { D(e, t) } }

            function P() { return le++ }

            function k(e) { e[re] = le++, e._state = void 0, e._result = void 0, e._subscribers = [] }

            function F(e, t) { this._instanceConstructor = e, this.promise = new e(v), this.promise[re] || k(this.promise), Y(t) ? (this._input = t, this.length = t.length, this._remaining = t.length, this._result = new Array(this.length), 0 === this.length ? w(this.promise, this._result) : (this.length = this.length || 0, this._enumerate(), 0 === this._remaining && w(this.promise, this._result))) : D(this.promise, M()) }

            function M() { return new Error("Array Methods must be provided an Array") }

            function V(e) { return new F(this, e).promise }

            function U(e) { var t = this; return new t(Y(e) ? function(n, i) { for (var r = e.length, a = 0; a < r; a++) t.resolve(e[a]).then(n, i) } : function(e, t) { return t(new TypeError("You must pass an array to race.")) }) }

            function G(e) { var t = this,
                    n = new t(v); return D(n, e), n }

            function B() { throw new TypeError("You must pass a resolver function as the first argument to the promise constructor") }

            function j() { throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.") }

            function H(e) { this[re] = P(), this._result = this._state = void 0, this._subscribers = [], v !== e && ("function" != typeof e && B(), this instanceof H ? L(this, e) : j()) }

            function z() { var e = void 0; if ("undefined" != typeof i) e = i;
                else if ("undefined" != typeof self) e = self;
                else try { e = Function("return this")() } catch (e) { throw new Error("polyfill failed because global object is unavailable in this environment") }
                var t = e.Promise; if (t) { var n = null; try { n = Object.prototype.toString.call(t.resolve()) } catch (e) {} if ("[object Promise]" === n && !t.cast) return }
                e.Promise = H } var q = void 0;
            q = Array.isArray ? Array.isArray : function(e) { return "[object Array]" === Object.prototype.toString.call(e) }; var Y = q,
                K = 0,
                W = void 0,
                X = void 0,
                $ = function(e, t) { ne[K] = e, ne[K + 1] = t, K += 2, 2 === K && (X ? X(f) : ie()) },
                Q = "undefined" != typeof window ? window : void 0,
                J = Q || {},
                Z = J.MutationObserver || J.WebKitMutationObserver,
                ee = "undefined" == typeof self && "undefined" != typeof t && "[object process]" === {}.toString.call(t),
                te = "undefined" != typeof Uint8ClampedArray && "undefined" != typeof importScripts && "undefined" != typeof MessageChannel,
                ne = new Array(1e3),
                ie = void 0;
            ie = ee ? s() : Z ? u() : te ? l() : void 0 === Q ? p() : d(); var re = Math.random().toString(36).substring(16),
                ae = void 0,
                oe = 1,
                se = 2,
                ce = new C,
                ue = new C,
                le = 0; return F.prototype._enumerate = function() { for (var e = this.length, t = this._input, n = 0; this._state === ae && n < e; n++) this._eachEntry(t[n], n) }, F.prototype._eachEntry = function(e, t) { var n = this._instanceConstructor,
                    i = n.resolve; if (i === g) { var r = E(e); if (r === h && e._state !== ae) this._settledAt(e._state, t, e._result);
                    else if ("function" != typeof r) this._remaining--, this._result[t] = e;
                    else if (n === H) { var a = new n(v);
                        S(a, e, r), this._willSettleAt(a, t) } else this._willSettleAt(new n(function(t) { return t(e) }), t) } else this._willSettleAt(i(e), t) }, F.prototype._settledAt = function(e, t, n) { var i = this.promise;
                i._state === ae && (this._remaining--, e === se ? D(i, n) : this._result[t] = n), 0 === this._remaining && w(i, this._result) }, F.prototype._willSettleAt = function(e, t) { var n = this;
                R(e, void 0, (function(e) { return n._settledAt(oe, t, e) }), (function(e) { return n._settledAt(se, t, e) })) }, H.all = V, H.race = U, H.resolve = g, H.reject = G, H._setScheduler = a, H._setAsap = o, H._asap = $, H.prototype = { constructor: H, then: h, "catch": function(e) { return this.then(null, e) } }, H.polyfill = z, H.Promise = H, H }))
    }).call(t, n(14), (function() { return this })())
}), (function(e, t) {
    function n() { throw new Error("setTimeout has not been defined") }

    function i() { throw new Error("clearTimeout has not been defined") }

    function r(e) { if (l === setTimeout) return setTimeout(e, 0); if ((l === n || !l) && setTimeout) return l = setTimeout, setTimeout(e, 0); try { return l(e, 0) } catch (t) { try { return l.call(null, e, 0) } catch (t) { return l.call(this, e, 0) } } }

    function a(e) { if (d === clearTimeout) return clearTimeout(e); if ((d === i || !d) && clearTimeout) return d = clearTimeout, clearTimeout(e); try { return d(e) } catch (t) { try { return d.call(null, e) } catch (t) { return d.call(this, e) } } }

    function o() { g && p && (g = !1, p.length ? h = p.concat(h) : v = -1, h.length && s()) }

    function s() { if (!g) { var e = r(o);
            g = !0; for (var t = h.length; t;) { for (p = h, h = []; ++v < t;) p && p[v].run();
                v = -1, t = h.length }
            p = null, g = !1, a(e) } }

    function c(e, t) { this.fun = e, this.array = t }

    function u() {} var l, d, f = e.exports = {};!(function() { try { l = "function" == typeof setTimeout ? setTimeout : n } catch (e) { l = n } try { d = "function" == typeof clearTimeout ? clearTimeout : i } catch (e) { d = i } })(); var p, h = [],
        g = !1,
        v = -1;
    f.nextTick = function(e) { var t = new Array(arguments.length - 1); if (arguments.length > 1)
            for (var n = 1; n < arguments.length; n++) t[n - 1] = arguments[n];
        h.push(new c(e, t)), 1 !== h.length || g || r(s) }, c.prototype.run = function() { this.fun.apply(null, this.array) }, f.title = "browser", f.browser = !0, f.env = {}, f.argv = [], f.version = "", f.versions = {}, f.on = u, f.addListener = u, f.once = u, f.off = u, f.removeListener = u, f.removeAllListeners = u, f.emit = u, f.prependListener = u, f.prependOnceListener = u, f.listeners = function(e) { return [] }, f.binding = function(e) { throw new Error("process.binding is not supported") }, f.cwd = function() { return "/" }, f.chdir = function(e) { throw new Error("process.chdir is not supported") }, f.umask = function() { return 0 } }), (function(e, t) {}), (function(e, t, n) { var i = n(2),
        r = n(17),
        a = n(9),
        o = n(18),
        s = r.create(),
        c = { action_data: n(21), async_request: n(27), audience_data: n(28), change_data: n(29), cleanup: n(30), client_metadata: n(31), cookie_options: n(33), event_data: n(34), event_emitter: n(35), dimension_data: n(36), directive: n(37), global: n(38), global_state: n(39), history: n(40), integration_settings: n(42), layer: n(43), layer_data: n(44), log: n(46), observed_redirect: n(47), pending_events: n(48), performance: n(49), plugins: n(50), provider_status: n(51), pending_redirect: n(52), rum: n(53), sandbox: n(54), session: n(55), tracker_optimizely: n(56), ua_data: n(57), view: n(58), view_data: n(59), visitor: n(60), visitor_attribute_entity: n(61), visitor_events: n(62), visitor_events_manager: n(67), visitor_id: n(68), visitor_bucketing: n(69), xdomain: n(70) };
    c["group_data"] = n(71), a.registerStores(c), i.forOwn(c, (function(e, t) { s.register("stores/" + t, a.getStore(t)) })), s.register("core/plugins/matchers/key_value", o), e.exports = s }), (function(e, t, n) {
    function i() { this.k = {} } var r = n(2);
    i.prototype.register = function(e, t) { if (1 === arguments.length) { var n = this; return void r.each(e, (function(e, t) { n.register(t, e) })) } if (this.k[e]) throw new Error("Module already registered for: " + e);
        this.k[e] = t }, i.prototype.get = function(e) { return this.k[e] }, i.prototype.getModuleKeys = function() { var e = this.k; return r.keys(e) }, i.prototype.evaluate = function(e) { var t = e.length,
            n = e.slice(0, t - 1),
            i = e[t - 1]; if ("function" != typeof i) throw new Error("Evaluate must take a function as last element in array"); var a = r.map(n, r.bind(this.get, this)); return i.apply(null, a) }, i.prototype.reset = function() { this.k = {} }, e.exports = { create: function() { return new i } } }), (function(e, t, n) { var i = n(2),
        r = n(19).getFieldValue,
        a = n(20);
    e.exports = function(e, t) { var n = r(e, t.name.split(".")); return i.isArray(n) ? i.some(n, i.partial(a.hasMatch, t.value, t.match)) : a.hasMatch(t.value, t.match, n) } }), (function(e, t, n) { var i = n(2);
    t.getFieldValue = function(e, t) { i.isArray(t) || (t = [t]); for (var n = e, r = 0; r < t.length; r++) { var a = t[r]; if (!i.isObject(n) || !n.hasOwnProperty(a)) return;
            n = n[a] } return n }, t.setFieldValue = function(e, t, n) { if (!i.isArray(t) || i.isEmpty(t)) throw new Error("Attempted to set an invalid key path: " + t); for (var r = e, a = 0; a < t.length - 1; a++) { var o = t[a];
            i.isObject(r[o]) || (r[o] = {}), r = r[o] }
        r[t[t.length - 1]] = n } }), (function(e, t, n) { var i = n(2);
    t.hasMatch = function(e, t, n) { var r = !i.isUndefined(n) && null !== n,
            a = !i.isUndefined(e) && null !== e,
            o = t || (a ? "exact" : "exists"); switch (o) {
            case "exists":
                return r;
            case "exact":
                return r && String(n) === e;
            case "substring":
                return r && String(n).indexOf(e) > -1;
            case "regex":
                try { if (a && r) { var s = new RegExp(e); return s.test(String(n)) } return !1 } catch (e) {} return !1;
            case "range":
                var c = e.split(":"),
                    u = parseFloat(c[0]),
                    l = parseFloat(c[1]),
                    d = parseFloat(n); return d >= u && d <= l;
            default:
                return !1 } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(22),
        o = n(23);
    e.exports = { initialize: function() { this.P = { actions: {}, actionState: {} }, this.on(r.DATA_LOADED, this.F), this.on(r.ACTION_EXECUTED, this.M), this.on(r.SET_CHANGE_APPLIER, this.V), this.on(r.REMOVE_ACTION_STATE, this.U) }, F: function(e) { var t = this;
            i.isEmpty(e.data.layers) || (i.each(e.data.layers, (function(e) { var n; if (e.changes) { var r = "layerId:" + e.id;
                    n = { id: r, layerId: e.id, changeSet: e.changes, type: "layer" }, a.deepFreeze(n), t.P.actions[r] = n }
                i.each(e.experiments, (function(r) { if (r.changes) { var o = "experimentId:" + r.id;
                        n = { id: o, layerId: e.id, experimentId: r.id, changeSet: r.changes, type: "experiment" }, a.deepFreeze(n), t.P.actions[o] = n }
                    i.each(r.variations, (function(o) { i.each(o.actions, (function(i) { var s = i.pageId || i.viewId,
                                c = r.id + ":" + o.id + ":" + s;
                            n = { id: c, layerId: e.id, experimentId: r.id, variationId: o.id, pageId: s, changeSet: i.changes, type: "variation" }, a.deepFreeze(n), t.P.actions[c] = n })) })) })) })), this.emitChange()) }, M: function(e) { var t = e.actionId;
            i.isUndefined(t) || this.P.actionState[t] || (this.P.actionState[t] = {}) }, V: function(e) { var t = e.actionId,
                n = e.changeId; return this.P.actionState[t] ? void(this.P.actionState[t][n] = e.changeApplier) : void o.warn("Action Data / Attempted to set changeApplier for inactive action: ", t) }, U: function(e) { delete this.P.actionState[e.actionId] }, get: function(e) { return a.safeReference(this.P.actions[e]) }, getActionState: function(e) { return a.safeReference(this.P.actionState[e]) }, getByChangeId: function(e) { return i.find(this.P.actions, { changeSet: [{ id: e }] }) }, getAllActionIdsByPageId: function(e) { return i.map(i.filter(this.P.actions, { pageId: e }), "id") }, getChangeApplier: function(e, t) { var n = this.P.actionState[t]; if (n) return n[e] }, getExperimentVariationActions: function(e, t) { return a.safeReference(i.filter(this.P.actions, { experimentId: e, variationId: t })) }, getLayerActions: function(e) { return a.safeReference(i.filter(this.P.actions, { id: "layerId:" + e })) }, getExperimentActions: function(e) { return a.safeReference(i.filter(this.P.actions, { id: "experimentId:" + e })) }, getAll: function() { return a.safeReference(i.values(this.P.actions)) } } }), (function(e, t, n) { var i = n(2),
        r = !1;
    t.deepFreeze = function e(t) { r && i.isObject(t) && !i.isFunction(t) && (i.forOwn(t, e), Object.freeze(t)) }, t.safeReference = function e(t) { return r ? !i.isObject(t) || i.isFunction(t) || Object.isFrozen(t) ? t : i.isArray(t) ? i.map(t, e) : i.reduce(t, (function(t, n, i) { return t[i] = e(n), t }), {}) : i.cloneDeep(t) } }), (function(e, t, n) {
    function i() { this.logLevel = null, this.logMatch = null, this.logs = [], this.timebase = o.now() } var r = n(2),
        a = n(7),
        o = n(24),
        s = n(25),
        c = n(9),
        u = n(26);
    i.prototype.G = function() { return !r.isNull(this.logLevel) }, i.prototype.setLogLevel = function(e) { var t = this.B(e);
        null === t ? console.error("Unknown log level: " + e) : this.logLevel !== t && (this.log("Setting log level to " + t), this.logLevel = t, this.flush()) }, i.prototype.setLogMatcher = function(e) { r.isString(e) ? this.logMatcher = e : this.logMatcher = "", this.logGroup = 0 }, i.prototype.shouldLog = function(e) { return this.G() && this.logLevel >= e }, i.prototype.matchesLogMessage = function(e, t) { var n = this.logMatcher; if (!this.logMatcher) return !0; if (this.logGroup) return "GROUPSTART" === e ? this.logGroup++ : "GROUPEND" === e && this.logGroup--, !0; var i = r.some(t, (function(e) { if (!r.isString(e)) try { e = u.stringify(e) } catch (e) {}
            return r.isString(e) && r.includes(e, n) })); return i && "GROUPSTART" === e && this.logGroup++, i }, i.prototype.storeLog = function(e, t) { var n = { logLevel: e, logMessage: t };
        c.dispatch(a.LOG, n) }, i.prototype.flush = function() { var e = n(16),
            t = e.get("stores/log");
        this.logGroup = 0; var i = t.getLogs();
        r.each(i, r.bind((function(e) { this.j(e.logLevel, e.logMessage, !0) }), this)) }, i.prototype.j = function(e, t, n) { var i, a = e; if (console) switch (e) {
            case "GROUPSTART":
                i = console.groupCollapsed, a = s.LogLevel.DEBUG; break;
            case "GROUPEND":
                i = console.groupEnd, a = s.LogLevel.DEBUG; break;
            case s.LogLevel.ERROR:
                i = console.error; break;
            case s.LogLevel.WARN:
                i = console.warn; break;
            case s.LogLevel.DEBUG:
                i = console.debug; break;
            default:
                i = console.log }
        try { n || this.G() && !this.shouldLog(a) || (r.isArray(t) && r.isString(t[0]) && (t = this.H(t)), this.storeLog(e, t)), i && this.shouldLog(a) && this.matchesLogMessage(e, t) && i.apply(console, t) } catch (e) { console && (console.error ? console.error(e) : console.log(e)) } }, i.prototype.debug = function() { this.j(s.LogLevel.DEBUG, [].slice.call(arguments)) }, i.prototype.log = function() { this.j(s.LogLevel.INFO, [].slice.call(arguments)) }, i.prototype.logAlways = function() { var e = this.H([].slice.call(arguments));
        console && console.log && console.log.apply && console.log.apply(console, e), this.storeLog(s.LogLevel.INFO, e) }, i.prototype.warn = function() { this.j(s.LogLevel.WARN, [].slice.call(arguments)) }, i.prototype.error = function(e) { var t = [].slice.call(arguments);
        1 === t.length && e.stack ? (this.j(s.LogLevel.ERROR, [this.z(), e]), this.j(s.LogLevel.INFO, [e.stack])) : this.j(s.LogLevel.ERROR, t) }, i.prototype.groupCollapsed = function() { this.j("GROUPSTART", [].slice.call(arguments)) }, i.prototype.groupEnd = function() { this.j("GROUPEND", [].slice.call(arguments)) }, i.prototype.H = function(e) { var t = this.z().toString(); return t.length < 6 && (t = ("     " + t).slice(-6)), [t + "| Optly / " + e[0]].concat(e.slice(1)) }, i.prototype.z = function() { return this.timebase ? o.now() - this.timebase : 0 }, i.prototype.B = function(e) { return e && (e = e.toUpperCase(), "TRUE" === e && (e = "INFO"), "FALSE" === e && (e = "OFF"), "ALL" === e && (e = "DEBUG"), !r.isUndefined(s.LogLevel[e])) ? s.LogLevel[e] : null }, e.exports = new i }), (function(e, t) { t.now = function() { return +new Date } }), (function(e, t, n) { var i = n(2),
        r = n(8);
    t.COOKIES = { OPT_OUT: "optimizelyOptOut", PREVIEW: "optimizelyPreview", REDIRECT: "optimizelyRedirectData", SESSION_STATE: "optimizelySessionState", TOKEN: "optimizelyToken", VISITOR_ID: "optimizelyEndUserId", VISITOR_UUID: "optimizelyPPID" }, t.LayerActivationTypes = { CONDITIONAL: "conditional", IMMEDIATE: "immediate", MANUAL: "manual", READY: "ready", TIMEOUT: "timeout" }, t.LogLevel = { OFF: 0, ERROR: 1, WARN: 2, INFO: 3, DEBUG: 4 }, t.Lifecycle = r({ preActivate: null, postVisitorProfileLoad: null, postViewsActivated: null, postActivate: null }), t.ViewActivationTypes = { immediate: "immediate", manual: "manual", callback: "callback", polling: "polling", URLChanged: "url_changed", DOMChanged: "dom_changed" }, t.StorageKeys = { PENDING_EVENTS: "pending_events", RELAYED_EVENTS: "relayed_events" }, t.PluginTypes = r({ visitorProfileProviders: null, viewProviders: null, audienceMatchers: null, viewMatchers: null, analyticsTrackers: null, viewTagLocators: null, userFeatureDefs: null, apiModules: null, changeAppliers: null, deciders: null, eventImplementations: null, viewTriggers: null }), t.ResourceTimingAttributes = r({ connectStart: null, connectEnd: null, decodedBodySize: null, domainLookupStart: null, domainLookupEnd: null, duration: null, encodedBodySize: null, fetchStart: null, requestStart: null, responseStart: null, responseEnd: null, secureConnectionStart: null, startTime: null, transferSize: null, serverTiming: null }), t.RUMPerformanceTimingAttributes = r({ blockTime: null }), t.AttributionTypes = r({ FIRST_TOUCH: null, LAST_TOUCH: null }), t.SandboxedFunctions = r({ XMLHttpRequest: null }), t.PerformanceData = r({ performance_marks: null, resource_timing: null, performance_timing: null }), t.PerformanceCounters = r({ mutation_observer_invocation: null, polling_invocation: null, match_selector_invocation: null }), t.VisitorStorageKeys = { EVENTS: "events", EVENT_QUEUE: "event_queue", LAYER_MAP: "layer_map", LAYER_STATES: "layer_states", SESSION_STATE: "session_state", VISITOR_PROFILE: "visitor_profile", VARIATION_MAP: "variation_map", TRACKER_OPTIMIZELY: "tracker_optimizely" }, t.AllStorageKeys = i.assign({}, t.StorageKeys, t.VisitorStorageKeys), t.ListTargetingKeyTypes = { COOKIE: "c", QUERY: "q", JS_VARIABLE: "j" } }), (function(e, t, n) {
    function i(e) { var t = [Array.prototype],
            n = [];
        r.each(t, (function(e) { r.isUndefined(e.toJSON) || (n.push(e.toJSON), delete e.toJSON) })); var i, a; try { i = e() } catch (e) { a = e } finally { r.each(n, (function(e, n) { t[n].toJSON = e })) } if (a) throw a; return i } var r = n(2);
    t.stringify = function() { return i(r.bind((function() { return JSON.stringify.apply(null, this) }), arguments)) }, t.parse = JSON.parse }), (function(e, t, n) { var i = n(7);
    e.exports = { initialize: function() { this.P = {}, this.on(i.REGISTER_ASYNC_DEFERRED, this.q), this.on(i.RESOLVE_DEFERRED, this.Y), this.on(i.REJECT_DEFERRED, this.K) }, getRequest: function(e) { return this.P[e] }, getPromise: function(e) { var t = this.getRequest(e); if (t) return t.promise }, q: function(e) { this.P[e.source] = { promise: e.promise, resolver: e.resolver, rejecter: e.rejecter } }, Y: function(e) { var t = this.getRequest(e.source); if (!t) throw new Error("No request registered for source: " + e.source);
            t.resolver(e.resolveWith) }, K: function(e) { var t = this.getRequest(e.source); if (!t) throw new Error("No request registered for source: " + e.source); if (!t.rejecter) throw new Error("No rejecter registered for source: " + e.source);
            t.rejecter(e.rejectWith) } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(22);
    e.exports = { initialize: function() { this.P = {}, this.on(r.DATA_LOADED, this.F) }, F: function(e) { i.isEmpty(e.data.audiences) || (i.each(e.data.audiences, i.bind((function(e) { a.deepFreeze(e), this.P[e.id] = e }), this)), this.emitChange()) }, getAll: function() { return a.safeReference(i.values(this.P)) }, getAudiencesMap: function() { return a.safeReference(this.P) }, get: function(e) { return a.safeReference(this.P[e]) }, getAudienceName: function(e) { var t = i.find(i.values(this.P), { id: e }); return t.name || "Aud " + e } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(22);
    e.exports = { initialize: function() { this.P = {}, this.on(r.ADD_CHANGE, this.W), this.on(r.DATA_LOADED, this.F) }, getChange: function(e) { return this.P[e] }, F: function(e) { i.isEmpty(e.data.changes) || i.each(e.data.changes, i.bind(this.W, this)) }, W: function(e) { a.deepFreeze(e), this.P[e.id] = e, this.emitChange() } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(25);
    e.exports = { initialize: function() { this.P = {}, i.each(a.Lifecycle, i.bind((function(e) { this.P[e] = [] }), this)), this.on(r.ADD_CLEANUP_FN, this.X), this.on(r.CLEAR_CLEANUP_FN, this.Q) }, getCleanupFns: function(e) { return i.cloneDeep(this.P[e]) }, X: function(e) { this.P[e.lifecycle].push(e.cleanupFn), this.emitChange() }, Q: function(e) { var t = this.P[e.lifecycle]; if (e.cleanupFn) { var n = t.indexOf(e.cleanupFn);
                n > -1 && (t.splice(n, 1), this.emitChange()) } else this.P[e.lifecycle] = [], this.emitChange() } } }), (function(e, t, n) { var i = n(7),
        r = n(32);
    e.exports = { initialize: function() { this.P = { name: r.NAME, version: r.VERSION }, this.on(i.SET_CLIENT_NAME, this.J), this.on(i.SET_CLIENT_VERSION, this.Z) }, getClientName: function() { return this.P.name }, getClientVersion: function() { return this.P.version }, J: function(e) { e && (this.P.name = e), this.emitChange() }, Z: function(e) { e && (this.P.version = e), this.emitChange() } } }), (function(e, t, n) { t.VERSION = "0.135.0", t.NAME = "js" }), (function(e, t, n) { var i = n(7),
        r = 15552e3,
        a = !0;
    e.exports = { initialize: function() { this.P = { currentDomain: null, defaultAgeSeconds: r, autoRefresh: a }, this.on(i.SET_COOKIE_DOMAIN, this.ee), this.on(i.SET_COOKIE_AGE, this.te), this.on(i.SET_COOKIE_AUTO_REFRESH, this.ne) }, getCurrentDomain: function() { return this.P.currentDomain }, getDefaultAgeInSeconds: function() { return this.P.defaultAgeSeconds }, getAutoRefresh: function() { return this.P.autoRefresh }, ee: function(e) { this.P.currentDomain = e, this.emitChange() }, te: function(e) { this.P.defaultAgeSeconds = e, this.emitChange() }, ne: function(e) { this.P.autoRefresh = e, this.emitChange() } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(22);
    e.exports = { initialize: function() { this.P = {}, this.on(r.DATA_LOADED, this.F) }, getAll: function() { return a.safeReference(i.values(this.P)) }, getEventsMap: function() { return a.safeReference(this.P) }, get: function(e) { return a.safeReference(this.P[e]) }, getByApiName: function(e) { return a.safeReference(i.find(i.values(this.P), { apiName: e })) }, getByPageId: function(e) { return a.safeReference(i.filter(this.P, { pageId: e })) }, F: function(e) { i.isEmpty(e.data.events) || (i.each(e.data.events, i.bind((function(e) { e.pageId || (e.pageId = e.viewId), a.deepFreeze(e), this.P[e.id] = e }), this)), this.emitChange()) } } }), (function(e, t, n) {
    function i(e) { var t = []; return e && r.isObject(e) ? (e.type && t.push(e.type), t.push(o), e.type && e.name && t.push(e.name), t.join("")) : o } var r = n(2),
        a = n(7),
        o = "|";
    e.exports = { initialize: function() { this.P = { handlers: {} }, this.on(a.ADD_EMITTER_HANDLER, this.re), this.on(a.REMOVE_EMITTER_HANDLER, this.ae) }, getHandlers: function(e, t) { var n = [null, { type: e.type }, { type: e.type, name: e.name }],
                a = []; return r.each(n, r.bind((function(e) { var t = i(e),
                    n = this.P.handlers[t];
                n && (a = a.concat(n)) }), this)), t && (a = r.filter(a, (function(e) { return !e.publicOnly }))), a }, re: function(e) { var t = i(e.filter);
            this.P.handlers[t] || (this.P.handlers[t] = []), this.P.handlers[t].push({ handler: e.handler, token: e.token, publicOnly: !!e.publicOnly, emitErrors: !!e.emitErrors }), this.emitChange() }, ae: function(e) { var t = !1,
                n = e.token;
            r.forOwn(this.P.handlers, r.bind((function(e, i) { var a = r.filter(e, (function(e) { return e.token !== n }));
                a.length !== e.length && (t = !0, this.P.handlers[i] = a) }), this)), t && this.emitChange() } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(22);
    e.exports = { initialize: function() { this.P = {}, this.on(r.DATA_LOADED, this.F) }, F: function(e) { i.isEmpty(e.data.dimensions) || (i.each(e.data.dimensions, i.bind((function(e) { a.deepFreeze(e), this.P[e.id] = e }), this)), this.emitChange()) }, getAll: function() { return a.safeReference(i.values(this.P)) }, getById: function(e) { return a.safeReference(this.P[e]) }, getByApiName: function(e) { return a.safeReference(i.find(i.values(this.P), { apiName: e })) } } }), (function(e, t, n) { var i = n(2),
        r = n(7);
    e.exports = { initialize: function() { this.P = { disabled: !1, forceAudienceIds: [], forceVariationIds: [], alreadyInitialized: !1, mutationObserverAPISupported: !1, isEditor: !1, isPreview: !1, isLegacyPreview: !1, isSlave: !1, previewLayerIds: [], projectToken: null, shouldOptOut: !1, trackingDisabled: !1, isRunningInV2Editor: !1, isRunningInDesktopApp: !1, forceTracking: !1 }, this.on(r.LOAD_DIRECTIVE, this.oe) }, getAll: function() { return i.cloneDeep(this.P) }, conflictInObservingChanges: function() { return !1 }, isDisabled: function() { return this.P.disabled }, isEditor: function() { return this.P.isEditor }, clientHasAlreadyInitialized: function() { return this.P.alreadyInitialized }, getForceAudienceIds: function() { return this.P.forceAudienceIds }, getForceVariationIds: function() { return this.P.forceVariationIds }, getPreviewLayerIds: function() { return this.P.previewLayerIds }, getProjectToken: function() { return this.P.projectToken }, getForceTracking: function() { return this.P.forceTracking }, shouldActivate: function() { return !this.P.isEditor && !this.isDisabled() }, shouldBootstrapDataForPreview: function() { return this.P.isPreview }, shouldBootstrapDataForEditor: function() { return this.P.isEditor }, shouldInitialize: function() { return !(this.shouldLoadPreview() || this.isDisabled() || this.getProjectToken()) }, shouldLoadPreview: function() { return !(this.P.isPreview || this.P.isLegacyPreview || !this.getProjectToken() || this.P.isEditor) }, shouldBailForDesktopApp: function() { return !this.P.isEditor && this.P.isRunningInDesktopApp }, shouldLoadInnie: function() { return !this.P.isSlave && !this.P.isEditor && this.P.isRunningInV2Editor }, shouldObserveChangesIndefinitely: function() { return !1 }, shouldObserveChangesUntilTimeout: function() { return !this.shouldObserveChangesIndefinitely() }, shouldOptOut: function() { return this.P.shouldOptOut }, shouldSendTrackingData: function() { return !this.P.trackingDisabled && (!!this.P.forceTracking || !this.P.isPreview && i.isEmpty(this.getForceVariationIds()) && i.isEmpty(this.getForceAudienceIds())) }, isSlave: function() { return this.P.isSlave }, isRunningInDesktopApp: function() { return this.P.isRunningInDesktopApp }, isRunningInV2Editor: function() { return this.P.isRunningInV2Editor }, oe: function(e) { i.extend(this.P, e), this.emitChange() } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(22);
    e.exports = { initialize: function() { this.P = { holdback: 0, isGlobalHoldback: null, listTargetingKeys: [], revision: null, projectId: null, accountId: null, namespace: null, activationId: null, activationTimestamp: null, dcpServiceId: null, dcpKeyfieldLocators: [], recommenderServices: [], anonymizeIP: null, projectJS: null, snippetId: null, plugins: [], domContentLoaded: !1, experimental: {} }, this.on(r.DATA_LOADED, this.se), this.on(r.ACTIVATE, this.ce), this.on(r.RECORD_GLOBAL_DECISION, this.ue), this.on(r.SET_DOMCONTENTLOADED, this.le) }, getRevision: function() { return this.P.revision }, getGlobalHoldbackThreshold: function() { return this.P.holdback }, getProjectId: function() { return this.P.projectId }, getSnippetId: function() { return this.P.snippetId }, getAccountId: function() { return this.P.accountId }, getNamespace: function() { return this.P.namespace }, getActivationId: function() { return this.P.activationId }, getActivationTimestamp: function() { return this.P.activationTimestamp }, getAnonymizeIP: function() { return this.P.anonymizeIP }, isGlobalHoldback: function() { return !!this.P.isGlobalHoldback }, getListTargetingKeys: function() { return this.P.listTargetingKeys.slice() }, getDCPServiceId: function() { return this.P.dcpServiceId }, getDCPKeyfieldLocators: function() { return this.P.dcpKeyfieldLocators }, getRecommenderServices: function() { return this.P.recommenderServices }, getProjectJS: function() { return this.P.projectJS }, getPlugins: function() { return this.P.plugins }, getExperimental: function() { return a.safeReference(this.P.experimental) }, domContentLoadedHasFired: function() { return this.P.domContentLoaded }, ce: function(e) { this.P.activationId = e.activationId, this.P.activationTimestamp = e.activationTimestamp, this.P.isGlobalHoldback = null }, ue: function(e) { var t = e.isGlobalHoldback; if (null !== this.P.isGlobalHoldback && this.P.isGlobalHoldback !== t) throw new Error("Attempted to change already set global holdback!");
            this.P.isGlobalHoldback = t, this.emitChange() }, se: function(e) { var t = i.pick(e.data, ["holdback", "accountId", "projectId", "snippetId", "namespace", "revision", "listTargetingKeys", "dcpServiceId", "dcpKeyfieldLocators", "recommenderServices", "anonymizeIP", "plugins", "projectJS", "experimental"]); if (0 !== i.keys(t).length) { var n = { listTargetingKeys: [], dcpServiceId: null, dcpKeyfieldLocators: [] };
                i.extend(this.P, n, t), this.emitChange() } }, le: function() { this.P.domContentLoaded = !0, this.emitChange() } } }), (function(e, t, n) { var i = n(2),
        r = n(7);
    e.exports = { initialize: function() { this.P = { effectiveReferrer: null, effectiveVariationId: null }, this.on(r.INITIALIZE_STATE, this.de) }, getEffectiveReferrer: function() { return this.P.effectiveReferrer }, getEffectiveVariationId: function() { return this.P.effectiveVariationId }, de: function(e) { i.isUndefined(e.effectiveReferrer) || (this.P.effectiveReferrer = e.effectiveReferrer), i.isUndefined(e.effectiveVariationId) || (this.P.effectiveVariationId = e.effectiveVariationId), this.emitChange() } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(41);
    e.exports = { initialize: function() { this.P = { originalPushState: null, originalReplaceState: null }, this.on(r.ENSURE_ORIGINAL_PUSHSTATE, this.fe), this.on(r.ENSURE_ORIGINAL_REPLACESTATE, this.pe) }, getOriginalPushState: function() { return this.P.originalPushState }, getOriginalReplaceState: function() { return this.P.originalReplaceState }, fe: function() { this.P.originalPushState || (this.P.originalPushState = i.bind(a.getGlobal("history").pushState, a.getGlobal("history"))) }, pe: function() { this.P.originalReplaceState || (this.P.originalReplaceState = i.bind(a.getGlobal("history").replaceState, a.getGlobal("history"))) } } }), (function(e, t, n) { var i = n(2),
        r = n(23);
    t.getUserAgent = function() { return window.navigator.userAgent }, t.getLocationSearch = function() { return window.location.search }, t.getNavigatorLanguage = function() { return window.navigator.language || window.navigator.userLanguage }, t.getHref = function() { return window.location.href }, t.getLocation = function() { return window.location }, t.setLocation = function(e) { window.location.replace(e) }, t.setGlobal = function(e, t) { window[e] = t }, t.getGlobal = function(e) { return window[e] }, t.addEventListener = function() { return window.addEventListener.apply(window, arguments) }, t.removeEventListener = function() { return window.removeEventListener.apply(window, arguments) }, t.isMutationObserverAPISupported = function() { return !i.isUndefined(window.MutationObserver) }, t.alert = function(e) { alert(e) }, t.setTimeout = function(e, t) { return setTimeout((function() { try { e() } catch (e) { r.warn("Deferred function threw error:", e) } }), t) }, t.setInterval = function(e, t) { return setInterval((function() { try { e() } catch (e) { r.warn("Polling function threw error:", e) } }), t) } }), (function(e, t, n) { var i = n(2),
        r = n(7);
    e.exports = { initialize: function() { this.P = {}, this.on(r.DATA_LOADED, this.F), this.on(r.SET_INTEGRATION_SETTINGS, this.he) }, F: function(e) { i.isEmpty(e.data.integrationSettings) || (i.each(e.data.integrationSettings, i.bind((function(e) { this.P[e.id] = e }), this)), this.emitChange()) }, he: function(e) { var t = this.P[e.id];
            t ? i.extend(t, e) : this.P[e.id] = e }, getAll: function() { return i.cloneDeep(i.values(this.P)) }, get: function(e) { return i.cloneDeep(this.P[e]) }, getReference: function(e) { return this.P[e] } } }), (function(e, t, n) {
    var i = n(2),
        r = n(7),
        a = n(23),
        o = "*";
    e.exports = {
        initialize: function() { this.P = {}, this.on(r.LOAD_PERSISTED_LAYER_STATES, this.ge), this.on(r.RECORD_LAYER_DECISION, this.ve), this.on(r.RECORD_LAYER_DECISION_EVENT_ID, this.me) },
        getLayerState: function(e, t) { if (this.P[e]) { var n = this.P[e]; if (i.keys(n).length > 1 && !t) throw new Error("View Id must be specified when more than one layerState for layer."); return t ? i.cloneDeep(i.find(n, { pageId: t })) : i.cloneDeep(n[o]) } },
        getLayerStates: function(e) { var t = []; for (var n in this.P) i.forEach(this.P[n], (function(n) {
                (i.isUndefined(e) || n.namespace === e) && t.push(i.cloneDeep(n)) })); return t },
        getLayerStatesForAnalytics: function() { var e = []; for (var t in this.P) i.forEach(this.P[t], (function(t) { e.push(i.pick(t, ["layerId", "decision", "decisionEventId"])) })); return e },
        ge: function(e) {
            e.merge || (this.P = {}), i.each(e.layerStates, i.bind((function(e) { var t = e.layerId;
                    e.pageId || (e.pageId = e.viewId); var n = e.pageId || o,
                        r = this.P[t]; if (i.isUndefined(r)) this.P[t] = {}, this.P[t][n] = e;
                    else { var a = r[n];
                        (!a || e.decisionTimestamp > (a.decisionTimestamp || 0)) && (this.P[t][n] = e) } }), this)),
                this.emitChange()
        },
        ve: function(e) { var t = { layerId: e.layerId, revision: e.revision, namespace: e.namespace, pageId: e.pageId, decisionTicket: e.decisionTicket, decision: e.decision, decisionActivationId: e.activationId, decisionTimestamp: e.timestamp, decisionEventId: null },
                n = this.P[e.layerId] || {};
            e.pageId ? (delete n[o], n[e.pageId] = t) : (n = {}, n[o] = t), this.P[e.layerId] = n, this.emitChange() },
        me: function(e) { var t = e.layerId,
                n = e.pageId || o; return this.P[t] ? this.P[t][n] ? (this.P[t][n].decisionEventId = e.decisionId, void this.emitChange()) : void a.warn("Not recording decision event: Layer state not found for view", n) : void a.warn("Not recording decision event: Campaign not registered", t) }
    }
}), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(22),
        o = n(45);
    e.exports = { initialize: function() { this.P = { layers: {}, experiments: {}, variations: {} }, this.on(r.DATA_LOADED, this.F) }, F: function(e) { if (!i.isEmpty(e.data.layers)) { var t = this;
                i.each(e.data.layers, (function(e) { i.each(e.experiments, (function(n) { e.pageIds || (e.pageIds = e.viewIds), n.campaignName || o.isSingleExperimentPolicy(e.policy) ? o.isSingleExperimentPolicy(e.policy) && e.groupId && (n.groupId = e.groupId) : n.campaignName = e.name, i.each(n.variations, (function(e) { i.each(e.actions, (function(e) { e.pageId || (e.pageId = e.viewId) })), t.P.variations[e.id] = e })), t.P.experiments[n.id] = n })), a.deepFreeze(e), t.P.layers[e.id] = e })), this.emitChange() } }, getAll: function() { return a.safeReference(i.values(this.P.layers)) }, getCampaignsMap: function() { return a.safeReference(this.P.layers) }, getExperimentsMap: function() { return a.safeReference(this.P.experiments) }, getVariationsMap: function() { return a.safeReference(this.P.variations) }, getCount: function() { return i.keys(this.P.layers).length }, getAllByPageIds: function(e) { return a.safeReference(i.filter(this.P.layers, (function(t) { return i.some(e, i.partial(i.includes, t.pageIds)) }))) }, get: function(e) { return a.safeReference(this.P.layers[e]) }, getLayerByExperimentId: function(e) { var t = i.find(this.P.layers, (function(t) { return i.find(t.experiments, { id: e }) })); return a.safeReference(t) }, getExperimentByVariationId: function(e) { var t; return i.some(this.P.layers, (function(n) { return i.some(n.experiments, (function(n) { return i.find(n.variations, { id: e }) && (t = n), t })), t })), a.safeReference(t) } } }), (function(e, t) { var n = "single_experiment",
        i = "multivariate";
    t.isSingleExperimentPolicy = function(e) { return e === n || e === i } }), (function(e, t, n) { var i = n(7);
    e.exports = { initialize: function() { this.P = { logs: [] }, this.on(i.LOG, this._e) }, getLogs: function() { return this.P.logs }, _e: function(e) { this.P.logs.push(e), this.emitChange() }, w: function() { return this.P.logs.slice() } } }), (function(e, t, n) { var i = n(7),
        r = n(22);
    e.exports = { initialize: function() { this.P = { data: null, hasTracked: null }, this.on(i.LOAD_REDIRECT_DATA, this.Ee), this.on(i.REGISTER_TRACKED_REDIRECT_DATA, this.ye) }, get: function() { return r.safeReference(this.P.data) }, hasTracked: function() { return this.P.hasTracked }, Ee: function(e) { r.deepFreeze(e), this.P.data = e, this.P.hasTracked = !1, this.emitChange() }, ye: function() { this.P.hasTracked = !0 } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(26),
        o = 1e3;
    e.exports = { initialize: function() { this.P = {}, this.on(r.SET_PENDING_EVENT, this.Ie), this.on(r.REMOVE_PENDING_EVENT, this.Te), this.on(r.LOAD_PENDING_EVENTS, this.Se) }, getEvents: function() { return this.P }, getEventsString: function() { return a.stringify(this.P) }, Ie: function(e) { i.keys(this.P).length >= o && this.Ae(); var t = e.id,
                n = e.retryCount;
            this.P[t] && this.P[t].retryCount === n || (this.P[t] = { id: t, timeStamp: e.timeStamp, data: e.data, retryCount: n }, this.emitChange()) }, Te: function(e) { delete this.P[e.id], this.emitChange() }, Se: function(e) { this.P = e.events, this.Ae(), this.emitChange() }, Ae: function() { for (var e = i.sortBy(this.P, "timeStamp"), t = 0; t <= e.length - o; t++) delete this.P[e[t].id];
            this.emitChange() } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(25);
    e.exports = { initialize: function() { this.P = {}, this.P[a.PerformanceData.performance_marks] = {}, this.on(r.SET_PERFORMANCE_MARKS_DATA, this.be) }, be: function(e) { i.isUndefined(this.P[a.PerformanceData.performance_marks][e.name]) && (this.P[a.PerformanceData.performance_marks][e.name] = []), this.P[a.PerformanceData.performance_marks][e.name].push(e.data), this.emitChange() }, getMarks: function() { return i.mapValues(this.P[a.PerformanceData.performance_marks], (function(e) { return i.map(e, (function(e) { return [e.startTime, e.duration] })) })) }, getDurationsFor: function(e) { return i.reduce(e, i.bind((function(e, t) { var n = this.P[a.PerformanceData.performance_marks][t]; return n && (e[t] = Math.round(i.reduce(n, (function(e, t) { return e + t.duration }), 0))), e }), this), {}) } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(25),
        o = n(23);
    e.exports = { initialize: function() { this.P = i.mapValues(a.PluginTypes, (function() { return {} })), this.on(r.REGISTER_PLUGIN, this.we) }, we: function(e) { var t = e.type,
                n = e.name,
                i = e.plugin; if (!t || !n) throw new Error("Missing information needed to register plugins: " + t + ":" + n); if (!this.P[t]) throw new Error("Invalid plugin type specified: " + t);
            this.P[t][n] = i, o.debug("Plugin Store: Registering Plugin :", e) }, getAllPlugins: function(e) { if (e) { if (this.P[e]) return this.P[e]; throw new Error("Invalid plugin type: " + e) } return this.P }, getPlugin: function(e, t) { if (!t || !e) throw new Error("Missing plugin parameters"); var n = this.getAllPlugins(e); return n[t] || null } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(19);
    e.exports = { initialize: function() { this.P = {}, this.on(r.SET_VISITOR_ATTRIBUTE_PENDING, this.De) }, getPendingAttributeValue: function(e) { return e = i.isArray(e) ? e.concat("pending") : [e, "pending"], a.getFieldValue(this.P, e) }, De: function(e) { a.setFieldValue(this.P, e.key, { pending: e.pending }), this.emitChange() } } }), (function(e, t, n) { var i = n(2),
        r = n(7);
    e.exports = { initialize: function() { this.P = { layerId: null }, this.on(r.ANNOUNCE_PENDING_REDIRECT, this.Ee) }, isExpectingRedirect: function() { return i.isString(this.P.layerId) }, getLayerId: function() { return this.P.layerId }, Ee: function(e) { this.isExpectingRedirect() || (this.P.layerId = e.layerId, this.emitChange()) } } }), (function(e, t, n) { var i = n(2),
        r = n(7);
    e.exports = { initialize: function() { this.P = { inRumSample: !1, id: null, src: null, RumHost: null, data: { extras: {} }, apis: {}, DOMObservation: {}, featuresNeeded: {} }, this.on(r.SET_RUM_DATA, this.Re), this.on(r.RECORD_API_USAGE, this.Ne), this.on(r.INITIALIZE_CHANGE_METRICS, this.Ce), this.on(r.RECORD_ACTIVATION_TYPE_USAGE, this.Oe), this.on(r.RECORD_AUDIENCE_USAGE, this.xe), this.on(r.RECORD_CHANGE_MACROTASK_RATE, this.Le), this.on(r.RECORD_CHANGE_OVERHEATED, this.Pe), this.on(r.RECORD_CHANGE_TYPE_USAGE, this.ke), this.on(r.RECORD_DOM_OBSERVATION_OCCURENCE, this.Fe), this.on(r.RECORD_INTEGRATION_USAGE, this.Me), this.on(r.RECORD_LAYER_FEATURE_USAGE, this.Ve), this.on(r.RECORD_LAYER_POLICY_USAGE, this.Ue), this.on(r.RECORD_VIEW_FEATURE_USAGE, this.Ge) }, Re: function(e) { i.merge(this.P, e), this.emitChange() }, Ne: function(e) { this.P.apis[e.methodName] || (this.P.apis[e.methodName] = 0), this.P.apis[e.methodName]++, this.emitChange() }, Ce: function() { i.isUndefined(this.P.data.extras.changeMacrotaskRate) && (this.P.data.extras.changeMacrotaskRate = 0), i.isUndefined(this.P.data.extras.numOverheatedChanges) && (this.P.data.extras.numOverheatedChanges = 0) }, Le: function(e) { i.isUndefined(this.P.data.extras.changeMacrotaskRate) && (this.P.data.extras.changeMacrotaskRate = 0), e.changeMacrotaskRate > this.P.data.extras.changeMacrotaskRate && (this.P.data.extras.changeMacrotaskRate = e.changeMacrotaskRate), this.emitChange() }, Pe: function() { i.isUndefined(this.P.data.extras.numOverheatedChanges) && (this.P.data.extras.numOverheatedChanges = 0), this.P.data.extras.numOverheatedChanges++, this.emitChange() }, Fe: function(e) { this.P.DOMObservation[e.counterName] || (this.P.DOMObservation[e.counterName] = 0), this.P.DOMObservation[e.counterName]++, this.emitChange() }, Be: function(e, t, n) { i.isUndefined(this.P.featuresNeeded[e]) && (this.P.featuresNeeded[e] = {}); var r = this.P.featuresNeeded[e];
            i.each(t, (function(e) { r[e] || (r[e] = {}), r[e][n] || (r[e][n] = !0) })) }, Me: function(e) { this.Be("integrations", e.integrations, e.layerId) }, ke: function(e) { this.Be("changeTypes", e.changeTypes, e.layerId) }, Oe: function(e) { this.Be("activationTypes", [e.activationType], e.entityId), this.emitChange() }, Ge: function(e) { this.Be("viewFeatures", e.featuresUsed, e.entityId), this.emitChange() }, Ve: function(e) { this.Be("layerFeatures", [e.feature], e.entityId), this.emitChange() }, Ue: function(e) { this.Be("policy", [e.policy], e.layerId), this.emitChange() }, xe: function(e) { this.Be("audiences", e.audienceTypes, e.layerId), this.emitChange() }, getSampleRum: function() { return this.P.inRumSample }, getRumId: function() { return this.P.id }, getRumHost: function() { return this.P.RumHost }, getApiData: function() { return this.P.apis }, getDOMObservationData: function() { return this.P.DOMObservation }, getRumData: function() { return i.cloneDeep(this.P.data) }, getScriptSrc: function() { return this.P.src }, getFeaturesNeededData: function() { var e = this.P.featuresNeeded,
                t = {}; return i.forOwn(e, (function(e, n) { var r = i.keys(e);
                i.isEmpty(r) || (t[n] = {}), i.forEach(r, (function(r) { t[n][r] = i.keys(e[r]).length })) })), t } } }), (function(e, t, n) { var i = n(7);
    e.exports = { initialize: function() { this.P = { initialized: !1, natives: {} }, this.on(i.SANDBOXED_FUNCTIONS_ADDED, this.je) }, je: function(e) { if (!e.sandboxedFunctions) throw new Error("No sandboxedFunctions found in payload");
            this.P.natives = e.sandboxedFunctions, this.P.initialized = !0, this.emitChange() }, getAll: function() { return this.P.natives }, get: function(e) { if (!e) throw new Error("Missing name parameter"); return this.P.natives[e] || null }, isInitialized: function() { return this.P.initialized } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(24),
        o = n(5),
        s = 18e5;
    e.exports = { initialize: function() { this.P = { lastSessionTimestamp: 0, sessionId: null }, this.on(r.REFRESH_SESSION, this.He), this.on(r.LOAD_SESSION_STATE, this.ze) }, getState: function() { return i.cloneDeep(this.P) }, getSessionId: function() { return this.P.sessionId }, ze: function(e) { this.P.sessionId = e.sessionId, this.P.lastSessionTimestamp = e.lastSessionTimestamp, this.emitChange() }, He: function() { var e = a.now(),
                t = this.P.lastSessionTimestamp;
            (!this.P.sessionId || e - t > s) && (this.P.sessionId = o.generate()), this.P.lastSessionTimestamp = e, this.emitChange() } } }), (function(e, t, n) { var i = n(2),
        r = n(7);
    e.exports = { initialize: function() { this.qe(), this.on(r.FINALIZE_BATCH_SNAPSHOT, this.Ye), this.on(r.REGISTER_PREVIOUS_BATCH, this.Ke), this.on(r.REGISTER_TRACKER_VISITOR, this.We), this.on(r.REGISTER_TRACKER_EVENT, this.Xe), this.on(r.REGISTER_TRACKER_DECISION, this.$e), this.on(r.RESET_TRACKER_EVENTS, this.Qe), this.on(r.RESET_TRACKER_STORE, this.qe), this.on(r.RESET_TRACKER_PREVIOUS_BATCHES, this.Je), this.on(r.SET_TRACKER_POLLING, this.Ze), this.on(r.SET_TRACKER_BATCHING, this.et), this.on(r.SET_TRACKER_SEND_EVENTS, this.tt), this.on(r.SET_TRACKER_PERSISTABLE_STATE, this.nt), this.on(r.SET_TRACKER_DIRTY, this.it), this.on(r.UPDATE_TRACKER_VISITOR_ATTRIBUTES, this.rt) }, getPersistableState: function() { return this.P.isDirty ? this.hasEventsToSend() || this.hasPreviousBatchesToSend() ? { data: this.P.data, decisions: this.P.decisions, decisionEvents: this.P.decisionEvents, previousBatches: this.P.previousBatches } : {} : null }, nt: function(e) { i.isEmpty(this.P.data) || i.isEmpty(e.data) || (this.Ye(), this.P.previousBatches.push(this.getEventBatch())), this.P.data = e.data || {}, this.P.decisions = e.decisions || [], this.P.decisionEvents = e.decisionEvents || [], i.isEmpty(this.P.previousBatches) || i.isEmpty(e.previousBatches) ? this.P.previousBatches = e.previousBatches || [] : this.P.previousBatches = this.P.previousBatches.concat(e.previousBatches), this.emitChange() }, it: function(e) { this.P.isDirty = e, this.emitChange() }, Xe: function(e) { var t = this.at();!i.isEmpty(t.snapshots) && i.isEmpty(this.P.decisionEvents) || this.ot(), this.st().events.push(e.event), this.P.decisions = e.decisions, this.it(!0) }, $e: function(e) { this.P.decisionEvents.push(e.decisionEvent), this.P.decisions = e.decisions, this.it(!0) }, We: function(e) { i.isEmpty(this.P.data) ? this.P.data = e.data : this.Ye(), this.P.data.visitors.push(e.visitor), this.P.decisions = e.decisions, this.P.decisionEvents = [], this.it(!0) }, Ke: function(e) { this.P.previousBatches.push(e), this.it(!0) }, qe: function() { this.P = { polling: !1, shouldBatch: !0, data: {}, decisions: [], decisionEvents: [], canSend: !1, isDirty: !1, previousBatches: [] }, this.emitChange() }, Qe: function() { var e = this.at();
            this.P.data.visitors = [e], e.snapshots = [], this.it(!0) }, Je: function() { this.P.previousBatches = [], this.it(!0) }, Ze: function(e) { this.P.polling = e, this.emitChange() }, et: function(e) { this.P.shouldBatch = e, this.emitChange() }, tt: function(e) { this.P.canSend = e, this.emitChange() }, getEventBatch: function() { return i.cloneDeep(this.P.data) }, getPreviousBatches: function() { return i.cloneDeep(this.P.previousBatches) }, ct: function() { return this.P.decisionEvents.slice() }, ut: function() { this.P.decisionEvents = [] }, dt: function() { return this.P.decisions.slice() }, isPolling: function() { return this.P.polling }, shouldBatch: function() { return this.P.shouldBatch }, st: function() { return i.last(this.at().snapshots) }, at: function() { return i.last(this.P.data.visitors) }, ot: function() { var e = this.ct(),
                t = this.at();
            t.snapshots.push({ decisions: this.dt(), events: e }), this.ut(), this.it(!0) }, Ye: function() { this.P.decisionEvents.length > 0 && this.ot() }, hasEventsToSend: function() { if (!i.isEmpty(this.P.decisionEvents)) return !0; if (!i.isEmpty(this.P.data)) { var e = i.some(this.P.data.visitors || [], (function(e) { return e.snapshots.length > 0 })); if (e) return !0 } return !1 }, hasPreviousBatchesToSend: function() { return !i.isEmpty(this.P.previousBatches) }, canSend: function() { return this.P.canSend }, rt: function(e) { var t = this.at();
            t && (t.attributes = e.attributes) } } }), (function(e, t, n) { var i = n(2),
        r = n(7);
    e.exports = { initialize: function() { this.P = {}, this.on(r.SET_UA_DATA, this.F) }, F: function(e) { i.isEmpty(this.P) && (this.P = e.data) }, get: function() { return i.cloneDeep(this.P) } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(23),
        o = { globalTags: {}, viewStates: {}, shouldBatch: !1 };
    e.exports = { initialize: function() { this.P = i.cloneDeep(o), this.on(r.REGISTER_VIEWS, this.ft), this.on(r.SET_VIEW_ACTIVE_STATE, this.pt), this.on(r.UPDATE_PARSED_VIEW_METADATA, this.ht), this.on(r.UPDATE_USER_SUPPLIED_METADATA, this.vt), this.on(r.TRACK_VIEW_ACTIVATED_EVENT, this.mt), this.on(r.SET_GLOBAL_TAGS, this._t), this.on(r.ACTIVATE, this.Et), this.on(r.SET_VIEW_BATCHING, this.et) }, getAll: function() { var e = {}; for (var t in this.P.viewStates) e[t] = this.getViewState(t); return e }, shouldBatch: function() { return this.P.shouldBatch }, getViewState: function(e) { var t = i.cloneDeep(this.P.viewStates[e]),
                n = this.P.globalTags; return t.metadata = i.extend({}, t.parsedMetadata, n, t.userSuppliedMetadata), t }, getActiveViewTags: function() { var e = this.getActiveViewStates(),
                t = i.map(e, (function(e) { return e.metadata })),
                n = [{}].concat(t); return i.extend.apply(i, n) }, getActivationEventId: function(e) { return this.P.viewStates[e] ? this.P.viewStates[e].activationEventId : null }, getActiveViewStates: function() { return i.reduce(this.P.viewStates, i.bind((function(e, t, n) { return this.isViewActive(n) && e.push(this.getViewState(n)), e }), this), []) }, isViewActive: function(e) { var t = this.P.viewStates[e]; return t || a.warn("No Page registered with id", e), !!t.isActive }, getGlobalTags: function() { return i.cloneDeep(this.P.globalTags) }, Et: function() { this.P.viewStates = {}, this.emitChange() }, ft: function(e) { i.each(e.views, i.bind((function(e) { var t = e.id;
                this.P.viewStates[t] = { id: t, isActive: !1, activatedTimestamp: null, activationEventId: null, parsedMetadata: {}, userSuppliedMetadata: {} } }), this)), this.emitChange() }, pt: function(e) { var t = e.view.id; if (!this.P.viewStates[t]) throw new Error("No view exists with id " + t);
            this.P.viewStates[t].isActive = e.isActive, e.isActive ? this.P.viewStates[t].activatedTimestamp = e.timestamp : (this.P.viewStates[t].parsedMetadata = {}, this.P.viewStates[t].userSuppliedMetadata = {}), this.emitChange() }, ht: function(e) { var t = e.pageId; if (!this.P.viewStates[t]) throw new Error("No view exists with id " + t);
            i.assign(this.P.viewStates[t].parsedMetadata, e.metadata), this.emitChange() }, vt: function(e) { var t = e.pageId; if (!this.P.viewStates[t]) throw new Error("No view exists with id " + t);
            i.assign(this.P.viewStates[t].userSuppliedMetadata, e.metadata), this.emitChange() }, mt: function(e) { var t = e.pageId;
            this.P.viewStates[t] && (this.P.viewStates[t].activationEventId = e.eventData.eventId, this.emitChange()) }, _t: function(e) { i.extend(this.P.globalTags, e), this.emitChange() }, et: function(e) { this.P.shouldBatch = e, this.emitChange() } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(22);
    e.exports = { initialize: function() { this.P = { views: {}, apiNamesToViews: {} }, this.on(r.DATA_LOADED, this.F) }, getAll: function() { return a.safeReference(i.values(this.P.views)) }, getPagesMap: function() { return a.safeReference(this.P.views) }, get: function(e) { return a.safeReference(this.P.views[e]) }, getByApiName: function(e) { return a.safeReference(this.P.apiNamesToViews[e]) }, apiNameToId: function(e) { var t = this.P.apiNamesToViews[e]; if (t) return t.id }, idToApiName: function(e) { var t = this.P.views[e]; if (t) return t.apiName }, getNumberOfPages: function() { return i.keys(this.P.views).length }, getAllViewsForActivationType: function(e) { return i.filter(this.P.views, { activationType: e }) }, F: function(e) { i.isEmpty(e.data.views) || (i.each(e.data.views, i.bind((function(e) { a.deepFreeze(e), this.P.views[e.id] = e, this.P.apiNamesToViews[e.apiName] = e }), this)), this.emitChange()) } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(19);
    e.exports = { initialize: function() { this.P = { profile: {}, metadata: {}, visitorId: null }, this.on(r.SET_VISITOR_ID_VIA_API, this.yt), this.on(r.SET_VISITOR_ATTRIBUTES, this.It), this.on(r.LOAD_EXISTING_VISITOR_PROFILE, this.Tt) }, getVisitorProfile: function() { return this.P.profile }, getVisitorProfileMetadata: function() { return this.P.metadata }, getAttribute: function(e) { var t = this.P.profile; return i.cloneDeep(a.getFieldValue(t, e)) }, getAttributeMetadata: function(e) { return i.cloneDeep(this.P.metadata[e]) }, getVisitorIdFromAPI: function() { return this.P.visitorId }, Tt: function(e) { this.P.profile = e.profile, this.P.metadata = e.metadata, this.emitChange() }, It: function(e) { i.each(e.attributes, i.bind((function(e) { var t = e.key;
                a.setFieldValue(this.P.profile, t, e.value), e.metadata && i.forOwn(e.metadata, i.bind((function(e, n) { a.setFieldValue(this.P.metadata, t.concat(n), e) }), this)) }), this)), this.emitChange() }, yt: function(e) { this.P.visitorId = e, this.emitChange() } } }), (function(e, t, n) { var i = n(2),
        r = n(7);
    e.exports = { initialize: function() { this.P = {}, this.on(r.DATA_LOADED, this.St) }, getCustomBehavioralAttributes: function() { return i.filter(this.P, (function(e) { return !!e.rule_json })) }, getVisitorAttribute: function(e) { var t = i.values(this.P); if (e.datasourceId && (t = i.filter(t, { dcp_datasource_id: String(e.datasourceId) })), e.attributeName && e.attributeId) throw new Error("Must not specify both attribute name and attribute ID"); if (e.attributeId) { var n = this.P[e.attributeId]; if (!n) throw new Error("Unrecognized attribute ID: " + e.attributeId); return n } if (e.attributeName) { var r = i.filter(t, { name: e.attributeName }); if (!r.length) throw new Error("Unrecognized attribute name: " + e.attributeName); if (r.length > 1) throw new Error("Too many attributes with name: " + e.attributeName); return r[0] } throw new Error("Must specify attribute name or attribute ID") }, St: function(e) { i.isEmpty(e.data.visitorAttributes) || (i.each(e.data.visitorAttributes, i.bind((function(e) { this.P[e.id] = e }), this)), this.emitChange()) } } }), (function(e, t, n) { var i = (n(2), n(7));
    n(63).Event;
    e.exports = { initialize: function() { this.P = { events: [], foreignEvents: {}, foreignEventQueues: {} }, this.on(i.SET_VISITOR_EVENTS, this.F), this.on(i.SET_FOREIGN_VISITOR_EVENTS, this.At), this.on(i.SET_FOREIGN_VISITOR_EVENT_QUEUE, this.bt) }, getEvents: function() { return this.P.events }, getForeignEvents: function() { return this.P.foreignEvents }, getForeignEventQueues: function() { return this.P.foreignEventQueues }, F: function(e) { this.P.events = e, this.emitChange() }, At: function(e) { this.P.foreignEvents[e.key] = e.value }, bt: function(e) { this.P.foreignEventQueues[e.key] = e.value } } }), (function(e, t, n) {
    function i(e, t, n, i, r) { this[o.FIELDS.NAME] = e, this[o.FIELDS.TYPE] = t, a.isString(n) && n.trim().length > 0 && (this[o.FIELDS.CATEGORY] = n), i && a.keys(i).length > 0 && (this[o.FIELDS.OPTIONS] = i), a.isUndefined(r) || (this[o.FIELDS.REVENUE] = r) }

    function r(e, t, n, i) { this.eventBase = e, this[o.FIELDS.TIME] = t, a.isUndefined(n) || (this[o.FIELDS.SESSION_ID] = n), a.isUndefined(i) || (this[o.FIELDS.SESSION_INDEX] = i) } var a = n(2),
        o = n(64),
        s = n(19).getFieldValue,
        c = n(65);
    t.EventBase = i, i.prototype.digest = function() { var e = function(e, t) { return encodeURIComponent(e) + "=" + encodeURIComponent(t) },
            t = []; if (t.push(e(o.FIELDS.NAME, this[o.FIELDS.NAME])), t.push(e(o.FIELDS.TYPE, this[o.FIELDS.TYPE])), this[o.FIELDS.CATEGORY] && t.push(e(o.FIELDS.CATEGORY, this[o.FIELDS.CATEGORY])), this[o.FIELDS.REVENUE] && t.push(e(o.FIELDS.REVENUE, this[o.FIELDS.REVENUE])), !this[o.FIELDS.OPTIONS]) return t.join("&"); var n = this[o.FIELDS.OPTIONS] || {},
            i = a.filter(a.keys(n), (function(e) { return n.hasOwnProperty(e) }));
        i = i.sort(); for (var r = 0; r < i.length; r++) t.push(e(i[r], n[i[r]])); return t.join("&") }, i.prototype.hash = function() { return this.hash_ ? this.hash_ : (this.hash_ = c.hashToHex(c.toByteString(this.digest()), c.Seed.BEHAVIOR_EVENT), this.hash_) }, i.prototype.setHash = function(e) { this.hash_ = e }, i.prototype.reHash = function() { this.hash_ = null, this.hash() }, i.prototype.equals = function(e) { if (this.hash() !== e.hash()) return !1; if (this[o.FIELDS.NAME] !== e[o.FIELDS.NAME] || this[o.FIELDS.TYPE] !== e[o.FIELDS.TYPE] || this[o.FIELDS.CATEGORY] !== e[o.FIELDS.CATEGORY] || this[o.FIELDS.REVENUE] !== e[o.FIELDS.REVENUE]) return !1; if (!this[o.FIELDS.OPTIONS] && !e[o.FIELDS.OPTIONS]) return !0; var t = this[o.FIELDS.OPTIONS] || {},
            n = e[o.FIELDS.OPTIONS] || {},
            i = a.filter(a.keys(t), (function(e) { return t.hasOwnProperty(e) })),
            r = a.filter(a.keys(n), (function(e) { return n.hasOwnProperty(e) })); if (i.length !== r.length) return !1; for (var s = 0; s < i.length; s++) { var c = i[s]; if (!n.hasOwnProperty(c) || t[c] !== n[c]) return !1 } return !0 }, i.prototype.getValueOrDefault = function(e, t) { var n = s(this, e); return a.isUndefined(n) ? t : n }, i.prototype.setFieldValue = function(e, t) { e !== o.FIELDS.NAME && e !== o.FIELDS.TYPE && e !== o.FIELDS.CATEGORY && e !== o.FIELDS.REVENUE && e !== o.FIELDS.OPTIONS || (this[e] = t, this.reHash()) }, t.Event = r, r.prototype.getValueOrDefault = function(e, t) { if (0 === e.length) return this; var n = {};
        n[o.FIELDS.TIME] = this[o.FIELDS.TIME], n[o.FIELDS.SESSION_ID] = this[o.FIELDS.SESSION_ID], n[o.FIELDS.SESSION_INDEX] = this[o.FIELDS.SESSION_INDEX]; var i = s(n, e); return a.isUndefined(i) ? this.eventBase.getValueOrDefault(e, t) : i }, r.prototype.setFieldValue = function(e, t) { e === o.FIELDS.TIME || e === o.FIELDS.SESSION_ID || e === o.FIELDS.SESSION_INDEX ? this[e] = t : this.eventBase.setFieldValue(e, t) }; var u = { n: "name", y: "type", c: "category", r: "revenue", s: "session_id", o: "tags", si: "session_index" };
    r.prototype.readableEvent = function() { var e, t, n = function(e) { return a.isString(e) ? '"' + e + '"' : e },
            i = this,
            r = [];
        a.each([o.FIELDS.NAME, o.FIELDS.TYPE, o.FIELDS.CATEGORY, o.FIELDS.REVENUE, o.FIELDS.SESSION_ID], (function(o) { e = u[o], t = i.getValueOrDefault([o]), a.isUndefined(t) || r.push(e + ": " + n(t)) })); var s = []; if (e = u[o.FIELDS.OPTIONS], t = i.getValueOrDefault([o.FIELDS.OPTIONS]), a.isUndefined(t) || (a.each(t, (function(e, t) { s.push(t + ": " + String(n(e))) })), r.push(e + ": {\n\t\t" + s.join(",\n\t\t") + "\n\t}")), t = i.getValueOrDefault([o.FIELDS.TIME]), a.isNumber(t) && (t = n(new Date(t).toString())), !a.isUndefined(t)) { var c = "timestamp";
            r.push(c + ": " + t) } return "{\n\t" + r.join(",\n\t") + "\n}" }, r.prototype.toObject = function(e) { var t, n, i = {},
            r = this;
        a.each([o.FIELDS.NAME, o.FIELDS.TYPE, o.FIELDS.CATEGORY, o.FIELDS.REVENUE, o.FIELDS.OPTIONS, o.FIELDS.SESSION_INDEX], (function(e) { t = u[e], n = r.getValueOrDefault([e], e === o.FIELDS.OPTIONS ? {} : void 0), a.isUndefined(n) || (i[t] = n) })); var s = u[o.FIELDS.OPTIONS],
            c = u[o.FIELDS.REVENUE]; if (e && e.revenueAsTag && i[c] && (i[s] = i[s] || {}, i[s][c] = i[c], delete i[c]), n = r.getValueOrDefault([o.FIELDS.TIME]), a.isNumber(n))
            if (e && e.timeAsTimestamp) { var l = "timestamp";
                i[l] = new Date(n) } else { var d = "time";
                i[d] = n }
        return i } }), (function(e, t) { t.FIELDS = { NAME: "n", TIME: "t", TYPE: "y", CATEGORY: "c", REVENUE: "r", SESSION_ID: "s", OPTIONS: "o", SESSION_INDEX: "si" }, t.FIELDS_V0_2 = { name: t.FIELDS.NAME, time: t.FIELDS.TIME, type: t.FIELDS.TYPE, category: t.FIELDS.CATEGORY, tags: t.FIELDS.OPTIONS, session_index: t.FIELDS.SESSION_INDEX } }), (function(e, t, n) { var i = n(66).v3,
        r = { IGNORING: 0, BUCKETING: 1, FALLBACK: 2, HOLDBACK: 3, BEHAVIOR_EVENT: 2716770798 },
        a = Math.pow(2, 32),
        o = function(e, t, n) { return Math.floor(c(e, t) * n) },
        s = function(e, t) { var n = i(e, t); return (n >>> 16).toString(16) + (65535 & n).toString(16) },
        c = function(e, t) { var n = i(e, t); return (n >>> 0) / a },
        u = function(e) { var t = String.fromCharCode; return e.replace(/[\S\s]/gi, (function(e) { e = e.charCodeAt(0); var n = t(255 & e); return e > 255 && (n = t(e >>> 8 & 255) + n), e > 65535 && (n = t(e >>> 16) + n), n })) };
    e.exports = { Seed: r, hashToHex: s, hashToInt: o, hashToReal: c, toByteString: u } }), (function(e, t, n) {!(function() {
        function t(e, t) { for (var n, i = e.length, r = t ^ i, a = 0; i >= 4;) n = 255 & e.charCodeAt(a) | (255 & e.charCodeAt(++a)) << 8 | (255 & e.charCodeAt(++a)) << 16 | (255 & e.charCodeAt(++a)) << 24, n = 1540483477 * (65535 & n) + ((1540483477 * (n >>> 16) & 65535) << 16), n ^= n >>> 24, n = 1540483477 * (65535 & n) + ((1540483477 * (n >>> 16) & 65535) << 16), r = 1540483477 * (65535 & r) + ((1540483477 * (r >>> 16) & 65535) << 16) ^ n, i -= 4, ++a; switch (i) {
                case 3:
                    r ^= (255 & e.charCodeAt(a + 2)) << 16;
                case 2:
                    r ^= (255 & e.charCodeAt(a + 1)) << 8;
                case 1:
                    r ^= 255 & e.charCodeAt(a), r = 1540483477 * (65535 & r) + ((1540483477 * (r >>> 16) & 65535) << 16) } return r ^= r >>> 13, r = 1540483477 * (65535 & r) + ((1540483477 * (r >>> 16) & 65535) << 16), r ^= r >>> 15, r >>> 0 }

        function n(e, t) { var n, i, r, a, o, s, c, u; for (n = 3 & e.length, i = e.length - n, r = t, o = 3432918353, s = 461845907, u = 0; u < i;) c = 255 & e.charCodeAt(u) | (255 & e.charCodeAt(++u)) << 8 | (255 & e.charCodeAt(++u)) << 16 | (255 & e.charCodeAt(++u)) << 24, ++u, c = (65535 & c) * o + (((c >>> 16) * o & 65535) << 16) & 4294967295, c = c << 15 | c >>> 17, c = (65535 & c) * s + (((c >>> 16) * s & 65535) << 16) & 4294967295, r ^= c, r = r << 13 | r >>> 19, a = 5 * (65535 & r) + ((5 * (r >>> 16) & 65535) << 16) & 4294967295, r = (65535 & a) + 27492 + (((a >>> 16) + 58964 & 65535) << 16); switch (c = 0, n) {
                case 3:
                    c ^= (255 & e.charCodeAt(u + 2)) << 16;
                case 2:
                    c ^= (255 & e.charCodeAt(u + 1)) << 8;
                case 1:
                    c ^= 255 & e.charCodeAt(u), c = (65535 & c) * o + (((c >>> 16) * o & 65535) << 16) & 4294967295, c = c << 15 | c >>> 17, c = (65535 & c) * s + (((c >>> 16) * s & 65535) << 16) & 4294967295, r ^= c } return r ^= e.length, r ^= r >>> 16, r = 2246822507 * (65535 & r) + ((2246822507 * (r >>> 16) & 65535) << 16) & 4294967295, r ^= r >>> 13, r = 3266489909 * (65535 & r) + ((3266489909 * (r >>> 16) & 65535) << 16) & 4294967295, r ^= r >>> 16, r >>> 0 } var i = n;
        i.v2 = t, i.v3 = n;
        e.exports = i })() }), (function(e, t, n) { var i = n(7);
    e.exports = { initialize: function() { this.P = { baseMap: {}, eventQueue: [], lastEvent: null, initialized: !1, cleared: !1 }, this.on(i.UPDATE_BEHAVIOR_STORE, this.wt) }, getBaseMap: function() { return this.P.baseMap }, getEventQueue: function() { return this.P.eventQueue }, getLastEvent: function() { return this.P.lastEvent }, getCleared: function() { return this.P.cleared }, getInitialized: function() { return this.P.initialized }, wt: function(e) { this.P[e.key] = e.value } } }), (function(e, t, n) { var i = n(2),
        r = n(7);
    e.exports = { initialize: function() { this.P = { randomId: null }, this.on(r.SET_VISITOR_ID, this.F) }, getBucketingId: function() { return this.getRandomId() }, getRandomId: function() { return this.P.randomId }, F: function(e) { i.extend(this.P, e), this.emitChange() } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(26);
    e.exports = { initialize: function() { this.P = { variationIdMap: {}, preferredLayerMap: {} }, this.on(r.UPDATE_VARIATION_ID_MAP, this.Dt), this.on(r.MERGE_VARIATION_ID_MAP, this.Rt), this.on(r.UPDATE_PREFERRED_LAYER_MAP, this.Nt), this.on(r.MERGE_PREFERRED_LAYER_MAP, this.Ct) }, getVariationIdMap: function() { return i.cloneDeep(this.P.variationIdMap) }, getVariationIdMapString: function() { return a.stringify(this.P.variationIdMap) }, Dt: function(e) { var t = this.P.variationIdMap,
                n = t[e.layerId] || {};
            n[e.experimentId] !== e.variationId && (n[e.experimentId] = e.variationId, this.P.variationIdMap[e.layerId] = n, this.emitChange()) }, Rt: function(e) { var t = this.getVariationIdMap(),
                n = e.variationIdMap;
            i.each(t || {}, (function(e, t) { n[t] ? i.assign(n[t], e) : n[t] = e })), this.P.variationIdMap = n, this.emitChange() }, getPreferredLayerMap: function() { return i.cloneDeep(this.P.preferredLayerMap) }, getPreferredLayerMapString: function() { return a.stringify(this.P.preferredLayerMap) }, getPreferredLayerId: function(e) { return this.P.preferredLayerMap[e] }, Nt: function(e) { this.P.preferredLayerMap[e.groupId] !== e.layerId && (this.P.preferredLayerMap[e.groupId] = e.layerId, this.emitChange()) }, Ct: function(e) { var t = this.getPreferredLayerMap(),
                n = e.preferredLayerMap;
            i.assign(n, t), this.P.preferredLayerMap = n, this.emitChange() } } }), (function(e, t, n) { var i = n(2),
        r = n(7);
    e.exports = { initialize: function() { this.P = { frames: [], defaultFrame: null, messages: [], subscribers: [], canonicalOrigins: null, disabled: !1 }, this.on(r.XDOMAIN_SET_DEFAULT_FRAME, this.Ot), this.on(r.XDOMAIN_ADD_FRAME, this.xt), this.on(r.XDOMAIN_SET_MESSAGE, this.Lt), this.on(r.XDOMAIN_ADD_SUBSCRIBER, this.Pt), this.on(r.XDOMAIN_SET_CANONICAL_ORIGINS, this.kt), this.on(r.XDOMAIN_SET_DISABLED, this.Ft) }, getMessages: function() { return i.cloneDeep(this.P.messages) }, getNextMessageId: function() { return this.P.messages.length }, getMessageById: function(e) { return this.P.messages[e] }, getSubscribers: function() { return this.P.subscribers }, getFrames: function() { return this.P.frames }, getNextFrameId: function() { return this.P.frames.length }, getDefaultFrame: function() { return this.P.defaultFrame }, getCanonicalOrigins: function() { return i.cloneDeep(this.P.canonicalOrigins) }, isDisabled: function() { return this.P.disabled }, Ot: function(e) { this.P.defaultFrame = e }, xt: function(e) { this.P.frames.push(e) }, Lt: function(e) { this.P.messages[e.messageId] = e.message }, Pt: function(e) { this.P.subscribers.push(e.subscriber) }, kt: function(e) { this.P.canonicalOrigins = e.canonicalOrigins }, Ft: function(e) { this.P.disabled = e.disabled } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(22);
    e.exports = { initialize: function() { this.P = {}, this.on(r.DATA_LOADED, this.F) }, F: function(e) { i.isEmpty(e.data.groups) || (i.each(e.data.groups, i.bind((function(e) { a.deepFreeze(e), this.P[e.id] = e }), this)), this.emitChange()) }, getAll: function() { return a.safeReference(i.values(this.P)) }, getGroupsMap: function() { return a.safeReference(this.P) }, get: function(e) { return a.safeReference(this.P[e]) } } }), (function(e, t, n) { var i = n(73);
    t.initializeStore = i.initialize, t.addEvent = i.addEvent, t.getEvents = i.getEvents, t.usageInBytes = i.usageInBytes }), (function(e, t, n) {
    function i(e) { y.dispatch(m.SET_VISITOR_EVENTS, e) }

    function r(e) { y.dispatch(m.UPDATE_BEHAVIOR_STORE, { key: "baseMap", value: e }) }

    function a(e) { y.dispatch(m.UPDATE_BEHAVIOR_STORE, { key: "eventQueue", value: e }) }

    function o(e) { y.dispatch(m.UPDATE_BEHAVIOR_STORE, { key: "lastEvent", value: e }) }

    function s(e) { y.dispatch(m.UPDATE_BEHAVIOR_STORE, { key: "cleared", value: e }) }

    function c() { y.dispatch(m.UPDATE_BEHAVIOR_STORE, { key: "initialized", value: !0 }) }

    function u() { return N.getEvents() }

    function l() { return C.getBaseMap() }

    function d() { return C.getEventQueue() }

    function f() { return C.getLastEvent() }

    function p() { return C.getCleared() }

    function h() { return C.getInitialized() }

    function g() { var e = u().concat(d()),
            t = !1; return e.length > x && (e = e.slice(-x), t = !0), i(e), a([]), t }
    var v = n(2),
        m = n(7),
        _ = n(24),
        E = n(74),
        y = n(9),
        I = n(23),
        T = n(75),
        S = t,
        A = n(63).Event,
        b = n(64),
        w = n(63).EventBase,
        D = n(89),
        R = n(16),
        N = R.get("stores/visitor_events"),
        C = R.get("stores/visitor_events_manager"),
        O = { EVENTBASE: "eb", HASH: "h", TIMEBASE: "tb", TIMESTAMPS: "ts", DELTA: "d", INDEX: "i" },
        x = 1e3;
    t.initialize = function(e, t) { if (!h()) { S.Mt(e, t); var n = u();
            n.length > 0 && o(n[n.length - 1]); var i = d();
            i.length > 0 && o(i[i.length - 1]), c() } }, t.addEvent = function(e) { I.debug("Behavior store: adding event", e); var t = S.Vt(e);
        o(t), a(d().concat(t)), D.reindexIfNecessary(f(), u(), d()), S.Ut(d()) }, t.getEvents = function() { return d().length > 0 && (g() && D.sessionize(u()), S.Gt(u()), S.Ut(d())), u() }, S.Mt = function(e, t) { S.Bt(e, t) && (S.Gt(u()), S.Ut(d())), D.sessionize(u()) }, S.Bt = function(e, t) { if (0 === e.length && 0 === t.length) return i([]), a([]), !1; var n = !1,
            r = e[0] || t[0]; return O.EVENTBASE in r ? (i(S.jt(e)), a(S.jt(t))) : (n = !0, i(S.Ht(e)), a(S.Ht(t))), d().length > 0 && (g(), n = !0), i(S._updateBaseMapAndMaybeDedupe(u())), S._migrateEventBasesAndUpdateStore() && (n = !0), n }, S.Ht = function(e) { for (var t = [], n = 0; n < e.length; n++) { var i = e[n],
                r = S.zt(i);
            t[n] = new A(r, i[b.FIELDS.TIME]) } return t }, S._migrateEventBasesAndUpdateStore = function() { var e = !1,
            t = S.qt(); return D.applyMigrations(t) && (e = !0, r({}), i(S._updateBaseMapAndMaybeDedupe(u())), a(S._updateBaseMapAndMaybeDedupe(d()))), e }, S.Yt = function() { return _.now() }, S.Vt = function(e) { var t, n = e.name,
            i = e.type || "default",
            r = e.category || E.OTHER,
            a = e.tags || {};
        e.revenue && (t = e.revenue); var o = new w(n, i, r, a, t);
        o = S.Kt(o); var s = S.Yt(),
            c = new A(o, s, -1); return D.updateSessionId(f(), c), D.updateSessionIndex(f(), c), c }, S._updateBaseMapAndMaybeDedupe = function(e) {
        for (var t = 0; t < e.length; t++) e[t].eventBase = S.Kt(e[t].eventBase);
        return e
    }, S.Gt = function(e) { var t = S.Wt(e);
        T.persistBehaviorEvents(t) }, S.Ut = function(e) { var t = S.Wt(e);
        T.persistBehaviorEventQueue(t) }, S.Xt = function() { p() || (i([]), a([]), S.Gt(u()), S.Ut(d()), r({}), o(null), s(!0)) }, S.Kt = function(e) { var t = e.hash(),
            n = l(),
            i = n[t]; if (v.isUndefined(i)) return n[t] = [e], r(n), e; for (var a = 0; a < i.length; a++)
            if (e.equals(i[a])) return i[a];
        return i.push(e), r(n), e }, S.qt = function() { var e = [],
            t = l(); for (var n in t) t.hasOwnProperty(n) && (e = e.concat(t[n])); return e }, S.Wt = function(e) { for (var t = function(e) { var t = {};
                t[b.FIELDS.NAME] = e.getValueOrDefault([b.FIELDS.NAME]), t[b.FIELDS.TYPE] = e.getValueOrDefault([b.FIELDS.TYPE]); var n = e.getValueOrDefault([b.FIELDS.CATEGORY]);
                v.isUndefined(n) || (t[b.FIELDS.CATEGORY] = n); var i = e.getValueOrDefault([b.FIELDS.REVENUE]);
                v.isUndefined(i) || (t[b.FIELDS.REVENUE] = i); var r = e.getValueOrDefault([b.FIELDS.OPTIONS]); return v.isUndefined(r) || (t[b.FIELDS.OPTIONS] = r), t }, n = O, i = [], r = "_idx_", a = 0; a < e.length; a++) { var o, s, c = e[a],
                u = c.eventBase; if (u.hasOwnProperty(r)) { o = i[u[r]]; var l = c[b.FIELDS.TIME] - (o[n.TIMEBASE] || 0);
                s = {}, s[n.DELTA] = l, s[n.INDEX] = a, o[n.TIMESTAMPS].push(s) } else o = {}, o[n.EVENTBASE] = t(c), o[n.HASH] = u.hash(), o[n.TIMEBASE] = c[b.FIELDS.TIME], s = {}, s[n.DELTA] = 0, s[n.INDEX] = a, o[n.TIMESTAMPS] = [s], i.push(o), u[r] = i.length - 1 } for (a = 0; a < e.length; a++) delete e[a].eventBase[r]; return i }, S.zt = function(e, t) { var n = new w(e[b.FIELDS.NAME], e[b.FIELDS.TYPE], e[b.FIELDS.CATEGORY], e[b.FIELDS.OPTIONS], e[b.FIELDS.REVENUE]); return v.isUndefined(t) || n.setHash(t), n }, S.jt = function(e) { for (var t = O, n = [], i = 0; i < e.length; i++)
            for (var r = e[i], a = S.zt(r[t.EVENTBASE], r[t.HASH]), o = r[t.TIMEBASE], s = r[t.TIMESTAMPS], c = 0; c < s.length; c++) { var u = s[c],
                    l = new A(a, o + u[t.DELTA]),
                    d = u[t.INDEX];
                n[d] = l }
        return n }, t.deserialize = function(e) { return S.jt(e) }, t.mergeAllEvents = function(e) { var t = [].concat.apply([], e); return t.sort(D.sessionSortPredicate), D.sessionize(t), t }
}), (function(e, t) { e.exports = { OTHER: "other" } }), (function(e, t, n) {
    function i() { return u(j.LAYER_MAP) || {} }

    function r(e, t) { N.dispatch(C.UPDATE_PREFERRED_LAYER_MAP, { groupId: e, layerId: t }) }

    function a() { var e = X.getPreferredLayerMapString();
        h(j.LAYER_MAP, e, !0) }

    function o(e) { N.dispatch(C.SET_TRACKER_PERSISTABLE_STATE, e) }

    function s(e, t) {
        function n(e, n) { var i;
            t.attributionType && (i = P.now()), N.dispatch(C.SET_VISITOR_ATTRIBUTES, { attributes: [{ key: e, value: n, metadata: { lastModified: i } }] }) } if (t.getter) { var i = t.provides;
            R.isArray(i) || (i = [i]); var r = t.isSticky && !R.isUndefined(V.getFieldValue(e, i)); if (!r) { var a; try { var o = k.evaluate(t.getter);
                    R.isFunction(o) && (o = o((function() { return V.getFieldValue(e, i) }), (function(e) { n(i, e) }))), R.isUndefined(o) || (t.isAsync ? (a = o.then((function(e) { n(i, e) }), (function(e) { G.warn('Failed to evaluate provider for "' + t.provides + '"; error was:', e) })), N.dispatch(C.SET_VISITOR_ATTRIBUTE_PENDING, { key: i, pending: a })) : n(i, o)) } catch (e) { G.warn('Failed to evaluate getter for provider for "' + t.provides + '"; error was: ' + e.message) } return a } } }

    function c() { I(u(j.EVENTS) || [], u(j.EVENT_QUEUE) || []); var e = f(j.LAYER_STATES);
        R.forEach(e, (function(e) { e.item = R.map(e.item, d) })), T(l(e)), S(u(j.SESSION_STATE) || {}), b(u(j.VISITOR_PROFILE) || {}); var n = u(j.TRACKER_OPTIMIZELY);
        n && o(n), t.loadForeignData(), t.removeLegacySessionStateCookies() }

    function u(e) { var t = g(e),
            n = U.getItem(t); if (!n) { var i = v(e);
            n = U.getItem(i), p(e, n) } return R.isString(n) && (n = w(n)), n }

    function l(e) { var t = []; return R.each(e, (function(e) { R.each(e.item, (function(n) { n.namespace = e.namespace, t.push(n) })) })), t }

    function d(e) { var t; return t = e.layerId ? e : { layerId: e.i, pageId: e.p, decisionTimestamp: e.t, decisionTicket: { audienceIds: e.a || [] }, decision: { layerId: e.i, experimentId: e.x || null, variationId: e.v || null, isLayerHoldback: e.h || !1 } } }

    function f(e) { var t = W.getBucketingId(),
            n = [],
            i = t + "\\$\\$([^$]+?)\\$\\$" + e,
            r = new RegExp(i); return R.each(U.keys(), (function(e) { var i = e.match(r); if (i) { var a = { namespace: i[1], userId: t, item: w(U.getItem(e)) };
                n.push(a) } })), n }

    function p(e, t) { var n = g(e),
            i = v(e);
        U.setItem(n, t), U.removeItem(i) }

    function h(e, t, n) { try { var i = g(e);
            n || (t = M.stringify(t)); try { U.removeItem(v(e)), U.setItem(i, t) } catch (e) { throw G.warn("Visitor / Unable to set localStorage key, error was:", e), new Error("Unable to set localStorage") }
            D.setItem(i, t) } catch (e) { G.warn("Unable to persist visitor data:", e.message) } }

    function g(e) { var n = W.getBucketingId(); if (!n) throw new Error("Visitor bucketingId not set"); var i = t.getNamespace(); if (!i) throw new Error("Namespace is not set"); return [n, i, e].join("$$") }

    function v(e) { var t = W.getBucketingId(); if (!t) throw new Error("Cannot get legacy key: visitor bucketingId not set"); return [t, e].join("$$") }

    function m(e, n) { if (!W.getBucketingId()) throw new Error("Cannot update local store because bucketingId not set"); if (E(e)) { var i = t.getStorageKeyFromKey(e); if (R.includes(j, i)) { var r = g(i); if (!(e.indexOf(r) <= 0))
                    if (i === j.EVENT_QUEUE) N.dispatch(C.SET_FOREIGN_VISITOR_EVENT_QUEUE, { key: e, value: x.deserialize(w(n)) });
                    else if (i === j.EVENTS) N.dispatch(C.SET_FOREIGN_VISITOR_EVENTS, { key: e, value: x.deserialize(w(n)) });
                else if (i === j.LAYER_STATES) N.dispatch(C.LOAD_PERSISTED_LAYER_STATES, { layerStates: R.map(w(n), d), merge: !0 });
                else if (i === j.VARIATION_MAP) N.dispatch(C.MERGE_VARIATION_ID_MAP, { variationIdMap: w(n) });
                else if (i === j.VISITOR_PROFILE) { var a = ["custom"],
                        o = w(n);
                    R.each(a, (function(e) { var t = Y.getPlugin(F.PluginTypes.visitorProfileProviders, e); if (t) { if (o.profile && o.metadata) { var n = _(o, e, t.attributionType); if (!R.isEmpty(n)) { var i = [];
                                    R.forOwn(n.data, (function(t, r) { var a = n.metadata[r],
                                            o = { key: [e, r], value: t, metadata: a };
                                        i.push(o) })), N.dispatch(C.SET_VISITOR_ATTRIBUTES, { attributes: i }) } } } else G.debug("Attribute type", e, "not used by any audiences") })) } } } }

    function _(e, t, n) { var i = $.getAttribute(t),
            r = $.getAttributeMetadata(t),
            a = e.profile[t],
            o = e.metadata[t]; if (R.isEmpty(i)) return { data: a, metadata: o }; var s = {}; return R.forOwn(a, (function(e, t) { var i;
            r && r[t] && (i = r[t].lastModified); var a;
            o && o[t] && (a = o[t].lastModified), (n === F.AttributionTypes.FIRST_TOUCH && i >= a || n === F.AttributionTypes.LAST_TOUCH && a >= i || R.isUndefined(i) && a) && (s.data = s.data || {}, s.data[t] = e, a && (s.metadata = s.metadata || {}, s.metadata[t] = s.metadata[t] || {}, s.metadata[t].lastModified = a)) })), s }

    function E(e) { var t = e.split("$$")[0]; return t.indexOf("://") > 0 }

    function y() { var e = $.getVisitorProfile(),
            t = $.getVisitorProfileMetadata(),
            n = Y.getAllPlugins(F.PluginTypes.visitorProfileProviders); if (n) { var i = R.reduce(n, (function(e, t) { return t.provides && (e[t.provides] = t), e }), {});
            e = R.omitBy(e, (function(e, t) { var n = i[t]; return n && n.isTransient })) } return { profile: e, metadata: t } }

    function I(e, t) { O.initializeStore(e, t) }

    function T(e) { N.dispatch(C.LOAD_PERSISTED_LAYER_STATES, { layerStates: R.filter(e, (function(e) { return !!e.decision })) }) }

    function S(e) { e = R.extend({ lastSessionTimestamp: 0, sessionId: null }, e), N.dispatch(C.LOAD_SESSION_STATE, e) }

    function A() { return "oeu" + P.now() + "r" + Math.random() }

    function b(e) { var t, n, i = Y.getAllPlugins(F.PluginTypes.visitorProfileProviders),
            r = R.filter(i, (function(e) { return R.isFunction(e.restorer) }));
        e.profile && e.metadata ? (t = e.profile, n = e.metadata) : (t = e, n = {}), t = R.reduce(t, (function(e, t, n) { var i = t,
                a = R.find(r, { provides: n }); return a && (i = a.restorer(t)), e[n] = i, e }), {}), N.dispatch(C.LOAD_EXISTING_VISITOR_PROFILE, { profile: t, metadata: n }) }

    function w(e) { try { return M.parse(e) } catch (t) { return G.debug("Failed to parse: ", e, t), null } } var D, R = n(2),
        N = n(9),
        C = n(7),
        O = n(72),
        x = n(73),
        L = n(76),
        P = n(24),
        k = n(16),
        F = n(25),
        M = n(26),
        V = n(19),
        U = n(82).LocalStorage,
        G = n(23),
        B = n(12).Promise,
        j = n(25).VisitorStorageKeys;
    D = n(85); var H = k.get("stores/cookie_options"),
        z = k.get("stores/global"),
        q = k.get("stores/layer"),
        Y = k.get("stores/plugins"),
        K = k.get("stores/session"),
        W = k.get("stores/visitor_id"),
        X = k.get("stores/visitor_bucketing"),
        $ = k.get("stores/visitor"),
        Q = k.get("stores/provider_status");
    t.getOrGenerateId = function() { var e = t.getCurrentId(); return e || (e = A()), { randomId: e } }, t.getCurrentId = function() { return $.getVisitorIdFromAPI() || L.get(F.COOKIES.VISITOR_ID) }, t.hasSomeData = function() { return U.keys().length > 0 }, t.setId = function(e) { var n = W.getBucketingId();
        N.dispatch(C.SET_VISITOR_ID, e), W.getBucketingId() !== n && (c(), t.deleteOldLocalData(), D.deleteData(e)); try { t.maybePersistVisitorId(e) } catch (e) { if (G.error("Visitor / Unable to persist visitorId, disabling tracking"), N.dispatch(C.LOAD_DIRECTIVE, { trackingDisabled: !0 }), e instanceof L.MismatchError) throw G.error("Visitor / Cookie not set to correct value:", e), new Error("Cookie mismatch error while persisting visitorId"); throw e }
        t.refreshSession() }, t.getVariationIdMap = function() { return u(j.VARIATION_MAP) || {} }, t.updateVariationIdMap = function(e, t, n) { N.dispatch(C.UPDATE_VARIATION_ID_MAP, { layerId: e, experimentId: t, variationId: n }) }, t.persistVariationIdMap = function() { var e = X.getVariationIdMapString();
        h(j.VARIATION_MAP, e, !0) }, t.getPreferredLayerMap = i, t.updatePreferredLayerMap = r, t.persistTrackerOptimizelyData = function(e) { h(j.TRACKER_OPTIMIZELY, e) }, t.refreshSession = function() { N.dispatch(C.REFRESH_SESSION) }, t.populateEagerVisitorData = function(e, n) { var i = R.filter(e, (function(e) { return !e.isLazy })),
            r = t.populateVisitorData(i, n); return r }, t.populateLazyVisitorData = function(e, n) { var i = R.filter(e, (function(e) { return e.isLazy })); return t.populateVisitorData(i, n) }, t.populateVisitorData = function(e, t) { t = t || {}; var n = R.partial(s, t),
            i = R(e).filter({ isAsync: !0 }).map(n).filter().value(); return R.forEach(R.filter(e, (function(e) { return !e.isAsync })), n), i.length > 0 ? B.all(i) : B.resolve() }, t.persistBehaviorEvents = function(e) { h(j.EVENTS, e) }, t.persistBehaviorEventQueue = function(e) { h(j.EVENT_QUEUE, e) }, t.persistLayerStates = function() { var e = q.getLayerStates(t.getNamespace());
        e = R.map(e, (function(e) { return R.omit(e, "namespace") })), h(j.LAYER_STATES, e) }, t.persistSessionState = function() { h(j.SESSION_STATE, K.getState()) }, t.persistVisitorProfile = function() { h(j.VISITOR_PROFILE, y()) }, t.persistVisitorBucketingStore = function() { t.persistVariationIdMap(), a() }, t.getUserIdFromKey = function(e, n) { var i; return R.includes(e, n) && R.includes(e, "_") && R.includes(e, "$$") && R.includes(e.slice(e.indexOf("$$")), t.getNamespace()) && (i = e.slice(e.indexOf("_") + 1, e.indexOf("$$"))), i }, t.maybePersistVisitorId = function(e) { L.remove(F.COOKIES.VISITOR_UUID), e.randomId && (H.getAutoRefresh() || t.getCurrentId() !== e.randomId ? (L.set(F.COOKIES.VISITOR_ID, e.randomId), G.log("Persisting visitorId:", e.randomId)) : G.log("Not persisting visitorId: value is not changed and also auto-refresh is disabled")) }, t.getAttribute = function(e) { return $.getAttribute(e) }, t.getPendingAttributeValue = function(e) { return Q.getPendingAttributeValue(e) }, t.isForeignKey = E, t.checkKeyForVisitorId = function(e) { var n = W.getBucketingId() || t.getCurrentId(),
            i = t.getIdFromKey(e); return !i || i === n }, t.getIdFromKey = function(e) { var n = e.split("$$")[0],
            i = t.getStorageKeyFromKey(e),
            r = R.includes(F.StorageKeys, i); if (r) return null; var a = n.indexOf("_"),
            o = a === -1; return o ? n : n.substring(a + 1) }, t.getStorageKeyFromKey = function(e) { var t, n = e.split("$$").pop(),
            i = n.indexOf("://") > -1; if (i) { var r = n.indexOf("_");
            t = n.substring(r + 1) } else t = n; return R.includes(R.values(F.AllStorageKeys), t) ? t : null }, t.deleteOldLocalData = function() { var e = U.keys();
        R.each(e, (function(e) { t.isForeignKey(e) || t.checkKeyForVisitorId(e) || U.removeItem(e) })) }, t.deleteOldForeignData = function() { var e = U.keys();
        R.each(e, (function(e) { t.isForeignKey(e) && U.removeItem(e) })) }, t.loadForeignData = function() { R.each(U.keys(), (function(e) { var t = U.getItem(e);
            t && m(e, t) })) }, t.getNamespace = function() { return z.getNamespace() }, t.serializeFieldKey = function(e) { return R.isArray(e) ? e.join("$$") : e }, t.removeLegacySessionStateCookies = function() { var e = L.getAll();
        R.forEach(R.keys(e), (function(e) { 0 === e.indexOf(F.COOKIES.SESSION_STATE + "$$") && L.remove(e) })) } }), (function(e, t, n) {
    function i(e, n) { n !== !1 && (n = !0); for (var i, a, o = e.hostname.split("."), s = [], c = null, l = o.length - 1; l >= 0; l--)
            if (s.unshift(o[l]), i = s.join("."), !r.includes(g, i)) { a = { domain: n ? "." + i : i }; try { t.set(v, Math.random().toString(), a), t.remove(v, a), c = a.domain; break } catch (e) {} }
        return d.dispatch(u.SET_COOKIE_DOMAIN, c), c } var r = n(2),
        a = n(77).create,
        o = n(24),
        s = n(81),
        c = n(41),
        u = n(7),
        l = n(16),
        d = n(9),
        f = l.get("stores/cookie_options"),
        p = t.SetError = a("CookieSetError"),
        h = t.MismatchError = a("CookieMismatchError");
    t.getAll = function(e) { r.isUndefined(e) && (e = !0); var n, i, a, o, c;
        n = s.getCookieString().split(/\s*;\s*/); var u = {}; for (a = 0; a < n.length; a++)
            if (i = n[a], o = i.indexOf("="), o > 0 && (c = t.safeDecodeURIComponent(i.substring(0, o)), void 0 === u[c])) { var l = i.substring(o + 1);
                e && (l = t.safeDecodeURIComponent(l)), u[c] = l }
        return u }, t.safeDecodeURIComponent = function(e) { try { return decodeURIComponent(e) } catch (t) { return e } }, t.get = function(e, n) { var i = t.getAll(n); return i[e] }, t.set = function(e, n, a, u) { a = r.extend({ encodeValue: !0 }, a), u !== !1 && (u = !0); var l = []; if (r.isUndefined(a.domain)) { var d = f.getCurrentDomain();
            d || (d = i(c.getLocation(), !0)), a.domain = d } if (a.domain && l.push("domain=" + a.domain), r.isUndefined(a.path) && (a.path = "/"), a.path && l.push("path=" + a.path), r.isUndefined(a.expires)) { var g = r.isUndefined(a.maxAge) ? f.getDefaultAgeInSeconds() : a.maxAge;
            a.expires = new Date(o.now() + 1e3 * g) } if (r.isUndefined(a.expires) || l.push("expires=" + a.expires.toUTCString()), a.secure && l.push("secure"), l = l.join(";"), s.setCookie(e + "=" + (a.encodeValue ? encodeURIComponent(n) : n) + ";" + l), u) { var v = a.encodeValue,
                m = t.get(e, v); if (m !== n) { if (!m) throw new p('Failed to set cookie "' + e + '"'); throw new h('Expected "' + n + '" for "' + e + '", got "' + m + '"') } } }, t.remove = function(e, n) { for (var i = c.getLocation().hostname.split("."); i.length > 0;) t.set(e, null, r.extend({}, n, { domain: "." + i.join("."), expires: new Date(0) }), !1), i.shift() }; var g = ["optimizely.test"],
        v = "optimizelyDomainTestCookie" }), (function(e, t, n) { var i = n(78),
        r = i("InternalError");
    t.BaseError = r, t.create = function(e) { return i(e, r) } }), (function(e, t, n) {
    function i(e, t) {
        function n(t) { if (!(this instanceof n)) return new n(t); try { throw new Error(t) } catch (t) { t.name = e, this.stack = t.stack }
            r && this.stack && (this.stack = a(this.stack, e, t)), this.message = t || "", this.name = e } return n.prototype = new(t || Error), n.prototype.constructor = n, n.prototype.inspect = function() { return this.message ? "[" + e + ": " + this.message + "]" : "[" + e + "]" }, n.prototype.name = e, n } var r = n(79)(),
        a = n(80);
    e.exports = i }), (function(e, t) { "use strict";
    e.exports = function() { var e = new Error("yep"); return !!e.stack && "Error: yep\n" === e.stack.substr(0, 11) } }), (function(e, t) { "use strict";
    e.exports = function(e, t, n) { var i = t; return n && (i += ": " + n), e = i + e.slice(e.indexOf("\n")) } }), (function(e, t, n) {
    function i() { return "loading" === t.getReadyState() } var r = n(16),
        a = r.get("stores/global");
    t.getDocumentElement = function() { return document.documentElement }, t.getCookieString = function() { return document.cookie || "" }, t.setCookie = function(e) { document.cookie = e }, t.querySelector = function(e) { return document.querySelector(e) }, t.querySelectorAll = function(e) { return document.querySelectorAll(e) }, t.parseUri = function(e) { var n = t.createElement("a"); return n.href = e, n }, t.childrenOf = function(e) { return Array.prototype.slice.call(e.querySelectorAll("*")) }, t.createElement = function(e) { return document.createElement(e) }, t.isReady = function() { return a.domContentLoadedHasFired() || "interactive" === document.readyState || "complete" === document.readyState }, t.isLoaded = function() { return "complete" === document.readyState }, t.addReadyHandler = function(e) { return document.addEventListener("DOMContentLoaded", e),
            function() { t.removeReadyHandler(e) } }, t.removeReadyHandler = function(e) { return function() { document.removeEventListener("DOMContentLoaded", e) } }, t.getReferrer = function() { return document.referrer }, t.getReadyState = function() { return document.readyState }, t.write = function(e) { if (!i()) throw new Error("Aborting attempt to write to already-loaded document");
        document.write(e) }, t.appendToHead = function(e) { return t.appendTo(document.head, e) }, t.appendTo = function(e, t) { e.appendChild(t) }, t.addEventListener = function(e, t, n) { return document.addEventListener(e, t, n),
            function() { document.removeEventListener(e, t, n) } }, t.getCurrentScript = function() { if (document.currentScript) return document.currentScript }, t.parentElement = function(e) { for (var t = e.parentNode; t.nodeType !== Node.ELEMENT_NODE;) t = t.parentNode; return t } }), (function(e, t, n) { var i, r, a = "optimizely_data",
        o = n(77).create,
        s = n(83),
        c = n(41),
        u = t.Error = o("StorageError"); try { r = c.getGlobal("localStorage") } catch (e) { throw new u("Unable to read localStorage: " + e.toString()) } if (!r) throw new u("localStorage is undefined");
    i = s.create(r, a), t.LocalStorage = i, t.isOptimizelyKey = function(e) { return e.slice(0, a.length) === a } }), (function(e, t, n) {
    function i(e, t) { this.$t = e, this.Qt = t } var r = n(2),
        a = n(23),
        o = "$$";
    i.prototype.Jt = function(e) { return [this.Qt, e].join(o) }, i.prototype.Zt = function(e) { return e.replace(this.Qt + o, "") }, i.prototype.setItem = function(e, t) { try { this.$t.setItem(this.Jt(e), t) } catch (t) { a.warn("Failed to save", e, "to localStorage:", t) } }, i.prototype.removeItem = function(e) { this.$t.removeItem(this.Jt(e)) }, i.prototype.getItem = function(e) { var t = null; try { t = this.$t.getItem(this.Jt(e)) } catch (e) {} return t }, i.prototype.keys = function() { var e = r.keys(this.$t); return r.map(r.filter(e, r.bind((function(e) { return r.includes(e, this.Qt) }), this)), r.bind(this.Zt, this)) }, i.prototype.allKeys = function() { return r.keys(this.$t) }, i.prototype.allValues = function() { return r.values(this.$t) }, e.exports = { create: function(e, t) { return new i(e, t) }, mockStorage: { keys: function() {}, getItem: function(e) {}, removeItem: function(e) {}, setItem: function(e, t) {} } } }), (function(e, t, n) {
    function i() { return c.getGlobal("performance") } var r = n(7),
        a = n(77).create,
        o = n(24),
        s = n(9),
        c = n(41),
        u = n(16),
        l = u.get("stores/rum"),
        d = "optimizely:",
        f = t.Error = a("PerformanceError");
    t.time = function(e) { if (l.getSampleRum()) { var t = i(); if (t && t.mark) { var n = d + e;
                t.clearMarks(n + "Begin"), t.mark(n + "Begin") } } }, t.timeEnd = function(e) { if (l.getSampleRum()) { var t = i(); if (t && t.mark) { var n = d + e,
                    a = t.getEntriesByName(n + "Begin"); if (0 === a.length) throw new f("Called timeEnd without matching time: " + e);
                t.clearMarks(n + "End"), t.mark(n + "End"); var o = t.getEntriesByName(n + "End"),
                    c = e + "Time",
                    u = o[0].startTime - a[0].startTime;
                s.dispatch(r.SET_PERFORMANCE_MARKS_DATA, { name: c, data: { startTime: Math.round(1e3 * a[0].startTime) / 1e3, duration: Math.round(1e3 * u) / 1e3 } }) } } }, t.now = function() { var e = i(); return e ? e.now() : o.now() } }), (function(e, t, n) {
    function i(e) { var t; if (!o.find(y.getFrames(), { origin: e.origin })) return void E.debug("XDomain", "No frame found for origin: " + e.origin); try { t = g.parse(e.data) } catch (t) { return void E.debug("XDomain", "Ignoring malformed message event:", e) } if ("ERROR" === t.type) l.dispatch(c.XDOMAIN_SET_DISABLED, { disabled: !0 }), d.emitInternalError(new I("Xdomain Error: " + t.response));
        else if ("SYNC" === t.type) o.each(y.getSubscribers(), (function(e) { e(t.response.key, t.response.value) }));
        else { var n = y.getMessageById(t.id); if (!n) { if (E.warn("XDomain", "No stored message found for ID", t.id), o.isNumber(t.id)) { var i = y.getNextMessageId();
                    t.id >= i ? d.emitInternalError(new I("Message ID is greater than expected maximum ID (" + t.id + ">" + i + ")")) : t.id < 0 ? d.emitInternalError(new I("Message ID is < 0: " + t.id)) : d.emitInternalError(new I("No stored message found for message ID: " + t.id)) } else d.emitInternalError(new I("Message ID is not a number: " + t.id)); return }
            n.resolver(t.response), l.dispatch(c.XDOMAIN_SET_MESSAGE, { messageId: t.id, message: o.extend({}, n, { endTime: p.now(), response: t.response }) }) } }

    function r(e, t) { return t || (t = y.getDefaultFrame()), new s(function(n) { var i = { data: o.extend({}, e, { id: y.getNextMessageId() }), resolver: n };
            t ? y.isDisabled() || a(i, t) : l.dispatch(c.XDOMAIN_SET_MESSAGE, { messageId: i.data.id, message: i }) }) }

    function a(e, t) { var n = e.data;
        l.dispatch(c.XDOMAIN_SET_MESSAGE, { messageId: e.data.id, message: o.extend({}, e, { startTime: p.now() }) }), t.target.postMessage(g.stringify(n), t.origin) } var o = n(2),
        s = n(12).Promise,
        c = n(7),
        u = n(16),
        l = n(9),
        d = n(86),
        f = n(77).create,
        p = n(24),
        h = n(81),
        g = n(26),
        v = n(88),
        m = n(75),
        _ = n(41),
        E = n(23),
        y = u.get("stores/xdomain"),
        I = t.Error = f("XDomainStorageError");
    t.setItem = function(e, t, n) { return r({ type: "PUT", key: e, value: t }, n) }, t.getItem = function(e, t) { return r({ type: "GET", key: e }, t) }, t.fetchAll = function(e) { return r({ type: "GETALL" }, e) }, t.deleteData = function(e, t) { return r({ type: "DELETE", visitorId: e }, t) }, t.subscribe = function(e) { l.dispatch(c.XDOMAIN_ADD_SUBSCRIBER, { subscriber: e }) }, t.loadIframe = function(e, t) { return new s(function(n) { var i = h.createElement("iframe");
            i.src = e + t, i.hidden = !0, i.setAttribute("aria-hidden", "true"), i.setAttribute("tabindex", "-1"), i.setAttribute("title", "Optimizely Internal Frame"), i.style.display = "none", i.height = 0, i.width = 0, i.onload = function() { var r = { id: y.getNextFrameId(), target: i.contentWindow, origin: e, path: t };
                l.dispatch(c.XDOMAIN_ADD_FRAME, r), n(r) }, h.appendTo(h.querySelector("body"), i) }) }, t.getXDomainUserId = function(e, t) { var n, i = {},
            r = o.keys(e); return o.each(t, (function(e) { i[e] = [], o.each(r, (function(t) { var r = m.getUserIdFromKey(t, e);!n && r && (n = r), r && !o.includes(i[e], r) && i[e].push(r) })) })), E.debug("XDomain: Found userIds:", i), n }, t.load = function(e, n) { _.addEventListener("message", i); var r = function() { return !!h.querySelector("body") },
            s = function() { return t.loadIframe(e, n) }; return v.pollFor(r).then(s).then((function(e) { l.dispatch(c.XDOMAIN_SET_DEFAULT_FRAME, e), y.isDisabled() || o.each(y.getMessages(), (function(t) { t.startTime || a(t, e) })) })) } }), (function(e, t, n) { var i = n(87);
    t.emitError = function(e, t, n) { var r = !0;
        i.emit({ type: "error", name: e.name || "Error", data: { error: e, metadata: t } }, n || !1, r) }, t.emitInternalError = function(e, n) { t.emitError(e, n, !0) }, t.emitAnalyticsEvent = function(e, t) { var n = { type: "analytics", name: "trackEvent", data: e };
        i.emit(n, t) } }), (function(e, t, n) { var i = n(2),
        r = n(5),
        a = n(7),
        o = n(86),
        s = n(16),
        c = n(9),
        u = n(23),
        l = s.get("stores/event_emitter");
    t.on = function(e) { return e.token || (e.token = r.generate()), c.dispatch(a.ADD_EMITTER_HANDLER, e), e.token }, t.off = function(e) { c.dispatch(a.REMOVE_EMITTER_HANDLER, { token: e }) }, t.emit = function(e, t, n) { var r = l.getHandlers(e, t);
        i.each(r, (function(i) { try { i.handler.call({ $di: s }, e) } catch (r) {!n && i.emitErrors ? (u.error("Error in handler for event:", e, r), o.emitError(r, null, t)) : u.warn("Suppressed error in handler for event:", e, r) } })) } }), (function(e, t, n) { var i = n(2),
        r = n(12).Promise,
        a = n(41),
        o = 100,
        s = 50;
    t.pollFor = function(e, t, n) { var c, u; return i.isFunction(n) ? u = n : (c = n || o, u = function() { return c--, c < -1 }), t = t || s, new r(function(n, i) {!(function r() { var o; if (!u()) { try { var s = e(); if (s) return n(s) } catch (e) { o = e } return a.setTimeout(r, t) }
                i(o || new Error("Poll timed out")) })() }) } }), (function(e, t, n) {
    function i(e, n) { var i;
        i = t.isInSameSession(e, n) ? e.getValueOrDefault([s.FIELDS.SESSION_ID]) : n.getValueOrDefault([s.FIELDS.TIME]), n.setFieldValue(s.FIELDS.SESSION_ID, i) }

    function r(e, n, i) { var r, a = e.getValueOrDefault([s.FIELDS.SESSION_INDEX]);
        r = t.isInSameSession(n, e) ? a : i ? a + 1 : a - 1, n.setFieldValue(s.FIELDS.SESSION_INDEX, r) } var a = n(63).Event,
        o = n(24),
        s = n(64),
        c = n(63).EventBase;
    t.CURRENT_SESSION_INDEX = 0; var u = 18e5;
    t.isInSameSession = function(e, t) { var n = e.getValueOrDefault([s.FIELDS.TIME], 0),
            i = t.getValueOrDefault([s.FIELDS.TIME], 0); return Math.abs(n - i) < u }, t.updateSessionId = function(e, t) { if (!e) return void t.setFieldValue(s.FIELDS.SESSION_ID, t.getValueOrDefault([s.FIELDS.TIME])); var n = e.getValueOrDefault([s.FIELDS.TIME]),
            r = e.getValueOrDefault([s.FIELDS.SESSION_ID]),
            o = t.getValueOrDefault([s.FIELDS.TIME]);
        n = "number" != typeof n ? o - 36e5 : n, r = "number" != typeof r ? n : r, e = new a(new c("", ""), n, r), i(e, t) }, t.updateSessionIndex = function(e, t) { if (!e) return void t.setFieldValue(s.FIELDS.SESSION_INDEX, 0); var n = e.getValueOrDefault([s.FIELDS.TIME]),
            i = e.getValueOrDefault([s.FIELDS.SESSION_INDEX]),
            o = t.getValueOrDefault([s.FIELDS.TIME]),
            u = e.getValueOrDefault([s.FIELDS.SESSION_ID]);
        n = "number" != typeof n ? o - 36e5 : n, i = "number" != typeof i ? 0 : i, u = "number" != typeof u ? n : u, e = new a(new c("", ""), n, u, i), r(e, t, !1) }, t.sessionize = function(e) { var n = e.length; if (0 !== n) { e[0].setFieldValue(s.FIELDS.SESSION_ID, e[0].getValueOrDefault([s.FIELDS.TIME])); for (var a = 1; a < n; a++) i(e[a - 1], e[a]); var c = t.CURRENT_SESSION_INDEX,
                l = e[n - 1].getValueOrDefault([s.FIELDS.TIME]),
                d = o.now();
            d - l > u && (c += 1), e[n - 1].setFieldValue(s.FIELDS.SESSION_INDEX, c); for (var a = n - 1; a > 0; a--) r(e[a], e[a - 1], !0) } }, t.reindexIfNecessary = function(e, t, n) {
        function i(e) { for (var t = 0; t < e.length; t++) { var n = e[t].getValueOrDefault([s.FIELDS.SESSION_INDEX]);
                e[t].setFieldValue(s.FIELDS.SESSION_INDEX, n + 1) } }
        e.getValueOrDefault([s.FIELDS.SESSION_INDEX]) === -1 && (i(t), i(n)) }, t.sessionSortPredicate = function(e, t) { return e[s.FIELDS.TIME] - t[s.FIELDS.TIME] }, t.applyMigrations = function(e) { return !1 } }), (function(e, t, n) { var i = n(12).Promise,
        r = n(41);
    t.estimateStorage = function() { var e = r.getGlobal("navigator"); try { return e.storage.estimate() } catch (e) { return i.resolve({ usage: null, quota: null }) } } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(24),
        o = n(9),
        s = n(26),
        c = n(23),
        u = n(12).Promise,
        l = n(92),
        d = 3;
    t.isCORSSupported = function() { var e = l.get("XMLHttpRequest"); return "withCredentials" in new e }, t.request = function(e) { return e = i.extend({ method: "GET", async: !0, contentType: "text/plain;charset=UTF-8" }, e), new u(function(n, r) { if (!t.isCORSSupported()) return r("CORS is not supported"); var a = l.get("XMLHttpRequest"),
                o = new a;
            o.onload = function() { e.success && e.success(o), n(o) }, o.onerror = function() { e.error && e.error(o), r(o) }, i.isObject(e.data) && (e.data = s.stringify(e.data)), o.open(e.method, e.url, e.async), e.withCredentials && (o.withCredentials = e.withCredentials), o.setRequestHeader("Content-Type", e.contentType), o.send(e.data) }) }, t.retryableRequest = function(e, n, s, l) { if (!n) return u.reject(new Error("No id specified for request.")); if (!t.isCORSSupported()) return u.reject(new Error("CORS is not supported."));
        i.isUndefined(l) && (l = d), i.isUndefined(s) && (s = 0); var f = { id: n, timeStamp: a.now(), data: e, retryCount: s }; return o.dispatch(r.SET_PENDING_EVENT, f), c.debug("Sending event ", n), t.request(e).then((function(e) { return o.dispatch(r.REMOVE_PENDING_EVENT, { id: n }), e }), (function(e) { throw f.retryCount >= l ? (o.dispatch(r.REMOVE_PENDING_EVENT, { id: n }), c.warn("Event ", f, " could not be sent after ", l, " attempts.")) : (f.retryCount++, o.dispatch(r.SET_PENDING_EVENT, f), c.debug("Event ", f, " failed to send, with error ", e, " It will be retried ", l - s, " times.")), e })) }, t.sendBeacon = t.request }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(24),
        o = n(16),
        s = n(81),
        c = n(25),
        u = n(9),
        l = n(23),
        d = o.get("stores/sandbox"),
        f = n(41);
    t.shouldSandbox = function() { return !1 }, t.get = function(e) { if (!e) throw new Error("Name is required"); if (t.shouldSandbox()) { d.isInitialized() || p(); var n = d.get(e); if (n) return n } return f.getGlobal(e) }; var p = function() { try { var e = "optimizely_" + a.now(),
                t = s.createElement("iframe");
            t.name = e, t.style.display = "none", s.appendToHead(t); var n = t.contentWindow,
                o = t.contentDocument;
            o.write("<script></script>"), o.close(); var d = i.mapValues(c.SandboxedFunctions, (function(e) { return n[e] }));
            u.dispatch(r.SANDBOXED_FUNCTIONS_ADDED, { sandboxedFunctions: d }), t.parentNode.removeChild(t) } catch (e) { l.warn("Unable to create a sandbox: ", e) } } }), (function(e, t, n) { var i = n(2),
        r = n(23),
        a = n(94),
        o = n(16),
        s = o.get("stores/plugins"),
        c = n(7),
        u = n(25),
        l = n(9),
        d = [n(107), n(108), n(129)],
        f = ["clientMetadata", "disable", "load", "optOut", "rum"];
    t.push = function(e, t) { var n, a, o, s; if (!i.isArray(e) && i.isObject(e)) s = i.isUndefined(e.version) ? 1 : e.version, n = e.type, o = [e];
        else if (i.isArray(e)) s = 0, n = e[0], o = e.slice(1);
        else { if (!i.isString(e)) return r.warn("API / Ignoring non-array/object/string argument:", e), !1;
            s = 0, n = e, o = [] } if (d[s] && (a = d[s][n]), t && f.indexOf(n) === -1) return r.debug("API / Ignoring non high priority function:", n, o), !1; if (!a) return r.warn('API / No function found for "' + n + '" (v' + s + ") with arguments:", o), !1;
        r.log('API / Executing: "' + n, '" with arguments:', o); try { a.apply(null, o), l.dispatch(c.RECORD_API_USAGE, { methodName: s ? "v" + s + "." + n : n }) } catch (e) { r.error(e) } return !0 }, t.get = function(e) { r.log('API / Getting module: "' + e + '"'); var t = a[e]; return t ? i.isArray(t) && (t = o.evaluate(t)) : t = s.getPlugin(u.PluginTypes.apiModules, e), t ? (l.dispatch(c.RECORD_API_USAGE, { methodName: "get." + e }), t) : void r.warn('Module "' + e + '" not found.') } }), (function(e, t, n) {
    function i(e, t, n, i) { var r = e.getLayerState(i),
            a = t.get(i),
            s = n.get(); if (!r || !a) return s ? { layer: { name: s.layerName, id: s.layerId, policy: s.layerPolicy, integrationStringVersion: s.integrationStringVersion }, experiment: { name: s.experimentName, id: s.experimentId }, variation: { name: s.variationName, id: s.variationId }, isLayerHoldback: !1 } : null; if (l.isSingleExperimentPolicy(a.policy) && r.decision.isLayerHoldback) return null; var c = r.decision.experimentId,
            u = r.decision.variationId; if (!c || !u) return null; var d, f; return (d = o.find(a.experiments, { id: c })) ? (f = o.find(d.variations, { id: u }), f ? { layer: { name: a.name, id: a.id, policy: a.policy, integrationStringVersion: a.integrationStringVersion }, experiment: { name: d.name, id: d.id }, variation: { name: f.name, id: f.id }, isLayerHoldback: r.decision.isLayerHoldback } : null) : null }

    function r(e, t, n, i, r, s) { var c = [],
            u = e.getLayerStates();
        s.onlySingleExperiments && (u = o.filter(u, (function(e) { var n = t.get(e.layerId); return n && l.isSingleExperimentPolicy(n.policy) }))); var f = o.map(u, (function(e) { var t = !!e.decision.variationId,
                    n = e.decisionActivationId && e.decisionActivationId === i.getActivationId(),
                    r = d.getExperimentAndVariation(),
                    a = r ? r.variationId : null,
                    s = t && e.decision.variationId === a; return o.extend(e, { isActive: t && n || s, visitorRedirected: s }) })),
            p = r ? o.filter(f, r) : f; return o.each(p, (function(e) { var i = a(e, t, n, s.includeOfferConsistency);
            i && c.push(i) })), c }

    function a(e, t, n, i) { var r, a, s = e.layerId,
            c = t.get(s) || {},
            u = o.map(c.experiments, (function(e) { return o.pick(e, ["id", "name"]) })); if (i || !c.decisionMetadata || !c.decisionMetadata.offerConsistency) { var l = { id: s, campaignName: c.name || null, experiment: null, allExperiments: u, variation: null, reason: e.decision.reason, isActive: e.isActive, visitorRedirected: e.visitorRedirected, isInCampaignHoldback: e.decision.isLayerHoldback };
            e.decision && e.decision.experimentId && (r = o.find(c.experiments, { id: e.decision.experimentId })), r && (l.experiment = o.pick(r, ["id", "name", "campaignName"])), r && e.decision.variationId && (a = o.find(r.variations, { id: e.decision.variationId })), a && (l.variation = o.pick(a, ["id", "name"])); var d = o.map(e.decisionTicket.audienceIds, (function(e) { return o.pick(n.get(e), ["id", "name"]) })); return l.audiences = d, c.decisionMetadata && c.decisionMetadata.offerConsistency && (l.pageId = e.pageId), l } }
    var o = n(2),
        s = n(95),
        c = n(96),
        u = n(98),
        l = n(45),
        d = n(99);
    t.data = ["stores/audience_data", "stores/client_metadata", "stores/event_data", "stores/layer_data", "stores/view_data", "stores/group_data", "stores/interest_group", "stores/tag_group", "stores/global", function(e, t, n, i, r, a, s, l, d) { var f = {},
            p = {},
            h = {},
            g = { audiences: e.getAudiencesMap(), events: n.getEventsMap(), campaigns: f, pages: r.getPagesMap(), experiments: p, variations: h, projectId: d.getProjectId(), snippetId: d.getSnippetId(), accountId: d.getAccountId(), dcpServiceId: d.getDCPServiceId(), revision: d.getRevision(), clientName: t.getClientName(), clientVersion: t.getClientVersion() },
            v = u.dereferenceChangeId; return o.each(i.getAll(), (function(e) { c.defineProperty(f, e.id, (function() { var t = o.extend({}, e); return c.defineProperty(t, "changes", (function() { return o.map(e.changes, v) }), "campaign"), c.defineProperty(t, "experiments", (function() { return o.map(e.experiments, (function(e) { return p[e.id] })) }), "campaign"), t }), "campaignMap", "byId"), o.each(e.experiments, (function(e) { c.defineProperty(p, e.id, (function() { var t = o.extend({}, e); return c.defineProperty(t, "changes", (function() { return o.map(e.changes, v) }), "experiment"), c.defineProperty(t, "variations", (function() { return o.map(e.variations, (function(e) { return h[e.id] })) }), "experiment"), t }), "experimentMap", "byId"), o.each(e.variations, (function(e) { c.defineProperty(h, e.id, (function() { var t = o.extend({}, e); return c.defineProperty(t, "actions", (function() { return o.map(e.actions, (function(e) { return o.extend({}, e, { changes: o.map(e.changes, v) }) })) }), "variation"), t }), "variationMap", "byId") })) })) })), g.groups = a.getGroupsMap(), g }], t.session = ["stores/session", function(e) { return e.getState() }], t.visitor = ["stores/visitor", function(e) { return o.cloneDeep(e.getVisitorProfile()) }], t.visitor_id = ["stores/visitor_id", function(e) { return { randomId: e.getRandomId() } }], t.state = ["stores/audience_data", "stores/layer_data", "stores/layer", "stores/view_data", "stores/view", "stores/global", "stores/observed_redirect", function(e, t, n, a, c, u, f) {
        return {
            getCampaignStates: function(i) { var a = {},
                    s = r(n, t, e, u, i, { includeOfferConsistency: !1 }); return o.each(s, (function(e) { a[e.id] = e })), a },
            getExperimentStates: function(i) {
                var a = r(n, t, e, u, i, { includeOfferConsistency: !1, onlySingleExperiments: !0 }),
                    s = ["audiences", "variation", "reason", "visitorRedirected", "isActive"],
                    c = o.reduce(a, (function(e, t) {
                        var n = t.allExperiments[0];
                        return e[n.id] = o.extend({}, o.pick(t, s), {
                            id: n.id,
                            experimentName: n.name,
                            isInExperimentHoldback: t.isInCampaignHoldback
                        }), e
                    }), {});
                return c
            },
            getCampaignStateLists: function(i) { var a = {},
                    s = r(n, t, e, u, i, { includeOfferConsistency: !0 }); return o.each(s, (function(e) { var t = e.id;
                    a[t] || (a[t] = []), a[t].push(e) })), a },
            getPageStates: function(e) { var t = c.getAll(),
                    n = o.reduce(t, (function(e, t) { var n = a.get(t.id); return e[t.id] = o.extend({}, o.pick(n, ["id", "name", "apiName", "category", "staticConditions", "tags"]), o.pick(t, ["isActive", "metadata"])), e }), {}); return e ? o.pickBy(n, e) : n },
            isGlobalHoldback: function() { return u.isGlobalHoldback() },
            getActivationId: function() { return u.getActivationId() },
            getVariationMap: function() { var e = n.getLayerStates(),
                    i = {}; return o.each(e, (function(e) { var n = t.get(e.layerId); if (e.decision && e.decision.experimentId && (i[e.decision.experimentId] = { id: e.decision.variationId, name: null, index: null }, n)) { var r = o.find(n.experiments, { id: e.decision.experimentId }); if (r && e.decision.variationId) var a = o.find(r.variations, { id: e.decision.variationId }),
                            s = o.findIndex(r.variations, { id: e.decision.variationId });
                        a && (i[e.decision.experimentId] = { id: e.decision.variationId, name: a.name, index: s }) } })), i },
            getActiveExperimentIds: function() { var e = {}; return o.each(this.getCampaignStateLists({ isActive: !0 }), (function(t) { o.each(t, (function(t) { e[t.experiment.id] = !0 })) })), o.keys(e) },
            getRedirectInfo: function() { var e = d.getExperimentAndVariation(); return e && (e.referrer = d.getReferrer()), e },
            getDecisionString: function(e) { if (!e) throw new Error("Must pass a config to getDecisionString");
                e = o.extend({ maxLength: 255, shouldCleanString: !1 }, e); var r = i(n, t, f, e.campaignId); return r ? s.generateAnalyticsString(r.layer, r.experiment, r.variation, r.isLayerHoldback, e.maxLength, e.shouldCleanString) : null },
            getDecisionObject: function(e) { if (!e) throw new Error("Must pass a config to getDecisionObject");
                e = o.extend({ maxLength: 255, shouldCleanString: !1 }, e); var r = i(n, t, f, e.campaignId); if (!r) return null; var a = s.formatNamesAndIdsForAnalytics(r.layer, r.experiment, r.variation, e.shouldCleanString),
                    c = o.mapValues(a.names, (function(t, n) { return s.combineAndTruncateIdAndName(t, a.idStrings[n], e.maxLength) })),
                    u = { experiment: c.experiment, variation: c.variation }; return l.isSingleExperimentPolicy(r.layer.policy) || o.extend(u, { campaign: c.layer, holdback: r.isLayerHoldback }), u }
        }
    }], t.utils = n(100).create(), t.jquery = ["env/jquery", function(e) { return e }], t.event_emitter = n(106)
}), (function(e, t, n) {
    function i(e) { return e.replace(/[^a-zA-Z0-9\.\~\!\*\(\)\']+/g, "_") }

    function r(e) { return !c.isEmpty(e) && c.includes(["and", "or", "not"], e[0]) }

    function a(e, t) { var n = ""; return c.isEmpty(t) ? n = d : (n = c.reduce(t, (function(t, n) { var r = e.get(n); return r ? t + i(r.name ? r.name : r.id) + "," : t }), ""), n = n.slice(0, -1)), n }

    function o(e, n, i, r, a, o) { if (!v.isSingleExperimentPolicy(e.policy) || !r) { var s = !v.isSingleExperimentPolicy(e.policy) && r,
                u = t.formatNamesAndIdsForAnalytics(e, n, i, o),
                d = [u.names.experiment, u.names.variation],
                p = [u.idStrings.experiment, u.idStrings.variation];
            v.isSingleExperimentPolicy(e.policy) || (d.unshift(u.names.layer), p.unshift(u.idStrings.layer)); var h = c.reduce(p, (function(e, t) { return e + t.length }), 0),
                g = d.length - 1 + (s ? 1 : 0),
                m = g * l.length,
                _ = h + m; if (s && (_ += f.length), _ > a) throw new Error("The analytics string size is too low to send the entity IDs."); for (var E = a - _, y = d.length, I = [], T = d.length - 1; T >= 0; T--) { var S = d[T],
                    A = Math.min(S.length, Math.floor(E / y));
                E -= A, y--, I.unshift(S.substring(0, A)) } var b = c.map(I, (function(e, t) { return e + p[t] })); return s && b.push(f), b.join(l) } }

    function s(e, n, i, r, a, o) { var s = r ? f : p,
            u = 3 * l.length,
            d = t.formatNamesAndIdsForAnalytics(e, n, i, o),
            h = d.names,
            g = d.idStrings,
            m = c.reduce(g, (function(e, t) { return e + t.length }), 0); if (m + u + s.length > a) throw new Error("The analytics string size is too low to send the campaign, experiment, and variation IDs."); var _ = a - m - u - s.length,
            E = {};
        E.variation = Math.min(h.variation.length, Math.floor(_ / 3)), _ -= E.variation, E.experiment = Math.min(h.experiment.length, Math.floor(_ / 2)), _ -= E.experiment, E.layer = _; var y = {};
        c.each(h, (function(e, t) { y[t] = e.substring(0, E[t]) })); var I = []; return v.isSingleExperimentPolicy(e.policy) || I.push(y.layer + g.layer), I = I.concat([y.experiment + g.experiment, y.variation + g.variation, s]), I.join(l) } var c = n(2),
        u = n(16),
        l = ":",
        d = "everyone_else",
        f = "holdback",
        p = "treatment",
        h = "",
        g = n(23),
        v = n(45);
    t.formatNamesAndIdsForAnalytics = function(e, t, n, o) { var s = { layer: e.name || h, experiment: t.name || h, variation: n.name || h }; if (o && (s = c.mapValues(s, i)), s.experiment === h && (!e.integrationStringVersion || 1 === e.integrationStringVersion))
            if (r(t.audienceIds)) s.experiment = "Exp";
            else { var l = u.get("stores/audience_data");
                s.experiment = a(l, t.audienceIds) }
        var d = { layer: "(" + i(e.id) + ")", experiment: "(" + i(t.id) + ")", variation: "(" + i(n.id) + ")" }; return { names: s, idStrings: d } }, t.combineAndTruncateIdAndName = function(e, t, n) { var i = n - t.length; if (i < 0 && (g.warn("maxLength must be at least long enough to fit the entity ID, which is length" + t.length + ". Defaulting to only use entity ID as name."), e = h), e === h) return t; if (e.length > i) { var r = Math.min(e.length, i); return e = e.substring(0, r), e + t } return e + " " + t }, t.generateAnalyticsString = function(e, t, n, i, r, a) { return e.integrationStringVersion && 2 === e.integrationStringVersion ? o(e, t, n, i, r, a) : s(e, t, n, i, r, a) } }), (function(e, t, n) { var i = n(97),
        r = n(7),
        a = n(9),
        o = n(23);
    t.defineProperty = function(e, t, n, s, c) { i(e, t, (function() { var e = ["prop", s, c || t].join("."); return o.debug('Evaluating getter: "' + e + '"'), a.dispatch(r.RECORD_API_USAGE, { methodName: e }), n() }), !0) } }), (function(e, t) { "use strict";

    function n(e, t, n, i) { Object.defineProperty(e, t, { get: function() { var e = n.call(this); return Object.defineProperty(this, t, { value: e, enumerable: !!i, writable: !0 }), e }, set: function(e) { return Object.defineProperty(this, t, { value: e, enumerable: !!i, writable: !0 }), e }, enumerable: !!i, configurable: !0 }) }
    e.exports = n }), (function(e, t, n) {
    function i(e) { var n = r.cloneDeep(e); return n.changes && (n.changes = r.map(n.changes, t.dereferenceChangeId)), n.experiments && r.each(n.experiments, (function(e) { e.changes && (e.changes = r.map(e.changes, t.dereferenceChangeId)), e.variations && r.each(e.variations, (function(e) { e.actions && r.each(e.actions, (function(e) { e.changes && (e.changes = r.map(e.changes, t.dereferenceChangeId)) })) })) })), n } var r = n(2),
        a = n(16),
        o = n(22),
        s = n(96),
        c = a.get("stores/change_data");
    t.translateDecisionToCampaignDecision = function(e) { return u(r.cloneDeep(e), { layerId: "campaignId", isLayerHoldback: "isCampaignHoldback" }) }, t.translateLayerEventToCampaignEvent = function(e) { var t = {}; return s.defineProperty(t, "campaign", (function() { var t = i(e.data.layer); return t }), "campaignEvent"), t.decisionTicket = e.data.decisionTicket, t.decision = this.translateDecisionToCampaignDecision(e.data.decision), t.audiences = e.data.audiences, { type: "lifecycle", name: "campaignDecided", data: t } }, t.translateViewActivatedToPageActivated = function(e) { return { type: "lifecycle", name: "pageActivated", data: { page: e.data.view } } }, t.dereferenceChangeId = function(e) { var t = c.getChange(e); return t ? o.safeReference(t) : e }; var u = function(e, t) { var n = r.omit(e, r.keys(t)); return r.each(t, (function(t, i) { n[t] = e[i] })), n } }), (function(e, t, n) { var i = n(2),
        r = n(16),
        a = r.get("stores/global_state"),
        o = r.get("stores/layer_data"),
        s = r.get("stores/observed_redirect");
    t.getReferrer = function() { var e = s.get(); return e ? e.referrer : i.isString(a.getEffectiveReferrer()) ? a.getEffectiveReferrer() : null }, t.getExperimentAndVariation = function() { var e = s.get(); if (e && i.isString(e.variationId)) return i.pick(e, ["experimentId", "variationId"]); if (i.isString(a.getEffectiveVariationId())) { var t = a.getEffectiveVariationId(),
                n = o.getExperimentByVariationId(t),
                r = n ? n.id : null; return { experimentId: r, variationId: t } } return null } }), (function(e, t, n) { var i = n(12).Promise,
        r = n(101).observeSelector,
        a = n(102).poll,
        o = n(104).waitForElement,
        s = n(105).waitUntil;
    t.create = function() { return { observeSelector: r, poll: a, Promise: i, waitForElement: o, waitUntil: s } } }), (function(e, t, n) {
    function i() { if (f.shouldObserveChangesIndefinitely()) { var e = { attributes: !0, childList: !0, subtree: !0, characterData: !0 },
                t = p.getDocumentElement(),
                n = new MutationObserver(function() { this.disconnect(), l.each(l.keys(_), a), this.observe(t, e) }); return function(i) { var r = _[i];
                n.observe(t, e), r.cancelObservation = function() { delete _[i], l.isEmpty(_) && n.disconnect() } } } return function(e) { var t = g.poll(l.partial(a, e));
            _[e].cancelObservation = function() { t(), delete _[e] } } }

    function r(e) { var t = _[e];
        t && t.cancelObservation && t.cancelObservation() }

    function a(e) { if (_[e]) { if (o(_[e])) return 0 === _[e].matchedCount && l.isFunction(_[e].options.onTimeout) && _[e].options.onTimeout(), void r(e); var t = document.querySelectorAll(_[e].selector);
            t.length && (l.each(t, (function(t) { t.en && t.en[e] || _[e].callbackQueue.push(t) })), s(e)) } }

    function o(e) { var t = e.options.timeout; if (null !== t)
            if ("function" == typeof t) try { return t() } catch (e) {} else if (Date.now() - e.startTime > t) return !0;
        return !1 }

    function s(e) { for (; _[e] && _[e].callbackQueue.length;) { var t = _[e].callbackQueue.shift(); if (c(t, e), _[e].matchedCount = _[e].matchedCount + 1, _[e].callback(t), _[e] && _[e].options.once) return void r(e) } }

    function c(e, t) { e.en || (e.en = {}), e.en[t] = !0 }

    function u(e) { try { document.querySelector(e) } catch (e) { return !1 } return !0 } var l = n(2),
        d = (n(7), n(16)),
        f = d.get("stores/directive"),
        p = n(81),
        h = (n(25), n(9), n(5).generate),
        g = n(102),
        v = n(41),
        m = (d.get("stores/rum"), { once: !1, onTimeout: null, timeout: null }),
        _ = {},
        E = function(e) {
            (E = i())(e) };
    t.observeSelector = function(e, t, n) { if (!u(e)) throw new Error("observeSelector expects a valid css selector as its first argument"); if (!l.isFunction(t)) throw new Error("observeSelector expects a function as its second argument"); if (n && (!l.isObject(n) || l.isFunction(n))) throw new Error("observeSelector expects an object as its third argument"); var i = h(); return n = l.assign({}, m, n || {}), _[i] = { callback: t, callbackQueue: [], matchedCount: 0, options: n, selector: e, startTime: Date.now() }, E(i), v.setTimeout(l.bind(a, null, i), 0), l.partial(r, i) } }), (function(e, t, n) {
    function i(e) { l[e] && a.each(l[e].callbacks, (function(e) { e.call(null) })) }

    function r(e, t) { l[t] && l[t].callbacks[e] && (delete l[t].callbacks[e], a.some(l[t].callbacks) || (clearInterval(l[t].id), delete l[t])) } var a = n(2),
        o = (n(7), n(16)),
        s = (n(25), n(9), n(5).generate),
        c = n(41),
        u = n(103).DEFAULT_INTERVAL,
        l = (o.get("stores/rum"), {});
    t.poll = function(e, t) { a.isNumber(t) || (t = u), l[t] || (l[t] = { callbacks: {}, id: c.setInterval(a.partial(i, t), t) }); var n = s(); return l[t].callbacks[n] = e, a.partial(r, n, t) }, t.cancelAll = function() { a.each(l, (function(e, t) { clearInterval(e.id), delete l[t] })) } }), (function(e, t) { e.exports = { DEFAULT_INTERVAL: 20 } }), (function(e, t, n) { var i = n(12).Promise,
        r = n(101).observeSelector;
    t.waitForElement = function(e) { return new i(function(t, n) { r(e, t, { once: !0 }) }) } }), (function(e, t, n) { var i = n(12).Promise,
        r = n(102).poll;
    t.waitUntil = function(e) { return new i(function(t, n) { if (e()) return void t(); var i = r((function() { e() && (i(), t()) })) }) } }), (function(e, t, n) { var i = n(87);
    t.on = function(e) { return e.publicOnly = !0, i.on(e) }, t.off = i.off, t.emit = function(e) { i.emit(e) } }), (function(e, t, n) {
    function i(e) { var t, n = {}; if (e)
            if (r(e)) t = Number(e);
            else { if ("object" != typeof e) throw new Error("tracker", "Revenue argument", e, "not a number."); if (n = a.extend({}, e), "revenue" in n) { if (!r(n["revenue"])) throw new Error("tracker", "Revenue value", n["revenue"], "not a number.");
                    t = Number(n["revenue"]), delete n["revenue"] } }
        return a.isUndefined(t) || (n.revenue = t), n }

    function r(e) { return a.isNumber(e) || a.isString(e) && Number(e) == e } var a = n(2),
        o = n(108);
    t.activateGeoDelayedExperiments = function(e, t) { t || (t = e.lists ? "odds" : "cdn3"), o.dataFromSource({ data: e, source: t }) }, t.activateSiteCatalyst = function(e) { e && e.sVariable && o.integrationSettings({ id: "adobe_analytics", settings: { sVariableReference: e.sVariable } }) }, t.bucketUser = t.bucketVisitor = function(e, t) { if (e && t) { var n = { experimentId: String(e) };
            t > 256 ? n.variationId = String(t) : n.variationIndex = String(t), o.bucketVisitor(n) } }, t.disable = function(e) { o.disable({ scope: e }) }, t.log = function(e) { a.isUndefined(e) && (e = !0), o.log({ level: e ? "INFO" : "OFF" }) }, t.optOut = function(e) { a.isUndefined(e) && (e = !0), o.optOut({ isOptOut: e }) }, t.setCookieDomain = function(e) { o.cookieDomain({ cookieDomain: e }) }, t.setCookieExpiration = function(e) { o.cookieExpiration({ cookieExpirationDays: e }) }, t.setDimensionValue = function(e, t) { var n = {};
        n[e] = t, o.user({ attributes: n }) }, t.setUserId = function(e) { o.user({ userId: e }) }, t.storeThirdPartyData = function(e, t) { o.dataFromSource({ source: e, data: t }) }, t.trackEvent = function(e, t) { o.event({ eventName: e, tags: i(t) }) } }), (function(e, t, n) {
    function i(e) { var t; return e.eventId && (t = y.create(e.eventId, e.eventName, "custom")), R.updateAllViewTags(),
            function() { var n = p.trackCustomEvent(e.eventName, e.tags, t);
                n ? b.log("API / Tracking custom event:", e.eventName, e.tags) : b.log("API / Not tracking custom event:", e.eventName) } }

    function r(e) { var t; return e.eventData && (t = y.create(e.eventData.id, e.eventData.apiName, "click", e.eventData)),
            function() { var e = p.trackClickEvent(t);
                e ? b.log("API / Tracking click event:", e) : b.log("API / Not tracking click event:", e) } }

    function a(e) { var t = e.eventData,
            n = A.createLayerState(t.layerId, t.experimentId, t.variationId),
            i = A.createSingle(t.layerId, t.experimentId, t.variationId); return function() { A.recordLayerDecision(n.layerId, n.decisionTicket, n.decision), b.log("API / Tracking decision event:", n), p.trackDecisionEvent(n.decision, n.decisionTicket, i) } }

    function o(e) { var t = R.create(e.eventData.id, e.eventData.apiName),
            n = R.createState(t.id); return function() { var e = p.trackViewActivation(t, n);
            e ? b.log("API / Tracking pageview event:", e) : b.log("API / Not tracking pageview event:", e) } } var s = n(2),
        c = n(7),
        u = n(93),
        l = n(94),
        d = n(109),
        f = n(25),
        p = n(110),
        h = n(117),
        g = n(6),
        v = n(77).create,
        m = n(24),
        _ = n(118),
        E = n(121),
        y = n(122),
        I = n(87),
        T = n(9),
        S = n(26),
        A = n(113),
        b = n(23),
        w = n(123),
        D = n(114),
        R = n(124),
        N = n(75),
        C = n(16),
        O = C.get("stores/dimension_data"),
        x = C.get("stores/view_data"),
        L = C.get("stores/visitor_id"),
        P = C.get("stores/layer_data"),
        k = C.get("stores/directive"),
        F = !1,
        M = 86400,
        V = 90,
        U = t.ApiListenerError = v("ApiListenerError");
    t.event = function(e) { var t; switch (e.eventType) {
            case "click":
                t = r(e); break;
            case "decision":
                t = a(e); break;
            case "pageview":
                t = o(e); break;
            case "custom":
            default:
                t = i(e) }
        L.getBucketingId() ? t() : T.dispatch(c.ADD_CLEANUP_FN, { lifecycle: f.Lifecycle.postActivate, cleanupFn: t }) }, t.clientMetadata = function(e) { F && (T.dispatch(c.SET_CLIENT_NAME, e.clientName), T.dispatch(c.SET_CLIENT_VERSION, e.clientVersion)) }, t.priorRedirectString = function(e) { F && D.load(e.value) }, t.microsnippetError = function(e) { if (F) { var t = e.errorData.metadata && e.errorData.metadata.err || {};
            t.name = e.errorData.code; var n = { engine: e.engine, msVersion: e.errorData.msVersion, requestId: e.errorData.requestId, projectId: e.errorData.projectId, snippetKey: e.errorData.snippetKey, args: e.errorData.args };
            E.handleError(t, n) } }, t.rum = function(e) { T.dispatch(c.SET_RUM_DATA, e.eventData) }, t.page = function(e) { var t = x.getByApiName(e.pageName); if (!t) throw new Error('Unknown page "' + e.pageName + '"'); var n = !e.hasOwnProperty("isActive") || e.isActive,
            i = function() { n ? R.activateViaAPI(t, e.tags) : (R.deactivate(t), b.log("API / Deactivated Page", R.description(t))) };
        L.getBucketingId() ? i() : T.dispatch(c.ADD_CLEANUP_FN, { lifecycle: f.Lifecycle.postViewsActivated, cleanupFn: i }) }, t.tags = function(e) { R.setGlobalTags(e.tags) }, t.user = function(e) { F && e.visitorId && (L.getBucketingId() ? (b.log("API / Setting visitor Id:", e.visitorId), N.setId({ randomId: e.visitorId })) : (b.log("API / Setting visitor Id for activation:", e.visitorId), T.dispatch(c.SET_VISITOR_ID_VIA_API, e.visitorId))), b.log("API / Setting visitor attributes:", e.attributes), s.each(e.attributes, (function(e, t) { var n, i = t,
                r = O.getById(t) || O.getByApiName(t);
            r && (i = r.id, n = r.segmentId || r.id); var a = function() { T.dispatch(c.SET_VISITOR_ATTRIBUTES, { attributes: [{ key: ["custom", i], value: { id: n, value: e }, metadata: { lastModified: m.now() } }] }) };
            L.getBucketingId() ? a() : T.dispatch(c.ADD_CLEANUP_FN, { lifecycle: f.Lifecycle.postVisitorProfileLoad, cleanupFn: a }) })) }, t.optOut = function(e) { var t = !e.hasOwnProperty("isOptOut") || e.isOptOut;
        _.setOptOut(t) }, t.cookieExpiration = function(e) { var t = e.cookieExpirationDays;
        t < V && (b.error('Argument "cookieExpirationDays"=', t, "less than minimum days:", V, ", setting to minimum."), t = V), b.log("API / Setting cookie age to", t, "days."), T.dispatch(c.SET_COOKIE_AGE, t * M) }, t.extendCookieLifetime = function(e) { e = s.extend({ isEnabled: !0 }, e), b.log("API / Setting cookie automatic lifetime extension to", e.isEnabled), T.dispatch(c.SET_COOKIE_AUTO_REFRESH, e.isEnabled) }, t.cookieDomain = function(e) { b.log("API / Setting cookie domain to", e.cookieDomain), T.dispatch(c.SET_COOKIE_DOMAIN, e.cookieDomain) }, t.disable = function(e) { if (e.scope) { if ("tracking" !== e.scope) throw new Error('Unknown "scope" for disable: ' + e.scope);
            b.log("API / Disabling tracking"), T.dispatch(c.LOAD_DIRECTIVE, { trackingDisabled: !0 }) } else b.log("API / Disabling everything"), T.dispatch(c.LOAD_DIRECTIVE, { disabled: !0 }) }, t.log = function(e) { var t = e.level,
            n = e.match;
        s.isUndefined(t) && (t = "INFO"), s.isUndefined(n) && (n = ""), b.setLogMatcher(n), b.setLogLevel(t) }, t.registerModule = function(e) { var t = "custom/" + e.moduleName; if (l[t] || u.get(t)) throw new Error('Module name "' + t + '" is reserved. Will not be registered as plugin.');
        w.registerApiModule(t, e.module) }, t.dataFromSource = function(e) { var t = e.source;
        g.makeAsyncRequest(t), g.resolveRequest(t, e.data) }, t.addListener = function(e) { if (!s.isFunction(e.handler)) throw new Error("A handler function must be supplied");
        e = s.omit(e, "type"), e.publicOnly = !0, e.emitErrors = !0; var t = e.handler;
        e.handler = function(e) { try { return t(e) } catch (e) { throw new U(e) } }, I.on(e) }, t.removeListener = function(e) { if (!e.token) throw new Error("Must supply a token to removeListener");
        I.off(e.token) }, t.load = function(e) { e.data = s.extend({}, e.data), d.normalizeClientData(e.data), T.dispatch(c.DATA_LOADED, { data: e.data }) }, t.integrationSettings = function(e) { if (!e.id) throw new Error("id is required"); if (!e.settings) throw new Error("settings is required");
        T.dispatch(c.SET_INTEGRATION_SETTINGS, s.extend({}, e.settings, { id: e.id })) }, t.bucketVisitor = function(e) { if (!e.variationId && s.isUndefined(e.variationIndex) || e.variationId && e.variationIndex) throw new Error("One of a variationId or a variationIndex is required."); if (!e.experimentId) throw new Error("An experimentId is required."); var t, n, i = e.campaignId; if (i) { if (t = P.get(i), !t) throw new Error("Could not find layer " + i) } else if (t = P.getLayerByExperimentId(e.experimentId), i = t.id, !i) throw new Error("Could not find layer for experiment " + e.experimentId); if (n = s.find(t.experiments, { id: e.experimentId }), !n) throw new Error("Could not find experiment " + e.experimentId + " in layer " + i); var r = e.variationId; if (s.isUndefined(e.variationIndex)) { if (!s.find(n.variations, { id: r })) throw new Error("Cound not find variation " + r + " in experiment " + e.experimentId) } else if (r = n.variations[e.variationIndex].id, !r) throw new Error("Could not find variation at index " + e.variationIndex + " in experiment " + e.experimentId);
        N.updateVariationIdMap(i, e.experimentId, r), L.getBucketingId() && N.persistVariationIdMap() }, t.waitForOriginSync = function(e) { if (!s.isArray(e.canonicalOrigins)) throw new Error("canonicalOrigins must be an array. Got: " + S.stringify(e.canonicalOrigins));
        s.each(e.canonicalOrigins, (function(e) { if (!s.isString(e)) throw new Error("Each item in canonicalOrigins must be a string. Found type " + typeof e) })), T.dispatch(c.XDOMAIN_SET_CANONICAL_ORIGINS, { canonicalOrigins: e.canonicalOrigins }) }, t.disableCrossOrigin = function() { b.log("API / cross origin tracking is DISABLED"), T.dispatch(c.XDOMAIN_SET_DISABLED, { disabled: !0 }) }, t.activate = function() { k.shouldActivate() ? h.emitActivateEvent() : b.debug("Not activating.") }, t.sendEvents = function() { h.emitSendEvents() }, t.holdEvents = function() { h.emitHoldEvents() } }), (function(e, t, n) { var i = n(2),
        r = n(25);
    t.normalizeClientData = function(e) {!e.listTargetingKeys && e.listTargetingCookies && (e.listTargetingKeys = i.map(e.listTargetingCookies, (function(e) { return { type: r.ListTargetingKeyTypes.COOKIE, key: e } })), delete e.listTargetingCookies) } }), (function(e, t, n) {
    function i(e, t) { var n = P.description(t),
            i = $.isExpectingRedirect(),
            r = $.getLayerId(); if (i && r === t.id) { var a = G.TrackLayerDecisionTimingFlags.preRedirectPolicy;
            e.timing = a, _(a, [G.PreRedirectPolicies.PERSIST_BEFORE_AND_TRACK_DURING_REDIRECT], e), k.log("Called trackLayerDecision for redirect Campaign", n, e) } else { var a = G.TrackLayerDecisionTimingFlags.nonRedirectPolicy;
            e.timing = a, _(a, [G.NonRedirectPolicies.TRACK_IMMEDIATELY], e), k.log("Called trackLayerDecision for non-redirect Campaign", n, e) } }

    function r(e, t, n, i, r, a) { var o = P.description(a),
            s = d(e, t, n, i);
        v("onLayerDecision", s, r ? "trackLayerDecision" : void 0), k.log("Analytics / Called onLayerDecision for Campaign", o, s) }

    function a(e, t, n) { var i = f({ activeViewStates: q.getActiveViewStates(), visitorProfile: Q.getVisitorProfile(), layerStates: K.getLayerStatesForAnalytics() }),
            r = n && n.pageId ? h(n) : q.getActiveViewTags(),
            a = y.extend({}, r, t),
            o = n && n.category ? n.category : N.OTHER; return y.extend(i, { eventEntityId: n && n.id, eventApiName: e, eventCategory: o, eventTags: a }) }

    function o(e, t) { var n = f({ activeViewStates: q.getActiveViewStates(), visitorProfile: Q.getVisitorProfile(), layerStates: K.getLayerStatesForAnalytics() }); return y.extend(n, { pageId: e.id, viewCategory: e.category, eventTags: t.metadata }) }

    function s(e) { var t = f({ activeViewStates: q.getActiveViewStates(), visitorProfile: Q.getVisitorProfile(), layerStates: K.getLayerStatesForAnalytics() }),
            n = e.config && e.config.selector ? e.config.selector : e.eventFilter.selector,
            i = e.apiName,
            r = e.category || N.OTHER,
            a = e.id,
            o = h(e); return y.extend(t, { eventApiName: i, eventCategory: r, eventEntityId: a, eventTags: o, pageId: e.pageId, selector: n }) }

    function c() { var e = f({ activeViewStates: [], visitorProfile: Q.getVisitorProfile(), layerStates: K.getLayerStatesForAnalytics() }); return y.extend(e, { eventTags: {} }) }

    function u(e, t, n, i) { var r = null,
            a = null,
            o = null; if (t.experimentId) { var s = y.find(i.experiments, { id: t.experimentId }); if (s && (r = s.name || null, o = s.integrationSettings, t.variationId)) { var c = y.find(s.variations, { id: t.variationId });
                c && (a = c.name || null) } } var u = M.getReferrer() || D.getReferrer(),
            l = { sessionId: z.getSessionId(), decisionTicketAudienceIds: n.audienceIds, visitorId: ee.getRandomId(), decisionId: e, activationId: H.getActivationId(), namespace: H.getNamespace(), timestamp: b.now(), pageId: n.pageId || null, variationId: t.variationId, variationName: a, experimentId: t.experimentId, experimentName: r, layerId: t.layerId, layerName: i.name, layerPolicy: i.policy, accountId: H.getAccountId(), projectId: H.getProjectId(), revision: String(H.getRevision()), clientName: j.getClientName(), clientVersion: j.getClientVersion(), referrer: u, integrationStringVersion: i.integrationStringVersion || 1, integrationSettings: y.extend({}, i.integrationSettings, o) }; return l }

    function l(e, t, n) { var i = y.extend({}, e, { isLayerHoldback: t, isGlobalHoldback: !1, clientName: y.isNull(e.clientName) ? A.NAME : e.clientName, integrationStringVersion: y.isNull(e.integrationStringVersion) ? 1 : e.integrationStringVersion, anonymizeIP: y.isNull(H.getAnonymizeIP()) ? void 0 : H.getAnonymizeIP(), activationId: H.getActivationId(), decisionTicketAudienceIds: [], sessionId: z.getSessionId(), activeViewStates: [], userFeatures: g(n), layerStates: K.getLayerStatesForAnalytics() }); return i }

    function d(e, t, n, i) { return { decisionId: e, timestamp: b.now(), revision: H.getRevision(), clientName: j.getClientName(), clientVersion: j.getClientVersion(), projectId: H.getProjectId(), accountId: H.getAccountId(), activationId: H.getActivationId(), sessionId: z.getSessionId(), visitorId: ee.getRandomId(), decision: t, decisionTicket: n, userFeatures: g(i), layerStates: K.getLayerStatesForAnalytics() } }

    function f(e) { var t = M.getReferrer() || D.getReferrer(),
            n = { eventId: B.generate(), timestamp: b.now(), revision: H.getRevision(), clientName: j.getClientName(), clientVersion: j.getClientVersion(), projectId: H.getProjectId(), accountId: H.getAccountId(), activationId: H.getActivationId(), sessionId: z.getSessionId(), isGlobalHoldback: H.isGlobalHoldback(), namespace: H.getNamespace(), referrer: t, visitorId: ee.getRandomId(), activeViewStates: e.activeViewStates, layerStates: e.layerStates, userFeatures: g(e.visitorProfile) }; return n }

    function p(e) { var t = q.getViewState(e),
            n = t && t.isActive ? t.metadata : {}; return n }

    function h(e) { var t = {}; return e.pageId ? p(e.pageId) : t }

    function g(e) { var t = Y.getAllPlugins(R.PluginTypes.visitorProfileProviders),
            n = y.filter(t, { shouldTrack: !0 }),
            i = { id: null, type: null, name: "", value: null, shouldIndex: !0 }; return y.reduce(n, (function(t, n) { try { var r = n.provides,
                    a = e[r],
                    o = []; if (!y.isUndefined(a)) { y.isObject(a) ? o = y.map(a, (function(e, t) { var n = y.isObject(e) ? e : { value: e }; return y.extend({}, { type: r, name: t }, n) })) : o.push({ type: r, value: a }); var s = y(o).map((function(e) { return y.pick(y.extend({}, i, e), y.keys(i)) })).filter((function(e) { return !!e.value })).value();
                    t = t.concat(s) } } catch (e) { k.warn("Error evaluating userFeature against visitorProfile:", e) } return t }), []) }

    function v(e, t, n) { var i = m(e, n);
        k.debug("Found " + i.length + " analytics integrations defining a " + e + " hook"), k.debug("Calling each with data: ", t), y.each(i, (function(e) { try { k.debug("Calling plugin: " + e.name), e.hookFn(t), k.debug("Called plugin: " + e.name) } catch (e) { k.error(e) } })) }

    function m(e, t) { var n = []; return y.each(Y.getAllPlugins(R.PluginTypes.analyticsTrackers), (function(i, r) { if (i[e] && (!t || !i[t])) try { n.push({ name: r, hookFn: w.evaluate(i[e]) }) } catch (e) { k.error(e) } })), n }

    function _(e, t, n) { var i = E(e, t);
        k.debug("Found " + i.length + " analytics integrations  defining a trackLayerDecision " + e + " timing of " + t.join("|")), k.debug("Calling each with data: ", n), y.each(i, (function(e) { try { k.debug("Calling plugin: " + e.name), e.hookFn(n), k.debug("Called plugin: " + e.name) } catch (e) { k.error(e) } })) }

    function E(e, t) { var n = []; return y.each(Y.getAllPlugins(R.PluginTypes.analyticsTrackers), (function(i, r) { y.includes(t, i[e]) && n.push({ name: r, hookFn: i.trackLayerDecision }) })), n } var y = n(2),
        I = n(7),
        T = n(86),
        S = n(72),
        A = n(32),
        b = n(24),
        w = n(16),
        D = n(81),
        R = n(25),
        N = n(74),
        C = n(87),
        O = n(111),
        x = n(112),
        L = n(9),
        P = n(113),
        k = n(23),
        F = (n(84), n(12).Promise),
        M = n(99),
        V = n(114),
        U = n(116),
        G = n(115),
        B = n(5),
        j = w.get("stores/client_metadata"),
        H = w.get("stores/global"),
        z = w.get("stores/session"),
        q = w.get("stores/view"),
        Y = w.get("stores/plugins"),
        K = w.get("stores/layer"),
        W = w.get("stores/layer_data"),
        X = w.get("stores/observed_redirect"),
        $ = w.get("stores/pending_redirect"),
        Q = w.get("stores/visitor"),
        J = w.get("stores/directive"),
        Z = w.get("stores/event_data"),
        ee = w.get("stores/visitor_id"),
        te = "COOKIE",
        ne = !0,
        ie = 1e3;
    t.trackClientActivation = function() { if (J.shouldSendTrackingData()) { var e = c(); return v("onClientActivation", e), e } }, t.trackCustomEvent = function(e, t, n) { t = t || {}, n || (n = Z.getByApiName(e)); var i = a(e, t, n),
            r = { name: e, type: x.CUSTOM, category: i.eventCategory, tags: y.omit(i.eventTags, "revenue") }; if (y.isUndefined(t.revenue) || (r.revenue = t.revenue), T.emitAnalyticsEvent({ name: n ? n.name || n.apiName : e, apiName: n ? n.apiName : void 0, type: x.CUSTOM, tags: y.omit(i.eventTags, "revenue"), category: i.eventCategory, metrics: r.revenue ? { revenue: r.revenue } : {} }, !J.shouldSendTrackingData()), J.shouldSendTrackingData()) return S.addEvent(r), v("onCustomEvent", i), i }, t.trackDecisionEvent = function(e, t, n) { n || (n = W.get(e.layerId)); var a = B.generate();
        L.dispatch(I.RECORD_LAYER_DECISION_EVENT_ID, { layerId: e.layerId, pageId: t.pageId, decisionId: a }); var o; if (te) { o = u(a, e, t, n); var s = $.isExpectingRedirect(),
                c = $.getLayerId(); if (s && c === n.id) { V.persist(o, te); var d = P.description(n);
                k.log("Relaying decision for redirect Campaign", d, P.description(n)) } } if (!J.shouldSendTrackingData()) return void k.log("Analytics / Not tracking decision for Campaign", P.description(n)); var f = Q.getVisitorProfile(); if (te) { var p = l(o, e.isLayerHoldback, f);
            i(p, n), r(a, e, t, f, !0, n) } else r(a, e, t, f, !1, n) }, t.trackPostRedirectDecisionEvent = function() { if (!J.shouldSendTrackingData()) return F.resolve(); if (X.hasTracked()) return F.resolve(); var e = X.get(); if (!e) return F.resolve(); var t = Q.getVisitorProfile(),
            n = l(e, !1, t),
            i = G.TrackLayerDecisionTimingFlags.postRedirectPolicy; if (n.timing = i, _(i, [G.PostRedirectPolicies.TRACK_IMMEDIATELY], n), ne) { var r = new F(function(e, t) { var n = C.on({ filter: { type: O.TYPES.LIFECYCLE, name: "originsSynced" }, handler: function() { e(), C.off(n) } }) }),
                a = U.makeTimeoutPromise(ie); return F.race([r, a]).then((function() { k.log("Calling trackers after successful sync") }), (function(e) { k.warn("Calling trackers after failed sync:", e) })).then((function() { t = Q.getVisitorProfile(), n = l(e, !1, t), n.timing = G.TrackLayerDecisionTimingFlags.postRedirectPolicy, _(G.TrackLayerDecisionTimingFlags.postRedirectPolicy, [G.PostRedirectPolicies.TRACK_AFTER_SYNC], n), L.dispatch(I.REGISTER_TRACKED_REDIRECT_DATA) }))["catch"]((function(e) { k.error("Error when calling trackers after sync:", e) })) } return _(G.TrackLayerDecisionTimingFlags.postRedirectPolicy, [G.PostRedirectPolicies.TRACK_AFTER_SYNC], n), L.dispatch(I.REGISTER_TRACKED_REDIRECT_DATA), F.resolve() }, t.trackClickEvent = function(e) { var t = s(e),
            n = { name: e.apiName, type: e.eventType, category: t.eventCategory, tags: t.eventTags }; if (T.emitAnalyticsEvent({ name: e.name || e.apiName, apiName: e ? e.apiName : void 0, type: e.eventType, category: t.eventCategory, tags: t.eventTags, metrics: {} }, !J.shouldSendTrackingData()), J.shouldSendTrackingData()) return S.addEvent(n), v("onClickEvent", t), t }, t.trackViewActivation = function(e, t) { if (t || (t = q.getViewState(e.id)), !t.isActive) return void k.debug("Inactive view passed to `trackViewActivation`"); var n = o(e, t),
            i = { name: e.apiName, type: x.PAGEVIEW, category: n.viewCategory, tags: n.eventTags }; return T.emitAnalyticsEvent({ name: e.name || e.apiName, apiName: e.apiName, type: x.PAGEVIEW, category: n.viewCategory, tags: n.eventTags, metrics: {} }, !J.shouldSendTrackingData()), J.shouldSendTrackingData() ? (S.addEvent(i), L.dispatch(I.TRACK_VIEW_ACTIVATED_EVENT, { pageId: e.id, eventData: n }), v("onPageActivated", n), n) : void 0 } }), (function(e, t) { t.TYPES = { ACTION: "action", ANALYTICS: "analytics", EDITOR: "editor", LIFECYCLE: "lifecycle" } }), (function(e, t) { e.exports = { CLICK: "click", CUSTOM: "custom", ENGAGEMENT: "engagement", PAGEVIEW: "pageview" } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(24),
        o = n(16),
        s = n(9),
        c = o.get("stores/global"),
        u = o.get("stores/session"),
        l = 2e3;
    t.recordLayerDecision = function(e, t, n) { return s.dispatch(r.RECORD_LAYER_DECISION, { layerId: e, decision: n, decisionTicket: t, sessionId: u.getSessionId(), activationId: c.getActivationId(), timestamp: a.now(), revision: c.getRevision(), namespace: c.getNamespace(), pageId: t.pageId }), n }, t.relatedAudienceIds = function(e) { var t = {},
            n = ["and", "or", "not"]; return i.each(e.experiments, (function(e) { i.each(i.flattenDeep(e.audienceIds), (function(e) { i.includes(n, e) || (t[e] = !0) })) })), i.keys(t) }, t.getActivationTimeout = function(e) { var t = e.activation; return t && null !== t.timeout && void 0 !== t.timeout ? t.timeout : l }, t.description = function(e) { return (e.name ? '"' + e.name + '" ' : "") + "(" + e.id + ")" }, t.createSingle = function(e, t, n) { var i = { id: e, policy: "single_experiment", holdback: 0, experiments: [{ id: t, variations: [{ id: n, actions: [] }] }] }; return i }, t.createLayerState = function(e, t, n) { var i = { layerId: e, decision: { layerId: e, experimentId: t, variationId: n, isLayerHoldback: !1 }, decisionTicket: { audienceIds: [] } }; return i } }), (function(e, t, n) {
    function i(e) { try { var t = r(e) } catch (e) { return m.error("Relay / Error computing redirect relay cookie: ", e), void p.emitError(e) }
        m.debug("Relay / Setting redirect relay cookie:", t); try { f.set(g.COOKIES.REDIRECT, t, { maxAge: 5, encodeValue: !1 }) } catch (e) { m.error("Relay / Failed to set redirect relay cookie", e), p.emitError(e) } }

    function r(e) { var t = [],
            n = l.reduce(e, (function(e, n, i) { var r = T[i]; return r ? (r.isMulti ? l.forEach(n, (function(t, n) { t = r.valueToValueString ? r.valueToValueString(t, n) : String(t), l.isNull(t) || (t = (r.encodeValueString || encodeURIComponent)(t), e.push(encodeURIComponent(r.relayName + y + n) + "=" + t)) })) : l.isNull(n) || (n = (r.valueToValueString || String)(n), n = (r.encodeValueString || encodeURIComponent)(n), e.push(r.relayName + "=" + n)), e) : (t.push(i), e) }), []); if (t.length) throw new Error("Relay / Don't know how to relay some fields:", t); return n.sort(), n.join("&") }

    function a() { var e = f.get(g.COOKIES.REDIRECT, !1); if (e) return m.log("Relay / Found redirect cookie:", e), e }

    function o(e) {
        var t = {},
            n = e.split("&");
        return l.forEach(n, (function(e) {
            var n = e.split("=");
            if (2 !== n.length) return void m.warn("Relay / Skipping invalid segment:", e);
            var i = f.safeDecodeURIComponent(n[0]),
                r = S[i];
            if (!r && (r = l.find(I, (function(e) { return e.isMulti && 0 === i.indexOf(e.relayName + y) })), !r)) return void m.warn("Relay / Skipping segment with unknown field identifier:", e, i);
            var a = n[1];
            try {
                if (r.isMulti) { t[r.name] = t[r.name] || {}; var o = i.substring(r.relayName.length + y.length);
                    a = (r.decodeValueString || f.safeDecodeURIComponent)(a), a = (r.valueFromValueString || l.identity)(a, o), t[r.name][o] = a } else a = (r.decodeValueString || f.safeDecodeURIComponent)(a),
                    a = (r.valueFromValueString || l.identity)(a), t[r.name] = a
            } catch (t) { return m.warn("Relay / Skipping segment due to decode or parse error:", e, t), void p.emitError(t) }
        })), t
    }

    function s(e, t) { var n = null; if (e) { var i = E.getPlugin(g.PluginTypes.analyticsTrackers, t); if (i && l.isFunction(i.serializeSettings)) try { n = i.serializeSettings(e) } catch (e) { m.warn("Analytics / Failed to persist integrationSettings for plugin:", t, e) } } return n }

    function c(e, t) { var n = null,
            i = E.getPlugin(g.PluginTypes.analyticsTrackers, t); if (i && l.isFunction(i.deserializeSettings)) try { n = i.deserializeSettings(e) } catch (e) { m.warn("Analytics / Failed to persist integrationSettings for plugin:", t, e) }
        return n }

    function u(e) { var t = e.pageId || void 0;
        v.dispatch(d.RECORD_LAYER_DECISION, { layerId: e.layerId, decision: { layerId: e.layerId, experimentId: e.experimentId, variationId: e.variationId, isLayerHoldback: !1 }, decisionTicket: { audienceIds: e.decisionTicketAudienceIds, bucketingId: e.visitorId, globalHoldback: 0, preferredVariationMap: void 0, pageId: t, activationId: e.activationId }, sessionId: e.sessionId, activationId: e.activationId, timestamp: e.timestamp, revision: e.revision, namespace: e.namespace, pageId: t }), v.dispatch(d.RECORD_LAYER_DECISION_EVENT_ID, { layerId: e.layerId, pageId: t, decisionId: e.decisionId }), v.dispatch(d.ACTION_EXECUTED, { sessionId: e.sessionId, layerId: e.layerId, pageId: e.pageId, timestamp: e.timestamp, activationId: e.activationId }) }
    var l = n(2),
        d = n(7),
        f = n(76),
        p = n(86),
        h = n(16),
        g = n(25),
        v = n(9),
        m = n(23),
        _ = n(115),
        E = h.get("stores/plugins"),
        y = ".",
        I = [{ name: "sessionId", relayName: "s" }, { name: "decisionTicketAudienceIds", relayName: "as", valueToValueString: function(e) { return l.map(e, encodeURIComponent).join(",") }, encodeValueString: l.identity, decodeValueString: l.identity, valueFromValueString: function(e) { return l.map(e.split(","), f.safeDecodeURIComponent) } }, { name: "decisionId", relayName: "d" }, { name: "activationId", relayName: "aId" }, { name: "pageId", relayName: "vId", isNullable: !0 }, { name: "variationId", relayName: "v", isNullable: !0 }, { name: "referrer", relayName: "r" }, { name: "timestamp", relayName: "t", valueFromValueString: Number }, { name: "visitorId", relayName: "i" }, { name: "projectId", relayName: "p" }, { name: "revision", relayName: "n" }, { name: "clientName", relayName: "cN", isNullable: !0 }, { name: "clientVersion", relayName: "cV" }, { name: "namespace", relayName: "ns" }, { name: "accountId", relayName: "a" }, { name: "layerId", relayName: "l" }, { name: "layerName", relayName: "lN", isNullable: !0 }, { name: "layerPolicy", relayName: "lP" }, { name: "experimentId", relayName: "x", isNullable: !0 }, { name: "experimentName", relayName: "xN", isNullable: !0 }, { name: "variationName", relayName: "vN", isNullable: !0 }, { name: "integrationStringVersion", relayName: "isv", valueFromValueString: Number, isNullable: !0 }, { name: "integrationSettings", relayName: "iS", isMulti: !0, valueToValueString: s, valueFromValueString: c, isNullable: !0 }],
        T = {},
        S = {};
    l.forEach(I, (function(e) { T[e.name] = e, S[e.relayName] = e })), t.persist = function(e, t) { t === _.RedirectRelayMedia.COOKIE ? i(e) : m.error("Relay / Unsupported redirect relay medium: " + t) }, t.load = function(e) { if (e || (e = a()), e) { var t = o(e); if (t) { var n = []; return l.forEach(I, (function(e) {
                    (l.isNull(t[e.name]) || l.isUndefined(t[e.name])) && (e.isNullable ? t[e.name] = null : (delete t[e.name], n.push(e.name))) })), n.length ? void m.error("Relay / Observed redirect data with missing fields:", n) : (v.dispatch(d.LOAD_REDIRECT_DATA, t), v.dispatch(d.ADD_CLEANUP_FN, { lifecycle: g.Lifecycle.postVisitorProfileLoad, cleanupFn: function() { u(t) } }), t) } } }
}), (function(e, t, n) { var i = n(8);
    t.TrackLayerDecisionTimingFlags = i({ preRedirectPolicy: null, postRedirectPolicy: null, nonRedirectPolicy: null }), t.PreRedirectPolicies = i({ PERSIST_BEFORE_AND_TRACK_DURING_REDIRECT: null, PERSIST_BEFORE_REDIRECT: null }), t.PostRedirectPolicies = i({ TRACK_IMMEDIATELY: null, TRACK_AFTER_SYNC: null }), t.NonRedirectPolicies = i({ TRACK_IMMEDIATELY: null }), t.RedirectRelayMedia = i({ COOKIE: null }) }), (function(e, t, n) { var i = n(12).Promise,
        r = n(41);
    t.makeTimeoutPromise = function(e) { return new i(function(t, n) { r.setTimeout((function() { n(new Error("Timed out after " + e + " ms")) }), e) }) } }), (function(e, t, n) {
    function i(e) { var t = ["type", "selector", "attributes", "value"],
            n = r.extend({}, e); return n.changeSet = r.map(e.changeSet, (function(e) { return r.pick(l.dereferenceChangeId(e), t) })), n } var r = n(2),
        a = n(16),
        o = a.get("stores/audience_data"),
        s = n(87),
        c = n(111),
        u = n(96),
        l = n(98);
    t.emitLayerDecided = function(e) { var t = e.decisionTicket ? e.decisionTicket.audienceIds : [],
            n = r.map(t, (function(e) { return { id: e, name: o.get(e).name } })),
            i = { type: c.TYPES.LIFECYCLE, name: "layerDecided", data: r.extend(e, { audiences: n }) },
            a = l.translateLayerEventToCampaignEvent(i);
        s.emit(i), s.emit(a) }, t.emitViewActivated = function(e) { var t = { type: c.TYPES.LIFECYCLE, name: "viewActivated", data: e },
            n = l.translateViewActivatedToPageActivated(t);
        s.emit(t), s.emit(n) }, t.emitViewsActivated = function(e) { var t = { type: c.TYPES.LIFECYCLE, name: "viewsActivated", data: e };
        s.emit(t) }, t.emitPageDeactivated = function(e) { var t = { type: c.TYPES.LIFECYCLE, name: "pageDeactivated", data: e };
        s.emit(t) }, t.emitActivateEvent = function() { s.emit({ type: c.TYPES.LIFECYCLE, name: "activate" }, !0) }, t.emitActivatedEvent = function() { s.emit({ type: c.TYPES.LIFECYCLE, name: "activated" }) }, t.emitInitializedEvent = function() { var e = { type: c.TYPES.LIFECYCLE, name: "initialized" };
        window.optimizely && (window.optimizely.initialized = !0), s.emit(e) }, t.emitOriginsSyncedEvent = function() { var e = { type: c.TYPES.LIFECYCLE, name: "originsSynced" };
        s.emit(e) }, t.emitActionAppliedEvent = function(e) { var t = { type: e.type, campaignId: e.layerId, pageId: e.pageId, experimentId: e.experimentId, variationId: e.variationId };
        u.defineProperty(t, "changes", (function() { return i(e).changeSet }), "actionAppliedEvent"); var n = { type: c.TYPES.ACTION, name: "applied", data: t };
        s.emit(n) }, t.emitActionsForDecisionAppliedEvent = function(e, t) { var n = { decision: e };
        u.defineProperty(n, "actions", (function() { return r.map(t, i) }), "appliedAllForDecisionEvent"); var a = { type: c.TYPES.ACTION, name: "appliedAllForDecision", data: n };
        s.emit(a) }, t.emitSendEvents = function() { var e = { type: c.TYPES.ANALYTICS, name: "sendEvents" };
        s.emit(e) }, t.emitHoldEvents = function() { var e = { type: c.TYPES.ANALYTICS, name: "holdEvents" };
        s.emit(e) } }), (function(e, t, n) {
    function i() { var e = Boolean(E.result(window.optimizely, "initialized"));
        b.dispatch(y.LOAD_DIRECTIVE, { alreadyInitialized: e }) }

    function r() { b.dispatch(y.LOAD_DIRECTIVE, { mutationObserverAPISupported: C.isMutationObserverAPISupported() }) }

    function a() { var e = C.getUserAgent() || ""; if (!E.isString(e)) return void w.warn("Directive / userAgent not a string");
        e = e.toLowerCase(); var t = ["googlebot", "yahoo! slurp", "bingbot", "bingpreview", "msnbot", "keynote", "ktxn", "khte", "gomezagent", "alertsite", "yottaamonitor", "pingdom.com_bot", "aihitbot", "baiduspider", "adsbot-google", "mediapartners-google", "applebot", "catchpoint", "phantomjs", "moatbot", "facebookexternalhit"],
            n = function(t) { if (E.includes(e, t)) return w.warn("Directive / Matches bot:", t), !0 };
        E.some(t, n) && (w.log("Directive / Disabling tracking"), b.dispatch(y.LOAD_DIRECTIVE, { trackingDisabled: !0 })) }

    function o() { var e = T.get(A.COOKIES.OPT_OUT),
            n = R.getQueryParamValue(O.OPT_OUT),
            i = "You have successfully opted out of Optimizely for this domain.",
            r = "You are NOT opted out of Optimizely for this domain.",
            a = "true" === n || "false" === n;
        e ? a && n !== e ? t.setOptOut("true" === n) : b.dispatch(y.LOAD_DIRECTIVE, { shouldOptOut: "true" === e }) : "true" === n && t.setOptOut(!0), a && C.alert("true" === n ? i : r) }

    function s() { var e = !1,
            t = [O.AB_PREVIEW, O.DISABLE];
        t.push(O.EDITOR); for (var n = 0; n < t.length; n++)
            if ("true" === R.getQueryParamValue(t[n])) { w.warn("Directive / Not activating because " + t[n] + " is set."), e = !0; break }
        b.dispatch(y.LOAD_DIRECTIVE, { disabled: e }) }

    function c() { b.dispatch(y.LOAD_DIRECTIVE, { isPreview: !1 }) }

    function u() { var e = R.getQueryParamValue(O.LEGACY_PREVIEW);
        e && w.log("Directive / Is legacy preview mode"), b.dispatch(y.LOAD_DIRECTIVE, { isLegacyPreview: !!e }) }

    function l() { b.dispatch(y.LOAD_DIRECTIVE, { isEditor: !1 }) }

    function d() { b.dispatch(y.LOAD_DIRECTIVE, { isSlave: !1 }) }

    function f() { var e = C.getGlobal("optlyDesktop"),
            t = !(!e || E.isUndefined(e["p13nInner"]));
        t && w.log("Directive / Is running in desktop app editor"), b.dispatch(y.LOAD_DIRECTIVE, { isRunningInDesktopApp: t }) }

    function p() { var e = "true" === R.getQueryParamValue(O.EDITOR_V2);
        e && w.log("Directive / Is running in editor"), b.dispatch(y.LOAD_DIRECTIVE, { isRunningInV2Editor: e }) }

    function h() { var e = T.get(A.COOKIES.TOKEN) || null,
            t = R.getQueryParamValue(O.TOKEN) || e;
        b.dispatch(y.LOAD_DIRECTIVE, { projectToken: t }) }

    function g() { var e = T.get(A.COOKIES.PREVIEW),
            t = [],
            n = R.getQueryParamValue(O.FORCE_AUDIENCES); if (n) t = T.safeDecodeURIComponent(n).split(",");
        else if (e) try { var i = N.parse(e);
            t = i.forceAudienceIds } catch (t) { var r = new D("Failed to parse previewCookie in registerForceAudienceIds: " + e),
                a = { originalMessage: t.message, userError: !0 };
            I.emitError(r, a) }
        t.length && (w.log("Directive / Force Audience IDs:", t), b.dispatch(y.LOAD_DIRECTIVE, { forceAudienceIds: t })) }

    function v() { var e = T.get(A.COOKIES.PREVIEW),
            t = [],
            n = R.getQueryParamValue(O.FORCE_VARIATIONS); if (n) t = T.safeDecodeURIComponent(n).split(",");
        else if (e) try { var i = N.parse(e);
            t = i.forceVariationIds } catch (t) { var r = new D("Failed to parse previewCookie in registerForceVariationIds: " + e),
                a = { originalMessage: t.message, userError: !0 };
            I.emitError(r, a) }
        t.length && (w.log("Directive / Force Variation IDs:", t), b.dispatch(y.LOAD_DIRECTIVE, { forceVariationIds: t })) }

    function m() { var e = R.getQueryParamValue(O.FORCE_TRACKING);
        e && b.dispatch(y.LOAD_DIRECTIVE, { forceTracking: e }) }

    function _() { var e = "OFF",
            t = R.getQueryParamValue("optimizely_log"); if (t) { var n = t.split(":"); "" !== n[0] && (e = String(n[0]).toUpperCase()), "undefined" != typeof n[1] && w.setLogMatch(n[1]) }
        w.setLogLevel(e) } var E = n(2),
        y = n(7),
        I = n(86),
        T = n(76),
        S = n(77).create,
        A = n(25),
        b = n(9),
        w = n(23),
        D = t.JSONParseError = S("JSONParseError"),
        R = n(119),
        N = n(26),
        C = n(41),
        O = { AB_PREVIEW: "optimizely_show_preview", DISABLE: "optimizely_disable", EDITOR: "optimizely_editor", EDITOR_V2: "optimizely_p13n", FORCE_AUDIENCES: "optimizely_x_audiences", FORCE_VARIATIONS: "optimizely_x", LEGACY_PREVIEW: "optimizely_show_preview", OPT_OUT: "optimizely_opt_out", PREVIEW_LAYER_IDS: "optimizely_preview_layer_ids", TOKEN: "optimizely_token", FORCE_TRACKING: "optimizely_force_tracking" };
    t.populateDirectiveData = function() { _(), a(), i(), r(), o(), s(), l(), c(), u(), d(), f(), p(), h(), g(), v(), m() }; var x = 31536e3;
    t.setOptOut = function(e) { e ? (w.warn("Directive / Opting out"), T.set(A.COOKIES.OPT_OUT, "true", { maxAge: 10 * x }, !0)) : T.remove(A.COOKIES.OPT_OUT), b.dispatch(y.LOAD_DIRECTIVE, { shouldOptOut: e }) } }), (function(e, t, n) { var i = n(120),
        r = n(41);
    t.getLanguage = function() { return r.getNavigatorLanguage() }, t.getQueryParams = i.getQueryParams, t.getQueryParamValue = i.getQueryParamValue, t.getUrl = function() { return r.getHref() } }), (function(e, t, n) { var i = n(2),
        r = n(41);
    t.getQueryParams = function() { var e = r.getLocationSearch() || ""; if (0 === e.indexOf("?") && (e = e.substring(1)), 0 === e.length) return []; for (var t = e.split("&"), n = [], i = 0; i < t.length; i++) { var a = "",
                o = "",
                s = t[i].split("=");
            s.length > 0 && (a = s[0]), s.length > 1 && (o = s[1]), n.push([a, o]) } return n }, t.getQueryParamValue = function(e) { for (var n = t.getQueryParams(), i = 0; i < n.length; i++)
            if (n[i][0] === e) return n[i][1] }, t.queryStringFromMap = function(e) { return i.map(e, (function(e, t) { return t + "=" + e })).join("&") } }), (function(e, t, n) { var i = n(2),
        r = n(77).BaseError,
        a = n(24),
        o = n(16),
        s = n(81),
        c = n(23),
        u = n(41),
        l = n(91),
        d = o.get("stores/client_metadata"),
        f = o.get("stores/global"),
        p = "https://errors.client.optimizely.com";
    t.handleError = function(e, t) {
        function n() { return l.request({ url: p + "/log", method: "POST", data: v, contentType: "application/json" }).then((function(e) { c.log("Error Monitor / Logged error with response: ", e) }), (function(e) { c.error("Failed to log error, response was: ", e) })) } var o = e.name || "Error",
            h = e.message || "",
            g = e.stack || null;
        e instanceof r && (h instanceof Error ? (h = h.message, g = e.message.stack) : g = null); var v = { timestamp: a.now(), clientEngine: d.getClientName(), clientVersion: d.getClientVersion(), accountId: f.getAccountId(), projectId: f.getProjectId(), errorClass: o, message: h, stacktrace: g },
            m = i.map(f.getExperimental(), (function(e, t) { return { key: "exp_" + t, value: String(e) } }));
        t && i.forEach(t, (function(e, t) { i.isObject(e) || m.push({ key: t, value: String(e) }) }), []), i.isEmpty(m) || (v.metadata = m), c.error("Logging error", v), s.isLoaded() ? n() : u.addEventListener("load", n) } }), (function(e, t, n) { var i = n(2);
    t.create = function(e, t, n, r) { var a = i.extend({ category: "other" }, r, { id: e, apiName: t, eventType: n }); return a } }), (function(e, t, n) { var i = n(2),
        r = n(7),
        a = n(16),
        o = n(25),
        s = n(87),
        c = n(9);
    t.registerApiModule = function(e, t) { i.isArray(t) && (t = a.evaluate(t)), c.dispatch(r.REGISTER_PLUGIN, { type: o.PluginTypes.apiModules, name: e, plugin: t }) }, t.registerDependency = function(e, t) { var n = a.get(e);
        n || a.register(e, t) }, t.registerVisitorProfileProvider = function(e) { c.dispatch(r.REGISTER_PLUGIN, { type: o.PluginTypes.visitorProfileProviders, name: e.provides, plugin: e }) }, t.registerViewProvider = function(e) { c.dispatch(r.REGISTER_PLUGIN, { type: o.PluginTypes.viewProviders, name: e.provides, plugin: e }) }, t.registerAudienceMatcher = function(e, t) { c.dispatch(r.REGISTER_PLUGIN, { type: o.PluginTypes.audienceMatchers, name: e, plugin: t }) }, t.registerViewMatcher = function(e, t) { c.dispatch(r.REGISTER_PLUGIN, { type: o.PluginTypes.viewMatchers, name: e, plugin: t }) }, t.registerAnalyticsTracker = function(e, t) { c.dispatch(r.REGISTER_PLUGIN, { type: o.PluginTypes.analyticsTrackers, name: e, plugin: t }) }, t.registerViewTagLocator = function(e, t) { c.dispatch(r.REGISTER_PLUGIN, { type: o.PluginTypes.viewTagLocators, name: e, plugin: t }) }, t.registerAudiencePlugin = function(e) { e.dependencies && i.each(e.dependencies, (function(e, n) { t.registerDependency(n, e) })); var n, r = "vendor." + e.vendor;
        n = i.isString(e.provider) ? a.get(e.provider)(e.vendor) : i.isFunction(e.provider) ? e.provider(e.vendor) : i.cloneDeep(e.provider), t.registerVisitorProfileProvider(i.extend(n, { provides: r })); var o;
        o = i.isString(e.matcher) ? a.get(e.matcher) : e.matcher; var s = { fieldsNeeded: [r], match: function(e, t) { return o(e[r], t) } };
        t.registerAudienceMatcher(r, s) }, t.registerWidget = function(e) { i.isArray(e) && (e = a.evaluate(e)); var t = s.on({ filter: { type: "showWidget", name: e.widgetId }, handler: e.showFn }),
            n = s.on({ filter: { type: "hideWidget", name: e.widgetId }, handler: e.hideFn }); return { showToken: t, hideToken: n } }, t.registerChangeApplier = function(e, t) { c.dispatch(r.REGISTER_PLUGIN, { type: o.PluginTypes.changeAppliers, name: e, plugin: t }) }, t.registerDecider = function(e, t) { c.dispatch(r.REGISTER_PLUGIN, { type: o.PluginTypes.deciders, name: e, plugin: t }) }, t.registerEventImplementation = function(e, t) { c.dispatch(r.REGISTER_PLUGIN, { type: o.PluginTypes.eventImplementations, name: e, plugin: t }) }, t.registerViewTrigger = function(e, t) { c.dispatch(r.REGISTER_PLUGIN, { type: o.PluginTypes.viewTriggers, name: e, plugin: t }) } }), (function(e, t, n) {
    function i(e, t) { r.forEach(e, (function(e) { if (e.eventType !== d.CUSTOM) { var n = v.getPlugin(l.PluginTypes.eventImplementations, e.eventType);
                n ? t ? n.attach(e) : n.detach(e) : p.warn("No implementation found for event type:", e.eventType, "needed for event:", e) } })) } var r = n(2),
        a = n(7),
        o = n(117),
        s = n(125),
        c = n(24),
        u = n(16),
        l = n(25),
        d = n(112),
        f = n(9),
        p = n(23),
        h = n(126),
        g = u.get("stores/event_data"),
        v = u.get("stores/plugins"),
        m = u.get("stores/rum"),
        _ = u.get("stores/view"),
        E = u.get("stores/view_data");
    t.parseViewTags = function(e) { var n = t.evaluateViewTags(e);
        t.setParsedViewTags(e.id, n) }, t.updateAllViewTags = function() { var e = _.getActiveViewStates();
        r.each(e, (function(e) { var n = E.get(e.id);
            t.parseViewTags(n) })) }, t.evaluateViewTags = function(e) { if (!e.tags) return {}; var t = r.reduce(e.tags, (function(e, t) { try { e[t.apiName] = h.getTagValue(t) } catch (e) { e instanceof h.Error ? p.warn("Page / Ignoring unparseable tag", t, e) : p.error(e) } return e }), {}); return t }, t.createViewTicket = function() { var e = {}; return r.each(v.getAllPlugins(l.PluginTypes.viewProviders), (function(t) { e[t.provides] = u.evaluate(t.getter) })), e }, t.registerViews = function(e) { f.dispatch(a.REGISTER_VIEWS, { views: e }) }, t.activateViaAPI = function(e, n) { n && t.setUserSuppliedViewTags(e.id, n), t.activateMultiple([e], n) }, t.getViewsAndActivate = function(e) { var n = E.getAllViewsForActivationType(e);
        t.activateMultiple(n) }, t.activateMultiple = function(e, n) { var s = [];
        r.each(e, (function(e) { var r, c = _.getViewState(e.id),
                u = t.createViewTicket(); if (c.isActive)
                if (e.deactivationEnabled) try { t.hasValidStaticConditions(e, u) || t.deactivate(e) } catch (n) { p.error("Page / Error evaluating whether to deactivate page ", t.description(e), n) } else p.log("Not activating Page, already active ", t.description(e));
                else { try { if (r = t.hasValidStaticConditions(e, u), !r) return void p.log("Page / Failed to match page conditions for " + t.description(e), e.staticConditions) } catch (n) { return void p.error("Page / Error evaluating whether to activate page ", t.description(e), n) } if (s.push(e), t.setViewActiveState(e, !0), p.log("Activated Page", t.description(e)), o.emitViewActivated({ view: e, metadata: n }), m.getSampleRum()) { var d = e.activationType || l.ViewActivationTypes.immediate;
                        f.dispatch(a.RECORD_ACTIVATION_TYPE_USAGE, { activationType: d, entityId: e.id }) } var h = g.getByPageId(e.id);
                    i(h, !0) } })), r.isEmpty(s) || o.emitViewsActivated({ views: s }) }, t.deactivate = function(e) { var n = _.getViewState(e.id); if (!n.isActive) return void p.log("Not deactivating Page, already inactive ", t.description(e));
        t.setViewActiveState(e, !1), p.log("Deactivated Page", t.description(e)), o.emitPageDeactivated({ page: e }); var r = g.getByPageId(e.id);
        i(r, !1) }, t.setViewActiveState = function(e, t) { f.dispatch(a.SET_VIEW_ACTIVE_STATE, { view: e, timestamp: c.now(), isActive: t }) }, t.setGlobalTags = function(e) { f.dispatch(a.SET_GLOBAL_TAGS, e) }, t.setParsedViewTags = function(e, t) { f.dispatch(a.UPDATE_PARSED_VIEW_METADATA, { pageId: e, metadata: t }) }, t.setUserSuppliedViewTags = function(e, t) { f.dispatch(a.UPDATE_USER_SUPPLIED_METADATA, { pageId: e, metadata: t }) }, t.hasValidStaticConditions = function(e, t) { var n = {}; if (r.isEmpty(e.staticConditions)) return !0; var i = v.getAllPlugins(l.PluginTypes.viewMatchers);
        p.groupCollapsed("Page / Evaluating staticConditions:", e.staticConditions), p.debug("Matching to current value:", t); var o = s.evaluate(e.staticConditions, (function(e) { var r = e.type,
                a = i[r]; if (!a) throw new Error("Page / No matcher found for type=" + r); return a && (n[e.type] || (n[e.type] = !0)), a.match(t, e) })); return p.groupEnd(), m.getSampleRum() && o && f.dispatch(a.RECORD_VIEW_FEATURE_USAGE, { featuresUsed: r.keys(n), entityId: e.id }), o }, t.description = function(e) { return '"' + e.name + '" (' + e.id + ")" }, t.shouldTriggerImmediately = function(e) { return e === l.ViewActivationTypes.DOMChanged || e === l.ViewActivationTypes.URLChanged || e === l.ViewActivationTypes.immediate || !e }, t.create = function(e, t) { var n = { id: e, apiName: t, category: "other" }; return n }, t.createState = function(e) { var t = { id: e, isActive: !0, metadata: {}, parsedMetadata: {}, userSuppliedMetadata: {} }; return t } }), (function(e, t, n) {
    function i(e, t) { for (var n, i, r = 0; r < e.length; r++) { if (n = o(e[r], t), n === !1) return !1;
            s.isUndefined(n) && (i = !0) } if (!i) return !0 }

    function r(e, t) { for (var n, i = !1, r = 0; r < e.length; r++) { if (n = o(e[r], t), n === !0) return !0;
            s.isUndefined(n) && (i = !0) } if (!i) return !1 }

    function a(e, t) { if (1 !== e.length) return !1; var n = o(e[0], t); return s.isUndefined(n) ? void 0 : !n }

    function o(e, t) { var n; if (s.isArray(e)) { var i, r;
            e[0] in d ? (i = e[0], r = e.slice(1)) : (i = l.OR, r = e), u.groupCollapsed('Condition / Applying operator "' + i + '" with args', c.stringify(r)); try { n = d[i](r, t), u.debug("Condition / Result:", n) } finally { u.groupEnd() } return n } return n = t(e), u.debug("Condition / Evaluated:", c.stringify(e), ":", n), n } var s = n(2),
        c = n(26),
        u = n(23),
        l = { AND: "and", OR: "or", NOT: "not" },
        d = {};
    d[l.AND] = i, d[l.OR] = r, d[l.NOT] = a, e.exports = { evaluate: o } }), (function(e, t, n) { var i = n(25).PluginTypes,
        r = n(16),
        a = r.get("stores/plugins");
    t.getTagValue = function(e) { var n = a.getPlugin(i.viewTagLocators, e.locatorType); if (!n) throw new t.Error("No locator registered for tag locatorType: " + e.locatorType); return n(e) }, t.enums = n(127), t.Error = n(128).Error }), (function(e, t) { t.locatorType = { CSS_SELECTOR: "css_selector", JAVASCRIPT: "javascript", URL_REGEX: "url_regex" }, t.valueType = { STRING: "string", NUMBER: "number", CURRENCY: "currency" }, t.nodeNames = { INPUT: "INPUT", SELECT: "SELECT" } }), (function(e, t, n) { var i = n(77).create;
    t.Error = i("TagError") }), (function(e, t) {}), (function(e, t, n) { var i = n(16);
    i.register("env/jquery", n(131)) }), (function(e, t, n) { n(41);
    e.exports = n(132) }), (function(e, t, n) {
    var i, r;
    /** @license
     * jQuery JavaScript Library v1.11.3
     * http://jquery.com/
     *
     * Includes Sizzle.js
     * http://sizzlejs.com/
     *
     * Copyright 2005, 2014 jQuery Foundation, Inc. and other contributors
     * Released under the MIT license
     * http://jquery.org/license
     *
     * Date: 2015-11-13T20:27Z
     */
    !(function(t, n) { "object" == typeof e && "object" == typeof e.exports ? e.exports = t.document ? n(t, !0) : function(e) { if (!e.document) throw new Error("jQuery requires a window with a document"); return n(e) } : n(t) })("undefined" != typeof window ? window : this, (function(n, a) {
        function o(e) { var t = "length" in e && e.length,
                n = ce.type(e); return "function" !== n && !ce.isWindow(e) && (!(1 !== e.nodeType || !t) || ("array" === n || 0 === t || "number" == typeof t && t > 0 && t - 1 in e)) }

        function s(e, t, n) { if (ce.isFunction(t)) return ce.grep(e, (function(e, i) { return !!t.call(e, i, e) !== n })); if (t.nodeType) return ce.grep(e, (function(e) { return e === t !== n })); if ("string" == typeof t) { if (ve.test(t)) return ce.filter(t, e, n);
                t = ce.filter(t, e) } return ce.grep(e, (function(e) { return ce.inArray(e, t) >= 0 !== n })) }

        function c(e, t) { do e = e[t]; while (e && 1 !== e.nodeType); return e }

        function u(e) { var t = Ae[e] = {}; return ce.each(e.match(Se) || [], (function(e, n) { t[n] = !0 })), t }

        function l() { _e.addEventListener ? (_e.removeEventListener("DOMContentLoaded", d, !1), n.removeEventListener("load", d, !1)) : (_e.detachEvent("onreadystatechange", d), n.detachEvent("onload", d)) }

        function d() {
            (_e.addEventListener || "load" === event.type || "complete" === _e.readyState) && (l(), ce.ready()) }

        function f(e, t, n) { if (void 0 === n && 1 === e.nodeType) { var i = "data-" + t.replace(Ne, "-$1").toLowerCase(); if (n = e.getAttribute(i), "string" == typeof n) { try { n = "true" === n || "false" !== n && ("null" === n ? null : +n + "" === n ? +n : Re.test(n) ? ce.parseJSON(n) : n) } catch (e) {}
                    ce.data(e, t, n) } else n = void 0 } return n }

        function p(e) { var t; for (t in e)
                if (("data" !== t || !ce.isEmptyObject(e[t])) && "toJSON" !== t) return !1;
            return !0 }

        function h(e, t, n, i) { if (ce.acceptData(e)) { var r, a, o = ce.expando,
                    s = e.nodeType,
                    c = s ? ce.cache : e,
                    u = s ? e[o] : e[o] && o; if (u && c[u] && (i || c[u].data) || void 0 !== n || "string" != typeof t) return u || (u = s ? e[o] = J.pop() || ce.guid++ : o), c[u] || (c[u] = s ? {} : { toJSON: ce.noop }), "object" != typeof t && "function" != typeof t || (i ? c[u] = ce.extend(c[u], t) : c[u].data = ce.extend(c[u].data, t)), a = c[u], i || (a.data || (a.data = {}), a = a.data), void 0 !== n && (a[ce.camelCase(t)] = n), "string" == typeof t ? (r = a[t], null == r && (r = a[ce.camelCase(t)])) : r = a, r } }

        function g(e, t, n) { if (ce.acceptData(e)) { var i, r, a = e.nodeType,
                    o = a ? ce.cache : e,
                    s = a ? e[ce.expando] : ce.expando; if (o[s]) { if (t && (i = n ? o[s] : o[s].data)) { ce.isArray(t) ? t = t.concat(ce.map(t, ce.camelCase)) : t in i ? t = [t] : (t = ce.camelCase(t), t = t in i ? [t] : t.split(" ")), r = t.length; for (; r--;) delete i[t[r]]; if (n ? !p(i) : !ce.isEmptyObject(i)) return }(n || (delete o[s].data, p(o[s]))) && (a ? ce.cleanData([e], !0) : oe.deleteExpando || o != o.window ? delete o[s] : o[s] = null) } } }

        function v() { return !0 }

        function m() { return !1 }

        function _() { try { return _e.activeElement } catch (e) {} }

        function E(e) { var t = Ge.split("|"),
                n = e.createDocumentFragment(); if (n.createElement)
                for (; t.length;) n.createElement(t.pop()); return n }

        function y(e, t) { var n, i, r = 0,
                a = typeof e.getElementsByTagName !== De ? e.getElementsByTagName(t || "*") : typeof e.querySelectorAll !== De ? e.querySelectorAll(t || "*") : void 0; if (!a)
                for (a = [], n = e.childNodes || e; null != (i = n[r]); r++) !t || ce.nodeName(i, t) ? a.push(i) : ce.merge(a, y(i, t)); return void 0 === t || t && ce.nodeName(e, t) ? ce.merge([e], a) : a }

        function I(e) { Pe.test(e.type) && (e.defaultChecked = e.checked) }

        function T(e, t) { return ce.nodeName(e, "table") && ce.nodeName(11 !== t.nodeType ? t : t.firstChild, "tr") ? e.getElementsByTagName("tbody")[0] || e.appendChild(e.ownerDocument.createElement("tbody")) : e }

        function S(e) { return e.type = (null !== ce.find.attr(e, "type")) + "/" + e.type, e }

        function A(e) { var t = Qe.exec(e.type); return t ? e.type = t[1] : e.removeAttribute("type"), e }

        function b(e, t) { for (var n, i = 0; null != (n = e[i]); i++) ce._data(n, "globalEval", !t || ce._data(t[i], "globalEval")) }

        function w(e, t) { if (1 === t.nodeType && ce.hasData(e)) { var n, i, r, a = ce._data(e),
                    o = ce._data(t, a),
                    s = a.events; if (s) { delete o.handle, o.events = {}; for (n in s)
                        for (i = 0, r = s[n].length; i < r; i++) ce.event.add(t, n, s[n][i]) }
                o.data && (o.data = ce.extend({}, o.data)) } }

        function D(e, t) { var n, i, r; if (1 === t.nodeType) { if (n = t.nodeName.toLowerCase(), !oe.noCloneEvent && t[ce.expando]) { r = ce._data(t); for (i in r.events) ce.removeEvent(t, i, r.handle);
                    t.removeAttribute(ce.expando) } "script" === n && t.text !== e.text ? (S(t).text = e.text, A(t)) : "object" === n ? (t.parentNode && (t.outerHTML = e.outerHTML), oe.html5Clone && e.innerHTML && !ce.trim(t.innerHTML) && (t.innerHTML = e.innerHTML)) : "input" === n && Pe.test(e.type) ? (t.defaultChecked = t.checked = e.checked, t.value !== e.value && (t.value = e.value)) : "option" === n ? t.defaultSelected = t.selected = e.defaultSelected : "input" !== n && "textarea" !== n || (t.defaultValue = e.defaultValue) } }

        function R(e, t) { var i, r = ce(t.createElement(e)).appendTo(t.body),
                a = n.getDefaultComputedStyle && (i = n.getDefaultComputedStyle(r[0])) ? i.display : ce.css(r[0], "display"); return r.detach(), a }

        function N(e) { var t = _e,
                n = it[e]; return n || (n = R(e, t), "none" !== n && n || (nt = (nt || ce("<iframe frameborder='0' width='0' height='0'/>")).appendTo(t.documentElement), t = (nt[0].contentWindow || nt[0].contentDocument).document, t.write(), t.close(), n = R(e, t), nt.detach()), it[e] = n), n }

        function C(e, t) { return { get: function() { var n = e(); if (null != n) return n ? void delete this.get : (this.get = t).apply(this, arguments) } } }

        function O(e, t) { if (t in e) return t; for (var n = t.charAt(0).toUpperCase() + t.slice(1), i = t, r = vt.length; r--;)
                if (t = vt[r] + n, t in e) return t;
            return i }

        function x(e, t) { for (var n, i, r, a = [], o = 0, s = e.length; o < s; o++) i = e[o], i.style && (a[o] = ce._data(i, "olddisplay"), n = i.style.display, t ? (a[o] || "none" !== n || (i.style.display = ""), "" === i.style.display && xe(i) && (a[o] = ce._data(i, "olddisplay", N(i.nodeName)))) : (r = xe(i), (n && "none" !== n || !r) && ce._data(i, "olddisplay", r ? n : ce.css(i, "display")))); for (o = 0; o < s; o++) i = e[o], i.style && (t && "none" !== i.style.display && "" !== i.style.display || (i.style.display = t ? a[o] || "" : "none")); return e }

        function L(e, t, n) { var i = ft.exec(t); return i ? Math.max(0, i[1] - (n || 0)) + (i[2] || "px") : t }

        function P(e, t, n, i, r) { for (var a = n === (i ? "border" : "content") ? 4 : "width" === t ? 1 : 0, o = 0; a < 4; a += 2) "margin" === n && (o += ce.css(e, n + Oe[a], !0, r)), i ? ("content" === n && (o -= ce.css(e, "padding" + Oe[a], !0, r)), "margin" !== n && (o -= ce.css(e, "border" + Oe[a] + "Width", !0, r))) : (o += ce.css(e, "padding" + Oe[a], !0, r), "padding" !== n && (o += ce.css(e, "border" + Oe[a] + "Width", !0, r))); return o }

        function k(e, t, n) { var i = !0,
                r = "width" === t ? e.offsetWidth : e.offsetHeight,
                a = rt(e),
                o = oe.boxSizing && "border-box" === ce.css(e, "boxSizing", !1, a); if (r <= 0 || null == r) { if (r = at(e, t, a), (r < 0 || null == r) && (r = e.style[t]), st.test(r)) return r;
                i = o && (oe.boxSizingReliable() || r === e.style[t]), r = parseFloat(r) || 0 } return r + P(e, t, n || (o ? "border" : "content"), i, a) + "px" }

        function F(e, t, n, i, r) { return new F.prototype.init(e, t, n, i, r) }

        function M() { return setTimeout((function() { mt = void 0 })), mt = ce.now() }

        function V(e, t) { var n, i = { height: e },
                r = 0; for (t = t ? 1 : 0; r < 4; r += 2 - t) n = Oe[r], i["margin" + n] = i["padding" + n] = e; return t && (i.opacity = i.width = e), i }

        function U(e, t, n) { for (var i, r = (St[t] || []).concat(St["*"]), a = 0, o = r.length; a < o; a++)
                if (i = r[a].call(n, t, e)) return i }

        function G(e, t, n) { var i, r, a, o, s, c, u, l, d = this,
                f = {},
                p = e.style,
                h = e.nodeType && xe(e),
                g = ce._data(e, "fxshow");
            n.queue || (s = ce._queueHooks(e, "fx"), null == s.unqueued && (s.unqueued = 0, c = s.empty.fire, s.empty.fire = function() { s.unqueued || c() }), s.unqueued++, d.always((function() { d.always((function() { s.unqueued--, ce.queue(e, "fx").length || s.empty.fire() })) }))), 1 === e.nodeType && ("height" in t || "width" in t) && (n.overflow = [p.overflow, p.overflowX, p.overflowY], u = ce.css(e, "display"), l = "none" === u ? ce._data(e, "olddisplay") || N(e.nodeName) : u, "inline" === l && "none" === ce.css(e, "float") && (oe.inlineBlockNeedsLayout && "inline" !== N(e.nodeName) ? p.zoom = 1 : p.display = "inline-block")), n.overflow && (p.overflow = "hidden", oe.shrinkWrapBlocks() || d.always((function() { p.overflow = n.overflow[0], p.overflowX = n.overflow[1], p.overflowY = n.overflow[2] }))); for (i in t)
                if (r = t[i], Et.exec(r)) { if (delete t[i], a = a || "toggle" === r, r === (h ? "hide" : "show")) { if ("show" !== r || !g || void 0 === g[i]) continue;
                        h = !0 }
                    f[i] = g && g[i] || ce.style(e, i) } else u = void 0;
            if (ce.isEmptyObject(f)) "inline" === ("none" === u ? N(e.nodeName) : u) && (p.display = u);
            else { g ? "hidden" in g && (h = g.hidden) : g = ce._data(e, "fxshow", {}), a && (g.hidden = !h), h ? ce(e).show() : d.done((function() { ce(e).hide() })), d.done((function() { var t;
                    ce._removeData(e, "fxshow"); for (t in f) ce.style(e, t, f[t]) })); for (i in f) o = U(h ? g[i] : 0, i, d), i in g || (g[i] = o.start, h && (o.end = o.start, o.start = "width" === i || "height" === i ? 1 : 0)) } }

        function B(e, t) { var n, i, r, a, o; for (n in e)
                if (i = ce.camelCase(n), r = t[i], a = e[n], ce.isArray(a) && (r = a[1], a = e[n] = a[0]), n !== i && (e[i] = a, delete e[n]), o = ce.cssHooks[i], o && "expand" in o) { a = o.expand(a), delete e[i]; for (n in a) n in e || (e[n] = a[n], t[n] = r) } else t[i] = r }

        function j(e, t, n) { var i, r, a = 0,
                o = Tt.length,
                s = ce.Deferred().always((function() { delete c.elem })),
                c = function() { if (r) return !1; for (var t = mt || M(), n = Math.max(0, u.startTime + u.duration - t), i = n / u.duration || 0, a = 1 - i, o = 0, c = u.tweens.length; o < c; o++) u.tweens[o].run(a); return s.notifyWith(e, [u, a, n]), a < 1 && c ? n : (s.resolveWith(e, [u]), !1) },
                u = s.promise({ elem: e, props: ce.extend({}, t), opts: ce.extend(!0, { specialEasing: {} }, n), originalProperties: t, originalOptions: n, startTime: mt || M(), duration: n.duration, tweens: [], createTween: function(t, n) { var i = ce.Tween(e, u.opts, t, n, u.opts.specialEasing[t] || u.opts.easing); return u.tweens.push(i), i }, stop: function(t) { var n = 0,
                            i = t ? u.tweens.length : 0; if (r) return this; for (r = !0; n < i; n++) u.tweens[n].run(1); return t ? s.resolveWith(e, [u, t]) : s.rejectWith(e, [u, t]), this } }),
                l = u.props; for (B(l, u.opts.specialEasing); a < o; a++)
                if (i = Tt[a].call(u, e, l, u.opts)) return i;
            return ce.map(l, U, u), ce.isFunction(u.opts.start) && u.opts.start.call(e, u), ce.fx.timer(ce.extend(c, { elem: e, anim: u, queue: u.opts.queue })), u.progress(u.opts.progress).done(u.opts.done, u.opts.complete).fail(u.opts.fail).always(u.opts.always) }

        function H(e) { return function(t, n) { "string" != typeof t && (n = t, t = "*"); var i, r = 0,
                    a = t.toLowerCase().match(Se) || []; if (ce.isFunction(n))
                    for (; i = a[r++];) "+" === i.charAt(0) ? (i = i.slice(1) || "*", (e[i] = e[i] || []).unshift(n)) : (e[i] = e[i] || []).push(n) } }

        function z(e, t, n, i) {
            function r(s) { var c; return a[s] = !0, ce.each(e[s] || [], (function(e, s) { var u = s(t, n, i); return "string" != typeof u || o || a[u] ? o ? !(c = u) : void 0 : (t.dataTypes.unshift(u), r(u), !1) })), c } var a = {},
                o = e === Kt; return r(t.dataTypes[0]) || !a["*"] && r("*") }

        function q(e, t) { var n, i, r = ce.ajaxSettings.flatOptions || {}; for (i in t) void 0 !== t[i] && ((r[i] ? e : n || (n = {}))[i] = t[i]); return n && ce.extend(!0, e, n), e }

        function Y(e, t, n) { for (var i, r, a, o, s = e.contents, c = e.dataTypes;
                "*" === c[0];) c.shift(), void 0 === r && (r = e.mimeType || t.getResponseHeader("Content-Type")); if (r)
                for (o in s)
                    if (s[o] && s[o].test(r)) { c.unshift(o); break }
            if (c[0] in n) a = c[0];
            else { for (o in n) { if (!c[0] || e.converters[o + " " + c[0]]) { a = o; break }
                    i || (i = o) }
                a = a || i } if (a) return a !== c[0] && c.unshift(a), n[a] }

        function K(e, t, n, i) { var r, a, o, s, c, u = {},
                l = e.dataTypes.slice(); if (l[1])
                for (o in e.converters) u[o.toLowerCase()] = e.converters[o]; for (a = l.shift(); a;)
                if (e.responseFields[a] && (n[e.responseFields[a]] = t), !c && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)), c = a, a = l.shift())
                    if ("*" === a) a = c;
                    else if ("*" !== c && c !== a) { if (o = u[c + " " + a] || u["* " + a], !o)
                    for (r in u)
                        if (s = r.split(" "), s[1] === a && (o = u[c + " " + s[0]] || u["* " + s[0]])) { o === !0 ? o = u[r] : u[r] !== !0 && (a = s[0], l.unshift(s[1])); break }
                if (o !== !0)
                    if (o && e["throws"]) t = o(t);
                    else try { t = o(t) } catch (e) { return { state: "parsererror", error: o ? e : "No conversion from " + c + " to " + a } } } return { state: "success", data: t } }

        function W(e, t, n, i) { var r; if (ce.isArray(t)) ce.each(t, (function(t, r) { n || $t.test(e) ? i(e, r) : W(e + "[" + ("object" == typeof r ? t : "") + "]", r, n, i) }));
            else if (n || "object" !== ce.type(t)) i(e, t);
            else
                for (r in t) W(e + "[" + r + "]", t[r], n, i) }

        function X() { try { return new n.XMLHttpRequest } catch (e) {} }

        function $() { try { return new n.ActiveXObject("Microsoft.XMLHTTP") } catch (e) {} }

        function Q(e) { return ce.isWindow(e) ? e : 9 === e.nodeType && (e.defaultView || e.parentWindow) }
        var J = [],
            Z = J.slice,
            ee = J.concat,
            te = J.push,
            ne = J.indexOf,
            ie = {},
            re = ie.toString,
            ae = ie.hasOwnProperty,
            oe = {},
            se = "1.11.3",
            ce = function(e, t) { return new ce.fn.init(e, t) },
            ue = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
            le = /^-ms-/,
            de = /-([\da-z])/gi,
            fe = function(e, t) { return t.toUpperCase() };
        ce.fn = ce.prototype = { jquery: se, constructor: ce, selector: "", length: 0, toArray: function() { return Z.call(this) }, get: function(e) { return null != e ? e < 0 ? this[e + this.length] : this[e] : Z.call(this) }, pushStack: function(e) { var t = ce.merge(this.constructor(), e); return t.prevObject = this, t.context = this.context, t }, each: function(e, t) { return ce.each(this, e, t) }, map: function(e) { return this.pushStack(ce.map(this, (function(t, n) { return e.call(t, n, t) }))) }, slice: function() { return this.pushStack(Z.apply(this, arguments)) }, first: function() { return this.eq(0) }, last: function() { return this.eq(-1) }, eq: function(e) { var t = this.length,
                    n = +e + (e < 0 ? t : 0); return this.pushStack(n >= 0 && n < t ? [this[n]] : []) }, end: function() { return this.prevObject || this.constructor(null) }, push: te, sort: J.sort, splice: J.splice }, ce.extend = ce.fn.extend = function() { var e, t, n, i, r, a, o = arguments[0] || {},
                s = 1,
                c = arguments.length,
                u = !1; for ("boolean" == typeof o && (u = o, o = arguments[s] || {}, s++), "object" == typeof o || ce.isFunction(o) || (o = {}), s === c && (o = this, s--); s < c; s++)
                if (null != (r = arguments[s]))
                    for (i in r) e = o[i], n = r[i], o !== n && (u && n && (ce.isPlainObject(n) || (t = ce.isArray(n))) ? (t ? (t = !1, a = e && ce.isArray(e) ? e : []) : a = e && ce.isPlainObject(e) ? e : {}, o[i] = ce.extend(u, a, n)) : void 0 !== n && (o[i] = n));
            return o }, ce.extend({ expando: "jQuery" + (se + Math.random()).replace(/\D/g, ""), isReady: !0, error: function(e) { throw new Error(e) }, noop: function() {}, isFunction: function(e) { return "function" === ce.type(e) }, isArray: Array.isArray || function(e) { return "array" === ce.type(e) }, isWindow: function(e) { return null != e && e == e.window }, isNumeric: function(e) { return !ce.isArray(e) && e - parseFloat(e) + 1 >= 0 }, isEmptyObject: function(e) { var t; for (t in e) return !1; return !0 }, isPlainObject: function(e) { var t; if (!e || "object" !== ce.type(e) || e.nodeType || ce.isWindow(e)) return !1; try { if (e.constructor && !ae.call(e, "constructor") && !ae.call(e.constructor.prototype, "isPrototypeOf")) return !1 } catch (e) { return !1 } if (oe.ownLast)
                    for (t in e) return ae.call(e, t); for (t in e); return void 0 === t || ae.call(e, t) }, type: function(e) { return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? ie[re.call(e)] || "object" : typeof e }, globalEval: function(e) { e && ce.trim(e) && (n.execScript || function(e) { n["eval"].call(n, e) })(e) }, camelCase: function(e) { return e.replace(le, "ms-").replace(de, fe) }, nodeName: function(e, t) { return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase() }, each: function(e, t, n) { var i, r = 0,
                    a = e.length,
                    s = o(e); if (n) { if (s)
                        for (; r < a && (i = t.apply(e[r], n), i !== !1); r++);
                    else
                        for (r in e)
                            if (i = t.apply(e[r], n), i === !1) break } else if (s)
                    for (; r < a && (i = t.call(e[r], r, e[r]), i !== !1); r++);
                else
                    for (r in e)
                        if (i = t.call(e[r], r, e[r]), i === !1) break; return e }, trim: function(e) { return null == e ? "" : (e + "").replace(ue, "") }, makeArray: function(e, t) { var n = t || []; return null != e && (o(Object(e)) ? ce.merge(n, "string" == typeof e ? [e] : e) : te.call(n, e)), n }, inArray: function(e, t, n) { var i; if (t) { if (ne) return ne.call(t, e, n); for (i = t.length, n = n ? n < 0 ? Math.max(0, i + n) : n : 0; n < i; n++)
                        if (n in t && t[n] === e) return n } return -1 }, merge: function(e, t) { for (var n = +t.length, i = 0, r = e.length; i < n;) e[r++] = t[i++]; if (n !== n)
                    for (; void 0 !== t[i];) e[r++] = t[i++]; return e.length = r, e }, grep: function(e, t, n) { for (var i, r = [], a = 0, o = e.length, s = !n; a < o; a++) i = !t(e[a], a), i !== s && r.push(e[a]); return r }, map: function(e, t, n) { var i, r = 0,
                    a = e.length,
                    s = o(e),
                    c = []; if (s)
                    for (; r < a; r++) i = t(e[r], r, n), null != i && c.push(i);
                else
                    for (r in e) i = t(e[r], r, n), null != i && c.push(i); return ee.apply([], c) }, guid: 1, proxy: function(e, t) { var n, i, r; if ("string" == typeof t && (r = e[t], t = e, e = r), ce.isFunction(e)) return n = Z.call(arguments, 2), i = function() { return e.apply(t || this, n.concat(Z.call(arguments))) }, i.guid = e.guid = e.guid || ce.guid++, i }, now: function() { return +new Date }, support: oe }), ce.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), (function(e, t) { ie["[object " + t + "]"] = t.toLowerCase() }));
        var pe =
            /*!
             * Sizzle CSS Selector Engine v2.2.0-pre
             * http://sizzlejs.com/
             *
             * Copyright 2008, 2014 jQuery Foundation, Inc. and other contributors
             * Released under the MIT license
             * http://jquery.org/license
             *
             * Date: 2014-12-16
             */
            (function(e) {
                function t(e, t, n, i) { var r, a, o, s, c, u, d, p, h, g; if ((t ? t.ownerDocument || t : G) !== x && O(t), t = t || x, n = n || [], s = t.nodeType, "string" != typeof e || !e || 1 !== s && 9 !== s && 11 !== s) return n; if (!i && P) { if (11 !== s && (r = _e.exec(e)))
                            if (o = r[1]) { if (9 === s) { if (a = t.getElementById(o), !a || !a.parentNode) return n; if (a.id === o) return n.push(a), n } else if (t.ownerDocument && (a = t.ownerDocument.getElementById(o)) && V(t, a) && a.id === o) return n.push(a), n } else { if (r[2]) return J.apply(n, t.getElementsByTagName(e)), n; if ((o = r[3]) && I.getElementsByClassName) return J.apply(n, t.getElementsByClassName(o)), n }
                        if (I.qsa && (!k || !k.test(e))) { if (p = d = U, h = t, g = 1 !== s && e, 1 === s && "object" !== t.nodeName.toLowerCase()) { for (u = b(e), (d = t.getAttribute("id")) ? p = d.replace(ye, "\\$&") : t.setAttribute("id", p), p = "[id='" + p + "'] ", c = u.length; c--;) u[c] = p + f(u[c]);
                                h = Ee.test(e) && l(t.parentNode) || t, g = u.join(",") } if (g) try { return J.apply(n, h.querySelectorAll(g)), n } catch (e) {} finally { d || t.removeAttribute("id") } } } return D(e.replace(ce, "$1"), t, n, i) }

                function n() {
                    function e(n, i) { return t.push(n + " ") > T.cacheLength && delete e[t.shift()], e[n + " "] = i } var t = []; return e }

                function i(e) { return e[U] = !0, e }

                function r(e) { var t = x.createElement("div"); try { return !!e(t) } catch (e) { return !1 } finally { t.parentNode && t.parentNode.removeChild(t), t = null } }

                function a(e, t) { for (var n = e.split("|"), i = e.length; i--;) T.attrHandle[n[i]] = t }

                function o(e, t) { var n = t && e,
                        i = n && 1 === e.nodeType && 1 === t.nodeType && (~t.sourceIndex || K) - (~e.sourceIndex || K); if (i) return i; if (n)
                        for (; n = n.nextSibling;)
                            if (n === t) return -1;
                    return e ? 1 : -1 }

                function s(e) { return function(t) { var n = t.nodeName.toLowerCase(); return "input" === n && t.type === e } }

                function c(e) { return function(t) { var n = t.nodeName.toLowerCase(); return ("input" === n || "button" === n) && t.type === e } }

                function u(e) { return i((function(t) { return t = +t, i((function(n, i) { for (var r, a = e([], n.length, t), o = a.length; o--;) n[r = a[o]] && (n[r] = !(i[r] = n[r])) })) })) }

                function l(e) { return e && "undefined" != typeof e.getElementsByTagName && e }

                function d() {}

                function f(e) { for (var t = 0, n = e.length, i = ""; t < n; t++) i += e[t].value; return i }

                function p(e, t, n) { var i = t.dir,
                        r = n && "parentNode" === i,
                        a = j++; return t.first ? function(t, n, a) { for (; t = t[i];)
                            if (1 === t.nodeType || r) return e(t, n, a) } : function(t, n, o) { var s, c, u = [B, a]; if (o) { for (; t = t[i];)
                                if ((1 === t.nodeType || r) && e(t, n, o)) return !0 } else
                            for (; t = t[i];)
                                if (1 === t.nodeType || r) { if (c = t[U] || (t[U] = {}), (s = c[i]) && s[0] === B && s[1] === a) return u[2] = s[2]; if (c[i] = u, u[2] = e(t, n, o)) return !0 } } }

                function h(e) { return e.length > 1 ? function(t, n, i) { for (var r = e.length; r--;)
                            if (!e[r](t, n, i)) return !1;
                        return !0 } : e[0] }

                function g(e, n, i) { for (var r = 0, a = n.length; r < a; r++) t(e, n[r], i); return i }

                function v(e, t, n, i, r) { for (var a, o = [], s = 0, c = e.length, u = null != t; s < c; s++)(a = e[s]) && (n && !n(a, i, r) || (o.push(a), u && t.push(s))); return o }

                function m(e, t, n, r, a, o) { return r && !r[U] && (r = m(r)), a && !a[U] && (a = m(a, o)), i((function(i, o, s, c) { var u, l, d, f = [],
                            p = [],
                            h = o.length,
                            m = i || g(t || "*", s.nodeType ? [s] : s, []),
                            _ = !e || !i && t ? m : v(m, f, e, s, c),
                            E = n ? a || (i ? e : h || r) ? [] : o : _; if (n && n(_, E, s, c), r)
                            for (u = v(E, p), r(u, [], s, c), l = u.length; l--;)(d = u[l]) && (E[p[l]] = !(_[p[l]] = d)); if (i) { if (a || e) { if (a) { for (u = [], l = E.length; l--;)(d = E[l]) && u.push(_[l] = d);
                                    a(null, E = [], u, c) } for (l = E.length; l--;)(d = E[l]) && (u = a ? ee(i, d) : f[l]) > -1 && (i[u] = !(o[u] = d)) } } else E = v(E === o ? E.splice(h, E.length) : E), a ? a(null, o, E, c) : J.apply(o, E) })) }

                function _(e) { for (var t, n, i, r = e.length, a = T.relative[e[0].type], o = a || T.relative[" "], s = a ? 1 : 0, c = p((function(e) { return e === t }), o, !0), u = p((function(e) { return ee(t, e) > -1 }), o, !0), l = [function(e, n, i) { var r = !a && (i || n !== R) || ((t = n).nodeType ? c(e, n, i) : u(e, n, i)); return t = null, r }]; s < r; s++)
                        if (n = T.relative[e[s].type]) l = [p(h(l), n)];
                        else { if (n = T.filter[e[s].type].apply(null, e[s].matches), n[U]) { for (i = ++s; i < r && !T.relative[e[i].type]; i++); return m(s > 1 && h(l), s > 1 && f(e.slice(0, s - 1).concat({ value: " " === e[s - 2].type ? "*" : "" })).replace(ce, "$1"), n, s < i && _(e.slice(s, i)), i < r && _(e = e.slice(i)), i < r && f(e)) }
                            l.push(n) }
                    return h(l) }

                function E(e, n) { var r = n.length > 0,
                        a = e.length > 0,
                        o = function(i, o, s, c, u) { var l, d, f, p = 0,
                                h = "0",
                                g = i && [],
                                m = [],
                                _ = R,
                                E = i || a && T.find["TAG"]("*", u),
                                y = B += null == _ ? 1 : Math.random() || .1,
                                I = E.length; for (u && (R = o !== x && o); h !== I && null != (l = E[h]); h++) { if (a && l) { for (d = 0; f = e[d++];)
                                        if (f(l, o, s)) { c.push(l); break }
                                    u && (B = y) }
                                r && ((l = !f && l) && p--, i && g.push(l)) } if (p += h, r && h !== p) { for (d = 0; f = n[d++];) f(g, m, o, s); if (i) { if (p > 0)
                                        for (; h--;) g[h] || m[h] || (m[h] = $.call(c));
                                    m = v(m) }
                                J.apply(c, m), u && !i && m.length > 0 && p + n.length > 1 && t.uniqueSort(c) } return u && (B = y, R = _), g }; return r ? i(o) : o } var y, I, T, S, A, b, w, D, R, N, C, O, x, L, P, k, F, M, V, U = "sizzle" + 1 * new Date,
                    G = e.document,
                    B = 0,
                    j = 0,
                    H = n(),
                    z = n(),
                    q = n(),
                    Y = function(e, t) { return e === t && (C = !0), 0 },
                    K = 1 << 31,
                    W = {}.hasOwnProperty,
                    X = [],
                    $ = X.pop,
                    Q = X.push,
                    J = X.push,
                    Z = X.slice,
                    ee = function(e, t) { for (var n = 0, i = e.length; n < i; n++)
                            if (e[n] === t) return n;
                        return -1 },
                    te = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
                    ne = "[\\x20\\t\\r\\n\\f]",
                    ie = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
                    re = ie.replace("w", "w#"),
                    ae = "\\[" + ne + "*(" + ie + ")(?:" + ne + "*([*^$|!~]?=)" + ne + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + re + "))|)" + ne + "*\\]",
                    oe = ":(" + ie + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + ae + ")*)|.*)\\)|)",
                    se = new RegExp(ne + "+", "g"),
                    ce = new RegExp("^" + ne + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ne + "+$", "g"),
                    ue = new RegExp("^" + ne + "*," + ne + "*"),
                    le = new RegExp("^" + ne + "*([>+~]|" + ne + ")" + ne + "*"),
                    de = new RegExp("=" + ne + "*([^\\]'\"]*?)" + ne + "*\\]", "g"),
                    fe = new RegExp(oe),
                    pe = new RegExp("^" + re + "$"),
                    he = { ID: new RegExp("^#(" + ie + ")"), CLASS: new RegExp("^\\.(" + ie + ")"), TAG: new RegExp("^(" + ie.replace("w", "w*") + ")"), ATTR: new RegExp("^" + ae), PSEUDO: new RegExp("^" + oe), CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ne + "*(even|odd|(([+-]|)(\\d*)n|)" + ne + "*(?:([+-]|)" + ne + "*(\\d+)|))" + ne + "*\\)|)", "i"), bool: new RegExp("^(?:" + te + ")$", "i"), needsContext: new RegExp("^" + ne + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ne + "*((?:-\\d)?\\d*)" + ne + "*\\)|)(?=[^-]|$)", "i") },
                    ge = /^(?:input|select|textarea|button)$/i,
                    ve = /^h\d$/i,
                    me = /^[^{]+\{\s*\[native \w/,
                    _e = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
                    Ee = /[+~]/,
                    ye = /'|\\/g,
                    Ie = new RegExp("\\\\([\\da-f]{1,6}" + ne + "?|(" + ne + ")|.)", "ig"),
                    Te = function(e, t, n) { var i = "0x" + t - 65536; return i !== i || n ? t : i < 0 ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320) },
                    Se = function() { O() }; try { J.apply(X = Z.call(G.childNodes), G.childNodes), X[G.childNodes.length].nodeType } catch (e) { J = { apply: X.length ? function(e, t) { Q.apply(e, Z.call(t)) } : function(e, t) { for (var n = e.length, i = 0; e[n++] = t[i++];);
                            e.length = n - 1 } } }
                I = t.support = {}, A = t.isXML = function(e) { var t = e && (e.ownerDocument || e).documentElement; return !!t && "HTML" !== t.nodeName }, O = t.setDocument = function(e) { var t, n, i = e ? e.ownerDocument || e : G; return i !== x && 9 === i.nodeType && i.documentElement ? (x = i, L = i.documentElement, n = i.defaultView, n && n !== n.top && (n.addEventListener ? n.addEventListener("unload", Se, !1) : n.attachEvent && n.attachEvent("onunload", Se)), P = !A(i), I.attributes = r((function(e) { return e.className = "i", !e.getAttribute("className") })), I.getElementsByTagName = r((function(e) { return e.appendChild(i.createComment("")), !e.getElementsByTagName("*").length })), I.getElementsByClassName = me.test(i.getElementsByClassName), I.getById = r((function(e) { return L.appendChild(e).id = U, !i.getElementsByName || !i.getElementsByName(U).length })), I.getById ? (T.find["ID"] = function(e, t) { if ("undefined" != typeof t.getElementById && P) { var n = t.getElementById(e); return n && n.parentNode ? [n] : [] } }, T.filter["ID"] = function(e) { var t = e.replace(Ie, Te); return function(e) { return e.getAttribute("id") === t } }) : (delete T.find["ID"], T.filter["ID"] = function(e) { var t = e.replace(Ie, Te); return function(e) { var n = "undefined" != typeof e.getAttributeNode && e.getAttributeNode("id"); return n && n.value === t } }), T.find["TAG"] = I.getElementsByTagName ? function(e, t) { return "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e) : I.qsa ? t.querySelectorAll(e) : void 0 } : function(e, t) { var n, i = [],
                            r = 0,
                            a = t.getElementsByTagName(e); if ("*" === e) { for (; n = a[r++];) 1 === n.nodeType && i.push(n); return i } return a }, T.find["CLASS"] = I.getElementsByClassName && function(e, t) { if (P) return t.getElementsByClassName(e) }, F = [], k = [], (I.qsa = me.test(i.querySelectorAll)) && (r((function(e) { L.appendChild(e).innerHTML = "<a id='" + U + "'></a><select id='" + U + "-\f]' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && k.push("[*^$]=" + ne + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || k.push("\\[" + ne + "*(?:value|" + te + ")"), e.querySelectorAll("[id~=" + U + "-]").length || k.push("~="), e.querySelectorAll(":checked").length || k.push(":checked"), e.querySelectorAll("a#" + U + "+*").length || k.push(".#.+[+~]") })), r((function(e) { var t = i.createElement("input");
                        t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && k.push("name" + ne + "*[*^$|!~]?="), e.querySelectorAll(":enabled").length || k.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), k.push(",.*:") }))), (I.matchesSelector = me.test(M = L.matches || L.webkitMatchesSelector || L.mozMatchesSelector || L.oMatchesSelector || L.msMatchesSelector)) && r((function(e) { I.disconnectedMatch = M.call(e, "div"), M.call(e, "[s!='']:x"), F.push("!=", oe) })), k = k.length && new RegExp(k.join("|")), F = F.length && new RegExp(F.join("|")), t = me.test(L.compareDocumentPosition), V = t || me.test(L.contains) ? function(e, t) { var n = 9 === e.nodeType ? e.documentElement : e,
                            i = t && t.parentNode; return e === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i))) } : function(e, t) { if (t)
                            for (; t = t.parentNode;)
                                if (t === e) return !0;
                        return !1 }, Y = t ? function(e, t) { if (e === t) return C = !0, 0; var n = !e.compareDocumentPosition - !t.compareDocumentPosition; return n ? n : (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1, 1 & n || !I.sortDetached && t.compareDocumentPosition(e) === n ? e === i || e.ownerDocument === G && V(G, e) ? -1 : t === i || t.ownerDocument === G && V(G, t) ? 1 : N ? ee(N, e) - ee(N, t) : 0 : 4 & n ? -1 : 1) } : function(e, t) { if (e === t) return C = !0, 0; var n, r = 0,
                            a = e.parentNode,
                            s = t.parentNode,
                            c = [e],
                            u = [t]; if (!a || !s) return e === i ? -1 : t === i ? 1 : a ? -1 : s ? 1 : N ? ee(N, e) - ee(N, t) : 0; if (a === s) return o(e, t); for (n = e; n = n.parentNode;) c.unshift(n); for (n = t; n = n.parentNode;) u.unshift(n); for (; c[r] === u[r];) r++; return r ? o(c[r], u[r]) : c[r] === G ? -1 : u[r] === G ? 1 : 0 }, i) : x }, t.matches = function(e, n) { return t(e, null, null, n) }, t.matchesSelector = function(e, n) { if ((e.ownerDocument || e) !== x && O(e), n = n.replace(de, "='$1']"), I.matchesSelector && P && (!F || !F.test(n)) && (!k || !k.test(n))) try { var i = M.call(e, n); if (i || I.disconnectedMatch || e.document && 11 !== e.document.nodeType) return i } catch (e) {}
                    return t(n, x, null, [e]).length > 0 }, t.contains = function(e, t) { return (e.ownerDocument || e) !== x && O(e), V(e, t) }, t.attr = function(e, t) {
                    (e.ownerDocument || e) !== x && O(e); var n = T.attrHandle[t.toLowerCase()],
                        i = n && W.call(T.attrHandle, t.toLowerCase()) ? n(e, t, !P) : void 0; return void 0 !== i ? i : I.attributes || !P ? e.getAttribute(t) : (i = e.getAttributeNode(t)) && i.specified ? i.value : null }, t.error = function(e) { throw new Error("Syntax error, unrecognized expression: " + e) }, t.uniqueSort = function(e) { var t, n = [],
                        i = 0,
                        r = 0; if (C = !I.detectDuplicates, N = !I.sortStable && e.slice(0), e.sort(Y), C) { for (; t = e[r++];) t === e[r] && (i = n.push(r)); for (; i--;) e.splice(n[i], 1) } return N = null, e }, S = t.getText = function(e) { var t, n = "",
                        i = 0,
                        r = e.nodeType; if (r) { if (1 === r || 9 === r || 11 === r) { if ("string" == typeof e.textContent) return e.textContent; for (e = e.firstChild; e; e = e.nextSibling) n += S(e) } else if (3 === r || 4 === r) return e.nodeValue } else
                        for (; t = e[i++];) n += S(t); return n }, T = t.selectors = { cacheLength: 50, createPseudo: i, match: he, attrHandle: {}, find: {}, relative: { ">": { dir: "parentNode", first: !0 }, " ": { dir: "parentNode" }, "+": { dir: "previousSibling", first: !0 }, "~": { dir: "previousSibling" } }, preFilter: { ATTR: function(e) { return e[1] = e[1].replace(Ie, Te), e[3] = (e[3] || e[4] || e[5] || "").replace(Ie, Te), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4) }, CHILD: function(e) { return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || t.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && t.error(e[0]), e }, PSEUDO: function(e) { var t, n = !e[6] && e[2]; return he["CHILD"].test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && fe.test(n) && (t = b(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3)) } }, filter: { TAG: function(e) { var t = e.replace(Ie, Te).toLowerCase(); return "*" === e ? function() { return !0 } : function(e) { return e.nodeName && e.nodeName.toLowerCase() === t } }, CLASS: function(e) { var t = H[e + " "]; return t || (t = new RegExp("(^|" + ne + ")" + e + "(" + ne + "|$)")) && H(e, (function(e) { return t.test("string" == typeof e.className && e.className || "undefined" != typeof e.getAttribute && e.getAttribute("class") || "") })) }, ATTR: function(e, n, i) { return function(r) { var a = t.attr(r, e); return null == a ? "!=" === n : !n || (a += "", "=" === n ? a === i : "!=" === n ? a !== i : "^=" === n ? i && 0 === a.indexOf(i) : "*=" === n ? i && a.indexOf(i) > -1 : "$=" === n ? i && a.slice(-i.length) === i : "~=" === n ? (" " + a.replace(se, " ") + " ").indexOf(i) > -1 : "|=" === n && (a === i || a.slice(0, i.length + 1) === i + "-")) } }, CHILD: function(e, t, n, i, r) { var a = "nth" !== e.slice(0, 3),
                                o = "last" !== e.slice(-4),
                                s = "of-type" === t; return 1 === i && 0 === r ? function(e) { return !!e.parentNode } : function(t, n, c) { var u, l, d, f, p, h, g = a !== o ? "nextSibling" : "previousSibling",
                                    v = t.parentNode,
                                    m = s && t.nodeName.toLowerCase(),
                                    _ = !c && !s; if (v) { if (a) { for (; g;) { for (d = t; d = d[g];)
                                                if (s ? d.nodeName.toLowerCase() === m : 1 === d.nodeType) return !1;
                                            h = g = "only" === e && !h && "nextSibling" } return !0 } if (h = [o ? v.firstChild : v.lastChild], o && _) { for (l = v[U] || (v[U] = {}), u = l[e] || [], p = u[0] === B && u[1], f = u[0] === B && u[2], d = p && v.childNodes[p]; d = ++p && d && d[g] || (f = p = 0) || h.pop();)
                                            if (1 === d.nodeType && ++f && d === t) { l[e] = [B, p, f]; break } } else if (_ && (u = (t[U] || (t[U] = {}))[e]) && u[0] === B) f = u[1];
                                    else
                                        for (;
                                            (d = ++p && d && d[g] || (f = p = 0) || h.pop()) && ((s ? d.nodeName.toLowerCase() !== m : 1 !== d.nodeType) || !++f || (_ && ((d[U] || (d[U] = {}))[e] = [B, f]), d !== t));); return f -= r, f === i || f % i === 0 && f / i >= 0 } } }, PSEUDO: function(e, n) { var r, a = T.pseudos[e] || T.setFilters[e.toLowerCase()] || t.error("unsupported pseudo: " + e); return a[U] ? a(n) : a.length > 1 ? (r = [e, e, "", n], T.setFilters.hasOwnProperty(e.toLowerCase()) ? i((function(e, t) { for (var i, r = a(e, n), o = r.length; o--;) i = ee(e, r[o]), e[i] = !(t[i] = r[o]) })) : function(e) { return a(e, 0, r) }) : a } }, pseudos: { not: i((function(e) { var t = [],
                                n = [],
                                r = w(e.replace(ce, "$1")); return r[U] ? i((function(e, t, n, i) { for (var a, o = r(e, null, i, []), s = e.length; s--;)(a = o[s]) && (e[s] = !(t[s] = a)) })) : function(e, i, a) { return t[0] = e, r(t, null, a, n), t[0] = null, !n.pop() } })), has: i((function(e) { return function(n) { return t(e, n).length > 0 } })), contains: i((function(e) { return e = e.replace(Ie, Te),
                                function(t) { return (t.textContent || t.innerText || S(t)).indexOf(e) > -1 } })), lang: i((function(e) { return pe.test(e || "") || t.error("unsupported lang: " + e), e = e.replace(Ie, Te).toLowerCase(),
                                function(t) { var n;
                                    do
                                        if (n = P ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return n = n.toLowerCase(), n === e || 0 === n.indexOf(e + "-"); while ((t = t.parentNode) && 1 === t.nodeType);
                                    return !1 } })), target: function(t) { var n = e.location && e.location.hash; return n && n.slice(1) === t.id }, root: function(e) { return e === L }, focus: function(e) { return e === x.activeElement && (!x.hasFocus || x.hasFocus()) && !!(e.type || e.href || ~e.tabIndex) }, enabled: function(e) { return e.disabled === !1 }, disabled: function(e) { return e.disabled === !0 }, checked: function(e) { var t = e.nodeName.toLowerCase(); return "input" === t && !!e.checked || "option" === t && !!e.selected }, selected: function(e) { return e.parentNode && e.parentNode.selectedIndex, e.selected === !0 }, empty: function(e) { for (e = e.firstChild; e; e = e.nextSibling)
                                if (e.nodeType < 6) return !1;
                            return !0 }, parent: function(e) { return !T.pseudos["empty"](e) }, header: function(e) { return ve.test(e.nodeName) }, input: function(e) { return ge.test(e.nodeName) }, button: function(e) { var t = e.nodeName.toLowerCase(); return "input" === t && "button" === e.type || "button" === t }, text: function(e) { var t; return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase()) }, first: u((function() { return [0] })), last: u((function(e, t) { return [t - 1] })), eq: u((function(e, t, n) { return [n < 0 ? n + t : n] })), even: u((function(e, t) { for (var n = 0; n < t; n += 2) e.push(n); return e })), odd: u((function(e, t) { for (var n = 1; n < t; n += 2) e.push(n); return e })), lt: u((function(e, t, n) { for (var i = n < 0 ? n + t : n; --i >= 0;) e.push(i); return e })), gt: u((function(e, t, n) { for (var i = n < 0 ? n + t : n; ++i < t;) e.push(i); return e })) } }, T.pseudos["nth"] = T.pseudos["eq"]; for (y in { radio: !0, checkbox: !0, file: !0, password: !0, image: !0 }) T.pseudos[y] = s(y); for (y in { submit: !0, reset: !0 }) T.pseudos[y] = c(y); return d.prototype = T.filters = T.pseudos, T.setFilters = new d, b = t.tokenize = function(e, n) { var i, r, a, o, s, c, u, l = z[e + " "]; if (l) return n ? 0 : l.slice(0); for (s = e, c = [], u = T.preFilter; s;) { i && !(r = ue.exec(s)) || (r && (s = s.slice(r[0].length) || s), c.push(a = [])), i = !1, (r = le.exec(s)) && (i = r.shift(), a.push({ value: i, type: r[0].replace(ce, " ") }), s = s.slice(i.length)); for (o in T.filter) !(r = he[o].exec(s)) || u[o] && !(r = u[o](r)) || (i = r.shift(), a.push({ value: i, type: o, matches: r }), s = s.slice(i.length)); if (!i) break } return n ? s.length : s ? t.error(e) : z(e, c).slice(0) }, w = t.compile = function(e, t) { var n, i = [],
                        r = [],
                        a = q[e + " "]; if (!a) { for (t || (t = b(e)), n = t.length; n--;) a = _(t[n]), a[U] ? i.push(a) : r.push(a);
                        a = q(e, E(r, i)), a.selector = e } return a }, D = t.select = function(e, t, n, i) { var r, a, o, s, c, u = "function" == typeof e && e,
                        d = !i && b(e = u.selector || e); if (n = n || [], 1 === d.length) { if (a = d[0] = d[0].slice(0), a.length > 2 && "ID" === (o = a[0]).type && I.getById && 9 === t.nodeType && P && T.relative[a[1].type]) { if (t = (T.find["ID"](o.matches[0].replace(Ie, Te), t) || [])[0], !t) return n;
                            u && (t = t.parentNode), e = e.slice(a.shift().value.length) } for (r = he["needsContext"].test(e) ? 0 : a.length; r-- && (o = a[r], !T.relative[s = o.type]);)
                            if ((c = T.find[s]) && (i = c(o.matches[0].replace(Ie, Te), Ee.test(a[0].type) && l(t.parentNode) || t))) { if (a.splice(r, 1), e = i.length && f(a), !e) return J.apply(n, i), n; break } } return (u || w(e, d))(i, t, !P, n, Ee.test(e) && l(t.parentNode) || t), n }, I.sortStable = U.split("").sort(Y).join("") === U, I.detectDuplicates = !!C, O(), I.sortDetached = r((function(e) { return 1 & e.compareDocumentPosition(x.createElement("div")) })), r((function(e) { return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href") })) || a("type|href|height|width", (function(e, t, n) { if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2) })), I.attributes && r((function(e) { return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value") })) || a("value", (function(e, t, n) { if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue })), r((function(e) { return null == e.getAttribute("disabled") })) || a(te, (function(e, t, n) { var i; if (!n) return e[t] === !0 ? t.toLowerCase() : (i = e.getAttributeNode(t)) && i.specified ? i.value : null })), t })(n);
        ce.find = pe, ce.expr = pe.selectors, ce.expr[":"] = ce.expr.pseudos, ce.unique = pe.uniqueSort, ce.text = pe.getText, ce.isXMLDoc = pe.isXML, ce.contains = pe.contains;
        var he = ce.expr.match.needsContext,
            ge = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
            ve = /^.[^:#\[\.,]*$/;
        ce.filter = function(e, t, n) { var i = t[0]; return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === i.nodeType ? ce.find.matchesSelector(i, e) ? [i] : [] : ce.find.matches(e, ce.grep(t, (function(e) { return 1 === e.nodeType }))) }, ce.fn.extend({ find: function(e) { var t, n = [],
                    i = this,
                    r = i.length; if ("string" != typeof e) return this.pushStack(ce(e).filter((function() { for (t = 0; t < r; t++)
                        if (ce.contains(i[t], this)) return !0 }))); for (t = 0; t < r; t++) ce.find(e, i[t], n); return n = this.pushStack(r > 1 ? ce.unique(n) : n), n.selector = this.selector ? this.selector + " " + e : e, n }, filter: function(e) { return this.pushStack(s(this, e || [], !1)) }, not: function(e) { return this.pushStack(s(this, e || [], !0)) }, is: function(e) { return !!s(this, "string" == typeof e && he.test(e) ? ce(e) : e || [], !1).length } });
        var me, _e = n.document,
            Ee = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
            ye = ce.fn.init = function(e, t) { var n, i; if (!e) return this; if ("string" == typeof e) { if (n = "<" === e.charAt(0) && ">" === e.charAt(e.length - 1) && e.length >= 3 ? [null, e, null] : Ee.exec(e), !n || !n[1] && t) return !t || t.jquery ? (t || me).find(e) : this.constructor(t).find(e); if (n[1]) { if (t = t instanceof ce ? t[0] : t, ce.merge(this, ce.parseHTML(n[1], t && t.nodeType ? t.ownerDocument || t : _e, !0)), ge.test(n[1]) && ce.isPlainObject(t))
                            for (n in t) ce.isFunction(this[n]) ? this[n](t[n]) : this.attr(n, t[n]); return this } if (i = _e.getElementById(n[2]), i && i.parentNode) { if (i.id !== n[2]) return me.find(e);
                        this.length = 1, this[0] = i } return this.context = _e, this.selector = e, this } return e.nodeType ? (this.context = this[0] = e, this.length = 1, this) : ce.isFunction(e) ? "undefined" != typeof me.ready ? me.ready(e) : e(ce) : (void 0 !== e.selector && (this.selector = e.selector, this.context = e.context), ce.makeArray(e, this)) };
        ye.prototype = ce.fn, me = ce(_e);
        var Ie = /^(?:parents|prev(?:Until|All))/,
            Te = { children: !0, contents: !0, next: !0, prev: !0 };
        ce.extend({ dir: function(e, t, n) { for (var i = [], r = e[t]; r && 9 !== r.nodeType && (void 0 === n || 1 !== r.nodeType || !ce(r).is(n));) 1 === r.nodeType && i.push(r), r = r[t]; return i }, sibling: function(e, t) { for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e); return n } }), ce.fn.extend({ has: function(e) { var t, n = ce(e, this),
                    i = n.length; return this.filter((function() { for (t = 0; t < i; t++)
                        if (ce.contains(this, n[t])) return !0 })) }, closest: function(e, t) { for (var n, i = 0, r = this.length, a = [], o = he.test(e) || "string" != typeof e ? ce(e, t || this.context) : 0; i < r; i++)
                    for (n = this[i]; n && n !== t; n = n.parentNode)
                        if (n.nodeType < 11 && (o ? o.index(n) > -1 : 1 === n.nodeType && ce.find.matchesSelector(n, e))) { a.push(n); break }
                return this.pushStack(a.length > 1 ? ce.unique(a) : a) }, index: function(e) { return e ? "string" == typeof e ? ce.inArray(this[0], ce(e)) : ce.inArray(e.jquery ? e[0] : e, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1 }, add: function(e, t) { return this.pushStack(ce.unique(ce.merge(this.get(), ce(e, t)))) }, addBack: function(e) { return this.add(null == e ? this.prevObject : this.prevObject.filter(e)) } }), ce.each({ parent: function(e) { var t = e.parentNode; return t && 11 !== t.nodeType ? t : null }, parents: function(e) { return ce.dir(e, "parentNode") }, parentsUntil: function(e, t, n) { return ce.dir(e, "parentNode", n) }, next: function(e) { return c(e, "nextSibling") }, prev: function(e) { return c(e, "previousSibling") }, nextAll: function(e) { return ce.dir(e, "nextSibling") }, prevAll: function(e) { return ce.dir(e, "previousSibling") }, nextUntil: function(e, t, n) { return ce.dir(e, "nextSibling", n) }, prevUntil: function(e, t, n) { return ce.dir(e, "previousSibling", n) }, siblings: function(e) { return ce.sibling((e.parentNode || {}).firstChild, e) }, children: function(e) { return ce.sibling(e.firstChild) }, contents: function(e) { return ce.nodeName(e, "iframe") ? e.contentDocument || e.contentWindow.document : ce.merge([], e.childNodes) } }, (function(e, t) { ce.fn[e] = function(n, i) { var r = ce.map(this, t, n); return "Until" !== e.slice(-5) && (i = n), i && "string" == typeof i && (r = ce.filter(i, r)), this.length > 1 && (Te[e] || (r = ce.unique(r)), Ie.test(e) && (r = r.reverse())), this.pushStack(r) } }));
        var Se = /\S+/g,
            Ae = {};
        ce.Callbacks = function(e) { e = "string" == typeof e ? Ae[e] || u(e) : ce.extend({}, e); var t, n, i, r, a, o, s = [],
                c = !e.once && [],
                l = function(u) { for (n = e.memory && u, i = !0, a = o || 0, o = 0, r = s.length, t = !0; s && a < r; a++)
                        if (s[a].apply(u[0], u[1]) === !1 && e.stopOnFalse) { n = !1; break }
                    t = !1, s && (c ? c.length && l(c.shift()) : n ? s = [] : d.disable()) },
                d = { add: function() { if (s) { var i = s.length;!(function t(n) { ce.each(n, (function(n, i) { var r = ce.type(i); "function" === r ? e.unique && d.has(i) || s.push(i) : i && i.length && "string" !== r && t(i) })) })(arguments), t ? r = s.length : n && (o = i, l(n)) } return this }, remove: function() { return s && ce.each(arguments, (function(e, n) { for (var i;
                                (i = ce.inArray(n, s, i)) > -1;) s.splice(i, 1), t && (i <= r && r--, i <= a && a--) })), this }, has: function(e) { return e ? ce.inArray(e, s) > -1 : !(!s || !s.length) }, empty: function() { return s = [], r = 0, this }, disable: function() { return s = c = n = void 0, this }, disabled: function() { return !s }, lock: function() { return c = void 0, n || d.disable(), this }, locked: function() { return !c }, fireWith: function(e, n) { return !s || i && !c || (n = n || [], n = [e, n.slice ? n.slice() : n], t ? c.push(n) : l(n)), this }, fire: function() { return d.fireWith(this, arguments), this }, fired: function() { return !!i } }; return d }, ce.extend({ Deferred: function(e) { var t = [
                        ["resolve", "done", ce.Callbacks("once memory"), "resolved"],
                        ["reject", "fail", ce.Callbacks("once memory"), "rejected"],
                        ["notify", "progress", ce.Callbacks("memory")]
                    ],
                    n = "pending",
                    i = { state: function() { return n }, always: function() { return r.done(arguments).fail(arguments), this }, then: function() { var e = arguments; return ce.Deferred((function(n) { ce.each(t, (function(t, a) { var o = ce.isFunction(e[t]) && e[t];
                                    r[a[1]]((function() { var e = o && o.apply(this, arguments);
                                        e && ce.isFunction(e.promise) ? e.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[a[0] + "With"](this === i ? n.promise() : this, o ? [e] : arguments) })) })), e = null })).promise() }, promise: function(e) { return null != e ? ce.extend(e, i) : i } },
                    r = {}; return i.pipe = i.then, ce.each(t, (function(e, a) { var o = a[2],
                        s = a[3];
                    i[a[1]] = o.add, s && o.add((function() { n = s }), t[1 ^ e][2].disable, t[2][2].lock), r[a[0]] = function() { return r[a[0] + "With"](this === r ? i : this, arguments), this }, r[a[0] + "With"] = o.fireWith })), i.promise(r), e && e.call(r, r), r }, when: function(e) { var t, n, i, r = 0,
                    a = Z.call(arguments),
                    o = a.length,
                    s = 1 !== o || e && ce.isFunction(e.promise) ? o : 0,
                    c = 1 === s ? e : ce.Deferred(),
                    u = function(e, n, i) { return function(r) { n[e] = this, i[e] = arguments.length > 1 ? Z.call(arguments) : r, i === t ? c.notifyWith(n, i) : --s || c.resolveWith(n, i) } }; if (o > 1)
                    for (t = new Array(o), n = new Array(o), i = new Array(o); r < o; r++) a[r] && ce.isFunction(a[r].promise) ? a[r].promise().done(u(r, i, a)).fail(c.reject).progress(u(r, n, t)) : --s; return s || c.resolveWith(i, a), c.promise() } });
        var be;
        ce.fn.ready = function(e) { return ce.ready.promise().done(e), this }, ce.extend({ isReady: !1, readyWait: 1, holdReady: function(e) { e ? ce.readyWait++ : ce.ready(!0) }, ready: function(e) { if (e === !0 ? !--ce.readyWait : !ce.isReady) { if (!_e.body) return setTimeout(ce.ready);
                    ce.isReady = !0, e !== !0 && --ce.readyWait > 0 || (be.resolveWith(_e, [ce]), ce.fn.triggerHandler && (ce(_e).triggerHandler("ready"), ce(_e).off("ready"))) } } }), ce.ready.promise = function(e) { if (!be)
                if (be = ce.Deferred(), "complete" === _e.readyState) setTimeout(ce.ready);
                else if (_e.addEventListener) _e.addEventListener("DOMContentLoaded", d, !1), n.addEventListener("load", d, !1);
            else { _e.attachEvent("onreadystatechange", d), n.attachEvent("onload", d); var t = !1; try { t = null == n.frameElement && _e.documentElement } catch (e) {}
                t && t.doScroll && !(function e() { if (!ce.isReady) { try { t.doScroll("left") } catch (t) { return setTimeout(e, 50) }
                        l(), ce.ready() } })() } return be.promise(e) };
        var we, De = "undefined";
        for (we in ce(oe)) break;
        oe.ownLast = "0" !== we, oe.inlineBlockNeedsLayout = !1, ce((function() { var e, t, n, i;
            n = _e.getElementsByTagName("body")[0], n && n.style && (t = _e.createElement("div"), i = _e.createElement("div"), i.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", n.appendChild(i).appendChild(t), typeof t.style.zoom !== De && (t.style.cssText = "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1", oe.inlineBlockNeedsLayout = e = 3 === t.offsetWidth, e && (n.style.zoom = 1)), n.removeChild(i)) })), (function() { var e = _e.createElement("div"); if (null == oe.deleteExpando) { oe.deleteExpando = !0; try { delete e.test } catch (e) { oe.deleteExpando = !1 } }
            e = null })(), ce.acceptData = function(e) { var t = ce.noData[(e.nodeName + " ").toLowerCase()],
                n = +e.nodeType || 1; return (1 === n || 9 === n) && (!t || t !== !0 && e.getAttribute("classid") === t) };
        var Re = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
            Ne = /([A-Z])/g;
        ce.extend({ cache: {}, noData: { "applet ": !0, "embed ": !0, "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" }, hasData: function(e) { return e = e.nodeType ? ce.cache[e[ce.expando]] : e[ce.expando], !!e && !p(e) }, data: function(e, t, n) { return h(e, t, n) }, removeData: function(e, t) { return g(e, t) }, _data: function(e, t, n) { return h(e, t, n, !0) }, _removeData: function(e, t) { return g(e, t, !0) } }), ce.fn.extend({ data: function(e, t) { var n, i, r, a = this[0],
                    o = a && a.attributes; if (void 0 === e) { if (this.length && (r = ce.data(a), 1 === a.nodeType && !ce._data(a, "parsedAttrs"))) { for (n = o.length; n--;) o[n] && (i = o[n].name, 0 === i.indexOf("data-") && (i = ce.camelCase(i.slice(5)), f(a, i, r[i])));
                        ce._data(a, "parsedAttrs", !0) } return r } return "object" == typeof e ? this.each((function() { ce.data(this, e) })) : arguments.length > 1 ? this.each((function() { ce.data(this, e, t) })) : a ? f(a, e, ce.data(a, e)) : void 0 }, removeData: function(e) { return this.each((function() { ce.removeData(this, e) })) } }), ce.extend({ queue: function(e, t, n) { var i; if (e) return t = (t || "fx") + "queue", i = ce._data(e, t), n && (!i || ce.isArray(n) ? i = ce._data(e, t, ce.makeArray(n)) : i.push(n)), i || [] }, dequeue: function(e, t) { t = t || "fx"; var n = ce.queue(e, t),
                    i = n.length,
                    r = n.shift(),
                    a = ce._queueHooks(e, t),
                    o = function() { ce.dequeue(e, t) }; "inprogress" === r && (r = n.shift(), i--), r && ("fx" === t && n.unshift("inprogress"), delete a.stop, r.call(e, o, a)), !i && a && a.empty.fire() }, _queueHooks: function(e, t) { var n = t + "queueHooks"; return ce._data(e, n) || ce._data(e, n, { empty: ce.Callbacks("once memory").add((function() { ce._removeData(e, t + "queue"), ce._removeData(e, n) })) }) } }), ce.fn.extend({ queue: function(e, t) { var n = 2; return "string" != typeof e && (t = e, e = "fx", n--), arguments.length < n ? ce.queue(this[0], e) : void 0 === t ? this : this.each((function() { var n = ce.queue(this, e, t);
                    ce._queueHooks(this, e), "fx" === e && "inprogress" !== n[0] && ce.dequeue(this, e) })) }, dequeue: function(e) { return this.each((function() { ce.dequeue(this, e) })) }, clearQueue: function(e) { return this.queue(e || "fx", []) }, promise: function(e, t) { var n, i = 1,
                    r = ce.Deferred(),
                    a = this,
                    o = this.length,
                    s = function() {--i || r.resolveWith(a, [a]) }; for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; o--;) n = ce._data(a[o], e + "queueHooks"), n && n.empty && (i++, n.empty.add(s)); return s(), r.promise(t) } });
        var Ce = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
            Oe = ["Top", "Right", "Bottom", "Left"],
            xe = function(e, t) { return e = t || e, "none" === ce.css(e, "display") || !ce.contains(e.ownerDocument, e) },
            Le = ce.access = function(e, t, n, i, r, a, o) { var s = 0,
                    c = e.length,
                    u = null == n; if ("object" === ce.type(n)) { r = !0; for (s in n) ce.access(e, t, s, n[s], !0, a, o) } else if (void 0 !== i && (r = !0, ce.isFunction(i) || (o = !0), u && (o ? (t.call(e, i), t = null) : (u = t, t = function(e, t, n) { return u.call(ce(e), n) })), t))
                    for (; s < c; s++) t(e[s], n, o ? i : i.call(e[s], s, t(e[s], n))); return r ? e : u ? t.call(e) : c ? t(e[0], n) : a },
            Pe = /^(?:checkbox|radio)$/i;
        !(function() { var e = _e.createElement("input"),
                t = _e.createElement("div"),
                n = _e.createDocumentFragment(); if (t.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", oe.leadingWhitespace = 3 === t.firstChild.nodeType, oe.tbody = !t.getElementsByTagName("tbody").length, oe.htmlSerialize = !!t.getElementsByTagName("link").length, oe.html5Clone = "<:nav></:nav>" !== _e.createElement("nav").cloneNode(!0).outerHTML, e.type = "checkbox", e.checked = !0, n.appendChild(e), oe.appendChecked = e.checked, t.innerHTML = "<textarea>x</textarea>", oe.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue, n.appendChild(t), t.innerHTML = "<input type='radio' checked='checked' name='t'/>", oe.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, oe.noCloneEvent = !0, t.attachEvent && (t.attachEvent("onclick", (function() { oe.noCloneEvent = !1 })), t.cloneNode(!0).click()), null == oe.deleteExpando) { oe.deleteExpando = !0; try { delete t.test } catch (e) { oe.deleteExpando = !1 } } })(), (function() { var e, t, i = _e.createElement("div"); for (e in { submit: !0, change: !0, focusin: !0 }) t = "on" + e, (oe[e + "Bubbles"] = t in n) || (i.setAttribute(t, "t"), oe[e + "Bubbles"] = i.attributes[t].expando === !1);
            i = null })();
        var ke = /^(?:input|select|textarea)$/i,
            Fe = /^key/,
            Me = /^(?:mouse|pointer|contextmenu)|click/,
            Ve = /^(?:focusinfocus|focusoutblur)$/,
            Ue = /^([^.]*)(?:\.(.+)|)$/;
        ce.event = {
            global: {},
            add: function(e, t, n, i, r) { var a, o, s, c, u, l, d, f, p, h, g, v = ce._data(e); if (v) { for (n.handler && (c = n, n = c.handler, r = c.selector), n.guid || (n.guid = ce.guid++), (o = v.events) || (o = v.events = {}), (l = v.handle) || (l = v.handle = function(e) { return typeof ce === De || e && ce.event.triggered === e.type ? void 0 : ce.event.dispatch.apply(l.elem, arguments) }, l.elem = e), t = (t || "").match(Se) || [""], s = t.length; s--;) a = Ue.exec(t[s]) || [], p = g = a[1], h = (a[2] || "").split(".").sort(), p && (u = ce.event.special[p] || {}, p = (r ? u.delegateType : u.bindType) || p, u = ce.event.special[p] || {}, d = ce.extend({ type: p, origType: g, data: i, handler: n, guid: n.guid, selector: r, needsContext: r && ce.expr.match.needsContext.test(r), namespace: h.join(".") }, c), (f = o[p]) || (f = o[p] = [], f.delegateCount = 0, u.setup && u.setup.call(e, i, h, l) !== !1 || (e.addEventListener ? e.addEventListener(p, l, !1) : e.attachEvent && e.attachEvent("on" + p, l))), u.add && (u.add.call(e, d), d.handler.guid || (d.handler.guid = n.guid)), r ? f.splice(f.delegateCount++, 0, d) : f.push(d), ce.event.global[p] = !0);
                    e = null } },
            remove: function(e, t, n, i, r) {
                var a, o, s, c, u, l, d, f, p, h, g, v = ce.hasData(e) && ce._data(e);
                if (v && (l = v.events)) {
                    for (t = (t || "").match(Se) || [""], u = t.length; u--;)
                        if (s = Ue.exec(t[u]) || [], p = g = s[1], h = (s[2] || "").split(".").sort(), p) {
                            for (d = ce.event.special[p] || {}, p = (i ? d.delegateType : d.bindType) || p, f = l[p] || [], s = s[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"),
                                c = a = f.length; a--;) o = f[a], !r && g !== o.origType || n && n.guid !== o.guid || s && !s.test(o.namespace) || i && i !== o.selector && ("**" !== i || !o.selector) || (f.splice(a, 1), o.selector && f.delegateCount--, d.remove && d.remove.call(e, o));
                            c && !f.length && (d.teardown && d.teardown.call(e, h, v.handle) !== !1 || ce.removeEvent(e, p, v.handle), delete l[p])
                        } else
                            for (p in l) ce.event.remove(e, p + t[u], n, i, !0);
                    ce.isEmptyObject(l) && (delete v.handle, ce._removeData(e, "events"))
                }
            },
            trigger: function(e, t, i, r) { var a, o, s, c, u, l, d, f = [i || _e],
                    p = ae.call(e, "type") ? e.type : e,
                    h = ae.call(e, "namespace") ? e.namespace.split(".") : []; if (s = l = i = i || _e, 3 !== i.nodeType && 8 !== i.nodeType && !Ve.test(p + ce.event.triggered) && (p.indexOf(".") >= 0 && (h = p.split("."), p = h.shift(), h.sort()), o = p.indexOf(":") < 0 && "on" + p, e = e[ce.expando] ? e : new ce.Event(p, "object" == typeof e && e), e.isTrigger = r ? 2 : 3, e.namespace = h.join("."), e.namespace_re = e.namespace ? new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = i), t = null == t ? [e] : ce.makeArray(t, [e]), u = ce.event.special[p] || {}, r || !u.trigger || u.trigger.apply(i, t) !== !1)) { if (!r && !u.noBubble && !ce.isWindow(i)) { for (c = u.delegateType || p, Ve.test(c + p) || (s = s.parentNode); s; s = s.parentNode) f.push(s), l = s;
                        l === (i.ownerDocument || _e) && f.push(l.defaultView || l.parentWindow || n) } for (d = 0;
                        (s = f[d++]) && !e.isPropagationStopped();) e.type = d > 1 ? c : u.bindType || p, a = (ce._data(s, "events") || {})[e.type] && ce._data(s, "handle"), a && a.apply(s, t), a = o && s[o], a && a.apply && ce.acceptData(s) && (e.result = a.apply(s, t), e.result === !1 && e.preventDefault()); if (e.type = p, !r && !e.isDefaultPrevented() && (!u._default || u._default.apply(f.pop(), t) === !1) && ce.acceptData(i) && o && i[p] && !ce.isWindow(i)) { l = i[o], l && (i[o] = null), ce.event.triggered = p; try { i[p]() } catch (e) {}
                        ce.event.triggered = void 0, l && (i[o] = l) } return e.result } },
            dispatch: function(e) { e = ce.event.fix(e); var t, n, i, r, a, o = [],
                    s = Z.call(arguments),
                    c = (ce._data(this, "events") || {})[e.type] || [],
                    u = ce.event.special[e.type] || {}; if (s[0] = e, e.delegateTarget = this, !u.preDispatch || u.preDispatch.call(this, e) !== !1) { for (o = ce.event.handlers.call(this, e, c), t = 0;
                        (r = o[t++]) && !e.isPropagationStopped();)
                        for (e.currentTarget = r.elem, a = 0;
                            (i = r.handlers[a++]) && !e.isImmediatePropagationStopped();) e.namespace_re && !e.namespace_re.test(i.namespace) || (e.handleObj = i, e.data = i.data, n = ((ce.event.special[i.origType] || {}).handle || i.handler).apply(r.elem, s), void 0 !== n && (e.result = n) === !1 && (e.preventDefault(), e.stopPropagation())); return u.postDispatch && u.postDispatch.call(this, e), e.result } },
            handlers: function(e, t) { var n, i, r, a, o = [],
                    s = t.delegateCount,
                    c = e.target; if (s && c.nodeType && (!e.button || "click" !== e.type))
                    for (; c != this; c = c.parentNode || this)
                        if (1 === c.nodeType && (c.disabled !== !0 || "click" !== e.type)) { for (r = [], a = 0; a < s; a++) i = t[a], n = i.selector + " ", void 0 === r[n] && (r[n] = i.needsContext ? ce(n, this).index(c) >= 0 : ce.find(n, this, null, [c]).length), r[n] && r.push(i);
                            r.length && o.push({ elem: c, handlers: r }) }
                return s < t.length && o.push({ elem: this, handlers: t.slice(s) }), o },
            fix: function(e) { if (e[ce.expando]) return e; var t, n, i, r = e.type,
                    a = e,
                    o = this.fixHooks[r]; for (o || (this.fixHooks[r] = o = Me.test(r) ? this.mouseHooks : Fe.test(r) ? this.keyHooks : {}), i = o.props ? this.props.concat(o.props) : this.props, e = new ce.Event(a), t = i.length; t--;) n = i[t], e[n] = a[n]; return e.target || (e.target = a.srcElement || _e), 3 === e.target.nodeType && (e.target = e.target.parentNode), e.metaKey = !!e.metaKey, o.filter ? o.filter(e, a) : e },
            props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
            fixHooks: {},
            keyHooks: { props: "char charCode key keyCode".split(" "), filter: function(e, t) { return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e } },
            mouseHooks: { props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "), filter: function(e, t) { var n, i, r, a = t.button,
                        o = t.fromElement; return null == e.pageX && null != t.clientX && (i = e.target.ownerDocument || _e, r = i.documentElement, n = i.body, e.pageX = t.clientX + (r && r.scrollLeft || n && n.scrollLeft || 0) - (r && r.clientLeft || n && n.clientLeft || 0), e.pageY = t.clientY + (r && r.scrollTop || n && n.scrollTop || 0) - (r && r.clientTop || n && n.clientTop || 0)), !e.relatedTarget && o && (e.relatedTarget = o === e.target ? t.toElement : o), e.which || void 0 === a || (e.which = 1 & a ? 1 : 2 & a ? 3 : 4 & a ? 2 : 0), e } },
            special: { load: { noBubble: !0 }, focus: { trigger: function() { if (this !== _() && this.focus) try { return this.focus(), !1 } catch (e) {} }, delegateType: "focusin" }, blur: { trigger: function() { if (this === _() && this.blur) return this.blur(), !1 }, delegateType: "focusout" }, click: { trigger: function() { if (ce.nodeName(this, "input") && "checkbox" === this.type && this.click) return this.click(), !1 }, _default: function(e) { return ce.nodeName(e.target, "a") } }, beforeunload: { postDispatch: function(e) { void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result) } } },
            simulate: function(e, t, n, i) { var r = ce.extend(new ce.Event, n, { type: e, isSimulated: !0, originalEvent: {} });
                i ? ce.event.trigger(r, null, t) : ce.event.dispatch.call(t, r), r.isDefaultPrevented() && n.preventDefault() }
        }, ce.removeEvent = _e.removeEventListener ? function(e, t, n) { e.removeEventListener && e.removeEventListener(t, n, !1) } : function(e, t, n) { var i = "on" + t;
            e.detachEvent && (typeof e[i] === De && (e[i] = null), e.detachEvent(i, n)) }, ce.Event = function(e, t) { return this instanceof ce.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && e.returnValue === !1 ? v : m) : this.type = e, t && ce.extend(this, t), this.timeStamp = e && e.timeStamp || ce.now(), void(this[ce.expando] = !0)) : new ce.Event(e, t) }, ce.Event.prototype = { isDefaultPrevented: m, isPropagationStopped: m, isImmediatePropagationStopped: m, preventDefault: function() { var e = this.originalEvent;
                this.isDefaultPrevented = v, e && (e.preventDefault ? e.preventDefault() : e.returnValue = !1) }, stopPropagation: function() { var e = this.originalEvent;
                this.isPropagationStopped = v, e && (e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0) }, stopImmediatePropagation: function() { var e = this.originalEvent;
                this.isImmediatePropagationStopped = v, e && e.stopImmediatePropagation && e.stopImmediatePropagation(), this.stopPropagation() } }, ce.each({ mouseenter: "mouseover", mouseleave: "mouseout", pointerenter: "pointerover", pointerleave: "pointerout" }, (function(e, t) { ce.event.special[e] = { delegateType: t, bindType: t, handle: function(e) { var n, i = this,
                        r = e.relatedTarget,
                        a = e.handleObj; return r && (r === i || ce.contains(i, r)) || (e.type = a.origType, n = a.handler.apply(this, arguments), e.type = t), n } } })), oe.submitBubbles || (ce.event.special.submit = { setup: function() { return !ce.nodeName(this, "form") && void ce.event.add(this, "click._submit keypress._submit", (function(e) { var t = e.target,
                        n = ce.nodeName(t, "input") || ce.nodeName(t, "button") ? t.form : void 0;
                    n && !ce._data(n, "submitBubbles") && (ce.event.add(n, "submit._submit", (function(e) { e._submit_bubble = !0 })), ce._data(n, "submitBubbles", !0)) })) }, postDispatch: function(e) { e._submit_bubble && (delete e._submit_bubble, this.parentNode && !e.isTrigger && ce.event.simulate("submit", this.parentNode, e, !0)) }, teardown: function() { return !ce.nodeName(this, "form") && void ce.event.remove(this, "._submit") } }), oe.changeBubbles || (ce.event.special.change = { setup: function() { return ke.test(this.nodeName) ? ("checkbox" !== this.type && "radio" !== this.type || (ce.event.add(this, "propertychange._change", (function(e) { "checked" === e.originalEvent.propertyName && (this._just_changed = !0) })), ce.event.add(this, "click._change", (function(e) { this._just_changed && !e.isTrigger && (this._just_changed = !1), ce.event.simulate("change", this, e, !0) }))), !1) : void ce.event.add(this, "beforeactivate._change", (function(e) { var t = e.target;
                    ke.test(t.nodeName) && !ce._data(t, "changeBubbles") && (ce.event.add(t, "change._change", (function(e) {!this.parentNode || e.isSimulated || e.isTrigger || ce.event.simulate("change", this.parentNode, e, !0) })), ce._data(t, "changeBubbles", !0)) })) }, handle: function(e) { var t = e.target; if (this !== t || e.isSimulated || e.isTrigger || "radio" !== t.type && "checkbox" !== t.type) return e.handleObj.handler.apply(this, arguments) }, teardown: function() { return ce.event.remove(this, "._change"), !ke.test(this.nodeName) } }), oe.focusinBubbles || ce.each({ focus: "focusin", blur: "focusout" }, (function(e, t) { var n = function(e) { ce.event.simulate(t, e.target, ce.event.fix(e), !0) };
            ce.event.special[t] = { setup: function() { var i = this.ownerDocument || this,
                        r = ce._data(i, t);
                    r || i.addEventListener(e, n, !0), ce._data(i, t, (r || 0) + 1) }, teardown: function() { var i = this.ownerDocument || this,
                        r = ce._data(i, t) - 1;
                    r ? ce._data(i, t, r) : (i.removeEventListener(e, n, !0), ce._removeData(i, t)) } } })), ce.fn.extend({ on: function(e, t, n, i, r) { var a, o; if ("object" == typeof e) { "string" != typeof t && (n = n || t, t = void 0); for (a in e) this.on(a, t, n, e[a], r); return this } if (null == n && null == i ? (i = t, n = t = void 0) : null == i && ("string" == typeof t ? (i = n, n = void 0) : (i = n, n = t, t = void 0)), i === !1) i = m;
                else if (!i) return this; return 1 === r && (o = i, i = function(e) { return ce().off(e), o.apply(this, arguments) }, i.guid = o.guid || (o.guid = ce.guid++)), this.each((function() { ce.event.add(this, e, i, n, t) })) }, one: function(e, t, n, i) { return this.on(e, t, n, i, 1) }, off: function(e, t, n) { var i, r; if (e && e.preventDefault && e.handleObj) return i = e.handleObj, ce(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this; if ("object" == typeof e) { for (r in e) this.off(r, t, e[r]); return this } return t !== !1 && "function" != typeof t || (n = t, t = void 0), n === !1 && (n = m), this.each((function() { ce.event.remove(this, e, n, t) })) }, trigger: function(e, t) { return this.each((function() { ce.event.trigger(e, t, this) })) }, triggerHandler: function(e, t) { var n = this[0]; if (n) return ce.event.trigger(e, t, n, !0) } });
        var Ge = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
            Be = / jQuery\d+="(?:null|\d+)"/g,
            je = new RegExp("<(?:" + Ge + ")[\\s/>]", "i"),
            He = /^\s+/,
            ze = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
            qe = /<([\w:]+)/,
            Ye = /<tbody/i,
            Ke = /<|&#?\w+;/,
            We = /<(?:script|style|link)/i,
            Xe = /checked\s*(?:[^=]|=\s*.checked.)/i,
            $e = /^$|\/(?:java|ecma)script/i,
            Qe = /^true\/(.*)/,
            Je = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
            Ze = { option: [1, "<select multiple='multiple'>", "</select>"], legend: [1, "<fieldset>", "</fieldset>"], area: [1, "<map>", "</map>"], param: [1, "<object>", "</object>"], thead: [1, "<table>", "</table>"], tr: [2, "<table><tbody>", "</tbody></table>"], col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"], td: [3, "<table><tbody><tr>", "</tr></tbody></table>"], _default: oe.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"] },
            et = E(_e),
            tt = et.appendChild(_e.createElement("div"));
        Ze.optgroup = Ze.option, Ze.tbody = Ze.tfoot = Ze.colgroup = Ze.caption = Ze.thead, Ze.th = Ze.td, ce.extend({ clone: function(e, t, n) { var i, r, a, o, s, c = ce.contains(e.ownerDocument, e); if (oe.html5Clone || ce.isXMLDoc(e) || !je.test("<" + e.nodeName + ">") ? a = e.cloneNode(!0) : (tt.innerHTML = e.outerHTML, tt.removeChild(a = tt.firstChild)), !(oe.noCloneEvent && oe.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || ce.isXMLDoc(e)))
                    for (i = y(a), s = y(e), o = 0; null != (r = s[o]); ++o) i[o] && D(r, i[o]); if (t)
                    if (n)
                        for (s = s || y(e), i = i || y(a), o = 0; null != (r = s[o]); o++) w(r, i[o]);
                    else w(e, a);
                return i = y(a, "script"), i.length > 0 && b(i, !c && y(e, "script")), i = s = r = null, a }, buildFragment: function(e, t, n, i) { for (var r, a, o, s, c, u, l, d = e.length, f = E(t), p = [], h = 0; h < d; h++)
                    if (a = e[h], a || 0 === a)
                        if ("object" === ce.type(a)) ce.merge(p, a.nodeType ? [a] : a);
                        else if (Ke.test(a)) { for (s = s || f.appendChild(t.createElement("div")), c = (qe.exec(a) || ["", ""])[1].toLowerCase(), l = Ze[c] || Ze._default, s.innerHTML = l[1] + a.replace(ze, "<$1></$2>") + l[2], r = l[0]; r--;) s = s.lastChild; if (!oe.leadingWhitespace && He.test(a) && p.push(t.createTextNode(He.exec(a)[0])), !oe.tbody)
                        for (a = "table" !== c || Ye.test(a) ? "<table>" !== l[1] || Ye.test(a) ? 0 : s : s.firstChild, r = a && a.childNodes.length; r--;) ce.nodeName(u = a.childNodes[r], "tbody") && !u.childNodes.length && a.removeChild(u); for (ce.merge(p, s.childNodes), s.textContent = ""; s.firstChild;) s.removeChild(s.firstChild);
                    s = f.lastChild } else p.push(t.createTextNode(a)); for (s && f.removeChild(s), oe.appendChecked || ce.grep(y(p, "input"), I), h = 0; a = p[h++];)
                    if ((!i || ce.inArray(a, i) === -1) && (o = ce.contains(a.ownerDocument, a), s = y(f.appendChild(a), "script"), o && b(s), n))
                        for (r = 0; a = s[r++];) $e.test(a.type || "") && n.push(a);
                return s = null, f }, cleanData: function(e, t) { for (var n, i, r, a, o = 0, s = ce.expando, c = ce.cache, u = oe.deleteExpando, l = ce.event.special; null != (n = e[o]); o++)
                    if ((t || ce.acceptData(n)) && (r = n[s], a = r && c[r])) { if (a.events)
                            for (i in a.events) l[i] ? ce.event.remove(n, i) : ce.removeEvent(n, i, a.handle);
                        c[r] && (delete c[r], u ? delete n[s] : typeof n.removeAttribute !== De ? n.removeAttribute(s) : n[s] = null, J.push(r)) } } }), ce.fn.extend({ text: function(e) { return Le(this, (function(e) { return void 0 === e ? ce.text(this) : this.empty().append((this[0] && this[0].ownerDocument || _e).createTextNode(e)) }), null, e, arguments.length) }, append: function() { return this.domManip(arguments, (function(e) { if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) { var t = T(this, e);
                        t.appendChild(e) } })) }, prepend: function() { return this.domManip(arguments, (function(e) { if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) { var t = T(this, e);
                        t.insertBefore(e, t.firstChild) } })) }, before: function() { return this.domManip(arguments, (function(e) { this.parentNode && this.parentNode.insertBefore(e, this) })) }, after: function() { return this.domManip(arguments, (function(e) { this.parentNode && this.parentNode.insertBefore(e, this.nextSibling) })) }, remove: function(e, t) { for (var n, i = e ? ce.filter(e, this) : this, r = 0; null != (n = i[r]); r++) t || 1 !== n.nodeType || ce.cleanData(y(n)), n.parentNode && (t && ce.contains(n.ownerDocument, n) && b(y(n, "script")), n.parentNode.removeChild(n)); return this }, empty: function() { for (var e, t = 0; null != (e = this[t]); t++) { for (1 === e.nodeType && ce.cleanData(y(e, !1)); e.firstChild;) e.removeChild(e.firstChild);
                    e.options && ce.nodeName(e, "select") && (e.options.length = 0) } return this }, clone: function(e, t) { return e = null != e && e, t = null == t ? e : t, this.map((function() { return ce.clone(this, e, t) })) }, html: function(e) { return Le(this, (function(e) { var t = this[0] || {},
                        n = 0,
                        i = this.length; if (void 0 === e) return 1 === t.nodeType ? t.innerHTML.replace(Be, "") : void 0; if ("string" == typeof e && !We.test(e) && (oe.htmlSerialize || !je.test(e)) && (oe.leadingWhitespace || !He.test(e)) && !Ze[(qe.exec(e) || ["", ""])[1].toLowerCase()]) { e = e.replace(ze, "<$1></$2>"); try { for (; n < i; n++) t = this[n] || {}, 1 === t.nodeType && (ce.cleanData(y(t, !1)), t.innerHTML = e);
                            t = 0 } catch (e) {} }
                    t && this.empty().append(e) }), null, e, arguments.length) }, replaceWith: function() { var e = arguments[0]; return this.domManip(arguments, (function(t) { e = this.parentNode, ce.cleanData(y(this)), e && e.replaceChild(t, this) })), e && (e.length || e.nodeType) ? this : this.remove() }, detach: function(e) { return this.remove(e, !0) }, domManip: function(e, t) { e = ee.apply([], e); var n, i, r, a, o, s, c = 0,
                    u = this.length,
                    l = this,
                    d = u - 1,
                    f = e[0],
                    p = ce.isFunction(f); if (p || u > 1 && "string" == typeof f && !oe.checkClone && Xe.test(f)) return this.each((function(n) { var i = l.eq(n);
                    p && (e[0] = f.call(this, n, i.html())), i.domManip(e, t) })); if (u && (s = ce.buildFragment(e, this[0].ownerDocument, !1, this), n = s.firstChild, 1 === s.childNodes.length && (s = n), n)) { for (a = ce.map(y(s, "script"), S), r = a.length; c < u; c++) i = s, c !== d && (i = ce.clone(i, !0, !0), r && ce.merge(a, y(i, "script"))), t.call(this[c], i, c); if (r)
                        for (o = a[a.length - 1].ownerDocument, ce.map(a, A), c = 0; c < r; c++) i = a[c], $e.test(i.type || "") && !ce._data(i, "globalEval") && ce.contains(o, i) && (i.src ? ce._evalUrl && ce._evalUrl(i.src) : ce.globalEval((i.text || i.textContent || i.innerHTML || "").replace(Je, "")));
                    s = n = null } return this } }), ce.each({ appendTo: "append", prependTo: "prepend", insertBefore: "before", insertAfter: "after", replaceAll: "replaceWith" }, (function(e, t) { ce.fn[e] = function(e) { for (var n, i = 0, r = [], a = ce(e), o = a.length - 1; i <= o; i++) n = i === o ? this : this.clone(!0), ce(a[i])[t](n), te.apply(r, n.get()); return this.pushStack(r) } }));
        var nt, it = {};
        !(function() { var e;
            oe.shrinkWrapBlocks = function() { if (null != e) return e;
                e = !1; var t, n, i; return n = _e.getElementsByTagName("body")[0], n && n.style ? (t = _e.createElement("div"), i = _e.createElement("div"), i.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", n.appendChild(i).appendChild(t), typeof t.style.zoom !== De && (t.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1", t.appendChild(_e.createElement("div")).style.width = "5px", e = 3 !== t.offsetWidth), n.removeChild(i), e) : void 0 } })();
        var rt, at, ot = /^margin/,
            st = new RegExp("^(" + Ce + ")(?!px)[a-z%]+$", "i"),
            ct = /^(top|right|bottom|left)$/;
        n.getComputedStyle ? (rt = function(e) { return e.ownerDocument.defaultView.opener ? e.ownerDocument.defaultView.getComputedStyle(e, null) : n.getComputedStyle(e, null) }, at = function(e, t, n) { var i, r, a, o, s = e.style; return n = n || rt(e), o = n ? n.getPropertyValue(t) || n[t] : void 0, n && ("" !== o || ce.contains(e.ownerDocument, e) || (o = ce.style(e, t)), st.test(o) && ot.test(t) && (i = s.width, r = s.minWidth, a = s.maxWidth, s.minWidth = s.maxWidth = s.width = o, o = n.width, s.width = i, s.minWidth = r, s.maxWidth = a)), void 0 === o ? o : o + "" }) : _e.documentElement.currentStyle && (rt = function(e) { return e.currentStyle }, at = function(e, t, n) { var i, r, a, o, s = e.style; return n = n || rt(e), o = n ? n[t] : void 0, null == o && s && s[t] && (o = s[t]), st.test(o) && !ct.test(t) && (i = s.left, r = e.runtimeStyle, a = r && r.left, a && (r.left = e.currentStyle.left), s.left = "fontSize" === t ? "1em" : o, o = s.pixelLeft + "px", s.left = i, a && (r.left = a)), void 0 === o ? o : o + "" || "auto" }), (function() {
            function e() { var e, t, i, r;
                t = _e.getElementsByTagName("body")[0], t && t.style && (e = _e.createElement("div"), i = _e.createElement("div"), i.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", t.appendChild(i).appendChild(e), e.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", a = o = !1, c = !0, n.getComputedStyle && (a = "1%" !== (n.getComputedStyle(e, null) || {}).top, o = "4px" === (n.getComputedStyle(e, null) || { width: "4px" }).width, r = e.appendChild(_e.createElement("div")), r.style.cssText = e.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", r.style.marginRight = r.style.width = "0", e.style.width = "1px", c = !parseFloat((n.getComputedStyle(r, null) || {}).marginRight), e.removeChild(r)), e.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", r = e.getElementsByTagName("td"), r[0].style.cssText = "margin:0;border:0;padding:0;display:none", s = 0 === r[0].offsetHeight, s && (r[0].style.display = "", r[1].style.display = "none", s = 0 === r[0].offsetHeight), t.removeChild(i)) } var t, i, r, a, o, s, c;
            t = _e.createElement("div"), t.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", r = t.getElementsByTagName("a")[0], i = r && r.style, i && (i.cssText = "float:left;opacity:.5", oe.opacity = "0.5" === i.opacity, oe.cssFloat = !!i.cssFloat, t.style.backgroundClip = "content-box", t.cloneNode(!0).style.backgroundClip = "", oe.clearCloneStyle = "content-box" === t.style.backgroundClip, oe.boxSizing = "" === i.boxSizing || "" === i.MozBoxSizing || "" === i.WebkitBoxSizing, ce.extend(oe, { reliableHiddenOffsets: function() { return null == s && e(), s }, boxSizingReliable: function() { return null == o && e(), o }, pixelPosition: function() { return null == a && e(), a }, reliableMarginRight: function() { return null == c && e(), c } })) })(), ce.swap = function(e, t, n, i) { var r, a, o = {}; for (a in t) o[a] = e.style[a], e.style[a] = t[a];
            r = n.apply(e, i || []); for (a in t) e.style[a] = o[a]; return r };
        var ut = /alpha\([^)]*\)/i,
            lt = /opacity\s*=\s*([^)]*)/,
            dt = /^(none|table(?!-c[ea]).+)/,
            ft = new RegExp("^(" + Ce + ")(.*)$", "i"),
            pt = new RegExp("^([+-])=(" + Ce + ")", "i"),
            ht = { position: "absolute", visibility: "hidden", display: "block" },
            gt = { letterSpacing: "0", fontWeight: "400" },
            vt = ["Webkit", "O", "Moz", "ms"];
        ce.extend({ cssHooks: { opacity: { get: function(e, t) { if (t) { var n = at(e, "opacity"); return "" === n ? "1" : n } } } }, cssNumber: { columnCount: !0, fillOpacity: !0, flexGrow: !0, flexShrink: !0, fontWeight: !0, lineHeight: !0, opacity: !0, order: !0, orphans: !0, widows: !0, zIndex: !0, zoom: !0 }, cssProps: { "float": oe.cssFloat ? "cssFloat" : "styleFloat" }, style: function(e, t, n, i) { if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) { var r, a, o, s = ce.camelCase(t),
                        c = e.style; if (t = ce.cssProps[s] || (ce.cssProps[s] = O(c, s)), o = ce.cssHooks[t] || ce.cssHooks[s], void 0 === n) return o && "get" in o && void 0 !== (r = o.get(e, !1, i)) ? r : c[t]; if (a = typeof n, "string" === a && (r = pt.exec(n)) && (n = (r[1] + 1) * r[2] + parseFloat(ce.css(e, t)), a = "number"), null != n && n === n && ("number" !== a || ce.cssNumber[s] || (n += "px"), oe.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (c[t] = "inherit"), !(o && "set" in o && void 0 === (n = o.set(e, n, i))))) try { c[t] = n } catch (e) {} } }, css: function(e, t, n, i) { var r, a, o, s = ce.camelCase(t); return t = ce.cssProps[s] || (ce.cssProps[s] = O(e.style, s)), o = ce.cssHooks[t] || ce.cssHooks[s], o && "get" in o && (a = o.get(e, !0, n)), void 0 === a && (a = at(e, t, i)), "normal" === a && t in gt && (a = gt[t]), "" === n || n ? (r = parseFloat(a), n === !0 || ce.isNumeric(r) ? r || 0 : a) : a } }), ce.each(["height", "width"], (function(e, t) { ce.cssHooks[t] = { get: function(e, n, i) { if (n) return dt.test(ce.css(e, "display")) && 0 === e.offsetWidth ? ce.swap(e, ht, (function() { return k(e, t, i) })) : k(e, t, i) }, set: function(e, n, i) { var r = i && rt(e); return L(e, n, i ? P(e, t, i, oe.boxSizing && "border-box" === ce.css(e, "boxSizing", !1, r), r) : 0) } } })), oe.opacity || (ce.cssHooks.opacity = { get: function(e, t) { return lt.test((t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : t ? "1" : "" }, set: function(e, t) { var n = e.style,
                    i = e.currentStyle,
                    r = ce.isNumeric(t) ? "alpha(opacity=" + 100 * t + ")" : "",
                    a = i && i.filter || n.filter || "";
                n.zoom = 1, (t >= 1 || "" === t) && "" === ce.trim(a.replace(ut, "")) && n.removeAttribute && (n.removeAttribute("filter"), "" === t || i && !i.filter) || (n.filter = ut.test(a) ? a.replace(ut, r) : a + " " + r) } }), ce.cssHooks.marginRight = C(oe.reliableMarginRight, (function(e, t) { if (t) return ce.swap(e, { display: "inline-block" }, at, [e, "marginRight"]) })), ce.each({ margin: "", padding: "", border: "Width" }, (function(e, t) { ce.cssHooks[e + t] = { expand: function(n) { for (var i = 0, r = {}, a = "string" == typeof n ? n.split(" ") : [n]; i < 4; i++) r[e + Oe[i] + t] = a[i] || a[i - 2] || a[0]; return r } }, ot.test(e) || (ce.cssHooks[e + t].set = L) })), ce.fn.extend({ css: function(e, t) { return Le(this, (function(e, t, n) { var i, r, a = {},
                        o = 0; if (ce.isArray(t)) { for (i = rt(e), r = t.length; o < r; o++) a[t[o]] = ce.css(e, t[o], !1, i); return a } return void 0 !== n ? ce.style(e, t, n) : ce.css(e, t) }), e, t, arguments.length > 1) }, show: function() { return x(this, !0) }, hide: function() { return x(this) }, toggle: function(e) { return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each((function() { xe(this) ? ce(this).show() : ce(this).hide() })) } }), ce.Tween = F, F.prototype = { constructor: F, init: function(e, t, n, i, r, a) { this.elem = e, this.prop = n, this.easing = r || "swing", this.options = t, this.start = this.now = this.cur(), this.end = i, this.unit = a || (ce.cssNumber[n] ? "" : "px") }, cur: function() { var e = F.propHooks[this.prop]; return e && e.get ? e.get(this) : F.propHooks._default.get(this) }, run: function(e) { var t, n = F.propHooks[this.prop]; return this.options.duration ? this.pos = t = ce.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : F.propHooks._default.set(this), this } }, F.prototype.init.prototype = F.prototype, F.propHooks = { _default: { get: function(e) { var t; return null == e.elem[e.prop] || e.elem.style && null != e.elem.style[e.prop] ? (t = ce.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0) : e.elem[e.prop] }, set: function(e) { ce.fx.step[e.prop] ? ce.fx.step[e.prop](e) : e.elem.style && (null != e.elem.style[ce.cssProps[e.prop]] || ce.cssHooks[e.prop]) ? ce.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now } } }, F.propHooks.scrollTop = F.propHooks.scrollLeft = { set: function(e) { e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now) } }, ce.easing = { linear: function(e) { return e }, swing: function(e) { return .5 - Math.cos(e * Math.PI) / 2 } }, ce.fx = F.prototype.init, ce.fx.step = {};
        var mt, _t, Et = /^(?:toggle|show|hide)$/,
            yt = new RegExp("^(?:([+-])=|)(" + Ce + ")([a-z%]*)$", "i"),
            It = /queueHooks$/,
            Tt = [G],
            St = { "*": [function(e, t) { var n = this.createTween(e, t),
                        i = n.cur(),
                        r = yt.exec(t),
                        a = r && r[3] || (ce.cssNumber[e] ? "" : "px"),
                        o = (ce.cssNumber[e] || "px" !== a && +i) && yt.exec(ce.css(n.elem, e)),
                        s = 1,
                        c = 20; if (o && o[3] !== a) { a = a || o[3], r = r || [], o = +i || 1;
                        do s = s || ".5", o /= s, ce.style(n.elem, e, o + a); while (s !== (s = n.cur() / i) && 1 !== s && --c) } return r && (o = n.start = +o || +i || 0, n.unit = a, n.end = r[1] ? o + (r[1] + 1) * r[2] : +r[2]), n }] };
        ce.Animation = ce.extend(j, { tweener: function(e, t) { ce.isFunction(e) ? (t = e, e = ["*"]) : e = e.split(" "); for (var n, i = 0, r = e.length; i < r; i++) n = e[i], St[n] = St[n] || [], St[n].unshift(t) }, prefilter: function(e, t) { t ? Tt.unshift(e) : Tt.push(e) } }), ce.speed = function(e, t, n) { var i = e && "object" == typeof e ? ce.extend({}, e) : { complete: n || !n && t || ce.isFunction(e) && e, duration: e, easing: n && t || t && !ce.isFunction(t) && t }; return i.duration = ce.fx.off ? 0 : "number" == typeof i.duration ? i.duration : i.duration in ce.fx.speeds ? ce.fx.speeds[i.duration] : ce.fx.speeds._default, null != i.queue && i.queue !== !0 || (i.queue = "fx"), i.old = i.complete, i.complete = function() { ce.isFunction(i.old) && i.old.call(this), i.queue && ce.dequeue(this, i.queue) }, i }, ce.fn.extend({ fadeTo: function(e, t, n, i) { return this.filter(xe).css("opacity", 0).show().end().animate({ opacity: t }, e, n, i) }, animate: function(e, t, n, i) { var r = ce.isEmptyObject(e),
                    a = ce.speed(t, n, i),
                    o = function() { var t = j(this, ce.extend({}, e), a);
                        (r || ce._data(this, "finish")) && t.stop(!0) }; return o.finish = o, r || a.queue === !1 ? this.each(o) : this.queue(a.queue, o) }, stop: function(e, t, n) { var i = function(e) { var t = e.stop;
                    delete e.stop, t(n) }; return "string" != typeof e && (n = t, t = e, e = void 0), t && e !== !1 && this.queue(e || "fx", []), this.each((function() { var t = !0,
                        r = null != e && e + "queueHooks",
                        a = ce.timers,
                        o = ce._data(this); if (r) o[r] && o[r].stop && i(o[r]);
                    else
                        for (r in o) o[r] && o[r].stop && It.test(r) && i(o[r]); for (r = a.length; r--;) a[r].elem !== this || null != e && a[r].queue !== e || (a[r].anim.stop(n), t = !1, a.splice(r, 1));!t && n || ce.dequeue(this, e) })) }, finish: function(e) { return e !== !1 && (e = e || "fx"), this.each((function() { var t, n = ce._data(this),
                        i = n[e + "queue"],
                        r = n[e + "queueHooks"],
                        a = ce.timers,
                        o = i ? i.length : 0; for (n.finish = !0, ce.queue(this, e, []), r && r.stop && r.stop.call(this, !0), t = a.length; t--;) a[t].elem === this && a[t].queue === e && (a[t].anim.stop(!0), a.splice(t, 1)); for (t = 0; t < o; t++) i[t] && i[t].finish && i[t].finish.call(this);
                    delete n.finish })) } }), ce.each(["toggle", "show", "hide"], (function(e, t) { var n = ce.fn[t];
            ce.fn[t] = function(e, i, r) { return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(V(t, !0), e, i, r) } })), ce.each({ slideDown: V("show"), slideUp: V("hide"), slideToggle: V("toggle"), fadeIn: { opacity: "show" }, fadeOut: { opacity: "hide" }, fadeToggle: { opacity: "toggle" } }, (function(e, t) { ce.fn[e] = function(e, n, i) { return this.animate(t, e, n, i) } })), ce.timers = [], ce.fx.tick = function() { var e, t = ce.timers,
                n = 0; for (mt = ce.now(); n < t.length; n++) e = t[n], e() || t[n] !== e || t.splice(n--, 1);
            t.length || ce.fx.stop(), mt = void 0 }, ce.fx.timer = function(e) { ce.timers.push(e), e() ? ce.fx.start() : ce.timers.pop() }, ce.fx.interval = 13, ce.fx.start = function() { _t || (_t = setInterval(ce.fx.tick, ce.fx.interval)) }, ce.fx.stop = function() { clearInterval(_t), _t = null }, ce.fx.speeds = { slow: 600, fast: 200, _default: 400 }, ce.fn.delay = function(e, t) { return e = ce.fx ? ce.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, (function(t, n) { var i = setTimeout(t, e);
                n.stop = function() { clearTimeout(i) } })) }, (function() { var e, t, n, i, r;
            t = _e.createElement("div"), t.setAttribute("className", "t"), t.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", i = t.getElementsByTagName("a")[0], n = _e.createElement("select"), r = n.appendChild(_e.createElement("option")), e = t.getElementsByTagName("input")[0], i.style.cssText = "top:1px", oe.getSetAttribute = "t" !== t.className, oe.style = /top/.test(i.getAttribute("style")), oe.hrefNormalized = "/a" === i.getAttribute("href"), oe.checkOn = !!e.value, oe.optSelected = r.selected, oe.enctype = !!_e.createElement("form").enctype, n.disabled = !0, oe.optDisabled = !r.disabled, e = _e.createElement("input"), e.setAttribute("value", ""), oe.input = "" === e.getAttribute("value"), e.value = "t", e.setAttribute("type", "radio"), oe.radioValue = "t" === e.value })();
        var At = /\r/g;
        ce.fn.extend({ val: function(e) { var t, n, i, r = this[0]; { if (arguments.length) return i = ce.isFunction(e), this.each((function(n) { var r;
                        1 === this.nodeType && (r = i ? e.call(this, n, ce(this).val()) : e, null == r ? r = "" : "number" == typeof r ? r += "" : ce.isArray(r) && (r = ce.map(r, (function(e) { return null == e ? "" : e + "" }))), t = ce.valHooks[this.type] || ce.valHooks[this.nodeName.toLowerCase()], t && "set" in t && void 0 !== t.set(this, r, "value") || (this.value = r)) })); if (r) return t = ce.valHooks[r.type] || ce.valHooks[r.nodeName.toLowerCase()], t && "get" in t && void 0 !== (n = t.get(r, "value")) ? n : (n = r.value, "string" == typeof n ? n.replace(At, "") : null == n ? "" : n) } } }), ce.extend({ valHooks: { option: { get: function(e) { var t = ce.find.attr(e, "value"); return null != t ? t : ce.trim(ce.text(e)) } }, select: { get: function(e) { for (var t, n, i = e.options, r = e.selectedIndex, a = "select-one" === e.type || r < 0, o = a ? null : [], s = a ? r + 1 : i.length, c = r < 0 ? s : a ? r : 0; c < s; c++)
                            if (n = i[c], (n.selected || c === r) && (oe.optDisabled ? !n.disabled : null === n.getAttribute("disabled")) && (!n.parentNode.disabled || !ce.nodeName(n.parentNode, "optgroup"))) { if (t = ce(n).val(), a) return t;
                                o.push(t) }
                        return o }, set: function(e, t) { for (var n, i, r = e.options, a = ce.makeArray(t), o = r.length; o--;)
                            if (i = r[o], ce.inArray(ce.valHooks.option.get(i), a) >= 0) try { i.selected = n = !0 } catch (e) { i.scrollHeight } else i.selected = !1;
                        return n || (e.selectedIndex = -1), r } } } }), ce.each(["radio", "checkbox"], (function() { ce.valHooks[this] = { set: function(e, t) { if (ce.isArray(t)) return e.checked = ce.inArray(ce(e).val(), t) >= 0 } }, oe.checkOn || (ce.valHooks[this].get = function(e) { return null === e.getAttribute("value") ? "on" : e.value }) }));
        var bt, wt, Dt = ce.expr.attrHandle,
            Rt = /^(?:checked|selected)$/i,
            Nt = oe.getSetAttribute,
            Ct = oe.input;
        ce.fn.extend({ attr: function(e, t) { return Le(this, ce.attr, e, t, arguments.length > 1) }, removeAttr: function(e) { return this.each((function() { ce.removeAttr(this, e) })) } }), ce.extend({ attr: function(e, t, n) { var i, r, a = e.nodeType; if (e && 3 !== a && 8 !== a && 2 !== a) return typeof e.getAttribute === De ? ce.prop(e, t, n) : (1 === a && ce.isXMLDoc(e) || (t = t.toLowerCase(), i = ce.attrHooks[t] || (ce.expr.match.bool.test(t) ? wt : bt)), void 0 === n ? i && "get" in i && null !== (r = i.get(e, t)) ? r : (r = ce.find.attr(e, t), null == r ? void 0 : r) : null !== n ? i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : (e.setAttribute(t, n + ""), n) : void ce.removeAttr(e, t)) }, removeAttr: function(e, t) { var n, i, r = 0,
                    a = t && t.match(Se); if (a && 1 === e.nodeType)
                    for (; n = a[r++];) i = ce.propFix[n] || n, ce.expr.match.bool.test(n) ? Ct && Nt || !Rt.test(n) ? e[i] = !1 : e[ce.camelCase("default-" + n)] = e[i] = !1 : ce.attr(e, n, ""), e.removeAttribute(Nt ? n : i) }, attrHooks: { type: { set: function(e, t) { if (!oe.radioValue && "radio" === t && ce.nodeName(e, "input")) { var n = e.value; return e.setAttribute("type", t), n && (e.value = n), t } } } } }), wt = { set: function(e, t, n) { return t === !1 ? ce.removeAttr(e, n) : Ct && Nt || !Rt.test(n) ? e.setAttribute(!Nt && ce.propFix[n] || n, n) : e[ce.camelCase("default-" + n)] = e[n] = !0, n } }, ce.each(ce.expr.match.bool.source.match(/\w+/g), (function(e, t) { var n = Dt[t] || ce.find.attr;
            Dt[t] = Ct && Nt || !Rt.test(t) ? function(e, t, i) { var r, a; return i || (a = Dt[t], Dt[t] = r, r = null != n(e, t, i) ? t.toLowerCase() : null, Dt[t] = a), r } : function(e, t, n) { if (!n) return e[ce.camelCase("default-" + t)] ? t.toLowerCase() : null } })), Ct && Nt || (ce.attrHooks.value = { set: function(e, t, n) { return ce.nodeName(e, "input") ? void(e.defaultValue = t) : bt && bt.set(e, t, n) } }), Nt || (bt = { set: function(e, t, n) { var i = e.getAttributeNode(n); if (i || e.setAttributeNode(i = e.ownerDocument.createAttribute(n)), i.value = t += "", "value" === n || t === e.getAttribute(n)) return t } }, Dt.id = Dt.name = Dt.coords = function(e, t, n) { var i; if (!n) return (i = e.getAttributeNode(t)) && "" !== i.value ? i.value : null }, ce.valHooks.button = { get: function(e, t) { var n = e.getAttributeNode(t); if (n && n.specified) return n.value }, set: bt.set }, ce.attrHooks.contenteditable = { set: function(e, t, n) { bt.set(e, "" !== t && t, n) } }, ce.each(["width", "height"], (function(e, t) { ce.attrHooks[t] = { set: function(e, n) { if ("" === n) return e.setAttribute(t, "auto"), n } } }))), oe.style || (ce.attrHooks.style = { get: function(e) { return e.style.cssText || void 0 }, set: function(e, t) { return e.style.cssText = t + "" } });
        var Ot = /^(?:input|select|textarea|button|object)$/i,
            xt = /^(?:a|area)$/i;
        ce.fn.extend({ prop: function(e, t) { return Le(this, ce.prop, e, t, arguments.length > 1) }, removeProp: function(e) { return e = ce.propFix[e] || e, this.each((function() { try { this[e] = void 0, delete this[e] } catch (e) {} })) } }), ce.extend({
            propFix: { "for": "htmlFor", "class": "className" },
            prop: function(e, t, n) {
                var i, r, a, o = e.nodeType;
                if (e && 3 !== o && 8 !== o && 2 !== o) return a = 1 !== o || !ce.isXMLDoc(e), a && (t = ce.propFix[t] || t, r = ce.propHooks[t]), void 0 !== n ? r && "set" in r && void 0 !== (i = r.set(e, n, t)) ? i : e[t] = n : r && "get" in r && null !== (i = r.get(e, t)) ? i : e[t];
            },
            propHooks: { tabIndex: { get: function(e) { var t = ce.find.attr(e, "tabindex"); return t ? parseInt(t, 10) : Ot.test(e.nodeName) || xt.test(e.nodeName) && e.href ? 0 : -1 } } }
        }), oe.hrefNormalized || ce.each(["href", "src"], (function(e, t) { ce.propHooks[t] = { get: function(e) { return e.getAttribute(t, 4) } } })), oe.optSelected || (ce.propHooks.selected = { get: function(e) { var t = e.parentNode; return t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex), null } }), ce.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], (function() { ce.propFix[this.toLowerCase()] = this })), oe.enctype || (ce.propFix.enctype = "encoding");
        var Lt = /[\t\r\n\f]/g;
        ce.fn.extend({ addClass: function(e) { var t, n, i, r, a, o, s = 0,
                    c = this.length,
                    u = "string" == typeof e && e; if (ce.isFunction(e)) return this.each((function(t) { ce(this).addClass(e.call(this, t, this.className)) })); if (u)
                    for (t = (e || "").match(Se) || []; s < c; s++)
                        if (n = this[s], i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(Lt, " ") : " ")) { for (a = 0; r = t[a++];) i.indexOf(" " + r + " ") < 0 && (i += r + " ");
                            o = ce.trim(i), n.className !== o && (n.className = o) }
                return this }, removeClass: function(e) { var t, n, i, r, a, o, s = 0,
                    c = this.length,
                    u = 0 === arguments.length || "string" == typeof e && e; if (ce.isFunction(e)) return this.each((function(t) { ce(this).removeClass(e.call(this, t, this.className)) })); if (u)
                    for (t = (e || "").match(Se) || []; s < c; s++)
                        if (n = this[s], i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(Lt, " ") : "")) { for (a = 0; r = t[a++];)
                                for (; i.indexOf(" " + r + " ") >= 0;) i = i.replace(" " + r + " ", " ");
                            o = e ? ce.trim(i) : "", n.className !== o && (n.className = o) }
                return this }, toggleClass: function(e, t) { var n = typeof e; return "boolean" == typeof t && "string" === n ? t ? this.addClass(e) : this.removeClass(e) : ce.isFunction(e) ? this.each((function(n) { ce(this).toggleClass(e.call(this, n, this.className, t), t) })) : this.each((function() { if ("string" === n)
                        for (var t, i = 0, r = ce(this), a = e.match(Se) || []; t = a[i++];) r.hasClass(t) ? r.removeClass(t) : r.addClass(t);
                    else n !== De && "boolean" !== n || (this.className && ce._data(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : ce._data(this, "__className__") || "") })) }, hasClass: function(e) { for (var t = " " + e + " ", n = 0, i = this.length; n < i; n++)
                    if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(Lt, " ").indexOf(t) >= 0) return !0;
                return !1 } }), ce.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), (function(e, t) { ce.fn[t] = function(e, n) { return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t) } })), ce.fn.extend({ hover: function(e, t) { return this.mouseenter(e).mouseleave(t || e) }, bind: function(e, t, n) { return this.on(e, null, t, n) }, unbind: function(e, t) { return this.off(e, null, t) }, delegate: function(e, t, n, i) { return this.on(t, e, n, i) }, undelegate: function(e, t, n) { return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n) } });
        var Pt = ce.now(),
            kt = /\?/,
            Ft = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
        ce.parseJSON = function(e) { if (n.JSON && n.JSON.parse) return n.JSON.parse(e + ""); var t, i = null,
                r = ce.trim(e + ""); return r && !ce.trim(r.replace(Ft, (function(e, n, r, a) { return t && n && (i = 0), 0 === i ? e : (t = r || n, i += !a - !r, "") }))) ? Function("return " + r)() : ce.error("Invalid JSON: " + e) }, ce.parseXML = function(e) { var t, i; if (!e || "string" != typeof e) return null; try { n.DOMParser ? (i = new DOMParser, t = i.parseFromString(e, "text/xml")) : (t = new ActiveXObject("Microsoft.XMLDOM"), t.async = "false", t.loadXML(e)) } catch (e) { t = void 0 } return t && t.documentElement && !t.getElementsByTagName("parsererror").length || ce.error("Invalid XML: " + e), t };
        var Mt, Vt, Ut = /#.*$/,
            Gt = /([?&])_=[^&]*/,
            Bt = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
            jt = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
            Ht = /^(?:GET|HEAD)$/,
            zt = /^\/\//,
            qt = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
            Yt = {},
            Kt = {},
            Wt = "*/".concat("*");
        try { Vt = location.href } catch (e) { Vt = _e.createElement("a"), Vt.href = "", Vt = Vt.href }
        Mt = qt.exec(Vt.toLowerCase()) || [], ce.extend({ active: 0, lastModified: {}, etag: {}, ajaxSettings: { url: Vt, type: "GET", isLocal: jt.test(Mt[1]), global: !0, processData: !0, async: !0, contentType: "application/x-www-form-urlencoded; charset=UTF-8", accepts: { "*": Wt, text: "text/plain", html: "text/html", xml: "application/xml, text/xml", json: "application/json, text/javascript" }, contents: { xml: /xml/, html: /html/, json: /json/ }, responseFields: { xml: "responseXML", text: "responseText", json: "responseJSON" }, converters: { "* text": String, "text html": !0, "text json": ce.parseJSON, "text xml": ce.parseXML }, flatOptions: { url: !0, context: !0 } }, ajaxSetup: function(e, t) { return t ? q(q(e, ce.ajaxSettings), t) : q(ce.ajaxSettings, e) }, ajaxPrefilter: H(Yt), ajaxTransport: H(Kt), ajax: function(e, t) {
                function n(e, t, n, i) { var r, l, m, _, y, T = t;
                    2 !== E && (E = 2, s && clearTimeout(s), u = void 0, o = i || "", I.readyState = e > 0 ? 4 : 0, r = e >= 200 && e < 300 || 304 === e, n && (_ = Y(d, I, n)), _ = K(d, _, I, r), r ? (d.ifModified && (y = I.getResponseHeader("Last-Modified"), y && (ce.lastModified[a] = y), y = I.getResponseHeader("etag"), y && (ce.etag[a] = y)), 204 === e || "HEAD" === d.type ? T = "nocontent" : 304 === e ? T = "notmodified" : (T = _.state, l = _.data, m = _.error, r = !m)) : (m = T, !e && T || (T = "error", e < 0 && (e = 0))), I.status = e, I.statusText = (t || T) + "", r ? h.resolveWith(f, [l, T, I]) : h.rejectWith(f, [I, T, m]), I.statusCode(v), v = void 0, c && p.trigger(r ? "ajaxSuccess" : "ajaxError", [I, d, r ? l : m]), g.fireWith(f, [I, T]), c && (p.trigger("ajaxComplete", [I, d]), --ce.active || ce.event.trigger("ajaxStop"))) } "object" == typeof e && (t = e, e = void 0), t = t || {}; var i, r, a, o, s, c, u, l, d = ce.ajaxSetup({}, t),
                    f = d.context || d,
                    p = d.context && (f.nodeType || f.jquery) ? ce(f) : ce.event,
                    h = ce.Deferred(),
                    g = ce.Callbacks("once memory"),
                    v = d.statusCode || {},
                    m = {},
                    _ = {},
                    E = 0,
                    y = "canceled",
                    I = { readyState: 0, getResponseHeader: function(e) { var t; if (2 === E) { if (!l)
                                    for (l = {}; t = Bt.exec(o);) l[t[1].toLowerCase()] = t[2];
                                t = l[e.toLowerCase()] } return null == t ? null : t }, getAllResponseHeaders: function() { return 2 === E ? o : null }, setRequestHeader: function(e, t) { var n = e.toLowerCase(); return E || (e = _[n] = _[n] || e, m[e] = t), this }, overrideMimeType: function(e) { return E || (d.mimeType = e), this }, statusCode: function(e) { var t; if (e)
                                if (E < 2)
                                    for (t in e) v[t] = [v[t], e[t]];
                                else I.always(e[I.status]);
                            return this }, abort: function(e) { var t = e || y; return u && u.abort(t), n(0, t), this } }; if (h.promise(I).complete = g.add, I.success = I.done, I.error = I.fail, d.url = ((e || d.url || Vt) + "").replace(Ut, "").replace(zt, Mt[1] + "//"), d.type = t.method || t.type || d.method || d.type, d.dataTypes = ce.trim(d.dataType || "*").toLowerCase().match(Se) || [""], null == d.crossDomain && (i = qt.exec(d.url.toLowerCase()), d.crossDomain = !(!i || i[1] === Mt[1] && i[2] === Mt[2] && (i[3] || ("http:" === i[1] ? "80" : "443")) === (Mt[3] || ("http:" === Mt[1] ? "80" : "443")))), d.data && d.processData && "string" != typeof d.data && (d.data = ce.param(d.data, d.traditional)), z(Yt, d, t, I), 2 === E) return I;
                c = ce.event && d.global, c && 0 === ce.active++ && ce.event.trigger("ajaxStart"), d.type = d.type.toUpperCase(), d.hasContent = !Ht.test(d.type), a = d.url, d.hasContent || (d.data && (a = d.url += (kt.test(a) ? "&" : "?") + d.data, delete d.data), d.cache === !1 && (d.url = Gt.test(a) ? a.replace(Gt, "$1_=" + Pt++) : a + (kt.test(a) ? "&" : "?") + "_=" + Pt++)), d.ifModified && (ce.lastModified[a] && I.setRequestHeader("If-Modified-Since", ce.lastModified[a]), ce.etag[a] && I.setRequestHeader("If-None-Match", ce.etag[a])), (d.data && d.hasContent && d.contentType !== !1 || t.contentType) && I.setRequestHeader("Content-Type", d.contentType), I.setRequestHeader("Accept", d.dataTypes[0] && d.accepts[d.dataTypes[0]] ? d.accepts[d.dataTypes[0]] + ("*" !== d.dataTypes[0] ? ", " + Wt + "; q=0.01" : "") : d.accepts["*"]); for (r in d.headers) I.setRequestHeader(r, d.headers[r]); if (d.beforeSend && (d.beforeSend.call(f, I, d) === !1 || 2 === E)) return I.abort();
                y = "abort"; for (r in { success: 1, error: 1, complete: 1 }) I[r](d[r]); if (u = z(Kt, d, t, I)) { I.readyState = 1, c && p.trigger("ajaxSend", [I, d]), d.async && d.timeout > 0 && (s = setTimeout((function() { I.abort("timeout") }), d.timeout)); try { E = 1, u.send(m, n) } catch (e) { if (!(E < 2)) throw e;
                        n(-1, e) } } else n(-1, "No Transport"); return I }, getJSON: function(e, t, n) { return ce.get(e, t, n, "json") }, getScript: function(e, t) { return ce.get(e, void 0, t, "script") } }), ce.each(["get", "post"], (function(e, t) { ce[t] = function(e, n, i, r) { return ce.isFunction(n) && (r = r || i, i = n, n = void 0), ce.ajax({ url: e, type: t, dataType: r, data: n, success: i }) } })), ce._evalUrl = function(e) { return ce.ajax({ url: e, type: "GET", dataType: "script", async: !1, global: !1, "throws": !0 }) }, ce.fn.extend({ wrapAll: function(e) { if (ce.isFunction(e)) return this.each((function(t) { ce(this).wrapAll(e.call(this, t)) })); if (this[0]) { var t = ce(e, this[0].ownerDocument).eq(0).clone(!0);
                    this[0].parentNode && t.insertBefore(this[0]), t.map((function() { for (var e = this; e.firstChild && 1 === e.firstChild.nodeType;) e = e.firstChild; return e })).append(this) } return this }, wrapInner: function(e) { return ce.isFunction(e) ? this.each((function(t) { ce(this).wrapInner(e.call(this, t)) })) : this.each((function() { var t = ce(this),
                        n = t.contents();
                    n.length ? n.wrapAll(e) : t.append(e) })) }, wrap: function(e) { var t = ce.isFunction(e); return this.each((function(n) { ce(this).wrapAll(t ? e.call(this, n) : e) })) }, unwrap: function() { return this.parent().each((function() { ce.nodeName(this, "body") || ce(this).replaceWith(this.childNodes) })).end() } }), ce.expr.filters.hidden = function(e) { return e.offsetWidth <= 0 && e.offsetHeight <= 0 || !oe.reliableHiddenOffsets() && "none" === (e.style && e.style.display || ce.css(e, "display")) }, ce.expr.filters.visible = function(e) { return !ce.expr.filters.hidden(e) };
        var Xt = /%20/g,
            $t = /\[\]$/,
            Qt = /\r?\n/g,
            Jt = /^(?:submit|button|image|reset|file)$/i,
            Zt = /^(?:input|select|textarea|keygen)/i;
        ce.param = function(e, t) { var n, i = [],
                r = function(e, t) { t = ce.isFunction(t) ? t() : null == t ? "" : t, i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t) }; if (void 0 === t && (t = ce.ajaxSettings && ce.ajaxSettings.traditional), ce.isArray(e) || e.jquery && !ce.isPlainObject(e)) ce.each(e, (function() { r(this.name, this.value) }));
            else
                for (n in e) W(n, e[n], t, r); return i.join("&").replace(Xt, "+") }, ce.fn.extend({ serialize: function() { return ce.param(this.serializeArray()) }, serializeArray: function() { return this.map((function() { var e = ce.prop(this, "elements"); return e ? ce.makeArray(e) : this })).filter((function() { var e = this.type; return this.name && !ce(this).is(":disabled") && Zt.test(this.nodeName) && !Jt.test(e) && (this.checked || !Pe.test(e)) })).map((function(e, t) { var n = ce(this).val(); return null == n ? null : ce.isArray(n) ? ce.map(n, (function(e) { return { name: t.name, value: e.replace(Qt, "\r\n") } })) : { name: t.name, value: n.replace(Qt, "\r\n") } })).get() } }), ce.ajaxSettings.xhr = void 0 !== n.ActiveXObject ? function() { return !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && X() || $() } : X;
        var en = 0,
            tn = {},
            nn = ce.ajaxSettings.xhr();
        n.attachEvent && n.attachEvent("onunload", (function() { for (var e in tn) tn[e](void 0, !0) })), oe.cors = !!nn && "withCredentials" in nn, nn = oe.ajax = !!nn, nn && ce.ajaxTransport((function(e) { if (!e.crossDomain || oe.cors) { var t; return { send: function(n, i) { var r, a = e.xhr(),
                            o = ++en; if (a.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields)
                            for (r in e.xhrFields) a[r] = e.xhrFields[r];
                        e.mimeType && a.overrideMimeType && a.overrideMimeType(e.mimeType), e.crossDomain || n["X-Requested-With"] || (n["X-Requested-With"] = "XMLHttpRequest"); for (r in n) void 0 !== n[r] && a.setRequestHeader(r, n[r] + "");
                        a.send(e.hasContent && e.data || null), t = function(n, r) { var s, c, u; if (t && (r || 4 === a.readyState))
                                if (delete tn[o], t = void 0, a.onreadystatechange = ce.noop, r) 4 !== a.readyState && a.abort();
                                else { u = {}, s = a.status, "string" == typeof a.responseText && (u.text = a.responseText); try { c = a.statusText } catch (e) { c = "" }
                                    s || !e.isLocal || e.crossDomain ? 1223 === s && (s = 204) : s = u.text ? 200 : 404 }
                            u && i(s, c, u, a.getAllResponseHeaders()) }, e.async ? 4 === a.readyState ? setTimeout(t) : a.onreadystatechange = tn[o] = t : t() }, abort: function() { t && t(void 0, !0) } } } })), ce.ajaxSetup({ accepts: { script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript" }, contents: { script: /(?:java|ecma)script/ }, converters: { "text script": function(e) { return ce.globalEval(e), e } } }), ce.ajaxPrefilter("script", (function(e) { void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET", e.global = !1) })), ce.ajaxTransport("script", (function(e) { if (e.crossDomain) { var t, n = _e.head || ce("head")[0] || _e.documentElement; return { send: function(i, r) { t = _e.createElement("script"), t.async = !0, e.scriptCharset && (t.charset = e.scriptCharset), t.src = e.url, t.onload = t.onreadystatechange = function(e, n) {
                            (n || !t.readyState || /loaded|complete/.test(t.readyState)) && (t.onload = t.onreadystatechange = null, t.parentNode && t.parentNode.removeChild(t), t = null, n || r(200, "success")) }, n.insertBefore(t, n.firstChild) }, abort: function() { t && t.onload(void 0, !0) } } } }));
        var rn = [],
            an = /(=)\?(?=&|$)|\?\?/;
        ce.ajaxSetup({ jsonp: "callback", jsonpCallback: function() { var e = rn.pop() || ce.expando + "_" + Pt++; return this[e] = !0, e } }), ce.ajaxPrefilter("json jsonp", (function(e, t, i) { var r, a, o, s = e.jsonp !== !1 && (an.test(e.url) ? "url" : "string" == typeof e.data && !(e.contentType || "").indexOf("application/x-www-form-urlencoded") && an.test(e.data) && "data"); if (s || "jsonp" === e.dataTypes[0]) return r = e.jsonpCallback = ce.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, s ? e[s] = e[s].replace(an, "$1" + r) : e.jsonp !== !1 && (e.url += (kt.test(e.url) ? "&" : "?") + e.jsonp + "=" + r), e.converters["script json"] = function() { return o || ce.error(r + " was not called"), o[0] }, e.dataTypes[0] = "json", a = n[r], n[r] = function() { o = arguments }, i.always((function() { n[r] = a, e[r] && (e.jsonpCallback = t.jsonpCallback, rn.push(r)), o && ce.isFunction(a) && a(o[0]), o = a = void 0 })), "script" })), ce.parseHTML = function(e, t, n) { if (!e || "string" != typeof e) return null; "boolean" == typeof t && (n = t, t = !1), t = t || _e; var i = ge.exec(e),
                r = !n && []; return i ? [t.createElement(i[1])] : (i = ce.buildFragment([e], t, r), r && r.length && ce(r).remove(), ce.merge([], i.childNodes)) };
        var on = ce.fn.load;
        ce.fn.load = function(e, t, n) { if ("string" != typeof e && on) return on.apply(this, arguments); var i, r, a, o = this,
                s = e.indexOf(" "); return s >= 0 && (i = ce.trim(e.slice(s, e.length)), e = e.slice(0, s)), ce.isFunction(t) ? (n = t, t = void 0) : t && "object" == typeof t && (a = "POST"), o.length > 0 && ce.ajax({ url: e, type: a, dataType: "html", data: t }).done((function(e) { r = arguments, o.html(i ? ce("<div>").append(ce.parseHTML(e)).find(i) : e) })).complete(n && function(e, t) { o.each(n, r || [e.responseText, t, e]) }), this }, ce.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], (function(e, t) { ce.fn[t] = function(e) { return this.on(t, e) } })), ce.expr.filters.animated = function(e) { return ce.grep(ce.timers, (function(t) { return e === t.elem })).length };
        var sn = n.document.documentElement;
        ce.offset = { setOffset: function(e, t, n) { var i, r, a, o, s, c, u, l = ce.css(e, "position"),
                    d = ce(e),
                    f = {}; "static" === l && (e.style.position = "relative"), s = d.offset(), a = ce.css(e, "top"), c = ce.css(e, "left"), u = ("absolute" === l || "fixed" === l) && ce.inArray("auto", [a, c]) > -1, u ? (i = d.position(), o = i.top, r = i.left) : (o = parseFloat(a) || 0, r = parseFloat(c) || 0), ce.isFunction(t) && (t = t.call(e, n, s)), null != t.top && (f.top = t.top - s.top + o), null != t.left && (f.left = t.left - s.left + r), "using" in t ? t.using.call(e, f) : d.css(f) } }, ce.fn.extend({ offset: function(e) { if (arguments.length) return void 0 === e ? this : this.each((function(t) { ce.offset.setOffset(this, e, t) })); var t, n, i = { top: 0, left: 0 },
                    r = this[0],
                    a = r && r.ownerDocument; if (a) return t = a.documentElement, ce.contains(t, r) ? (typeof r.getBoundingClientRect !== De && (i = r.getBoundingClientRect()), n = Q(a), { top: i.top + (n.pageYOffset || t.scrollTop) - (t.clientTop || 0), left: i.left + (n.pageXOffset || t.scrollLeft) - (t.clientLeft || 0) }) : i }, position: function() { if (this[0]) { var e, t, n = { top: 0, left: 0 },
                        i = this[0]; return "fixed" === ce.css(i, "position") ? t = i.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), ce.nodeName(e[0], "html") || (n = e.offset()), n.top += ce.css(e[0], "borderTopWidth", !0), n.left += ce.css(e[0], "borderLeftWidth", !0)), { top: t.top - n.top - ce.css(i, "marginTop", !0), left: t.left - n.left - ce.css(i, "marginLeft", !0) } } }, offsetParent: function() { return this.map((function() { for (var e = this.offsetParent || sn; e && !ce.nodeName(e, "html") && "static" === ce.css(e, "position");) e = e.offsetParent; return e || sn })) } }), ce.each({ scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, (function(e, t) { var n = /Y/.test(t);
            ce.fn[e] = function(i) { return Le(this, (function(e, i, r) { var a = Q(e); return void 0 === r ? a ? t in a ? a[t] : a.document.documentElement[i] : e[i] : void(a ? a.scrollTo(n ? ce(a).scrollLeft() : r, n ? r : ce(a).scrollTop()) : e[i] = r) }), e, i, arguments.length, null) } })), ce.each(["top", "left"], (function(e, t) { ce.cssHooks[t] = C(oe.pixelPosition, (function(e, n) { if (n) return n = at(e, t), st.test(n) ? ce(e).position()[t] + "px" : n })) })), ce.each({ Height: "height", Width: "width" }, (function(e, t) { ce.each({ padding: "inner" + e, content: t, "": "outer" + e }, (function(n, i) { ce.fn[i] = function(i, r) { var a = arguments.length && (n || "boolean" != typeof i),
                        o = n || (i === !0 || r === !0 ? "margin" : "border"); return Le(this, (function(t, n, i) { var r; return ce.isWindow(t) ? t.document.documentElement["client" + e] : 9 === t.nodeType ? (r = t.documentElement, Math.max(t.body["scroll" + e], r["scroll" + e], t.body["offset" + e], r["offset" + e], r["client" + e])) : void 0 === i ? ce.css(t, n, o) : ce.style(t, n, i, o) }), t, a ? i : void 0, a, null) } })) })), ce.fn.size = function() { return this.length }, ce.fn.andSelf = ce.fn.addBack, i = [], r = function() { return ce }.apply(t, i), !(void 0 !== r && (e.exports = r));
        var cn = n.jQuery,
            un = n.$;
        return ce.noConflict = function(e) { return n.$ === ce && (n.$ = un), e && n.jQuery === ce && (n.jQuery = cn), ce }, typeof a === De && (n.jQuery = n.$ = ce), ce
    }))
}), (function(e, t, n) { var i = n(23),
        r = n(41),
        a = "optimizelyDataApi";
    t.registerFunction = function(e, t) { var n = r.getGlobal(a);
        n || (n = {}, r.setGlobal(a, n)), n[e] || (n[e] = t) }, t.unregisterFunction = function(e) { var t = r.getGlobal(a);
        t && t[e] && (t[e] = function() { i.log('Ignoring attempt to call "' + a + "." + e + '" which has been unregistered.') }) }, t.getFunction = function(e) { return r.getGlobal(a)[e] } }), (function(e, t, n) { var i = n(81),
        r = n(23),
        a = n(91);
    t.addScriptAsync = function(e, t) { var n = i.querySelector("head"),
            a = i.createElement("script");
        a.type = "text/javascript", a.async = !0, a.src = e, t && (a.onload = t), n.insertBefore(a, n.firstChild), r.debug("Asynchronously requesting " + e) }, t.addScriptSync = function(e, n) { try { var o = "optimizely_synchronous_script_" + Math.floor(1e5 * Math.random()); if (e.indexOf('"') !== -1) return void r.error("Blocked attempt to load unsafe script: " + e);
            i.write('<script id="' + o + '" src="' + e + '"></script>'); var s = i.querySelector("#" + o); if (!s) throw new Error("Document.write failed to append script");
            s.onload = n, s.onerror = function(i) { r.warn("Failed to load script (" + e + ") synchronously:", i), t.addScriptAsync(e, n) } } catch (i) { r.debug("Document.write failed for " + e + ": " + i.message); var c = function(e) { var t = new Function(e.responseText);
                t(), n && n() }; return a.request({ url: e, async: !1, contentType: "text/plain", success: c })["catch"]((function(i) { r.error("Failed to load " + e + " via synchronous XHR: " + i.message), t.addScriptAsync(e, n) })) } } }), (function(e, t, n) {
    function i() { var e = null;
        D.isNumber(e) && 0 === me.getCount() ? (Q.log("Activating after delay of", e, "ms because no Experiments are running"), K.dispatch(x.SET_RUM_DATA, { data: { activateDfd: !0 } }), ue.setTimeout(P.emitActivateEvent, e)) : P.emitActivateEvent() }

    function r(e) { Ne.handleError(e.data.error, e.data.metadata) }

    function a() { D.isArray(window.optimizely) && (window.optimizely = D.filter(window.optimizely, (function(e) { var t = !0; return !Re.push(e, t) }))) }

    function o() { var e = n(85),
            i = !!ce.getCurrentId(),
            r = !!i && ce.hasSomeData();
        i ? r ? Q.log("xd / Existing visitor; has data on this origin") : Q.log("xd / Existing visitor; new to this origin") : Q.log("xd / New visitor"); var a = ge.getAccountId(),
            o = "https://a8894411182.cdn-pci.optimizely.com".replace("__SUBDOMAIN__", "a" + a + "."),
            c = "/client_storage/a" + a + ".html";
        e.subscribe((function(e, t) { ce.checkKeyForVisitorId(e) && $.setItem(e, t) })); var u = e.fetchAll().then((function(t) { var n = De.getCanonicalOrigins(); if (n) { var i = e.getXDomainUserId(t, n);
                i && (Q.log("Syncing cross-origin visitor randomId:", i), ce.maybePersistVisitorId({ randomId: i })) } return ce.deleteOldForeignData(), t })).then(t.persistItemsWithId).then((function(e) { if (ce.loadForeignData(), i && !r) { var t = !D.isEmpty(e);
                Q.debug("xd / Loaded foreign data? ", t), s(t) }
            Q.log("Loaded visitor data from foreign origins"), P.emitOriginsSyncedEvent() }), (function(e) { throw i && !r && (Q.debug("xd / Failed to load foreign data:", e), s(!1, e)), e })); return ne.all([e.load(o, c)["catch"]((function(e) { throw Q.debug("xd / Failed to load iframe:", e), i && !r && s(!1, e), e })), u["catch"]((function(e) { Q.debug("xd / Ignored error syncing foreign data (expected if waitForOriginSync used):", e.message), Q.debug("xd / Enqueuing sync to happen after visitorId set."), K.dispatch(x.ADD_CLEANUP_FN, { lifecycle: H.Lifecycle.postVisitorProfileLoad, cleanupFn: P.emitOriginsSyncedEvent }) }))]) }

    function s(e, t) { K.dispatch(x.SET_RUM_DATA, { data: { extras: { xdAttempt: e, xdError: t ? t.toString() : void 0 } } }) }

    function c(e) { var t = Te.getVisitorProfile(); return ce.populateEagerVisitorData(e, t) }

    function u(e, t, n) { e = e || []; var i = ye.getAllPlugins(H.PluginTypes.visitorProfileProviders),
            r = ge.getGlobalHoldbackThreshold(),
            a = Te.getVisitorProfile();
        ce.populateLazyVisitorData(i, a); var o = be.getBucketingId(); if (!o) throw new Error("bucketingId not set"); var s, c = Te.getVisitorProfile(); if (t) { var u = we.getVariationIdMap();
            s = u[t.id] } var l = { bucketingId: o, visitorProfile: c, audiences: e, globalHoldback: r, preferredVariationMap: s, layer: t }; return t && n && G.isPageIdRelevant(t) ? D.map(n, (function(e) { return G.createTicket(D.extend({}, l, { pageId: e })) })) : [G.createTicket(l)] }

    function l(e) { return { bucketingId: be.getBucketingId(), preferredLayerId: we.getPreferredLayerMap()[e.id] } }

    function d(e) { var n = me.getAllByPageIds(e),
            i = he.getForceVariationIds(),
            r = he.getForceAudienceIds(),
            a = !D.isEmpty(i);
        a && Q.log("Force variations are in use. Disabling mutual exclusivity."); var o = a ? { individual: n } : D.reduce(n, (function(e, t) { return t.groupId ? e.groups[t.groupId] || (e.groups[t.groupId] = ve.get(t.groupId)) : e.individual.push(t), e }), { groups: {}, individual: [] });
        Q.log("Deciding Campaigns/Experiments for Page(s)", e); var s = D.map(o.groups, W.description).join(", ");
        Q.log("Groups:", s); var c = D.map(o.individual, X.description).join(", ");
        Q.log("Campaigns/Experiments not in Groups (by Campaign id):", c); var u = D.map(o.groups, D.partial(f, i, r, e)) || [],
            l = D.map(o.individual, (function(n) { var a = D.filter(n.pageIds, D.partial(D.includes, e)); return t.decideAndExecuteLayerASAP(i, r, a, n) })),
            d = u.concat(l); return ne.all(d).then((function(t) { var n = D.filter(t, (function(e) { return !!e })); return Q.log("All Campaigns/Experiments for Page(s) (by Campaign id)", e, "resolved:", D.map(n, X.description).join(", ")), n })) }

    function f(e, n, i, r) { try { var a = l(r),
                o = G.decideGroup(r, a); if (o.reason) return Q.debug("Not activating Group", W.description(r), "; reason:", o.reason), Ie.getSampleRum() && K.dispatch(x.RECORD_LAYER_FEATURE_USAGE, { feature: "mutex", entityId: r.id }), ne.resolve(); var s = me.get(o.layerId); if (!s) return Q.debug("Visitor was bucketed into a Campaign (" + o.layerId + ") which is not in this snippet"), ne.resolve(); var c = D.filter(s.pageIds, D.partial(D.includes, i)); return D.isEmpty(c) ? (Q.debug("Not activating Group", W.description(r), "; reason: visitor was bucketed into a Campaign/Experiment not related to the currently-activating Page(s)"), ne.resolve()) : (Ie.getSampleRum() && K.dispatch(x.RECORD_LAYER_FEATURE_USAGE, { feature: "mutex", entityId: r.id }), t.decideAndExecuteLayerASAP(e, n, c, s)) } catch (e) { return Q.error("Error getting decision for Group", W.description(r), "; ", e), ne.reject(e) } }

    function p(e, t, n, i) { return new ne(function(r, a) { try { _(i, e, t, n, (function(a) { D.each(a, (function(r) { var a = r.pageId ? [r.pageId] : n;
                        Q.debug("Deciding layer: ", i, "with decisionTicket: ", r, "and actionViewIds: ", a), h(i, e, t, a, r) })), r(i) })) } catch (e) { Q.error("Error getting decision for Campaign: " + X.description(i), e), a(e) } }) }

    function h(e, n, i, r, a) { var o = X.description(e);
        Q.log("Activating Campaign", o, "on Page(s)", r), i.length && (Q.log("Applying force audienceIds:", i, "to Campaign", o), a = D.cloneDeep(a), a.audienceIds = i); var s = t.decideLayer(e, a, n),
            c = !(!n.length && !i.length),
            u = t.getActionsForDecision(e, s, c),
            l = w(u.actions, r); if (u.maybeExecute ? g(l, e, s, r) : Q.warn("Not preparing actions because LIVE_CHANGES is false"), D.forEach(r, (function() { L.trackDecisionEvent(s, a) })), P.emitLayerDecided({ layer: e, decisionTicket: a, decision: s }), s.error) throw s.error; if (Ie.getSampleRum()) { K.dispatch(x.RECORD_LAYER_POLICY_USAGE, { policy: e.policy, layerId: e.id }); var d = m(u.actions);
            K.dispatch(x.RECORD_CHANGE_TYPE_USAGE, { changeTypes: D.keys(d), layerId: e.id }), D.isEmpty(e.integrationSettings) || K.dispatch(x.RECORD_INTEGRATION_USAGE, { integrations: D.keys(e.integrationSettings), layerId: e.id }) } return G.isInCohort(s) ? void(u.maybeExecute ? v(l, e, s, r) : Q.warn("Not executing actions because LIVE_CHANGES is false")) : void Q.log("Not activating Campaign: " + X.description(e) + "; not in the cohort because:", s.reason) }

    function g(e, t, n, i) { var r = X.description(t);
        Q.log("Preparing actions", e, "for Campaign", r, "on Page(s)", i), D.forEach(e, O.prepareAction) }

    function v(e, t, n, i) { var r = X.description(t); return Q.log("Executing actions", e, "for Campaign", r, "on Page(s)", i), ne.all(D.map(e, (function(e) { return O.executePreparedAction(e).then(D.partial(P.emitActionAppliedEvent, e)) }))).then((function() { Q.log("All page actions for", n, "applied:", e), P.emitActionsForDecisionAppliedEvent(n, e) }))["catch"]((function(e) { Q.warn("Error evaluating page actions for decision", n, "because:", e) })) }

    function m(e) { var t = {}; return D.each(e, (function(e) { D.each(e.changeSet, (function(e) { t[e.type] || (t[e.type] = !0) })) })), t }

    function _(e, t, n, i, r) { if (t.length || n.length) return void r(u([], void 0, i)); var a = X.relatedAudienceIds(e),
            o = D.reduce(a, (function(e, t) { var n = de.get(t); return n && e.push(n), e }), []),
            s = ye.getAllPlugins(H.PluginTypes.audienceMatchers); if (Ie.getSampleRum()) { var c = {}; if (D.each(o, (function(e) { D.extend(c, E(e.conditions, s)) })), !D.isEmpty(c)) { var l = D.keys(c);
                K.dispatch(x.RECORD_AUDIENCE_USAGE, { audienceTypes: l, layerId: e.id }) } }
        T(o, s, X.getActivationTimeout(e), (function() { var t = u(o, e, i);
            D.map(t, (function(t) { y(t, o, e) })), r(t) })) }

    function E(e, t) { var n = {}; return D.each(e, (function(e) { D.isArray(e) ? D.extend(n, E(e, t)) : D.isObject(e) && t[e.type] && (n[e.type] = !0) })), n }

    function y(e, t, n) { var i = D.map(e.audienceIds, D.bind(de.get, de)),
            r = D.filter(t, (function(t) { return !D.includes(e.audienceIds, t.id) }));
        Q.log("When deciding Campaign", X.description(n), "visitor is in audiences:", I(i), "and not in audiences:", I(r)) }

    function I(e) { var t = []; return D.each(e, (function(e) { t.push(e.name, e) })), t }

    function T(e, t, n, i) { var r = D.reduce(e, (function(e, n) { return D.extend(e, k.requiredAudienceFieldsForConditions(n.conditions, t)) }), {}),
            a = D.reduce(r, (function(e, t) { if (D.isUndefined(ce.getAttribute(t))) { var n = ce.getPendingAttributeValue(t);
                    D.isUndefined(n) || e.push(n) } return e }), []); if (0 === a.length) return i(); var o = [].concat(e),
            s = ie.firstToResolve(D.map(a, (function(e) { return ne.resolve(e).then((function() { var e = Te.getVisitorProfile(); if (o = D.filter(o, (function(n) { return D.isUndefined(k.isInAudience(e, n, t)) })), !D.isEmpty(o)) throw new Error("At least one audience is still pending") })) })));
        ne.race([s, new ne(function(e, t) { ue.setTimeout(t, n) })]).then((function() { Q.log("Activating Campaign after pending Audiences resolved", e), i() }), (function() { Q.log("Activating Campaign after timeout on Audiences", e), i() })) }

    function S(e, t, n) { var i, r = X.description(e); return i = n.length ? G.getDummyLayerDecision(e, n) : G.decideLayer(e, t), Q.log("Recording decision for Campaign", r, t, "->", i), X.recordLayerDecision(e.id, t, i), i.variationId && i.experimentId && ce.updateVariationIdMap(e.id, i.experimentId, i.variationId), e.groupId && ce.updatePreferredLayerMap(e.groupId, e.id), i }

    function A(e) { var t = pe.getCleanupFns(e); if (t.length > 0) { for (; t.length > 0;) t.shift()();
            K.dispatch(x.CLEAR_CLEANUP_FN, { lifecycle: e }) } }

    function b(e, t, n) { var i = X.description(e),
            r = "NOT applying changes for Campaign",
            a = { actions: [], maybeExecute: !1 }; return a.actions = [].concat(fe.getLayerActions(t.layerId) || [], fe.getExperimentActions(t.experimentId) || [], fe.getExperimentVariationActions(t.experimentId, t.variationId) || []), !n && ge.isGlobalHoldback() ? (Q.log(r, i, "(visitor is in global holdback)"), a) : t.isLayerHoldback ? (Q.log(r, i, "(visitor is in layer holdback)"), a) : t.experimentId && t.variationId ? (a.maybeExecute = !0, Q.log("Got Actions for Campaign:", i, a.actions), a) : (Q.log(r, i, "(visitor is not eligible for any Experiments)"), a) }

    function w(e, t) { return D.filter(e, (function(e) { return D.isUndefined(e.pageId) || D.includes(t, e.pageId) })) }
    var D = n(2),
        R = n(77).create,
        N = t.ActivationCodeError = R("ActivationCodeError"),
        C = t.ProjectJSError = R("ProjectJSError"),
        O = n(136),
        x = n(7),
        L = n(110),
        P = n(117),
        k = n(140),
        F = n(76),
        M = n(86),
        V = n(109),
        U = n(24),
        G = n(141),
        B = n(16),
        j = n(81),
        H = n(25),
        z = n(87),
        q = n(111),
        Y = n(146),
        K = n(9),
        W = n(145),
        X = n(113),
        $ = n(82).LocalStorage,
        Q = n(23),
        J = n(147),
        Z = n(84),
        ee = n(123),
        te = n(88),
        ne = n(12).Promise,
        ie = n(148),
        re = n(114),
        ae = n(116),
        oe = n(138),
        se = n(124),
        ce = n(75),
        ue = n(41),
        B = n(16),
        le = B.get("stores/session"),
        de = B.get("stores/audience_data"),
        fe = B.get("stores/action_data"),
        pe = B.get("stores/cleanup"),
        he = B.get("stores/directive"),
        ge = B.get("stores/global"),
        ve = B.get("stores/group_data"),
        me = B.get("stores/layer_data"),
        _e = B.get("stores/layer"),
        Ee = B.get("stores/pending_events"),
        ye = B.get("stores/plugins"),
        Ie = B.get("stores/rum"),
        Te = B.get("stores/visitor"),
        Se = B.get("stores/view_data"),
        Ae = B.get("stores/view"),
        be = B.get("stores/visitor_id"),
        we = B.get("stores/visitor_bucketing"),
        De = B.get("stores/xdomain"),
        Re = n(93),
        Ne = n(121),
        Ce = n(1),
        Oe = 1e3,
        xe = !1,
        Le = !1,
        Pe = 1e3,
        ke = t;
    t.initialize = function(e) { var n = e.clientData; if (V.normalizeClientData(e.clientData), z.on({ filter: { type: "error" }, handler: r }), K.dispatch(x.DATA_LOADED, { data: n }), Q.log("Initialized with DATA:", n), a(), he.isDisabled() || he.shouldOptOut()) return void Q.log("Controller / Is disabled");
        Ce.queueBeacons(), j.isReady() ? K.dispatch(x.SET_DOMCONTENTLOADED) : j.addReadyHandler((function() { K.dispatch(x.SET_DOMCONTENTLOADED) })); var o = !1,
            s = F.get(H.COOKIES.REDIRECT); if (s) { var c = s.match(/^(\d+)\|(.*)/); if (c) { Q.debug("Found legacy redirect data:", s); var u = c[1],
                    l = c[2];
                K.dispatch(x.INITIALIZE_STATE, { effectiveVariationId: u, effectiveReferrer: l }), o = !0 } }
        Z.time("projectJS"); var d = ge.getProjectJS(); if (D.isFunction(d)) try { Y.apply(d) } catch (e) { Q.error("Error while executing projectJS: ", e), M.emitError(new C(e)) }
        Z.timeEnd("projectJS"), D.each(e.plugins || [], (function(e) { try { e(ee) } catch (e) { M.emitInternalError(e) } })), D.each(ge.getPlugins() || [], (function(e) { try { Y.apply(e, [ee]) } catch (e) { M.emitError(e) } })), o || re.load(); var f = z.on({ filter: { type: "lifecycle", name: "activated" }, handler: function() { Te.observe(ce.persistVisitorProfile), _e.observe(ce.persistLayerStates), le.observe(ce.persistSessionState), Ee.observe(J.persistPendingEvents), we.observe(ce.persistVisitorBucketingStore), z.off(f) } });
        z.on({ filter: { type: "lifecycle", name: "viewsActivated" }, handler: t.onViewsActivated }), z.on({ filter: { type: "lifecycle", name: "pageDeactivated" }, handler: t.onPageDeactivated }), t.initializeApi(); var p = J.getPendingEvents(); if (p && (K.dispatch(x.LOAD_PENDING_EVENTS, { events: p }), J.retryPendingEvents(p)), z.on({ filter: { type: "lifecycle", name: "activate" }, handler: t.activate }), P.emitInitializedEvent(), !he.shouldActivate()) return ne.resolve(); var h = []; if (De.isDisabled()) i();
        else { var g = t.initializeXDomainStorage();
            h.push(g); var v = Boolean(De.getCanonicalOrigins()); if (v) { var m = ae.makeTimeoutPromise(Pe);
                ne.race([g, m])["catch"]((function(e) { Q.error("Failed to initialize xDomain storage: ", e) })).then(i)["catch"](Ne.handleError) } else i() } return ne.all(h) }, t.activate = function() {
        try {
            var e = [];
            Q.log("Activated client"), D.forEach(Ae.getActiveViewStates(), (function(e) { se.deactivate(Se.get(e.id)) })), A(H.Lifecycle.preActivate);
            var t = U.now();
            K.dispatch(x.ACTIVATE, { activationId: String(t), activationTimestamp: t });
            var n = Se.getAll();
            se.registerViews(n), ce.setId(ce.getOrGenerateId()), e.push(L.trackPostRedirectDecisionEvent()), K.dispatch(x.MERGE_VARIATION_ID_MAP, { variationIdMap: ce.getVariationIdMap() }), K.dispatch(x.MERGE_PREFERRED_LAYER_MAP, { preferredLayerMap: ce.getPreferredLayerMap() }), A(H.Lifecycle.postVisitorProfileLoad), e.push(c(ye.getAllPlugins(H.PluginTypes.visitorProfileProviders)).then((function() { Q.log("Populated visitor profile") })));
            var i = u(),
                r = G.decideGlobal(i);
            Q.log("Made global decision", i, "->", r), K.dispatch(x.RECORD_GLOBAL_DECISION, r);
            var a = L.trackClientActivation();
            a ? Q.log("Tracked activation event", a) : Q.log("Not tracking activation event");
            var o = ke.setUpViewActivation(n);
            return xe ? se.activateMultiple(o) : D.each(o, (function(e) { se.activateMultiple([e]) })), A(H.Lifecycle.postViewsActivated), A(H.Lifecycle.postActivate), P.emitActivatedEvent(), ne.all(e).then((function() {
                z.emit({
                    type: q.TYPES.LIFECYCLE,
                    name: "activateDeferredDone"
                }), Q.log("All immediate effects of activation resolved")
            }), M.emitError)
        } catch (e) { return M.emitError(e), ne.reject(e) }
    }, ke.setUpViewActivation = function(e) { var t = []; return D.each(e, (function(e) { se.shouldTriggerImmediately(e.activationType) ? t.push(e) : e.activationType === H.ViewActivationTypes.callback ? (Q.debug("Setting up conditional activation for Page", se.description(e)), ke.activateViewOnCallback(e)) : e.activationType === H.ViewActivationTypes.polling ? (Q.debug("Setting up polling activation for Page", se.description(e)), te.pollFor(D.partial(Y.apply, e.activationCode), null, D.partial(oe.isTimedOut, U.now())).then((function() { se.activateMultiple([e]) }))["catch"]((function(t) { Q.warn("Failed to activate view ", e, t) }))) : e.activationType !== H.ViewActivationTypes.manual && M.emitError(new Error("Unknown view activationType: " + e.activationType)) })), t }, ke.activateViewOnCallback = function(e) { var t = function(t) { var n = D.extend({}, t, { pageName: e.apiName, type: "page" });
                Re.push(n) },
            n = { pageId: e.id };
        Object.defineProperty(n, "isActive", { get: function() { return Ae.isViewActive(e.id) } }); try { Y.apply(e.activationCode, [t, n]) } catch (t) { var i = new N("(" + t.toString() + ") in activationCode for " + se.description(e));
            M.emitError(i, { originalError: t, userError: !0 }) } }, t.onViewsActivated = function(e) { var t, n = e.data.views,
            i = D.map(n, "id"); try { if (!be.getBucketingId()) throw new Error("View activated with no visitorId set"); var r = d(i)["catch"](M.emitError); return t = ne.all(D.map(n, (function(e) { var t = function() { se.parseViewTags(e); var t = L.trackViewActivation(e);
                    t ? Q.log("Tracked activation for Page", se.description(e), t) : Q.log("Not Tracking activation for Page", se.description(e)) }; return j.isReady() ? ne.resolve(t()) : te.pollFor(j.isReady, Oe).then(t) }))), ne.all([r, t]) } catch (e) { M.emitError(e) } }, t.onPageDeactivated = function(e) { var t = e.data.page,
            n = fe.getAllActionIdsByPageId(t.id);
        D.each(n, (function(e) { var n = fe.getActionState(e);
            n && (D.each(n, (function(e, n) { if (e.cancel) try { e.cancel(), Q.debug("Controller / Canceled change", n, "observation due to deactivation of page:", t) } catch (e) { Q.error("Controller / Error canceling change", n, "observation upon deactivation of page.", e) }
                if (t.undoOnDeactivation && e.undo) try { e.undo(), Q.debug("Controller / Undid change", n, "due to deactivation of page:", t) } catch (e) { Q.error("Controller / Error undoing change upon deactivation of page.", e) } })), K.dispatch(x.REMOVE_ACTION_STATE, { actionId: e }), Q.debug("Controller / Undid changes and/or canceled change observation due to deactivation of page:", t, e)) })) }, t.initializeApi = function() { var e = { push: Re.push };
        Le || (e.get = Re.get); var t = window.optimizely;
        D.isArray(t) && D.each(t, (function(t) { e.push(t) })), e.data = { note: "Obsolete, use optimizely.get('data') instead" }, e.state = {}, window.optimizely = e }, t.persistItemsWithId = function(e) { return D.each(e, (function(e, t) { ce.checkKeyForVisitorId(t) && $.setItem(t, e) })), e }, t.initializeXDomainStorage = o, t.decideAndExecuteLayerASAP = p, t.decideLayer = S, t.getActionsForDecision = b
}), (function(e, t, n) {
    function i(e, t, n) { var i = m.getActionState(t.id); if (!i) return void p.warn("Action / Attempted to prepare change for inactive action: ", t); var r = m.getChangeApplier(e.id, t.id); if (!a.isUndefined(r)) return void p.warn("Action / Attempted to prepare a change which is already being applied: ", e); var s = { changeId: e.id, actionId: t.id, changeApplier: y.create(e, t, n) };
        f.dispatch(o.SET_CHANGE_APPLIER, s) }

    function r(e, t, n, o) { if (a.includes(o, t)) return void p.error("Change with id " + t + " has circular dependencies: " + o.concat(t)); if (!e[t]) { var u = _.getChange(t); if (!u) { var d = "Change with id " + t + " is absent"; return o.length && (d += " but listed as a dependency for " + o[o.length - 1]), void p.warn(d) }
            e[t] = new h(function(d) { var f = a.map(u.dependencies || [], (function(i) { return r(e, i, n, o.concat([t])) })); if (u.src) { var v = "change_" + u.src,
                        E = c.makeAsyncRequest(v, (function() { return g.addScriptAsync("https://cdn-pci.optimizely.com/public/8894411182/data" + u.src, (function() { c.resolveRequest(v) })) })).then((function() { var e = _.getChange(u.id);
                            e || s.emitError(new T("Failed to load async change from src: " + u.src)), i(e, n, l.now()) }));
                    f.push(E) }
                h.all(f).then((function() { var e = l.now(),
                        i = m.getChangeApplier(t, n.id); return i ? (p.debug("Action / Applying change:", u), i.apply().then((function(t) { t ? p.log(t) : p.debug("Action / Applied change for the first time in " + (l.now() - e) + "ms:", u), d() }))) : (p.debug("Action / Not applying change ", t, " - No changeApplier found."), void d()) }))["catch"]((function(e) { p.error("Action / Failed to apply change:", u, e), d() })) }) } return e[t] } var a = n(2),
        o = n(7),
        s = n(86),
        c = n(6),
        u = n(77).create,
        l = n(24),
        d = n(16),
        f = n(9),
        p = n(23),
        h = n(12).Promise,
        g = n(134),
        v = d.get("stores/global"),
        m = d.get("stores/action_data"),
        _ = d.get("stores/change_data"),
        E = d.get("stores/session"),
        y = n(137),
        I = n(138);
    I.initialize(); var T = u("ActionError");
    t.prepareAction = function(e) { p.debug("Action / Preparing:", e), f.dispatch(o.ACTION_EXECUTED, { actionId: e.id, sessionId: E.getSessionId(), layerId: e.layerId, pageId: e.pageId, timestamp: l.now(), activationId: v.getActivationId() }); var t = l.now();
        a.forEach(e.changeSet, (function(n) { var r = a.isObject(n) ? n.id : n,
                s = _.getChange(r);
            s || (f.dispatch(o.ADD_CHANGE, n), s = _.getChange(n.id)), s.src || i(s, e, t) })) }, t.executePreparedAction = function(e) { p.debug("Action / Executing:", e); var t = {},
            n = a.map(e.changeSet, (function(n) { var i = a.isObject(n) ? n.id : n; return r(t, i, e, []) })); return h.all(n).then((function() { p.debug("changes for action id=" + e.id + " applied") })) } }), (function(e, t, n) { var i = n(13).Promise,
        r = n(24),
        a = n(16),
        o = a.get("stores/plugins"),
        s = n(25),
        c = n(23);
    t.create = function(e, t, n) { var a = { identifier: e.id, action: t, startTime: n || r.now() }; try { var u = o.getPlugin(s.PluginTypes.changeAppliers, e.type); if (!u) throw new Error("Unrecognized change type " + e.type); return new u(e, a) } catch (e) { c.error("Change applier was never properly constructed:", e); var l = { apply: function() { return i.reject(e) } }; return l } } }), (function(e, t, n) {
    function i() { "interactive" !== document.readyState && "complete" !== document.readyState || (t.domReadyTime = Date.now()) } var r = n(139),
        a = n(16).get("stores/directive");
    t.domReadyTime = null, t.initialize = function() { i(), document.addEventListener("readystatechange", i, !0) }, t.isTimedOut = function(e) { var n = Date.now(); if (!t.domReadyTime || !e) return !1; var i = Math.max(e, t.domReadyTime); return a.isEditor() && (i = t.domReadyTime), !(n - i < r.SELECTOR_POLLING_MAX_TIME) } }), (function(e, t) { e.exports = { SELECTOR_POLLING_MAX_TIME: 2e3, CHANGE_DATA_KEY: "optimizelyChangeData", CHANGE_ID_ATTRIBUTE_PREFIX: "data-optly-" } }), (function(e, t, n) {
    function i(e, t) { return function(n) { var i = n.type,
                a = t[i]; if (!a) throw new Error("Audience / No matcher found for type=" + i); if (a.fieldsNeeded)
                for (var s = r(a.fieldsNeeded, n), l = 0; l < s.length; l++) { var d = s[l],
                        f = c.getFieldValue(e, d); if (o.isUndefined(f)) return void u.debug("Audience / Required field", d, "for type", i, "has no value") }
            u.debug("Matching condition:", n, "to values:", e); var p = a.match(e, n); if (!o.isUndefined(p)) return !!p } }

    function r(e, t) { var n = "function" == typeof e ? e(t) : e; return o.isString(n) && (n = [n]), o.isArray(n) ? n : (u.warn("Couldn't determine fieldsNeeded for matcher; assuming []"), []) }

    function a(e) { return e.name ? e.name + " (" + e.id + ")" : e.id } var o = n(2),
        s = n(125),
        c = n(19),
        u = n(23),
        l = n(75);
    t.isInAudience = function(e, t, n) { var r = i(e, n);
        u.groupCollapsed("Checking audience", t.name, t.id, t), u.debug("Visitor Profile:", e); var o; try { var c = s.evaluate(t.conditions, r) } catch (e) { o = e, c = !1 } return u.groupEnd(), o && u.error("Audience / Error evaluating audience", a(t), ":", o), u.log("Is " + (c ? "in" : "NOT in") + " audience:", a(t)), c }, t.requiredAudienceFieldsForConditions = function e(t, n) { var i = {}; return o.each(t, (function(t) { if (o.isArray(t)) o.extend(i, e(t, n));
            else if (o.isObject(t)) { var a = n[t.type]; if (a) { var s = r(a.fieldsNeeded, t);
                    o.each(s, (function(e) { i[l.serializeFieldKey(e)] = e })) } } })), i } }), (function(e, t, n) {
    function i(e, t) { m.debug("Decision / Deciding layer for group: ", g.description(e)); var n, i, r = t.preferredLayerId,
            a = !!r; if (a) m.debug("Decision / Using preferredLayerMap to select layer for group:", g.description(e)), n = r;
        else try { n = l.chooseWeightedCandidate(t.bucketingId, e.id, e.weightDistributions), n && "None" !== n || (i = 'Group traffic allocation. Visitor maps to a "hole" in the bucket space left by an experiment or campaign that\'s since been removed from the group') } catch (e) { i = "Group traffic allocation. Visitor maps to a point in the bucket space which has never been covered by any experiment or campaign." }
        if (i) return { layerId: null, reason: i }; if (!s.find(e.weightDistributions, { entityId: n })) { var o = a ? " sticky-" : " non-sticky ",
                c = "Visitor was" + o + "bucketed into a campaign (" + n + ") which is not in the group"; if (!a) throw new f(c); return { layerId: null, reason: c } } return { layerId: n } }

    function r(e, t) { for (var n = 0; n < e.experiments.length; n++)
            for (var i = 0; i < e.experiments[n].variations.length; i++)
                if (t.indexOf(e.experiments[n].variations[i].id) > -1) return { experimentId: e.experiments[n].id, variationId: e.experiments[n].variations[i].id };
        return null }

    function a(e) { var t = E.getPlugin(h.PluginTypes.deciders, e); if (s.isEmpty(t)) throw new Error("No deciders found for policy: " + e); return t }

    function o(e, t) { var n = E.getAllPlugins(h.PluginTypes.audienceMatchers); return s.reduce(t, (function(t, i) { return u.isInAudience(e, i, n) && t.push(i.id), t }), []) } var s = n(2),
        c = n(86),
        u = n(140),
        l = n(142),
        d = n(143),
        f = n(144).DecisionError,
        p = n(16),
        h = n(25),
        g = n(145),
        v = n(113),
        m = n(23),
        _ = n(45),
        E = p.get("stores/plugins"),
        y = p.get("stores/global"),
        I = p.get("stores/layer_data");
    t.isPageIdRelevant = function(e) { if (!e) return !1; var t = a(e.policy); return s.isFunction(t.includePageIdInDecisionTicket) ? t.includePageIdInDecisionTicket(e) : t.includePageIdInDecisionTicket === !0 }, t.createTicket = function(e) { var t = s.pick(e, ["bucketingId", "globalHoldback", "preferredVariationMap", "pageId"]); return s.extend(t, { audienceIds: o(e.visitorProfile, e.audiences), activationId: y.getActivationId() }), t }, t.decideGlobal = function(e) { var t = l.isHoldback(e.bucketingId, { id: null, holdback: e.globalHoldback }); return { isGlobalHoldback: t } }, t.decideGroup = i, t.decideLayer = function(e, t) { m.debug("Deciding: ", e, t); var n, i, r = a(e.policy),
            o = { layerId: e.id, experimentId: null, variationId: null, isLayerHoldback: l.isHoldback(t.bucketingId, e) }; if (s.isEmpty(e.experiments)) throw new f("No experiments in layer."); try { if (r.decideLayer) { m.debug("Decision / Using decider's custom decideLayer."); var u = r.decideLayer(e, t);
                n = u.experiment, i = u.variation } else m.debug("Decision / Using default decideLayer behavior."), n = r.selectExperiment(e, t.audienceIds, t.bucketingId), i = d.selectVariation(n, t.audienceIds, t.bucketingId, t.activationId, t.preferredVariationMap) } catch (e) { e instanceof f ? o.reason = e.message : o.error = e } return o.experimentId = n ? n.id : null, o.variationId = i ? i.id : null, o.error && (o.error.name = "DecisionEngineError", c.emitError(o.error)), o }, t.getDummyLayerDecision = function(e, t) { var n, i = r(e, t); return i ? (m.log("Decision / Applying force variation:", i.variationId, "to Campaign", v.description(e)), n = { layerId: e.id, variationId: i.variationId, experimentId: i.experimentId, isLayerHoldback: !1, reason: "force" }) : (m.log("No variation matches ids:", t, "in Campaign", v.description(e)), n = { layerId: e.id, variationId: null, experimentId: null, isLayerHoldback: !1, reason: "force" }), n }, t.isInCohort = function(e) { if (!e.experimentId || !e.variationId) return !1; var t = I.get(e.layerId); return !(_.isSingleExperimentPolicy(t.policy) && e.isLayerHoldback) } }), (function(e, t, n) { var i = n(65),
        r = t.TOTAL_POINTS = 1e4;
    t.bucketingNumber = function(e, t, n) { var a = i.hashToInt(e + t, n, r); return a }, t.isHoldback = function(e, n) { return t.bucketingNumber(e, n.id, i.Seed.IGNORING) < (n.holdback || 0) }, t.chooseWeightedCandidate = function(e, n, r) { for (var a = t.bucketingNumber(e, n, i.Seed.BUCKETING), o = 0; o < r.length; o++)
            if (r[o].endOfRange > a) return r[o].entityId;
        throw new Error("Unable to choose candidate") } }), (function(e, t, n) { var i = n(2),
        r = n(142),
        a = n(125),
        o = n(144).DecisionError,
        s = n(23),
        c = "impression";
    t.isValidExperiment = function(e, t) { var n, r = i.partial(i.includes, e); return s.groupCollapsed("Decision / Evaluating audiences for experiment:", t, e), n = !t.audienceIds || a.evaluate(t.audienceIds, r), s.groupEnd(), s.debug("Decision / Experiment", t, "is valid?", n), n }, t.selectVariation = function(e, t, n, a, u) { if (!e.variations || 0 === e.variations.length) throw new o('No variations in selected experiment "' + e.id + '"'); if (!e.weightDistributions && e.variations.length > 1) throw new o('On selected experiment "' + e.id + '", weightDistributions must be defined if # variations > 1'); var l; if (e.bucketingStrategy && e.bucketingStrategy === c)
            if (1 === e.variations.length) l = e.variations[0].id;
            else { var d = a;
                l = r.chooseWeightedCandidate(n + d, e.id, e.weightDistributions) }
        else if (u && u[e.id]) { s.debug("Decision / Using preferredVariationMap to select variation for experiment:", e.id); var f = u[e.id]; if (!i.find(e.variations, { id: f })) return s.debug("Decision / Preferred variation:", f, "not found on experiment:", e.id, ". Visitor not bucketed."), null;
            l = f } else l = 1 === e.variations.length ? e.variations[0].id : r.chooseWeightedCandidate(n, e.id, e.weightDistributions); var p = i.find(e.variations, { id: l }); if (p) return s.debug("Decision / Selected variation:", p), p; throw new o('Unable to find selected variation: "' + l + '".') }, t.getExperimentById = function(e, t) { var n = i.find(e.experiments, { id: t }); if (n) return n; throw new o("Unable to find selected experiment.") }, t.hasVariationActionsOnView = function(e, t) { return s.debug("Decision / Checking variation:", e, "for actions on pageId:", t), !!i.find(e.actions, (function(e) { return e.pageId === t && !i.isEmpty(e.changes) })) } }), (function(e, t) {
    function n(e) { this.message = e }
    n.prototype = new Error, t.DecisionError = n }), (function(e, t, n) {
    function i(e) { return r.map(e.weightDistributions, "entityId") } var r = n(2);
    t.description = function(e) { var t = !!e.name,
            n = t ? '"' + e.name + '" ' : "",
            r = i(e).join(", "); return n + "(id " + e.id + ", campaigns: " + r + ")" } }), (function(module, exports, __webpack_require__) { var createError = __webpack_require__(78),
        di = __webpack_require__(16),
        Logger = __webpack_require__(23),
        CSP_MODE = !1,
        EXEC_WITH_JQUERY = !0,
        ExecError = exports.Error = createError("ExecError");
    exports.apply = function(e, t) { t = t || [], EXEC_WITH_JQUERY && (t = t.concat(di.get("env/jquery"))); try { return e.apply(void 0, t) } catch (n) { throw Logger.warn("Error applying function", e, "with args:", t, n), new ExecError(n) } }, exports.eval = function(str) { if (CSP_MODE) throw new ExecError("eval is not supported in CSP mode"); try { return EXEC_WITH_JQUERY && (str = "var $ = optimizely.get('jquery');" + str), eval(str) } catch (e) { throw Logger.warn("Error executing JS:", str, e), new ExecError(e) } } }), (function(e, t, n) { var i = n(2),
        r = n(86),
        a = n(25),
        o = n(26),
        s = n(82).LocalStorage,
        c = n(23),
        u = n(91),
        l = n(16),
        d = l.get("stores/pending_events"),
        f = a.StorageKeys.PENDING_EVENTS;
    t.persistPendingEvents = function() { try { var e = d.getEventsString();
            s.setItem(f, e), n(85).setItem(f, e) } catch (e) { c.warn("PendingEvents / Unable to set localStorage key, error was: ", e), r.emitInternalError(e) } }, t.getPendingEvents = function() { try { return o.parse(s.getItem(f)) } catch (e) { return null } }, t.retryPendingEvents = function(e) { i.forOwn(e, (function(e, t) { u.retryableRequest(e.data, t, e.retryCount) })), i.isEmpty(e) || c.log("Retried pending events: ", e) } }), (function(e, t, n) { var i = n(2),
        r = n(12).Promise;
    t.firstToResolve = function(e) { return new r(function(t) { i.each(e, (function(e) { r.resolve(e).then(t, (function() {})) })) }) } }), (function(e, t, n) {
    function i(e) { var t = !1; if (a.isArray(window.optimizely) && a.each(window.optimizely, (function(n) { a.isArray(n) && "verifyPreviewProject" === n[0] && String(n[1]) === e && (t = !0) })), !t) throw new Error("Preview projectId: " + e + " does not match expected") }

    function r() { s.on({ filter: { type: c.TYPES.ANALYTICS, name: "trackEvent" }, handler: f }), s.on({ filter: { type: c.TYPES.LIFECYCLE, name: "viewActivated" }, handler: f }), s.on({ filter: { type: c.TYPES.LIFECYCLE, name: "layerDecided" }, handler: f }), s.on({ filter: { type: "error" }, publicOnly: !0, handler: f }) } var a = n(2),
        o = n(16),
        s = n(87),
        c = n(111),
        u = n(41),
        l = o.get("stores/directive"),
        d = "optimizelyPreview",
        f = function(e) { var t = u.getGlobal(d);
            t.push(e) };
    t.initialize = function(e) { l.isSlave() && i(e), r() }, t.setupPreviewGlobal = function() { u.getGlobal(d) || u.setGlobal(d, []) }, t.pushToPreviewGlobal = function(e) { f(e) } }), (function(e, t, n) { e.exports = function(e) { e.registerVisitorProfileProvider(n(151)) } }), (function(e, t) { e.exports = { provides: "visitorId", getter: ["stores/visitor_id", function(e) { return e.getRandomId() }] } }), (function(e, t, n) { e.exports = function(e) { e.registerVisitorProfileProvider(n(153)), e.registerAudienceMatcher("behavior", n(155)) } }), (function(e, t, n) { var i = n(154);
    e.exports = { provides: "events", isTransient: !0, getter: [function() { return i.getEvents() }] } }), (function(e, t, n) { var i = n(2),
        r = n(73),
        a = n(16),
        o = a.get("stores/visitor_events"),
        s = 1e3;
    t.getEvents = function() { var e = r.getEvents(),
            t = [].concat.apply([], i.values(o.getForeignEvents())),
            n = [].concat.apply([], i.values(o.getForeignEventQueues())),
            a = r.mergeAllEvents([e, t, n]); return a.slice(a.length - s) } }), (function(e, t, n) { var i = n(2),
        r = n(26),
        a = n(156),
        o = n(157);
    e.exports = { fieldsNeeded: ["events"], match: function(e, t) { var n = [],
                s = r.parse(t.value); return n = i.isUndefined(s.version) ? [s] : a.buildFromSpecV0_1(s), i.every(n, (function(t) { return o.isSatisfied(t, e.events) })) } } }), (function(e, t, n) {
    function i(e) { return e = (e || "").toString().trim(), p[e] || e }

    function r(e, t, n) { var i = { where: t }; if (e.count && (i["limit"] = e.count), e.modifier === s.FREQUENCY_FILTERS.MOST_FREQUENT) { var r = s.getFieldKeyPathForSource(e.name, n),
                a = s.aggregate("count"),
                o = s.aggregateField("count"),
                l = s.groupField(r); return c.extend(i, { select: [{ field: l }], groupBy: s.groupBy([r]), aggregate: [a], orderBy: [{ field: o, direction: "DESC" }] }) } return c.extend(i, { orderBy: [{ field: [u.FIELDS.TIME], direction: "DESC" }] }) }

    function a(e) { var t = []; if (c.isUndefined(e)) throw new Error("rule is undefined"); if (!c.isObject(e)) throw new Error("rule is not an Object"); "0.2" !== e["version"] && t.push('version: not "0.2"'), e["filter"] && (c.isArray(e["filter"]) ? c.each(e["filter"], (function(e, n) { var i = s.validateFieldKeyPathV0_2(e["field"], s.FieldPurpose.FILTER);
            i && t.push("filter[" + n + "]: " + i); var r = s.validateComparatorAndValue(e["comparator"], e["value"]);
            r && t.push("filter[" + n + "]: " + r) })) : t.push("filter: not an array")); var n = [],
            i = []; if (e["sort"] && (e["reduce"] && e["reduce"]["aggregator"] && "nth" !== e["reduce"]["aggregator"] && t.push("sort: superfluous because we can apply aggregator " + l.stringify(e["reduce"]["aggregator"]) + " to unsorted items"), c.isArray(e["sort"]) ? c.each(e["sort"], (function(e, r) { var a = s.validateFieldKeyPathV0_2(e["field"], s.FieldPurpose.SORT);
                a && t.push("sort[" + r + "]: " + a), e["field"] && "frequency" === e["field"][0] ? n.push(e) : i.push(e); var c = o(e["direction"]);
                c && t.push("sort[" + r + "]: " + c) })) : t.push("sort: not an array"), n.length && i.length && t.push('sort: sorting by non-["frequency"] field is pointless because we are going to sort the picked values by ["frequency"]'), n.length && !e["pick"] && t.push('sort: sorting by ["frequency"] is impossible because no values have been picked')), e["pick"]) { e["reduce"] && "count" === e["reduce"]["aggregator"] && t.push('pick: superfluous because we can apply aggregator "count" to raw events'); var r = s.validateFieldKeyPathV0_2(e["pick"]["field"]);
            r && t.push("pick: " + r) } if (e["reduce"]) { var a = e["reduce"]["aggregator"],
                u = "aggregator " + (l.stringify(a) || String(a)),
                d = e["reduce"]["n"],
                f = "index " + (l.stringify(d) || String(d));
            c.includes(["sum", "avg", "max", "min", "count", "nth"], a) || t.push("reduce: " + u + " is unknown"), c.includes(["sum", "avg", "max", "min"], a) && (e["pick"] || t.push("reduce: " + u + " is impossible to use because no values have been picked")), "nth" === a ? ((!c.isNumber(d) || isNaN(d) || parseInt(d, 10) !== d || d < 0) && t.push("reduce: " + f + " is not a non-negative integer (mandated by " + u + ")"), e["sort"] || t.push('reduce: aggregator "nth" is meaningless without a specific sort order')) : c.isUndefined(d) || t.push("reduce: " + f + " is defined (not mandated by " + u + ")") } if (t.length) throw new Error(t.join("\n")) }

    function o(e) { var t = "direction " + (l.stringify(e) || String(e)); if (!c.includes(["ascending", "descending"], e)) return t + ' is not "ascending" or "descending"' } var s = t,
        c = n(2),
        u = { FIELDS: n(64).FIELDS, FIELDS_V0_2: n(64).FIELDS_V0_2 },
        l = n(26),
        d = n(23),
        f = n(157);
    s.MILLIS_IN_A_DAY = 864e5, s.aggregateField = function(e, t) { return c.isString(t) && (t = [t]), t = t || f.DEFAULT_FIELD, [f.generateAlias(e, t)] }, s.groupField = function(e) { return c.isString(e) && (e = [e]), e = e || f.DEFAULT_FIELD, [e.join(".")] }; var p = { "<": "lt", "<=": "lte", ">": "gt", ">=": "gte", "=": "eq", "==": "eq" };
    s.fieldComparison = function(e, t, n) { return e = i(e), c.isString(t) && (t = [t]), "exists" === e ? { op: e, args: [{ field: t }] } : { op: e, args: [{ field: t }, { value: n }] } }, s.relativeTimeComparison = function(e, t) { return { op: i(e), args: [{ op: "-", args: [{ eval: "now" }, { field: [u.FIELDS.TIME] }] }, { value: t * s.MILLIS_IN_A_DAY }] } }, s.rangeTimeComparison = function(e) { return c.isArray(e) ? { op: "between", args: [{ field: [u.FIELDS.TIME] }, { value: [e[0] || +new Date(0), e[1] || +new Date] }] } : (d.error("Rule builder", "rangeTimeComparison passed invalid range", e), null) }, s.groupBy = function(e) { for (var t = [], n = 0; n < e.length; n++) t[n] = { field: e[n] }; return t }, s.aggregate = function(e, t) { return c.isString(t) && (t = [t]), t = t || f.DEFAULT_FIELD, { op: e, args: [{ field: t }] } }, s.SOURCE_TYPES = { BEHAVIOR: "events", CUSTOM_BEHAVIOR: "custom_behavior", DCP: "dcp" }, s.FREQUENCY_FILTERS = { MOST_FREQUENT: "most_frequent", LEAST_FREQUENT: "least_frequent" }, s.RECENCY_FILTERS = { MOST_RECENT: "most_recent", LEAST_RECENT: "least_recent" }, s.getFieldKeyPathForSource = function(e, t) { t = t || s.SOURCE_TYPES.BEHAVIOR; var n = []; return c.isString(e) ? (n = [e], t !== s.SOURCE_TYPES.BEHAVIOR || c.includes(c.values(u.FIELDS), e) || (n = [u.FIELDS.OPTIONS, e])) : n = e, n }, s.buildFromSpecV0_1 = function(e) { if (!(e.action || e.filters && 0 !== e.filters.length)) throw new Error('Audience spec must have an "action" field or at least one "filter" ' + l.stringify(e)); var t = s.fieldComparison("gt", u.FIELDS.TIME, 0),
            n = [],
            i = []; if (e.action && (i.push(s.fieldComparison("eq", u.FIELDS.NAME, e.action.value)), e.action.type && i.push(s.fieldComparison("eq", u.FIELDS.TYPE, e.action.type))), e.time)
            if ("last_days" === e.time.type) i.push(s.relativeTimeComparison("lte", e.time.days));
            else if ("range" === e.time.type) { var a = s.rangeTimeComparison([e.time.start, e.time.stop]);
            a && i.push(a) } else d.error("Rule builder", 'Audience spec has bad "time" type', e.time.type); if (t = { op: "and", args: i }, e.count && n.push({ where: s.fieldComparison(e.count.comparator, "0", e.count.value), from: { select: [{ field: s.aggregateField("count") }], where: t, aggregate: [s.aggregate("count")] } }), e.filters && c.each(e.filters, (function(r) { var a, o, c = s.getFieldKeyPathForSource(r.name, e.source); if (r.modifier === s.FREQUENCY_FILTERS.MOST_FREQUENT ? (a = s.aggregate("count"), o = s.aggregateField("count")) : r.modifier === s.RECENCY_FILTERS.MOST_RECENT && (a = s.aggregate("max", u.FIELDS.TIME), o = s.aggregateField("max", u.FIELDS.TIME)), a) { var l = c,
                        d = s.groupField(l);
                    n.push({ where: s.fieldComparison(r.comparator, "0", r.value), from: { select: [{ field: d }], where: t, groupBy: s.groupBy([l]), aggregate: [a], orderBy: [{ field: o, direction: "DESC" }], limit: 1 } }) } else i.push(s.fieldComparison(r.comparator, c, r.value)) })), e.pick) { if (n.length > 0) throw new Error('A "pick" clause must not be specified with "count" or "most_recent", "most_frequent" modifiers' + l.stringify(e)); return [r(e.pick, t, e.source)] } return n.length > 0 ? n : [{ where: t }] }, s.buildFromSpecV0_2 = function(e) { a(e); var t = { where: { op: "and", args: c.map(e["filter"] || [], (function(e) { return "age" === e["field"][0] ? s.relativeTimeComparison(e["comparator"] || "eq", e["value"] / s.MILLIS_IN_A_DAY) : s.fieldComparison(e["comparator"] || "eq", s.convertFieldKeyPathFromSpecV0_2(e["field"]), e["value"]) })) } }; if (e["reduce"] && "count" === e["reduce"]["aggregator"]) return c.extend(t, { aggregate: [{ op: "count", args: [{ field: ["*"] }] }], select: [{ field: ["_count_*"] }] }); var n = [],
            i = []; if (e["sort"] && (c.each(e["sort"], (function(e) { c.includes(["ascending", "descending"], e["direction"]) && (c.includes(["time", "age"], e["field"][0]) && i.push(e), "frequency" === e["field"][0] && n.push(e)) })), i.length && !n.length && (t["orderBy"] = c.filter(c.map(i, (function(e) { return "time" === e["field"][0] ? { field: s.convertFieldKeyPathFromSpecV0_2(["time"]), direction: "ascending" === e["direction"] ? "ASC" : "DESC" } : "age" === e["field"][0] ? { field: s.convertFieldKeyPathFromSpecV0_2(["time"]), direction: "ascending" === e["direction"] ? "DESC" : "ASC" } : void 0 }))))), e["pick"] && e["pick"]["field"]) { var r = s.convertFieldKeyPathFromSpecV0_2(e["pick"]["field"]); if (e["reduce"] && c.includes(["avg", "max", "min", "sum"], e["reduce"]["aggregator"])) return c.extend(t, { aggregate: [{ op: e["reduce"]["aggregator"], args: [{ field: r }] }], select: [{ field: [f.generateAlias(e["reduce"]["aggregator"], r)] }] });
            t = n.length ? c.extend(t, { groupBy: [{ field: r }], aggregate: [{ op: "count", args: [{ field: ["*"] }] }], orderBy: [{ field: ["_count_*"], direction: "ascending" === n[0]["direction"] ? "ASC" : "DESC" }], select: [{ field: [r.join(".")] }] }) : c.extend(t, { select: [{ field: r }] }) } if (e["reduce"] && "nth" === e["reduce"]["aggregator"]) { var o = e["reduce"]["n"]; if (c.isNumber(o) && o >= 0 && Number(o) === Math.floor(Number(o))) return c.extend(t, { offset: o, limit: 1 }) } return t }, s.convertFieldKeyPathFromSpecV0_2 = function(e) { return "tags" === e[0] && "revenue" === e[1] ? ["r"] : [u.FIELDS_V0_2[e[0]]].concat(e.slice(1)) }, s.FieldPurpose = { FILTER: "filter", SORT: "sort", PICK: "pick" }, s.validateFieldKeyPathV0_2 = function(e, t) { var n = "field " + (l.stringify(e) || String(e)); if (!c.isArray(e) || !c.every(e, c.isString)) return n + " is not an array of strings"; if ("tags" === e[0] && e.length > 2 || "tags" !== e[0] && e.length > 1) return n + " includes too many strings"; if ("tags" === e[0] && e.length < 2) return n + " does not specify an exact tag"; if (e.length < 1) return n + " does not specify a top-level field"; var i = c.keys(u.FIELDS_V0_2),
            r = ["age", "frequency"]; return t === s.FieldPurpose.FILTER && (i.push("age"), r = ["frequency"]), t === s.FieldPurpose.SORT && (i = ["time", "age", "frequency"], r = ["name", "type", "category", "tags"]), c.includes(r, e[0]) ? n + " is not supported here" : c.includes(i, e[0]) ? void 0 : n + " is unknown" }, s.validateComparatorAndValue = function(e, t) { var n = "comparator " + (l.stringify(e) || String(e)),
            i = "value " + (l.stringify(t) || String(t)); if (!c.isString(e) && !c.isUndefined(e)) return n + " is not a string"; switch (e) {
            case void 0:
            case "eq":
            case "is":
            case "contains":
                break;
            case "lt":
            case "gt":
            case "lte":
            case "gte":
                if (!c.isNumber(t)) return i + " is not a number (mandated by " + n + ")"; break;
            case "in":
                if (!c.isArray(t)) return i + " is not an array (mandated by " + n + ")"; break;
            case "between":
                if (!(c.isArray(t) && 2 === t.length && c.isNumber(t[0]) && c.isNumber(t[1]) && t[0] <= t[1])) return i + " is not a pair of increasing numbers (mandated by " + n + ")"; break;
            case "regex":
                if (!(c.isString(t) || c.isArray(t) && 2 === t.length && c.isString(t[0]) && c.isString(t[1]))) return i + " is not a pattern string or a [pattern string, flags string] array (mandated by " + n + ")"; break;
            case "exists":
                if (!c.isUndefined(t)) return i + " is not undefined (mandated by " + n + ")"; break;
            default:
                return n + " is unknown" } } }), (function(e, t, n) {
    var i = n(2),
        r = n(25),
        a = n(23),
        o = i.bind(a.log, a),
        s = n(24),
        c = n(19).getFieldValue,
        u = n(26),
        l = function(e, t, n) { if (e.getValueOrDefault) return e.getValueOrDefault(t, n); if (!i.isArray(t)) return n; var r = c(e, t); return "undefined" == typeof r && (r = n), r },
        d = function(e) { return "string" == typeof e ? e.trim().toLowerCase() : e };
    t.clause = { WHERE: "where", GROUP_BY: "groupBy", AGGREGATE: "aggregate", HAVING: "having", ORDER_BY: "orderBy", SELECT: "select", OFFSET: "offset", LIMIT: "limit", FROM: "from" }, t.DEFAULT_FIELD = ["*"], t.booleanOperators = { eq: function(e) { var t = i.map(e, d); return t[0] == t[1] }, is: function(e) { return e[0] === e[1] }, gt: function(e) { return e[0] > e[1] }, lt: function(e) { return e[0] < e[1] }, gte: function(e) { return e[0] >= e[1] }, lte: function(e) { return e[0] <= e[1] }, "in": function(e) { var t = i.map(e[1] || [], d); return i.includes(t, d(e[0])) }, between: function(e) { return e[1][0] <= e[0] && e[0] <= e[1][1] }, contains: function(e) { var t = i.map(e, (function(e) { return "string" == typeof e ? e.toLowerCase() : e })); return (t[0] || "").indexOf(t[1]) !== -1 }, regex: function(e) { try { var t, n; return i.isString(e[1]) ? (t = e[1], n = "i") : (t = e[1][0] || "", n = e[1][1] || ""), new RegExp(t, n).test(e[0]) } catch (e) { return a.error("Rules", 'In operator "regex", error: ' + (e.message || "invalid RegExp /" + [t, n].join("/"))), !1 } }, exists: function(e) { return "undefined" != typeof e[0] }, and: function(e) { return i.every(e, (function(e) { return e })) }, or: function(e) { return i.some(e, (function(e) { return e })) }, not: function(e) { return !e[0] } }, t.arithmeticOperators = { "+": function(e) { return (e[0] || 0) + (e[1] || 0) }, "-": function(e) { return (e[0] || 0) - (e[1] || 0) }, "/": function(e) { return (e[0] || 0) / (e[1] || 1) }, "%": function(e) { return (e[0] || 0) % (e[1] || 1) } }, t.aggregateOperators = { sum: function(e, n) { for (var i = e[0] || t.DEFAULT_FIELD, r = 0, a = 0; a < n.length; a++) r += l(n[a], i, 0); return r }, avg: function(e, n) { if (0 === n.length) return 0; for (var i = e[0] || t.DEFAULT_FIELD, r = 0, a = 0; a < n.length; a++) r += l(n[a], i, 0); return r / n.length }, max: function(e, n) { for (var i = e[0] || t.DEFAULT_FIELD, r = Number.NEGATIVE_INFINITY, a = 0; a < n.length; a++) r = Math.max(r, l(n[a], i, Number.NEGATIVE_INFINITY)); return r }, min: function(e, n) { for (var i = e[0] || t.DEFAULT_FIELD, r = Number.POSITIVE_INFINITY, a = 0; a < n.length; a++) r = Math.min(r, l(n[a], i, Number.POSITIVE_INFINITY)); return r }, count: function(e, t) { return t.length } };
    var f = { now: function() { return s.now() } },
        p = function(e) { return e in t.booleanOperators ? t.booleanOperators[e] : e in t.arithmeticOperators ? t.arithmeticOperators[e] : null },
        h = function(e, t) { if (t.hasOwnProperty("value")) return t["value"]; if (t.hasOwnProperty("field")) return l(e, t["field"]); if (t.hasOwnProperty("eval")) return t["eval"] in f ? f[t["eval"]]() : void a.error("Rules", "Unknown function: " + t["eval"]); if (!t["op"]) return void a.error("Rules", "No operator specified: " + u.stringify(t)); var n = p(t["op"]); if (!n) return void a.error("Rules", "Unknown operator: " + t["op"]); var r = i.partial(h, e),
                o = t["args"] || [],
                s = i.map(o, (function(e) { return r(e) })); return n(s, e) },
        g = function(e, t) { var n = {}; if ("undefined" == typeof e || !i.isArray(e) || 0 === e.length) return n["*"] = { fieldValues: {}, events: t }, n; for (var r = i.map(e, (function(e) { return e["field"] })), a = 0; a < t.length; a++) { for (var o = t[a], s = [], c = {}, d = 0; d < r.length; d++) { var f = r[d],
                        p = l(o, f),
                        h = f.join(".");
                    c[h] = p, s.push(encodeURIComponent(h) + "=" + encodeURIComponent(u.stringify(p))) } var g = s.join("&");
                n.hasOwnProperty(g) || (n[g] = { fieldValues: c, events: [] }), n[g].events.push(o) } return n };
    t.generateAlias = function(e, t) { return "_" + e + "_" + t.join(".") };
    var v = function(e, n) { var r = {}; return i.each(n, (function(n, i) { r[i] = {}; for (var o = 0; o < e.length; o++) { var s = e[o],
                        c = s["op"]; if (c in t.aggregateOperators) { var u = (s["args"] && s["args"][0] || {})["field"] || t.DEFAULT_FIELD,
                            l = t.generateAlias(c, u),
                            d = t.aggregateOperators[c]([u], n.events);
                        r[i][l] = d } else a.error("Rules", "Unknown aggregate operator " + c) } })), r },
        m = function(e, t) { var n = []; return i.each(e, (function(e, r) { var a = i.extend({}, e.fieldValues),
                    o = t[r] || {};
                i.extend(a, o), n.push(a) })), n },
        _ = function(e, t) { return i.isArray(e) ? 0 === e.length ? t : t.sort((function(t, n) { for (var i = 0; i < e.length; i++) { var r = e[i],
                        a = r["direction"] || "ASC",
                        o = "ASC" === a ? 1 : -1,
                        s = r["field"],
                        c = l(t, s, 0),
                        u = l(n, s, 0); if (c < u) return -o; if (c > u) return o } return 0 })) : (o("Rules", "groupBy rule must be an array"), t) };
    t.rewrite = function(e) {
        function n(e, s) { if (i.isArray(e) && ("and" !== e[0] && "or" !== e[0] && "not" !== e[0] && a.error("Rules", "Unexpected operation " + e[0] + ". Continuing optimistically."), e = { op: e[0], args: e.slice(1) }), e.hasOwnProperty("field") || e.hasOwnProperty("value") || e.hasOwnProperty("eval")) return e; if (s && e["op"] in t.aggregateOperators) { var c = (e["args"] && e["args"][0] || {})["field"] || t.DEFAULT_FIELD,
                    u = t.generateAlias(e["op"], c); return u in o || (r.push({ op: e["op"], args: e["args"] }), o[u] = !0), { field: [u] } } for (var l = [], d = e["args"] || [], f = 0; f < d.length; f++) l[f] = n(d[f], s); return { op: e["op"], args: l } } var r = [],
            o = {},
            s = {};
        e.hasOwnProperty(t.clause.WHERE) && (s[t.clause.WHERE] = n(e[t.clause.WHERE], !1)), e.hasOwnProperty(t.clause.HAVING) && (s[t.clause.HAVING] = n(e[t.clause.HAVING], !0)), (e.hasOwnProperty(t.clause.AGGREGATE) || r.length > 0) && (s[t.clause.AGGREGATE] = (e[t.clause.AGGREGATE] || []).concat(r)); for (var c = [t.clause.GROUP_BY, t.clause.ORDER_BY, t.clause.SELECT, t.clause.OFFSET, t.clause.LIMIT], u = 0; u < c.length; u++) e.hasOwnProperty(c[u]) && (s[c[u]] = e[c[u]]); return e.hasOwnProperty(t.clause.FROM) && (s[t.clause.FROM] = t.rewrite(e[t.clause.FROM])), s };
    var E = function(e, n) {
            n = n || 0;
            var r = [];
            if (e.hasOwnProperty(t.clause.WHERE) ? e[t.clause.WHERE]["op"] ? e[t.clause.WHERE]["op"] in t.booleanOperators || r.push("Non-boolean WHERE clause operator") : r.push("Missing WHERE clause operator") : r.push("Missing WHERE clause"), e.hasOwnProperty(t.clause.HAVING) && (e[t.clause.HAVING]["op"] ? e[t.clause.HAVING]["op"] in t.booleanOperators || r.push("Non-boolean HAVING clause operator") : r.push("Missing HAVING clause operator")),
                e.hasOwnProperty(t.clause.GROUP_BY) && !e.hasOwnProperty(t.clause.AGGREGATE) && r.push("No AGGREGATE clause specified with GROUP_BY clause"), e.hasOwnProperty(t.clause.SELECT)) { var a = e[t.clause.SELECT]; if (i.isArray(a))
                    for (var o = 0; o < a.length; o++) a[o]["op"] && a[o]["op"] in t.aggregateOperators && r.push('In SELECT clause, aggregate operator "' + a[o]["op"] + '" specified in selector at index ' + o);
                else r.push("SELECT clause must be an array") }
            if (e.hasOwnProperty(t.clause.OFFSET)) { var s = e[t.clause.OFFSET];
                (!i.isNumber(s) || Number(s) < 0 || Number(s) !== Math.floor(Number(s))) && r.push("OFFSET must be a non-negative integer") }
            if (e.hasOwnProperty(t.clause.LIMIT)) { var c = e[t.clause.LIMIT];
                (!i.isNumber(c) || Number(c) < 0 || Number(c) !== Math.floor(Number(c))) && r.push("LIMIT must be a non-negative integer") }
            return n > 0 && (r = i.map(r, (function(e) { return "Sub-rule " + n + ": " + e }))), e.hasOwnProperty(t.clause.FROM) && (r = r.concat(E(e[t.clause.FROM], n + 1))), r
        },
        y = function(e, t) { return i.map(t, (function(t) { return i.map(e, (function(e) { return h(t, e) })) })) },
        I = function(e, n) { var r = n; if (e.hasOwnProperty(t.clause.FROM) && (a.debug("Evaluating FROM clause:", e[t.clause.FROM]), r = I(e[t.clause.FROM], r), a.debug("Results after FROM:", r)), a.debug("Evaluating WHERE clause:", e[t.clause.WHERE]), r = i.filter(r, (function(n) { return h(n, e[t.clause.WHERE]) })), a.debug("Results after WHERE:", r), e.hasOwnProperty(t.clause.AGGREGATE)) { a.debug("Evaluating AGGREGATE clause:", e[t.clause.AGGREGATE]); var o = g(e[t.clause.GROUP_BY], r),
                    s = v(e[t.clause.AGGREGATE], o);
                r = m(o, s), a.debug("Results after AGGREGATE:", r) }
            e.hasOwnProperty(t.clause.HAVING) && (a.debug("Evaluating HAVING clause:", e[t.clause.HAVING]), r = i.filter(r, (function(n) { return h(n, e[t.clause.HAVING]) })), a.debug("Results after HAVING:", r)), e.hasOwnProperty(t.clause.ORDER_BY) && (a.debug("Evaluating ORDER_BY clause:", e[t.clause.ORDER_BY]), r = _(e[t.clause.ORDER_BY], r), a.debug("Results after ORDER_BY:", r)); var c = 0;
            e.hasOwnProperty(t.clause.OFFSET) && (a.debug("Evaluating OFFSET clause:", e[t.clause.OFFSET]), c = Number(e[t.clause.OFFSET])); var u; return e.hasOwnProperty(t.clause.LIMIT) && (a.debug("Evaluating LIMIT clause:", e[t.clause.LIMIT]), u = c + Number(e[t.clause.LIMIT])), (c > 0 || !i.isUndefined(u)) && (r = r.slice(c, u), a.debug("Results after OFFSET/LIMIT:", r)), e.hasOwnProperty(t.clause.SELECT) && (a.debug("Evaluating SELECT clause:", e[t.clause.SELECT]), r = y(e[t.clause.SELECT], r), a.debug("Results after SELECT:", r)), r };
    t.execute = function(e, n) { e = t.rewrite(e), a.shouldLog(r.LogLevel.DEBUG) && a.groupCollapsed("Evaluating Behavioral Rule"), a.debug("Rule:", e, u.stringify(e)), a.debug("Events:", n); var i = E(e); if (i.length > 0) throw new Error("Rule " + u.stringify(e) + " has violations: " + i.join("\n")); var o = I(e, n); return a.debug("Rule result:", o), a.shouldLog(r.LogLevel.DEBUG) && a.groupEnd(), o }, t.isSatisfied = function(e, n) { try { return t.execute(e, n).length > 0 } catch (t) { return a.error("Rules", "Error " + t.toString() + " while evaluating rule " + u.stringify(e)), !1 } }
}), (function(e, t, n) { e.exports = function(e) { e.registerVisitorProfileProvider(n(159)) } }), (function(e, t, n) { var i = n(2),
        r = n(160),
        a = n(154),
        o = n(26),
        s = n(156);
    e.exports = { provides: "customBehavior", shouldTrack: !0, isLazy: !1, getter: ["stores/global", "stores/visitor_attribute_entity", function(e, t) { var n = e.getProjectId(),
                c = i.filter(i.map(t.getCustomBehavioralAttributes(n), (function(e) { try { return { id: e.id, granularity: r.GRANULARITY.ALL, rule: s.buildFromSpecV0_2(o.parse(e.rule_json)) } } catch (e) { return } }))),
                u = a.getEvents(); return r.evaluate(c, u) }] } }), (function(e, t, n) {
    function i(e) { if (0 === e.length) return []; for (var t = e.length - 1, n = o.FIELDS.SESSION_ID, i = e[t][n]; t > 0 && i === e[t - 1][n];) t--; return e.slice(t) }

    function r(e, t) { if (0 === e.length || t <= 0) return []; var n = +new Date - t * s.MILLIS_IN_A_DAY;
        n -= n % s.MILLIS_IN_A_DAY; for (var i = e.length; i > 0 && n <= e[i - 1][o.FIELDS.TIME];) i--; return e.slice(i) } var a = n(23),
        o = { FIELDS: n(64).FIELDS },
        s = n(156),
        c = n(157);
    t.GRANULARITY = { ALL: "all", CURRENT_SESSION: "current_session", LAST_30_DAYS: "last_30_days", LAST_60_DAYS: "last_60_days" }, t.evaluate = function(e, n) { var o = {}; if (0 === n.length) { for (var s = 0; s < e.length; s++) o[e[s].id] = e[s].defaultValue; return o } var u = i(n),
            l = r(n, 60); for (s = 0; s < e.length; s++) { var d = e[s],
                f = n;
            d.granularity === t.GRANULARITY.CURRENT_SESSION ? f = u : d.granularity === t.GRANULARITY.LAST_60_DAYS && (f = l); try { var p = f;
                d.rule && (p = c.execute(d.rule, f)), o[d.id] = d.defaultValue, 1 === p.length ? o[d.id] = p[0][0] || d.defaultValue : a.debug("Behavior / Rule for", d.id, "returned", p.length, "results, expected 1") } catch (e) { a.error("Behavior / Rule for", d.id, "failed with", e.message || "") } } return o } }), (function(e, t, n) { e.exports = function(e) { e.registerVisitorProfileProvider(n(162)), e.registerAudienceMatcher("first_session", n(163)) } }), (function(e, t, n) { var i = n(64),
        r = n(154),
        a = n(19).getFieldValue,
        o = n(89).CURRENT_SESSION_INDEX;
    e.exports = { provides: "first_session", shouldTrack: !0, getter: [function() { var e = r.getEvents(); if (e && e.length > 0) { var t = e[0],
                    n = a(t, [i.FIELDS.SESSION_INDEX]); return n === o } return !0 }] } }), (function(e, t) { e.exports = { fieldsNeeded: ["first_session"], match: function(e) { return !!e.first_session } } }), (function(e, t, n) { e.exports = function(e) { e.registerApiModule("behavior", n(165)) } }), (function(e, t, n) {
    function i(e, t) { var n = d.buildFromSpecV0_1(t); if (1 !== n.length) throw new Error("Invalid query descriptor; verify that no aggregators are specified"); return f.execute(n[0], e) }

    function r(e, t) { return u.map(e, (function(e) { return u.isFunction(e.toObject) ? e.toObject(t) : e })) }

    function a(e, t) { if (!e) return ["Descriptor not defined"]; var n = []; return e.count && n.push('Unexpected "count" clause specified'), e.pick && e.pick.modifier && t.indexOf(e.pick.modifier) === -1 && n.push('Invalid "pick" modifier "' + e.pick.modifier + '"'), u.each(e.filters, (function(e) { u.isUndefined(e.modifier) || n.push('Unexpected "filter" modifier "' + e.modifier + '"') })), n.length > 0 ? n : void 0 }

    function o(e, t) { var n, o = { revenueAsTag: !1, timeAsTimestamp: !0 }; if (u.isUndefined(t)) return n = l.getEvents(e), r(n, o); if (u.isNumber(t)) { if (t <= 0) throw new Error("Count must be a positive integer, got " + t); return n = l.getEvents(e), r(n.slice(-t), o) } var s = a(t, u.values(d.RECENCY_FILTERS)); if (s) throw new Error(s.join("\n")); return n = l.getEvents(e), r(i(n, t), o) }

    function s(e, t) { if (t = u.cloneDeep(t) || {}, !t.pick) throw new Error('No "pick" clause provided in query descriptor'); if (!t.pick.name) throw new Error('No field name provided in "pick" clause');
        t.pick.modifier = t.pick.modifier || d.FREQUENCY_FILTERS.MOST_FREQUENT; var n = a(t, u.values(d.FREQUENCY_FILTERS)); if (n) throw new Error(n.join("\n")); var r = l.getEvents(e); return i(r, t) }

    function c(e, t) { var n = d.buildFromSpecV0_2(t),
            i = l.getEvents(e),
            a = r(f.execute(n, i), { revenueAsTag: !0, timeAsTimestamp: !1 }); return (t.pick || t.reduce && "count" === t.reduce.aggregator) && (a = u.flatten(a)), t.reduce && (a = a[0]), a } var u = n(2),
        l = n(154),
        d = n(156),
        f = n(157);
    e.exports = ["stores/visitor_events", function(e) { return { getEvents: u.partial(o, e), getByFrequency: u.partial(s, e), query: u.partial(c, e) } }] }), (function(e, t, n) { e.exports = function(e) { e.registerDependency("sources/browser_id", n(167)), e.registerVisitorProfileProvider(n(172)), e.registerVisitorProfileProvider(n(173)), e.registerAudienceMatcher("browser_version", n(174)) } }), (function(e, t, n) { var i = n(168);
    t.getId = function() { return i.get().browser.id }, t.getVersion = function() { return i.get().browser.version } }), (function(e, t, n) { var i = n(2),
        r = n(169),
        a = n(41),
        o = n(7),
        s = n(16),
        c = n(9),
        u = s.get("stores/ua_data");
    t.get = function() { var e = u.get(); return i.isEmpty(e) && (e = r.parseUA(a.getUserAgent()), c.dispatch(o.SET_UA_DATA, { data: e })), e } }), (function(e, t, n) {
    function i(e) { if (e = (e || "").toLowerCase(), e in c) return e; var t = a.keys(c); return a.find(t, (function(t) { var n = c[t]; return a.includes(n, e) })) || "unknown" }

    function r(e, t, n) { return t ? t : "unknown" === e ? "unknown" : n ? "mobile" : "desktop_laptop" } var a = n(2),
        o = n(170);
    t.parseUA = function(e) { var t = new o(e),
            n = t.getBrowser(),
            a = t.getOS(),
            c = t.getDevice(),
            l = (a.name || "unknown").toLowerCase(),
            d = (n.name || "unknown").toLowerCase(),
            f = s(c.type, d, l); return { browser: { id: i(n.name), version: n.version }, platform: { name: l, version: a.version }, device: { model: u[c.model] || "unknown", type: r(d, c.type, f), isMobile: f } } }; var s = function(e, t, n) { if (a.includes(["mobile", "tablet"], e)) return !0; if (a.includes(["opera mini"], t)) return !0; var i = ["android", "blackberry", "ios", "windows phone"]; return !!a.includes(i, n) },
        c = { gc: ["chrome", "chromium", "silk", "yandex", "maxthon", "chrome webview"], edge: ["edge"], ie: ["internet explorer", "iemobile"], ff: ["firefox", "iceweasel"], opera: ["opera", "opera mini", "opera tablet"], safari: ["safari", "mobile safari", "webkit"], ucbrowser: ["uc browser"] },
        u = { iPhone: "iphone", iPad: "ipad" } }), (function(e, t, n) { var i;!(function(r, a) { "use strict"; var o = "0.7.17",
            s = "",
            c = "?",
            u = "function",
            l = "undefined",
            d = "object",
            f = "string",
            p = "major",
            h = "model",
            g = "name",
            v = "type",
            m = "vendor",
            _ = "version",
            E = "architecture",
            y = "console",
            I = "mobile",
            T = "tablet",
            S = "smarttv",
            A = "wearable",
            b = "embedded",
            w = { extend: function(e, t) { var n = {}; for (var i in e) t[i] && t[i].length % 2 === 0 ? n[i] = t[i].concat(e[i]) : n[i] = e[i]; return n }, has: function(e, t) { return "string" == typeof e && t.toLowerCase().indexOf(e.toLowerCase()) !== -1 }, lowerize: function(e) { return e.toLowerCase() }, major: function(e) { return typeof e === f ? e.replace(/[^\d\.]/g, "").split(".")[0] : a }, trim: function(e) { return e.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "") } },
            D = { rgx: function(e, t) { for (var n, i, r, o, s, c, l = 0; l < t.length && !s;) { var f = t[l],
                            p = t[l + 1]; for (n = i = 0; n < f.length && !s;)
                            if (s = f[n++].exec(e))
                                for (r = 0; r < p.length; r++) c = s[++i], o = p[r], typeof o === d && o.length > 0 ? 2 == o.length ? typeof o[1] == u ? this[o[0]] = o[1].call(this, c) : this[o[0]] = o[1] : 3 == o.length ? typeof o[1] !== u || o[1].exec && o[1].test ? this[o[0]] = c ? c.replace(o[1], o[2]) : a : this[o[0]] = c ? o[1].call(this, c, o[2]) : a : 4 == o.length && (this[o[0]] = c ? o[3].call(this, c.replace(o[1], o[2])) : a) : this[o] = c ? c : a;
                        l += 2 } }, str: function(e, t) { for (var n in t)
                        if (typeof t[n] === d && t[n].length > 0) { for (var i = 0; i < t[n].length; i++)
                                if (w.has(t[n][i], e)) return n === c ? a : n } else if (w.has(t[n], e)) return n === c ? a : n; return e } },
            R = { browser: { oldsafari: { version: { "1.0": "/8", 1.2: "/1", 1.3: "/3", "2.0": "/412", "2.0.2": "/416", "2.0.3": "/417", "2.0.4": "/419", "?": "/" } } }, device: { amazon: { model: { "Fire Phone": ["SD", "KF"] } }, sprint: { model: { "Evo Shift 4G": "7373KT" }, vendor: { HTC: "APA", Sprint: "Sprint" } } }, os: { windows: { version: { ME: "4.90", "NT 3.11": "NT3.51", "NT 4.0": "NT4.0", 2000: "NT 5.0", XP: ["NT 5.1", "NT 5.2"], Vista: "NT 6.0", 7: "NT 6.1", 8: "NT 6.2", 8.1: "NT 6.3", 10: ["NT 6.4", "NT 10.0"], RT: "ARM" } } } },
            N = { browser: [
                    [/(opera\smini)\/([\w\.-]+)/i, /(opera\s[mobiletab]+).+version\/([\w\.-]+)/i, /(opera).+version\/([\w\.]+)/i, /(opera)[\/\s]+([\w\.]+)/i],
                    [g, _],
                    [/(opios)[\/\s]+([\w\.]+)/i],
                    [
                        [g, "Opera Mini"], _
                    ],
                    [/\s(opr)\/([\w\.]+)/i],
                    [
                        [g, "Opera"], _
                    ],
                    [/(kindle)\/([\w\.]+)/i, /(lunascape|maxthon|netfront|jasmine|blazer)[\/\s]?([\w\.]+)*/i, /(avant\s|iemobile|slim|baidu)(?:browser)?[\/\s]?([\w\.]*)/i, /(?:ms|\()(ie)\s([\w\.]+)/i, /(rekonq)\/([\w\.]+)*/i, /(chromium|flock|rockmelt|midori|epiphany|silk|skyfire|ovibrowser|bolt|iron|vivaldi|iridium|phantomjs|bowser|quark)\/([\w\.-]+)/i],
                    [g, _],
                    [/(trident).+rv[:\s]([\w\.]+).+like\sgecko/i],
                    [
                        [g, "IE"], _
                    ],
                    [/(edge)\/((\d+)?[\w\.]+)/i],
                    [g, _],
                    [/(yabrowser)\/([\w\.]+)/i],
                    [
                        [g, "Yandex"], _
                    ],
                    [/(puffin)\/([\w\.]+)/i],
                    [
                        [g, "Puffin"], _
                    ],
                    [/((?:[\s\/])uc?\s?browser|(?:juc.+)ucweb)[\/\s]?([\w\.]+)/i],
                    [
                        [g, "UCBrowser"], _
                    ],
                    [/(comodo_dragon)\/([\w\.]+)/i],
                    [
                        [g, /_/g, " "], _
                    ],
                    [/(micromessenger)\/([\w\.]+)/i],
                    [
                        [g, "WeChat"], _
                    ],
                    [/(QQ)\/([\d\.]+)/i],
                    [g, _],
                    [/m?(qqbrowser)[\/\s]?([\w\.]+)/i],
                    [g, _],
                    [/xiaomi\/miuibrowser\/([\w\.]+)/i],
                    [_, [g, "MIUI Browser"]],
                    [/;fbav\/([\w\.]+);/i],
                    [_, [g, "Facebook"]],
                    [/headlesschrome(?:\/([\w\.]+)|\s)/i],
                    [_, [g, "Chrome Headless"]],
                    [/\swv\).+(chrome)\/([\w\.]+)/i],
                    [
                        [g, /(.+)/, "$1 WebView"], _
                    ],
                    [/((?:oculus|samsung)browser)\/([\w\.]+)/i],
                    [
                        [g, /(.+(?:g|us))(.+)/, "$1 $2"], _
                    ],
                    [/android.+version\/([\w\.]+)\s+(?:mobile\s?safari|safari)*/i],
                    [_, [g, "Android Browser"]],
                    [/(chrome|omniweb|arora|[tizenoka]{5}\s?browser)\/v?([\w\.]+)/i],
                    [g, _],
                    [/(dolfin)\/([\w\.]+)/i],
                    [
                        [g, "Dolphin"], _
                    ],
                    [/((?:android.+)crmo|crios)\/([\w\.]+)/i],
                    [
                        [g, "Chrome"], _
                    ],
                    [/(coast)\/([\w\.]+)/i],
                    [
                        [g, "Opera Coast"], _
                    ],
                    [/fxios\/([\w\.-]+)/i],
                    [_, [g, "Firefox"]],
                    [/version\/([\w\.]+).+?mobile\/\w+\s(safari)/i],
                    [_, [g, "Mobile Safari"]],
                    [/version\/([\w\.]+).+?(mobile\s?safari|safari)/i],
                    [_, g],
                    [/webkit.+?(gsa)\/([\w\.]+).+?(mobile\s?safari|safari)(\/[\w\.]+)/i],
                    [
                        [g, "GSA"], _
                    ],
                    [/webkit.+?(mobile\s?safari|safari)(\/[\w\.]+)/i],
                    [g, [_, D.str, R.browser.oldsafari.version]],
                    [/(konqueror)\/([\w\.]+)/i, /(webkit|khtml)\/([\w\.]+)/i],
                    [g, _],
                    [/(navigator|netscape)\/([\w\.-]+)/i],
                    [
                        [g, "Netscape"], _
                    ],
                    [/(swiftfox)/i, /(icedragon|iceweasel|camino|chimera|fennec|maemo\sbrowser|minimo|conkeror)[\/\s]?([\w\.\+]+)/i, /(firefox|seamonkey|k-meleon|icecat|iceape|firebird|phoenix|palemoon|basilisk|waterfox)\/([\w\.-]+)$/i, /(mozilla)\/([\w\.]+).+rv\:.+gecko\/\d+/i, /(polaris|lynx|dillo|icab|doris|amaya|w3m|netsurf|sleipnir)[\/\s]?([\w\.]+)/i, /(links)\s\(([\w\.]+)/i, /(gobrowser)\/?([\w\.]+)*/i, /(ice\s?browser)\/v?([\w\._]+)/i, /(mosaic)[\/\s]([\w\.]+)/i],
                    [g, _]
                ], cpu: [
                    [/(?:(amd|x(?:(?:86|64)[_-])?|wow|win)64)[;\)]/i],
                    [
                        [E, "amd64"]
                    ],
                    [/(ia32(?=;))/i],
                    [
                        [E, w.lowerize]
                    ],
                    [/((?:i[346]|x)86)[;\)]/i],
                    [
                        [E, "ia32"]
                    ],
                    [/windows\s(ce|mobile);\sppc;/i],
                    [
                        [E, "arm"]
                    ],
                    [/((?:ppc|powerpc)(?:64)?)(?:\smac|;|\))/i],
                    [
                        [E, /ower/, "", w.lowerize]
                    ],
                    [/(sun4\w)[;\)]/i],
                    [
                        [E, "sparc"]
                    ],
                    [/((?:avr32|ia64(?=;))|68k(?=\))|arm(?:64|(?=v\d+;))|(?=atmel\s)avr|(?:irix|mips|sparc)(?:64)?(?=;)|pa-risc)/i],
                    [
                        [E, w.lowerize]
                    ]
                ], device: [
                    [/\((ipad|playbook);[\w\s\);-]+(rim|apple)/i],
                    [h, m, [v, T]],
                    [/applecoremedia\/[\w\.]+ \((ipad)/],
                    [h, [m, "Apple"],
                        [v, T]
                    ],
                    [/(apple\s{0,1}tv)/i],
                    [
                        [h, "Apple TV"],
                        [m, "Apple"]
                    ],
                    [/(archos)\s(gamepad2?)/i, /(hp).+(touchpad)/i, /(hp).+(tablet)/i, /(kindle)\/([\w\.]+)/i, /\s(nook)[\w\s]+build\/(\w+)/i, /(dell)\s(strea[kpr\s\d]*[\dko])/i],
                    [m, h, [v, T]],
                    [/(kf[A-z]+)\sbuild\/[\w\.]+.*silk\//i],
                    [h, [m, "Amazon"],
                        [v, T]
                    ],
                    [/(sd|kf)[0349hijorstuw]+\sbuild\/[\w\.]+.*silk\//i],
                    [
                        [h, D.str, R.device.amazon.model],
                        [m, "Amazon"],
                        [v, I]
                    ],
                    [/\((ip[honed|\s\w*]+);.+(apple)/i],
                    [h, m, [v, I]],
                    [/\((ip[honed|\s\w*]+);/i],
                    [h, [m, "Apple"],
                        [v, I]
                    ],
                    [/(blackberry)[\s-]?(\w+)/i, /(blackberry|benq|palm(?=\-)|sonyericsson|acer|asus|dell|meizu|motorola|polytron)[\s_-]?([\w-]+)*/i, /(hp)\s([\w\s]+\w)/i, /(asus)-?(\w+)/i],
                    [m, h, [v, I]],
                    [/\(bb10;\s(\w+)/i],
                    [h, [m, "BlackBerry"],
                        [v, I]
                    ],
                    [/android.+(transfo[prime\s]{4,10}\s\w+|eeepc|slider\s\w+|nexus 7|padfone)/i],
                    [h, [m, "Asus"],
                        [v, T]
                    ],
                    [/(sony)\s(tablet\s[ps])\sbuild\//i, /(sony)?(?:sgp.+)\sbuild\//i],
                    [
                        [m, "Sony"],
                        [h, "Xperia Tablet"],
                        [v, T]
                    ],
                    [/android.+\s([c-g]\d{4}|so[-l]\w+)\sbuild\//i],
                    [h, [m, "Sony"],
                        [v, I]
                    ],
                    [/\s(ouya)\s/i, /(nintendo)\s([wids3u]+)/i],
                    [m, h, [v, y]],
                    [/android.+;\s(shield)\sbuild/i],
                    [h, [m, "Nvidia"],
                        [v, y]
                    ],
                    [/(playstation\s[34portablevi]+)/i],
                    [h, [m, "Sony"],
                        [v, y]
                    ],
                    [/(sprint\s(\w+))/i],
                    [
                        [m, D.str, R.device.sprint.vendor],
                        [h, D.str, R.device.sprint.model],
                        [v, I]
                    ],
                    [/(lenovo)\s?(S(?:5000|6000)+(?:[-][\w+]))/i],
                    [m, h, [v, T]],
                    [/(htc)[;_\s-]+([\w\s]+(?=\))|\w+)*/i, /(zte)-(\w+)*/i, /(alcatel|geeksphone|lenovo|nexian|panasonic|(?=;\s)sony)[_\s-]?([\w-]+)*/i],
                    [m, [h, /_/g, " "],
                        [v, I]
                    ],
                    [/(nexus\s9)/i],
                    [h, [m, "HTC"],
                        [v, T]
                    ],
                    [/d\/huawei([\w\s-]+)[;\)]/i, /(nexus\s6p)/i],
                    [h, [m, "Huawei"],
                        [v, I]
                    ],
                    [/(microsoft);\s(lumia[\s\w]+)/i],
                    [m, h, [v, I]],
                    [/[\s\(;](xbox(?:\sone)?)[\s\);]/i],
                    [h, [m, "Microsoft"],
                        [v, y]
                    ],
                    [/(kin\.[onetw]{3})/i],
                    [
                        [h, /\./g, " "],
                        [m, "Microsoft"],
                        [v, I]
                    ],
                    [/\s(milestone|droid(?:[2-4x]|\s(?:bionic|x2|pro|razr))?(:?\s4g)?)[\w\s]+build\//i, /mot[\s-]?(\w+)*/i, /(XT\d{3,4}) build\//i, /(nexus\s6)/i],
                    [h, [m, "Motorola"],
                        [v, I]
                    ],
                    [/android.+\s(mz60\d|xoom[\s2]{0,2})\sbuild\//i],
                    [h, [m, "Motorola"],
                        [v, T]
                    ],
                    [/hbbtv\/\d+\.\d+\.\d+\s+\([\w\s]*;\s*(\w[^;]*);([^;]*)/i],
                    [
                        [m, w.trim],
                        [h, w.trim],
                        [v, S]
                    ],
                    [/hbbtv.+maple;(\d+)/i],
                    [
                        [h, /^/, "SmartTV"],
                        [m, "Samsung"],
                        [v, S]
                    ],
                    [/\(dtv[\);].+(aquos)/i],
                    [h, [m, "Sharp"],
                        [v, S]
                    ],
                    [/android.+((sch-i[89]0\d|shw-m380s|gt-p\d{4}|gt-n\d+|sgh-t8[56]9|nexus 10))/i, /((SM-T\w+))/i],
                    [
                        [m, "Samsung"], h, [v, T]
                    ],
                    [/smart-tv.+(samsung)/i],
                    [m, [v, S], h],
                    [/((s[cgp]h-\w+|gt-\w+|galaxy\snexus|sm-\w[\w\d]+))/i, /(sam[sung]*)[\s-]*(\w+-?[\w-]*)*/i, /sec-((sgh\w+))/i],
                    [
                        [m, "Samsung"], h, [v, I]
                    ],
                    [/sie-(\w+)*/i],
                    [h, [m, "Siemens"],
                        [v, I]
                    ],
                    [/(maemo|nokia).*(n900|lumia\s\d+)/i, /(nokia)[\s_-]?([\w-]+)*/i],
                    [
                        [m, "Nokia"], h, [v, I]
                    ],
                    [/android\s3\.[\s\w;-]{10}(a\d{3})/i],
                    [h, [m, "Acer"],
                        [v, T]
                    ],
                    [/android.+([vl]k\-?\d{3})\s+build/i],
                    [h, [m, "LG"],
                        [v, T]
                    ],
                    [/android\s3\.[\s\w;-]{10}(lg?)-([06cv9]{3,4})/i],
                    [
                        [m, "LG"], h, [v, T]
                    ],
                    [/(lg) netcast\.tv/i],
                    [m, h, [v, S]],
                    [/(nexus\s[45])/i, /lg[e;\s\/-]+(\w+)*/i, /android.+lg(\-?[\d\w]+)\s+build/i],
                    [h, [m, "LG"],
                        [v, I]
                    ],
                    [/android.+(ideatab[a-z0-9\-\s]+)/i],
                    [h, [m, "Lenovo"],
                        [v, T]
                    ],
                    [/linux;.+((jolla));/i],
                    [m, h, [v, I]],
                    [/((pebble))app\/[\d\.]+\s/i],
                    [m, h, [v, A]],
                    [/android.+;\s(oppo)\s?([\w\s]+)\sbuild/i],
                    [m, h, [v, I]],
                    [/crkey/i],
                    [
                        [h, "Chromecast"],
                        [m, "Google"]
                    ],
                    [/android.+;\s(glass)\s\d/i],
                    [h, [m, "Google"],
                        [v, A]
                    ],
                    [/android.+;\s(pixel c)\s/i],
                    [h, [m, "Google"],
                        [v, T]
                    ],
                    [/android.+;\s(pixel xl|pixel)\s/i],
                    [h, [m, "Google"],
                        [v, I]
                    ],
                    [/android.+(\w+)\s+build\/hm\1/i, /android.+(hm[\s\-_]*note?[\s_]*(?:\d\w)?)\s+build/i, /android.+(mi[\s\-_]*(?:one|one[\s_]plus|note lte)?[\s_]*(?:\d\w?)?[\s_]*(?:plus)?)\s+build/i, /android.+(redmi[\s\-_]*(?:note)?(?:[\s_]*[\w\s]+)?)\s+build/i],
                    [
                        [h, /_/g, " "],
                        [m, "Xiaomi"],
                        [v, I]
                    ],
                    [/android.+(mi[\s\-_]*(?:pad)(?:[\s_]*[\w\s]+)?)\s+build/i],
                    [
                        [h, /_/g, " "],
                        [m, "Xiaomi"],
                        [v, T]
                    ],
                    [/android.+;\s(m[1-5]\snote)\sbuild/i],
                    [h, [m, "Meizu"],
                        [v, T]
                    ],
                    [/android.+a000(1)\s+build/i, /android.+oneplus\s(a\d{4})\s+build/i],
                    [h, [m, "OnePlus"],
                        [v, I]
                    ],
                    [/android.+[;\/]\s*(RCT[\d\w]+)\s+build/i],
                    [h, [m, "RCA"],
                        [v, T]
                    ],
                    [/android.+[;\/]\s*(Venue[\d\s]*)\s+build/i],
                    [h, [m, "Dell"],
                        [v, T]
                    ],
                    [/android.+[;\/]\s*(Q[T|M][\d\w]+)\s+build/i],
                    [h, [m, "Verizon"],
                        [v, T]
                    ],
                    [/android.+[;\/]\s+(Barnes[&\s]+Noble\s+|BN[RT])(V?.*)\s+build/i],
                    [
                        [m, "Barnes & Noble"], h, [v, T]
                    ],
                    [/android.+[;\/]\s+(TM\d{3}.*\b)\s+build/i],
                    [h, [m, "NuVision"],
                        [v, T]
                    ],
                    [/android.+[;\/]\s*(zte)?.+(k\d{2})\s+build/i],
                    [
                        [m, "ZTE"], h, [v, T]
                    ],
                    [/android.+[;\/]\s*(gen\d{3})\s+build.*49h/i],
                    [h, [m, "Swiss"],
                        [v, I]
                    ],
                    [/android.+[;\/]\s*(zur\d{3})\s+build/i],
                    [h, [m, "Swiss"],
                        [v, T]
                    ],
                    [/android.+[;\/]\s*((Zeki)?TB.*\b)\s+build/i],
                    [h, [m, "Zeki"],
                        [v, T]
                    ],
                    [/(android).+[;\/]\s+([YR]\d{2}x?.*)\s+build/i, /android.+[;\/]\s+(Dragon[\-\s]+Touch\s+|DT)(.+)\s+build/i],
                    [
                        [m, "Dragon Touch"], h, [v, T]
                    ],
                    [/android.+[;\/]\s*(NS-?.+)\s+build/i],
                    [h, [m, "Insignia"],
                        [v, T]
                    ],
                    [/android.+[;\/]\s*((NX|Next)-?.+)\s+build/i],
                    [h, [m, "NextBook"],
                        [v, T]
                    ],
                    [/android.+[;\/]\s*(Xtreme\_?)?(V(1[045]|2[015]|30|40|60|7[05]|90))\s+build/i],
                    [
                        [m, "Voice"], h, [v, I]
                    ],
                    [/android.+[;\/]\s*(LVTEL\-?)?(V1[12])\s+build/i],
                    [
                        [m, "LvTel"], h, [v, I]
                    ],
                    [/android.+[;\/]\s*(V(100MD|700NA|7011|917G).*\b)\s+build/i],
                    [h, [m, "Envizen"],
                        [v, T]
                    ],
                    [/android.+[;\/]\s*(Le[\s\-]+Pan)[\s\-]+(.*\b)\s+build/i],
                    [m, h, [v, T]],
                    [/android.+[;\/]\s*(Trio[\s\-]*.*)\s+build/i],
                    [h, [m, "MachSpeed"],
                        [v, T]
                    ],
                    [/android.+[;\/]\s*(Trinity)[\-\s]*(T\d{3})\s+build/i],
                    [m, h, [v, T]],
                    [/android.+[;\/]\s*TU_(1491)\s+build/i],
                    [h, [m, "Rotor"],
                        [v, T]
                    ],
                    [/android.+(KS(.+))\s+build/i],
                    [h, [m, "Amazon"],
                        [v, T]
                    ],
                    [/android.+(Gigaset)[\s\-]+(Q.+)\s+build/i],
                    [m, h, [v, T]],
                    [/\s(tablet|tab)[;\/]/i, /\s(mobile)(?:[;\/]|\ssafari)/i],
                    [
                        [v, w.lowerize], m, h
                    ],
                    [/(android.+)[;\/].+build/i],
                    [h, [m, "Generic"]]
                ], engine: [
                    [/windows.+\sedge\/([\w\.]+)/i],
                    [_, [g, "EdgeHTML"]],
                    [/(presto)\/([\w\.]+)/i, /(webkit|trident|netfront|netsurf|amaya|lynx|w3m)\/([\w\.]+)/i, /(khtml|tasman|links)[\/\s]\(?([\w\.]+)/i, /(icab)[\/\s]([23]\.[\d\.]+)/i],
                    [g, _],
                    [/rv\:([\w\.]+).*(gecko)/i],
                    [_, g]
                ], os: [
                    [/microsoft\s(windows)\s(vista|xp)/i],
                    [g, _],
                    [/(windows)\snt\s6\.2;\s(arm)/i, /(windows\sphone(?:\sos)*)[\s\/]?([\d\.\s]+\w)*/i, /(windows\smobile|windows)[\s\/]?([ntce\d\.\s]+\w)/i],
                    [g, [_, D.str, R.os.windows.version]],
                    [/(win(?=3|9|n)|win\s9x\s)([nt\d\.]+)/i],
                    [
                        [g, "Windows"],
                        [_, D.str, R.os.windows.version]
                    ],
                    [/\((bb)(10);/i],
                    [
                        [g, "BlackBerry"], _
                    ],
                    [/(blackberry)\w*\/?([\w\.]+)*/i, /(tizen)[\/\s]([\w\.]+)/i, /(android|webos|palm\sos|qnx|bada|rim\stablet\sos|meego|contiki)[\/\s-]?([\w\.]+)*/i, /linux;.+(sailfish);/i],
                    [g, _],
                    [/(symbian\s?os|symbos|s60(?=;))[\/\s-]?([\w\.]+)*/i],
                    [
                        [g, "Symbian"], _
                    ],
                    [/\((series40);/i],
                    [g],
                    [/mozilla.+\(mobile;.+gecko.+firefox/i],
                    [
                        [g, "Firefox OS"], _
                    ],
                    [/(nintendo|playstation)\s([wids34portablevu]+)/i, /(mint)[\/\s\(]?(\w+)*/i, /(mageia|vectorlinux)[;\s]/i, /(joli|[kxln]?ubuntu|debian|[open]*suse|gentoo|(?=\s)arch|slackware|fedora|mandriva|centos|pclinuxos|redhat|zenwalk|linpus)[\/\s-]?(?!chrom)([\w\.-]+)*/i, /(hurd|linux)\s?([\w\.]+)*/i, /(gnu)\s?([\w\.]+)*/i],
                    [g, _],
                    [/(cros)\s[\w]+\s([\w\.]+\w)/i],
                    [
                        [g, "Chromium OS"], _
                    ],
                    [/(sunos)\s?([\w\.]+\d)*/i],
                    [
                        [g, "Solaris"], _
                    ],
                    [/\s([frentopc-]{0,4}bsd|dragonfly)\s?([\w\.]+)*/i],
                    [g, _],
                    [/(haiku)\s(\w+)/i],
                    [g, _],
                    [/cfnetwork\/.+darwin/i, /ip[honead]+(?:.*os\s([\w]+)\slike\smac|;\sopera)/i],
                    [
                        [_, /_/g, "."],
                        [g, "iOS"]
                    ],
                    [/(mac\sos\sx)\s?([\w\s\.]+\w)*/i, /(macintosh|mac(?=_powerpc)\s)/i],
                    [
                        [g, "Mac OS"],
                        [_, /_/g, "."]
                    ],
                    [/((?:open)?solaris)[\/\s-]?([\w\.]+)*/i, /(aix)\s((\d)(?=\.|\)|\s)[\w\.]*)*/i, /(plan\s9|minix|beos|os\/2|amigaos|morphos|risc\sos|openvms)/i, /(unix)\s?([\w\.]+)*/i],
                    [g, _]
                ] },
            C = function(e, t) { if ("object" == typeof e && (t = e, e = a), !(this instanceof C)) return new C(e, t).getResult(); var n = e || (r && r.navigator && r.navigator.userAgent ? r.navigator.userAgent : s),
                    i = t ? w.extend(N, t) : N; return this.getBrowser = function() { var e = { name: a, version: a }; return D.rgx.call(e, n, i.browser), e.major = w.major(e.version), e }, this.getCPU = function() { var e = { architecture: a }; return D.rgx.call(e, n, i.cpu), e }, this.getDevice = function() { var e = { vendor: a, model: a, type: a }; return D.rgx.call(e, n, i.device), e }, this.getEngine = function() { var e = { name: a, version: a }; return D.rgx.call(e, n, i.engine), e }, this.getOS = function() { var e = { name: a, version: a }; return D.rgx.call(e, n, i.os), e }, this.getResult = function() { return { ua: this.getUA(), browser: this.getBrowser(), engine: this.getEngine(), os: this.getOS(), device: this.getDevice(), cpu: this.getCPU() } }, this.getUA = function() { return n }, this.setUA = function(e) { return n = e, this }, this };
        C.VERSION = o, C.BROWSER = { NAME: g, MAJOR: p, VERSION: _ }, C.CPU = { ARCHITECTURE: E }, C.DEVICE = { MODEL: h, VENDOR: m, TYPE: v, CONSOLE: y, MOBILE: I, SMARTTV: S, TABLET: T, WEARABLE: A, EMBEDDED: b }, C.ENGINE = { NAME: g, VERSION: _ }, C.OS = { NAME: g, VERSION: _ }, typeof t !== l ? (typeof e !== l && e.exports && (t = e.exports = C), t.UAParser = C) : "function" === u && n(171) ? (i = function() { return C }.call(t, n, t, e), !(i !== a && (e.exports = i))) : r && (r.UAParser = C) })("object" == typeof window ? window : this) }), (function(e, t) {
    (function(t) { e.exports = t }).call(t, {}) }), (function(e, t) { e.exports = { provides: "browserId", shouldTrack: !0, isSticky: !0, getter: ["sources/browser_id", function(e) { return e.getId() }] } }), (function(e, t) { e.exports = { provides: "browserVersion", getter: ["sources/browser_id", function(e) { return e.getVersion() }] } }), (function(e, t, n) { var i = n(175).compareVersion;
    e.exports = { fieldsNeeded: ["browserVersion", "browserId"], match: function(e, t) { var n = t.value,
                r = e.browserId,
                a = e.browserVersion; if (0 === n.indexOf(r)) { var o = n.substr(r.length); return 0 === i(a, o) } return !1 } } }), (function(e, t, n) { var i = n(2);
    t.compareVersion = function(e, t) { if (!t) return 0; for (var n = t.toString().split("."), r = e.toString().split("."), a = 0; a < n.length; a++) { if (i.isUndefined(r[a])) return -1; if (isNaN(Number(r[a]))) { if (r[a] !== n[a]) return -1 } else { if (Number(r[a]) < Number(n[a])) return -1; if (Number(r[a]) > Number(n[a])) return 1 } } return 0 } }), (function(e, t, n) { e.exports = function(e) { e.registerVisitorProfileProvider(n(177)), e.registerAudienceMatcher("campaign", n(178)) } }), (function(e, t, n) { var i = n(119);
    e.exports = { provides: "campaign", shouldTrack: !0, isSticky: !0, getter: [function() { return i.getQueryParamValue("utm_campaign") }] } }), (function(e, t, n) { var i = n(20);
    e.exports = { fieldsNeeded: ["campaign"], match: function(e, t) { return i.hasMatch(t.value, t.match, e.campaign) } } }), (function(e, t, n) { e.exports = function(e) { e.registerAudienceMatcher("code", n(180)) } }), (function(e, t, n) { var i = n(2),
        r = n(146);
    t.fieldsNeeded = [], t.match = function(e, t) { if (i.isUndefined(t.value)) return !0; if ("function" == typeof t.value) try { return Boolean(r.apply(t.value)) } catch (e) { return !1 } else try { return Boolean(r.eval(t.value)) } catch (e) { return !1 }
        return !1 } }), (function(e, t, n) { e.exports = function(e) { e.registerVisitorProfileProvider(n(182)), e.registerAudienceMatcher("cookies", n(183)) } }), (function(e, t, n) { var i = n(76);
    e.exports = { provides: "cookies", isLazy: !0, getter: [function() { return i.getAll() }] } }), (function(e, t, n) { var i = n(20);
    e.exports = { fieldsNeeded: ["cookies"], match: function(e, t) { var n = t.name,
                r = t.value,
                a = t.match,
                o = e.cookies[n]; return i.hasMatch(r, a, o) } } }), (function(e, t, n) { e.exports = function(e) { e.registerVisitorProfileProvider(n(185)); var t = n(186);
        e.registerAudienceMatcher("custom_attribute", t), e.registerAudienceMatcher("custom_dimension", t) } }), (function(e, t, n) { var i = n(2),
        r = n(25),
        a = n(23),
        o = n(16),
        s = o.get("stores/dimension_data");
    e.exports = { provides: "custom", attributionType: r.AttributionTypes.LAST_TOUCH, restorer: function(e) { return i.reduce(e, (function(e, t, n) { var r = t,
                    o = n,
                    c = s.getByApiName(n),
                    u = s.getById(n); return i.isObject(t) && !t.id && (c && !u ? (o = c.id, r = { id: c.segmentId || c.id, value: t.value }) : u || a.warn("Unable to determine ID for custom attribute:", n, "; segmentation is disabled.")), e[o] = r, e }), {}) }, shouldTrack: !0 } }), (function(e, t, n) { var i = n(2),
        r = n(20);
    t.match = function(e, t) { var n; return e.custom && (n = e.custom[t.name]), i.isObject(n) && (n = n.value), r.hasMatch(t.value, t.match, n) } }), (function(e, t, n) { e.exports = function(e) { e.registerDependency("sources/device", n(188)), e.registerVisitorProfileProvider(n(189)), e.registerAudienceMatcher("device", n(190)) } }), (function(e, t, n) { var i = n(168);
    t.getDevice = function() { var e = i.get().device; return "unknown" !== e.model ? e.model : "tablet" === e.type ? "tablet" : e.isMobile ? "mobile" : "desktop" } }), (function(e, t) { e.exports = { provides: "device", shouldTrack: !0, isSticky: !0, getter: ["sources/device", function(e) { return e.getDevice() }] } }), (function(e, t) { e.exports = { fieldsNeeded: ["device"], match: function(e, t) { return e.device === t.value } } }), (function(e, t, n) { e.exports = function(e) { e.registerVisitorProfileProvider(n(192)), e.registerAudienceMatcher("device_type", n(193)) } }), (function(e, t, n) { var i = n(168);
    e.exports = { provides: "device_type", shouldTrack: !0, isSticky: !0, getter: [function() { var e = i.get().device; switch (e.type) {
                case "mobile":
                    return "phone";
                case "tablet":
                case "desktop_laptop":
                    return e.type;
                default:
                    return "other" } }] } }), (function(e, t) { e.exports = { fieldsNeeded: ["device_type"], match: function(e, t) { return e.device_type === t.value } } }), (function(e, t, n) { e.exports = function(e) { e.registerVisitorProfileProvider(n(195)), e.registerAudienceMatcher("location", n(197)) } }), (function(e, t, n) { var i = n(196);
    e.exports = { provides: "location", isAsync: !0, getter: [function() { return i.getIPDerivedGeolocation() }] } }), (function(e, t, n) {
    function i() { a.addScriptAsync(s) } var r = n(6),
        a = n(134),
        o = "cdn3";
    t.getIP = function() { return r.makeAsyncRequest(o, i).then((function(e) { return e.ip })) }, t.getIPDerivedGeolocation = function() { return r.makeAsyncRequest(o, i).then((function(e) { return e.location })) }; var s = "https://cdn-pci.optimizely.com/js/geo2.js" }), (function(e, t) { t.fieldsNeeded = ["location"], t.match = function(e, t) { if (!e.hasOwnProperty("location")) return !1; var n = e.location,
            i = t.value,
            r = i.split("|"),
            a = (r[0] || "").trim(),
            o = (r[1] || "").trim(),
            s = (r[2] || "").trim(),
            c = (r[3] || "").trim(); switch (r.length) {
            case 1:
                if (n.country === a) return !0; break;
            case 2:
                if (n.region === o && n.country === a) return !0; break;
            case 3:
                if (n.city === s && (n.region === o || "" === o) && n.country === a) return !0; break;
            case 4:
                if (n.continent === c) return !0 } return !1 } }), (function(e, t, n) { e.exports = function(e) { e.registerVisitorProfileProvider(n(199)), e.registerAudienceMatcher("query", n(200)) } }), (function(e, t, n) { var i = n(119);
    e.exports = { provides: "queryParams", getter: [function() { return i.getQueryParams() }] } }), (function(e, t, n) { var i = n(2),
        r = n(20);
    t.fieldsNeeded = ["queryParams"], t.match = function(e, t) { var n = i.find(e.queryParams, (function(e) { return e[0] === t.name })); return r.hasMatch(t.value, t.match, n ? n[1] : null) } }), (function(e, t, n) { e.exports = function(e) { e.registerVisitorProfileProvider(n(202)), e.registerAudienceMatcher("referrer", n(203)) } }), (function(e, t, n) { var i = n(81),
        r = n(99);
    e.exports = { provides: "referrer", shouldTrack: !0, isSticky: !0, getter: [function() { var e = r.getReferrer() || i.getReferrer(); return "" === e && (e = null), e }] } }), (function(e, t, n) { var i = n(204);
    t.fieldsNeeded = ["referrer"], t.match = function(e, t) { return null !== e.referrer && i(e.referrer, t) } }), (function(e, t, n) {
    function i(e) { var t = e.indexOf("?"); return t !== -1 && (e = e.substring(0, t)), t = e.indexOf("#"), t !== -1 && (e = e.substring(0, t)), e }

    function r(e) { return a(i(e)) }

    function a(e, t) { e = e.replace("/?", "?"), e = e.toLowerCase().replace(/[\/&?]+$/, ""); var n = l.slice(0);
        t || (n = n.concat(c)); for (var i = n.length, r = 0; r < i; r++) { var a = n[r],
                o = new RegExp("^" + a);
            e = e.replace(o, "") } return e }

    function o(e) { var t = e.split("?"); if (t[1]) { var n = t[1].split("#"),
                i = n[0],
                r = n[1],
                a = i.split("&"),
                o = []; return s.each(a, (function(e) { 0 !== e.indexOf(u) && o.push(e) })), t[1] = "", o.length > 0 && (t[1] = "?" + o.join("&")), r && (t[1] += "#" + r), t.join("") } return e } var s = n(2);
    e.exports = function(e, t) { e = o(e); var n = t.value; switch (t.match) {
            case "exact":
                return e = a(e), e === a(n);
            case "regex":
                try { return Boolean(e.match(n)) } catch (e) {} return !1;
            case "simple":
                return e = r(e), n = r(n), e === n;
            case "substring":
                return e = a(e, !0), n = a(n, !0), e.indexOf(n) !== -1;
            default:
                return !1 } }; var c = ["www."],
        u = "optimizely_",
        l = ["https?://.*?.?optimizelyedit.(com|test)/", "https?://.*.?optimizelypreview.(com|test)/", "https?://(edit|preview)(-hrd|-devel)?.optimizely.(com|test)/", "https?://.*?.?optimizelyedit(-hrd)?.appspot.com/", "https?://"] }), (function(e, t, n) { e.exports = function(e) { e.registerVisitorProfileProvider(n(206)), e.registerAudienceMatcher("source_type", n(208)) } }), (function(e, t, n) { var i = n(119),
        r = n(81),
        a = n(99),
        o = n(207),
        s = ["google\\.\\w{2,3}(\\.\\w{2,3})?/(search|url)", "https://(www)?\\.google\\..*?/$", "bing\\.\\w{2,3}(\\.\\w{2,3})?/(search|url)", "yahoo\\.\\w{2,3}(\\.\\w{2,3})?/search", "baidu\\.\\w{2,3}(\\.\\w{2,3})?/s?"];
    e.exports = { provides: "source_type", shouldTrack: !0, isSticky: !1, getter: [function() { return function(e, t) { var n = function() { if (i.getQueryParamValue("utm_source") || i.getQueryParamValue("gclid") || i.getQueryParamValue("otm_source")) return "campaign"; for (var e = a.getReferrer() || r.getReferrer(), t = 0; t < s.length; t++) { var n = s[t],
                                c = e.match(n); if (c) return "search" } return e && o.guessDomain(e) !== o.guessDomain(i.getUrl()) ? "referral" : "direct" },
                    c = function(e, t) { return !e || "direct" !== t },
                    u = e(),
                    l = n();
                c(u, l) && t(l) } }] } }), (function(e, t) { t.guessDomain = function(e, t) { if (!e) return ""; try { return t ? e.match(/:\/\/(.[^\/]+)/)[1] : e.match(/:\/\/(?:www[0-9]?\.)?(.[^\/:]+)/)[1] } catch (e) { return "" } } }), (function(e, t, n) { var i = n(20);
    t.fieldsNeeded = ["source_type"], t.match = function(e, t) { return i.hasMatch(t.value, t.match, e.source_type) } }), (function(e, t, n) { e.exports = function(e) { e.registerVisitorProfileProvider(n(210)), e.registerVisitorProfileProvider(n(211)), e.registerAudienceMatcher("time_and_day", n(212)) } }), (function(e, t, n) { var i = n(24);
    e.exports = { provides: "currentTimestamp", shouldTrack: !0, isLazy: !0, getter: [function() { return i.now() }] } }), (function(e, t) { e.exports = { provides: "offset", shouldTrack: !0, isLazy: !0, getter: [function() { return (new Date).getTimezoneOffset() }] } }), (function(e, t, n) { var i = n(213);
    t.fieldsNeeded = ["currentTimestamp"], t.match = function(e, t) { return i.test(t.value, new Date(e.currentTimestamp)) } }), (function(e, t, n) {
    function i(e) { var t = e.split(o); if (3 !== t.length) throw new Error("Invalid time and day string " + e); var n = t[2].split(s); return { start_time: t[0], end_time: t[1], days: n } }

    function r(e) { var t = e.split(c); if (2 !== t.length) throw new Error("optly.timeAndDayInterval.timeStringToMinutes: Invalid time string " + e); return 60 * parseInt(t[0], 10) + parseInt(t[1], 10) } var a = n(2),
        o = "_",
        s = ",",
        c = ":";
    t.test = function(e, t) { var n = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"],
            o = i(e),
            s = r(o.start_time),
            c = r(o.end_time),
            u = 60 * t.getHours() + t.getMinutes(),
            l = n[t.getDay()]; return u >= s && u <= c && a.includes(o.days, l) } }), (function(e, t, n) {
    function i(e) {
        function t(e, t, n) { try { c(t), e[n] = t } catch (e) { O.emitError(new X("Bad value for eventTags[" + n + "]: " + e.message)) } return e } var n = C.keys(ie),
            i = C.omit(e, n),
            r = C.pick(e, n),
            a = C.reduce(i, t, {}),
            o = C.reduce(r, (function(e, n, i) { var r = ie[i];
                r.excludeFeature || t(a, n, i); try { r.validate(n), e[i] = r.sanitize(n), a[i] = e[i] } catch (e) { O.emitError(new X("Bad value for eventMetrics[" + i + "]: " + e.message)) } return e }), {}); return o.tags = a, o }

    function r(e) { var t = C.extend({ entity_id: e.pageId, key: e.pageId, timestamp: e.timestamp, uuid: e.eventId, type: J }, i(e.eventTags)); return t }

    function a(e) { return C.extend({ entity_id: e.eventEntityId, key: e.eventApiName, timestamp: e.timestamp, uuid: e.eventId, type: e.eventCategory }, i(e.eventTags)) }

    function o(e) {
        return C.extend({
            entity_id: e.eventEntityId,
            key: e.eventApiName,
            timestamp: e.timestamp,
            uuid: e.eventId,
            type: e.eventCategory
        }, i(e.eventTags))
    }

    function s(e) { return { entity_id: null, type: $, uuid: e.eventId, timestamp: e.timestamp } }

    function c(e) { if (null == e) throw new Error("Feature value is null"); if ("object" == typeof e) { var t; try { t = F.stringify(e) } catch (e) {} throw new Error('Feature value is complex: "' + t || '[object]"') } }

    function u(e) { if (null == e) throw new Error("Metric value is null"); if (!C.isNumber(e)) throw new Error("Metric value is not numeric") }

    function l(e) { return C.reduce(e, (function(e, t) { try { c(t.value), e.push({ entity_id: t.id || null, key: t.name, type: t.type, value: t.value }) } catch (e) { M.warn("Error evaluating user feature", t, e) } return e }), []) }

    function d(e, t, n) { Y.dispatch(P.REGISTER_TRACKER_EVENT, { event: e, decisions: n }), f(t), D() }

    function f(e) { var t = l(e);
        Y.dispatch(P.UPDATE_TRACKER_VISITOR_ATTRIBUTES, { attributes: t }) }

    function p(e) { var t = l(e.userFeatures),
            n = { account_id: e.accountId, anonymize_ip: e.anonymizeIP, client_name: e.clientName, client_version: e.clientVersion, project_id: e.projectId, visitors: [{ session_id: g(e.sessionId), visitor_id: e.visitorId, attributes: t, snapshots: [{ decisions: [{ campaign_id: e.layerId, experiment_id: e.experimentId, variation_id: e.variationId, is_campaign_holdback: e.isLayerHoldback }], events: [{ uuid: e.decisionId, entity_id: e.layerId, timestamp: e.timestamp, type: Q }] }] }] };
        Y.dispatch(P.REGISTER_PREVIOUS_BATCH, n), D() }

    function h(e) { var t = C.isNull(K.getAnonymizeIP()) ? void 0 : K.getAnonymizeIP(),
            n = { account_id: e.accountId, anonymize_ip: t, client_name: e.clientName, client_version: e.clientVersion, project_id: e.projectId, visitors: [] };
        n.revision = e.revision, ee && (n.enrich_decisions = !0); var i = { session_id: g(e.sessionId), visitor_id: e.visitorId, attributes: [], snapshots: [] },
            r = R(e.layerStates);
        Y.dispatch(P.REGISTER_TRACKER_VISITOR, { data: n, visitor: i, decisions: r }), D() }

    function g(e) { return se ? oe : e }

    function v(e) { var t = { entity_id: e.layerId, type: Q, uuid: e.decisionId, timestamp: e.timestamp };
        Y.dispatch(P.REGISTER_TRACKER_DECISION, { decisionEvent: t, decisions: R(e.layerStates) }), f(e.userFeatures), D() }

    function m() { if (!W.canSend()) return void M.debug("Not sending events (holding)"); var e = W.hasEventsToSend(),
            t = W.hasPreviousBatchesToSend(); return e || t ? (t && (C.each(W.getPreviousBatches(), _), Y.dispatch(P.RESET_TRACKER_PREVIOUS_BATCHES)), void(e && (Y.dispatch(P.FINALIZE_BATCH_SNAPSHOT), _(W.getEventBatch()), Y.dispatch(P.RESET_TRACKER_EVENTS)))) : void M.debug("Not sending events because there are no events to send") }

    function _(e) { M.debug("Sending ticket:", e); var t = x.generate();
        B.retryableRequest({ url: L, method: "POST", data: E(e) }, t) }

    function E(e) { var t = C.extend({}, C.pick(e, ["account_id", "anonymize_ip", "client_name", "client_version", "enrich_decisions", "project_id", "revision"]), { visitors: C.map(e.visitors, y) }); return t }

    function y(e) { return { visitor_id: e.visitor_id, session_id: oe, attributes: C.map(e.attributes, I), snapshots: C.map(e.snapshots, T) } }

    function I(e) { return w(e, { entity_id: "e", key: "k", type: "t", value: "v" }) }

    function T(e) { var t = e.events; return t = S(t), { activationTimestamp: K.getActivationTimestamp(), decisions: C.map(e.decisions, A), events: C.map(t, b) } }

    function S(e) { var t = C.reduce(e, (function(e, t) { var n; if (n = t.type !== J || !C.isEmpty(t.tags) || !C.isEmpty(C.pick(t, C.keys(ie))) || t.key && t.entity_id !== t.key ? t.uuid : t.type, e[n]) { var i = e[n].timestamp;
                t.timestamp > i && (i = t.timestamp), e[n] = C.extend({}, e[n], { key: Z, entity_id: e[n].entity_id + "-" + t.entity_id, timestamp: i }) } else e[n] = t; return e }), {}); return C.values(t) }

    function A(e) { return w(e, { campaign_id: "c", experiment_id: "x", is_campaign_holdback: "h", variation_id: "v" }) }

    function b(e) { return e.key === Q && (e.type = Q, delete e.key), w(e, { entity_id: "e", key: "k", quantity: "q", revenue: "$", tags: "a", timestamp: "t", uuid: "u", value: "v", type: "y" }) }

    function w(e, t) { return C.reduce(e, (function(e, n, i) { return i in t && (e[t[i] || i] = n), e }), {}) }

    function D() {
        function e() { var t = !re || H.isLoaded();
            t && m(), W.isPolling() && G.setTimeout(e, ne) } return W.shouldBatch() ? void(W.isPolling() || (G.setTimeout(e, ne), Y.dispatch(P.SET_TRACKER_POLLING, !0), G.setTimeout((function() { Y.dispatch(P.SET_TRACKER_BATCHING, !1), Y.dispatch(P.SET_TRACKER_POLLING, !1) }), te))) : void m() }

    function R(e) { return C.map(e, (function(e) { return { campaign_id: e.layerId, experiment_id: e.decision.experimentId, variation_id: e.decision.variationId, is_campaign_holdback: e.decision.isLayerHoldback } })) }

    function N() { var e = W.getPersistableState(); if (e) try { M.debug("Persisting pending batch:", e), U.persistTrackerOptimizelyData(e), Y.dispatch(P.SET_TRACKER_DIRTY, !1) } catch (e) { M.debug("Failed to persist pending batch:", e) } }
    var C = n(2),
        O = n(86),
        x = n(5),
        L = "https://logx.optimizely.com/v1/events",
        P = n(7),
        k = n(77).create,
        F = n(26),
        M = n(23),
        V = n(115),
        U = n(75),
        G = n(41),
        B = n(91),
        j = n(16),
        H = n(81),
        z = n(87),
        q = n(111),
        Y = n(9),
        K = j.get("stores/global"),
        W = j.get("stores/tracker_optimizely"),
        X = t.Error = k("OptimizelyTrackerError"),
        $ = "client_activation",
        Q = "campaign_activated",
        J = "view_activated",
        Z = "multi-event",
        ee = !1,
        te = 1e4,
        ne = 1e3,
        ie = { revenue: { validate: u, sanitize: Math.floor, excludeFeature: !0 }, quantity: { validate: u, sanitize: Math.floor, excludeFeature: !0 }, value: { validate: u, sanitize: C.identity } },
        re = !1,
        ae = !1,
        oe = "AUTO",
        se = !0,
        ce = [function() { return function(e) { v(C.extend(e, e.decision)) } }],
        ue = function(e) { e.timing === V.TrackLayerDecisionTimingFlags.postRedirectPolicy ? p(e) : v(e) },
        le = [function() { return function(e) { d(r(e), e.userFeatures, R(e.layerStates)) } }],
        de = [function() { return function(e) { h(e), d(s(e), e.userFeatures, R(e.layerStates)) } }],
        fe = [function() { return function(e) { d(o(e), e.userFeatures, R(e.layerStates)) } }],
        pe = [function() { return function(e) { d(a(e), e.userFeatures, R(e.layerStates)) } }],
        he = { onLayerDecision: ce, trackLayerDecision: ue, postRedirectPolicy: V.PostRedirectPolicies.TRACK_AFTER_SYNC, nonRedirectPolicy: V.NonRedirectPolicies.TRACK_IMMEDIATELY, onPageActivated: le, onClientActivation: de, onClickEvent: pe, onCustomEvent: fe };
    e.exports = function(e) { e.registerAnalyticsTracker("optimizely", he), z.on({ filter: { type: q.TYPES.ANALYTICS, name: "sendEvents" }, handler: function() { Y.dispatch(P.SET_TRACKER_SEND_EVENTS, !0), W.isPolling() || m() } }), z.on({ filter: { type: q.TYPES.ANALYTICS, name: "holdEvents" }, handler: function() { Y.dispatch(P.SET_TRACKER_SEND_EVENTS, !1) } }), Y.dispatch(P.SET_TRACKER_SEND_EVENTS, !ae); var t = z.on({ filter: { type: "lifecycle", name: "activated" }, handler: function() { W.observe(N), z.off(t) } }) }
}), (function(e, t, n) { e.exports = function(e) { e.registerViewProvider(n(216)), e.registerViewMatcher("url", n(217)) } }), (function(e, t, n) { var i = n(119);
    e.exports = { provides: "url", getter: [function() { return i.getUrl() }] } }), (function(e, t, n) { var i = n(204);
    e.exports = { fieldsNeeded: ["url"], match: function(e, t) { return i(e.url, t) } } }), (function(e, t, n) { var i = n(126).enums.locatorType;
    e.exports = function(e) { e.registerViewTagLocator(i.JAVASCRIPT, n(219)) } }), (function(e, t, n) { var i = n(220),
        r = n(146);
    e.exports = function(e, t) { var n = e.locator; if ("function" == typeof n) { var a = r.apply(n, t); return i(e.valueType, a) } return null } }), (function(e, t, n) { var i = n(2),
        r = n(126).enums,
        a = n(126).Error;
    e.exports = function(e, t) { if (i.isUndefined(t) || i.isNull(t)) return t; var n = t; switch (e) {
            case r.valueType.STRING:
                i.isString(t) || (n = String(t)); break;
            case r.valueType.NUMBER:
                if (n = Number(t), isNaN(n)) throw new a(t + " is not a number"); break;
            case r.valueType.CURRENCY:
                if (n = Number(t), isNaN(n) || Math.floor(n) !== n) throw new a(t + " cannot be parsed as currency (must be an integer)"); break;
            default:
                throw new a("Unknown ViewTag type: " + e) } return n } }), (function(e, t, n) {
    function i(e) { return "apiName: " + e.apiName + ", selector: " + e.eventFilter.selector } var r = n(110),
        a = n(222),
        o = n(23),
        s = n(124);
    e.exports = function(e) { var t = new a(function(e) { s.updateAllViewTags(); var t = r.trackClickEvent(e);
            t ? o.log("Tracking click event:", e) : o.log("Not tracking click event:", e) });
        e.registerEventImplementation("click", { attach: function(e) { t.hasEvents() || t.listen(), t.addEvent(e), o.debug("Started listening for click event (" + i(e) + "):", e) }, detach: function(e) { t.removeEvent(e), t.hasEvents() || t.unlisten(), o.debug("Stopped listening for click event (" + i(e) + "):", e) } }) } }), (function(e, t, n) {
    function i(e) { this.handler = e, this.events = [], this.unlistenFn = null, this.clickHandler = a.bind((function(e) { a.forEach(this.events, a.bind((function(t) { try { var n = t.config && t.config.selector ? t.config.selector : t.eventFilter.selector;
                    r(e, n, t) && this.handler(t) } catch (e) { o.emitError(new l("Unable to handle click for selector" + n + ":" + e.message)) } }), this)) }), this) }

    function r(e, t, n) { for (var i = e.target, r = 0; i;) { var s; try { s = u(i, t) } catch (s) { var c = { typeofElementValue: typeof i, nodeName: a.result(i, ["nodeName"], null), nodeType: a.result(i, ["nodeType"], null), targetName: a.result(e, ["target", "nodeName"], null), targetType: a.result(e, ["target", "nodeType"], null), numParentsTraversed: r, selector: t, errorMessage: s.message, eventId: n.id }; return o.emitError(new l("Unable to evaluate match for element"), c), !1 } if (s) return !0;
            i = i.parentElement, r++ } return !1 } var a = n(2),
        o = n(86),
        s = n(77).create,
        c = n(81),
        u = n(223),
        l = t.Error = s("ClickDelegateError");
    i.prototype.listen = function() { this.unlistenFn = c.addEventListener("click", this.clickHandler, !0) }, i.prototype.unlisten = function() { this.unlistenFn && (this.unlistenFn(), this.unlistenFn = null) }, i.prototype.hasEvents = function() { return this.events.length > 0 }, i.prototype.addEvent = function(e) { this.events.push(e) }, i.prototype.removeEvent = function(e) { this.events = a.filter(this.events, (function(t) { return t.apiName !== e.apiName })) }, e.exports = i }), (function(e, t, n) { e.exports = n(224) }), (function(e, t) { "use strict";

    function n(e, t) { if (r) return r.call(e, t); for (var n = e.parentNode.querySelectorAll(t), i = 0; i < n.length; i++)
            if (n[i] == e) return !0;
        return !1 } var i = Element.prototype,
        r = i.matches || i.matchesSelector || i.webkitMatchesSelector || i.mozMatchesSelector || i.msMatchesSelector || i.oMatchesSelector;
    e.exports = n }), (function(e, t, n) { e.exports = function(e) { e.registerApiModule("recommender", n(226)) } }), (function(e, t, n) {
    function i(e) { return e === A.SELF_CONTAINED_MULTI_COL }

    function r(e) { return e === A.SEPARATE_CATALOG_SINGLE_COL }

    function a(e, t) { if (!i(t)) return w in e ? y.parse(e[w]) : (I.warn('recommender / Expected key "' + w + '" not found'), []); var n = new RegExp("^" + D + "(\\d+)$"),
            r = E.reduce(e, (function(t, i, r) { var a = r.match(n); if (a) { var o = y.parse(e[r]);
                    o && E.isObject(o) && (t[a[1]] = o) } return t }), []); return E.filter(r, (function(e) { return !E.isUndefined(e) })) }

    function o(e, t) { return [e, encodeURIComponent(t)].join("/") }

    function s(e, t) { var n = function(e, t, n) { if (!E.isArray(e) || E.isEmpty(e)) throw new Error("No " + n + "s available"); var i; if (t) { if (i = E.find(e, { id: t }), !i) throw new Error("No " + n + " found with specified id " + t) } else { if (e.length > 1) throw new Error("No " + n + " id specified when there are more than one available.");
                    i = e[0] } return i },
            i = e.getRecommenderServices(),
            r = n(i, t && t.recommenderServiceId, "recommender service");
        r.idTagName || (r.idTagName = "id"); var a = n(r.recommenders, t && t.recommenderId, "recommender"); return { recommenderService: r, recommender: a } }

    function c(e) { var t = y.parse(e); return t.data }

    function u(e, t) { return S.request({ url: o(e, t) }).then((function(e) { var t = c(e.response),
                n = {}; return b in t && (n = y.parse(t[b])), n })) }

    function l(e, t, n, i) { var s = !(!i || !i.fetchWithMetadata),
            u = i && i.overrideServingUrl || t.servingUrl; return S.request({ url: o(u, n) }).then((function(t) { var n = [],
                o = c(t.response); return n = a(o, e.serviceVersion), s && r(e.serviceVersion) ? d(e, E.map(n, e.idTagName), i).then((function(e) { return e = E.map(e, (function(e) { return e instanceof Error && (e = {}), delete e["__proto__"], e })), E.merge(n, e) })) : n })) }

    function d(e, t, n) { var i = n && n.overrideCatalogUrl || e.catalogUrl; return E.isString(t) ? u(i, t) : T.all(E.map(t, (function(e) { return u(i, e)["catch"]((function(t) { return I.warn("recommender / Failed to get recommendations for item:", e, t), t })) }))) }

    function f(e, t, n) { var i = s(e, n),
            r = i.recommenderService,
            a = i.recommender; return l(r, a, t, { overrideServingUrl: n && n.overrideServingUrl, overrideCatalogUrl: n && n.overrideCatalogUrl, fetchWithMetadata: !0 }) }

    function p(e, t, n) { var i = s(e, n),
            a = i.recommenderService,
            o = i.recommender; if (!r(a.serviceVersion)) throw new Error("Recommender service " + a.id + " serves self-contained recos. Use `fetchRecommendations` API call"); return l(a, o, t, { overrideServingUrl: n && n.overrideServingUrl, fetchWithMetadata: !1 }) }

    function h(e, t, n) { var i = s(e, n),
            a = i.recommenderService; if (!r(a.serviceVersion)) throw new Error("Recommender service " + a.id + " serves self-contained recos. Use `fetchRecommendations` API call"); return E.isArray(t) && t.length > R ? T.reject("Cannot fetch more than " + R + " items") : d(a, t, n) }

    function g(e, t, n, i) { return new v(e, t, n, i) }

    function v(e, t, n, i) { this.recommenderSettings = t; var r = s(e, t);
        this.recommenderService = r.recommenderService, this.recommender = r.recommender, this.targetId = n, i && (this.preFilter = i.preFilter, this.canonicalize = i.canonicalize, this.postFilter = i.postFilter), this._globalStore = e, this.reset() }

    function m() { var e = { recosNoMeta: [], recosNoMetaOffset: 0, recos: [], recosOffset: 0 }; return I.debug("recommender / fetching recommended items for", this.targetId, "from recommender", this.recommender.id), r(this.recommenderService.serviceVersion) ? p(this._globalStore, this.targetId, this.recommenderSettings).then(E.bind((function(t) { return e.recosNoMeta = E.filter(t, this.preFilter), e }), this)) : f(this._globalStore, this.targetId, this.recommenderSettings).then(E.bind((function(t) { return e.recos = E(t).filter(this.preFilter).map(this.canonicalize).filter(this.postFilter).value(), e }), this)) }

    function _(e) { var t = e.numNeeded,
            n = e.buffer,
            i = n.recosNoMeta.length - n.recosNoMetaOffset; if (t <= 0 || i <= 0) return T.resolve(e); var r = 5,
            a = Math.max(t, r),
            o = E.slice(n.recosNoMeta, n.recosNoMetaOffset, n.recosNoMetaOffset + a),
            s = E.map(o, this.recommenderService.idTagName);
        n.recosNoMetaOffset += s.length; var c = e.recosSoFar; return I.debug("recommender / fetching metadata for", s, "from recommender", this.recommender.id), h(this._globalStore, s, this.recommenderSettings).then(E.bind((function(i) { i = E.map(i, (function(e) { return e instanceof Error ? {} : e })), n.recos = E(o).merge(i).map(this.canonicalize).filter(this.postFilter).value(); var r = E.slice(n.recos, 0, t); return n.recosOffset = r.length, e.recosSoFar = c.concat(r), e.numNeeded -= r.length, e.numNeeded > 0 ? _.call(this, e) : e }), this)) } var E = n(2),
        y = n(26),
        I = n(23),
        T = n(12).Promise,
        S = n(91),
        A = { SELF_CONTAINED_MULTI_COL: 1, SEPARATE_CATALOG_SINGLE_COL: 2 },
        b = "itemMetadata",
        w = "recos",
        D = "reco",
        R = 20;
    v.prototype.reset = function() { this._bufferPromise = null }, v.prototype.next = function(e) { var t = Math.floor(E.isNumber(e) ? e : 1); if (t <= 0) throw new Error("Invalid argument " + e + ". Must be positive integer");
        this._bufferPromise || (this._bufferPromise = m.call(this)); var n = this._bufferPromise.then(E.bind((function(e) { var n = [],
                i = e.recos.length - e.recosOffset; return i > 0 && (n = e.recos.slice(e.recosOffset, e.recosOffset + t), e.recosOffset += n.length, t -= n.length), t <= 0 ? { result: n, buffer: e } : _.call(this, { buffer: e, recosSoFar: n, numNeeded: t }).then((function(e) { return { result: e.recosSoFar, buffer: e.buffer } })) }), this)); return this._bufferPromise = n.then((function(e) { return e.buffer })), n.then((function(e) { return e.result })) }, e.exports = ["stores/global", function(e) { return { fetchRecommendations: E.partial(f, e), fetchRecommendedItems: E.partial(p, e), fetchItemMetadata: E.partial(h, e), getRecommendationsFetcher: E.partial(g, e) } }] }), (function(e, t, n) {
    function i(e, t) { if (this.change = e, this.identifier = t.identifier, this.startTime = t.startTime, d.shouldObserveChangesIndefinitely()) { h.dispatch(a.INITIALIZE_CHANGE_METRICS), this.rateMeter = new v(E.MOVING_WINDOW_MILLISECONDS); var n = r.isNull(E.MAX_MACROTASKS_IN_MOVING_WINDOW) ? Number.POSITIVE_INFINITY : E.MAX_MACROTASKS_IN_MOVING_WINDOW;
            this.rateMeter.addListener(n, r.bind((function() { g.warn("AppendChange", this, "has overheated and will no longer apply or reapply"), this.cancel(), h.dispatch(a.RECORD_CHANGE_OVERHEATED), o.emitError(new y("Change " + this.identifier + " has overheated"), { layerId: t.action && t.action.layerId, experimentId: t.action && t.action.experimentId, variationId: t.action && t.action.variationId, changeId: e.id, changeType: e.type, movingWindowMilliseconds: E.MOVING_WINDOW_MILLISECONDS, maxMacroTasksInMovingWindow: n }) }), this)); for (var i = Math.min(n, 50), s = 0; s <= i; s++) this.rateMeter.addListener(s, r.partial((function(e) { h.dispatch(a.RECORD_CHANGE_MACROTASK_RATE, { changeMacrotaskRate: e }) }), s)) } } var r = n(2),
        a = n(7),
        o = n(86),
        s = n(139),
        c = n(77).create,
        u = n(228),
        l = n(229),
        d = n(16).get("stores/directive"),
        f = n(81),
        p = n(230),
        h = n(9),
        g = n(23),
        v = n(231),
        m = n(138),
        _ = n(100).create(),
        E = { MOVING_WINDOW_MILLISECONDS: 1e3, MAX_MACROTASKS_IN_MOVING_WINDOW: 10 },
        y = c("ChangeOverheatError");
    i.prototype.numberOfRootNodes = function(e) { var t = document.createElement("div"); return t.innerHTML = e, t.childNodes.length }, i.prototype.getSiblingElements = function(e, t, n) { for (var i = e, r = [], a = 0; a < t; a++) n ? (r.push(i.nextSibling), i = i.nextSibling) : (r.push(i.previousSibling), i = i.previousSibling); return r }, i.prototype.apply = function() { this.applyDeferred = l(); try { var e = this.numberOfRootNodes(this.change.value); if (!e) throw new Error("No DOM elements to be created for this change: " + this.change.value); var t = r.partial(this.applyDeferred.reject, new Error("Unable to find selector.")),
                n = {};
            d.shouldObserveChangesUntilTimeout() ? n = { timeout: r.partial(m.isTimedOut, this.startTime), onTimeout: t } : d.isEditor() && _.waitUntil(r.partial(m.isTimedOut, this.startTime)).then(t, t), this.unobserveSelector = _.observeSelector(this.change.selector, r.bind(this.maybeApplyToElement, this), n); var i = f.querySelectorAll(this.change.selector);
            r.each(i, r.bind(this.maybeApplyToElement, this)) } catch (e) { this.applyDeferred.reject(e) } return this.applyDeferred }, i.prototype.maybeApplyToElement = function(e) { var t = s.CHANGE_ID_ATTRIBUTE_PREFIX + this.change.id; if (e.hasAttribute(t)) return g.debug("Not applying AppendChange to element", e, "because it was inserted by this change"), void this.applyDeferred.resolve();
        this.rateMeter && this.rateMeter.countCurrentTick(); var n = r.bind(this.applyOrReapplyToElement, this, e, t);
        n(), this.applyDeferred.resolve() }, i.prototype.applyOrReapplyToElement = function(e, t) { var n; switch (this.change.operator) {
            case p.DOMInsertionType.AFTER:
                n = p.insertAdjacentHTMLType.AFTER_END; break;
            case p.DOMInsertionType.APPEND:
                n = p.insertAdjacentHTMLType.BEFORE_END; break;
            case p.DOMInsertionType.BEFORE:
                n = p.insertAdjacentHTMLType.BEFORE_BEGIN; break;
            case p.DOMInsertionType.PREPEND:
                n = p.insertAdjacentHTMLType.AFTER_BEGIN; break;
            default:
                n = p.insertAdjacentHTMLType.BEFORE_END }
        e.insertAdjacentHTML(n, this.change.value), e.setAttribute(t, ""), u.setData(e, this.change.id, this.identifier, []); var i, a, o = this.numberOfRootNodes(this.change.value) - 1;
        n === p.insertAdjacentHTMLType.BEFORE_END ? (i = e.lastChild, a = this.getSiblingElements(i, o, !1)) : n === p.insertAdjacentHTMLType.AFTER_BEGIN ? (i = e.firstChild, a = this.getSiblingElements(i, o, !0)) : n === p.insertAdjacentHTMLType.BEFORE_BEGIN ? (i = e.previousSibling, a = this.getSiblingElements(i, o, !1)) : n === p.insertAdjacentHTMLType.AFTER_END && (i = e.nextSibling, a = this.getSiblingElements(i, o, !0)), a.unshift(i), r.each(a, r.bind((function(e) { var n = e.nodeType === Node.ELEMENT_NODE ? e : f.parentElement(e);
            n.setAttribute(t, ""); var i = u.getData(n, this.change.id, this.identifier) || [];
            i.push(e), u.setData(n, this.change.id, this.identifier, i), r.each(f.childrenOf(n), (function(e) { e.setAttribute(t, "") })) }), this)) }, i.prototype.cancel = function() { this.unobserveSelector && this.unobserveSelector() }, e.exports = function(e) { e.registerChangeApplier(p.changeType.APPEND, i) } }), (function(e, t, n) {
    function i(e, t) { return [e, t].join("_") } var r = n(2),
        a = n(139).CHANGE_DATA_KEY;
    t.getData = function(e, t, n) { var r = i(t, n); return e[a] && e[a][r] ? e[a][r] : null }, t.hasData = function(e) { return Boolean(e && e[a] && !r.isEmpty(e[a])) }, t.removeData = function(e, t, n) { e[a] && delete e[a][i(t, n)] }, t.setData = function(e, t, n, r) { if ("object" != typeof r) throw new Error("setData expects an object");
        e[a] || (e[a] = {}), e[a][i(t, n)] = r } }), (function(e, t, n) { var i = n(2),
        r = n(12).Promise,
        a = function() { var e, t, n = new r(function(n, i) { e = n, t = i }); return n.resolve = function() { return e.apply(null, i.toArray(arguments)), n }, n.reject = function() { return t.apply(null, i.toArray(arguments)), n }, n };
    e.exports = a }), (function(e, t, n) { var i = n(8);
    e.exports = { changeType: { CUSTOM_CODE: "custom_code", ATTRIBUTE: "attribute", APPEND: "append", REARRANGE: "rearrange", REDIRECT: "redirect", WIDGET: "widget" }, DOMInsertionType: { AFTER: "after", APPEND: "append", BEFORE: "before", PREPEND: "prepend" }, insertAdjacentHTMLType: { AFTER_BEGIN: "afterbegin", AFTER_END: "afterend", BEFORE_BEGIN: "beforebegin", BEFORE_END: "beforeend" }, selectorChangeType: { CLASS: "class", HTML: "html", HREF: "href", SRC: "src", STYLE: "style", TEXT: "text", HIDE: "hide", REMOVE: "remove" }, changeApplierState: i({ APPLIED: null, APPLYING: null, UNAPPLIED: null, UNDOING: null }), changeState: i({ BLOCKED: null, UNAPPLIED: null, APPLIED: null, APPLYING: null, UNDOING: null, TIMED_OUT: null, IGNORED: null, ERROR: null }) } }), (function(e, t, n) {
    function i(e) { this.windowLength = e, this.count = 0, this.listeners = {}, this.isIncrementingTick = !1 } var r = n(2),
        a = n(23),
        o = n(41);
    i.prototype.countCurrentTick = function() { this.isIncrementingTick || (this.isIncrementingTick = !0, this.increment(), o.setTimeout(r.bind((function() { this.isIncrementingTick = !1 }), this), 0)) }, i.prototype.increment = function() { this.count += 1, r.forEach(this.listeners[String(this.count)], (function(e) { e() })), o.setTimeout(r.bind(this.decrement, this), this.windowLength) }, i.prototype.decrement = function() { this.count -= 1, this.count < 0 && (a.warn("Decremented down to negative count: ", this.count), this.count = 0) }, i.prototype.addListener = function(e, t) { this.listeners[e] || (this.listeners[e] = []), this.listeners[e].push(t) }, e.exports = i }), (function(e, t, n) {
    function i(e, t) { if (this.change = r.cloneDeep(e), this.change = y.transformVisibilityAttributesToCSS(this.change), this.identifier = t.identifier, this.startTime = t.startTime, this.disconnectObserverQueue = [], d.shouldObserveChangesIndefinitely()) { h.dispatch(a.INITIALIZE_CHANGE_METRICS), this.rateMeter = new m(S.MOVING_WINDOW_MILLISECONDS); var n = r.isNull(S.MAX_MACROTASKS_IN_MOVING_WINDOW) ? Number.POSITIVE_INFINITY : S.MAX_MACROTASKS_IN_MOVING_WINDOW;
            this.rateMeter.addListener(n, r.bind((function() { g.warn("AttributeChange", this, "has overheated and will no longer apply or reapply"), this.cancel(), h.dispatch(a.RECORD_CHANGE_OVERHEATED), o.emitError(new b("Change " + this.identifier + " has overheated"), { layerId: t.action && t.action.layerId, experimentId: t.action && t.action.experimentId, variationId: t.action && t.action.variationId, changeId: e.id, changeType: e.type, movingWindowMilliseconds: S.MOVING_WINDOW_MILLISECONDS, maxMacroTasksInMovingWindow: n }) }), this)); for (var i = Math.min(n, 50), s = 0; s <= i; s++) this.rateMeter.addListener(s, r.partial((function(e) { h.dispatch(a.RECORD_CHANGE_MACROTASK_RATE, { changeMacrotaskRate: e }) }), s)) }
        this.cancelled = !1 } var r = n(2),
        a = n(7),
        o = n(86),
        s = n(139),
        c = n(77).create,
        u = n(228),
        l = n(229),
        d = n(16).get("stores/directive"),
        f = n(81),
        p = n(230),
        h = n(9),
        g = n(23),
        v = n(233),
        m = n(231),
        _ = n(138),
        E = n(41),
        y = n(234),
        I = n(100).create(),
        T = { attributes: !0, childList: !0, subtree: !0, characterData: !0 },
        S = { MOVING_WINDOW_MILLISECONDS: 1e3, MAX_MACROTASKS_IN_MOVING_WINDOW: 10 },
        A = !1,
        b = c("ChangeOverheatError");
    i.prototype.apply = function() { this.applyDeferred = l(); try { if (r.isEmpty(this.change.attributes) && r.isEmpty(this.change.css)) return g.debug("Not applying empty AttributeChange"), this.applyDeferred.resolve(), this.applyDeferred; var e = r.partial(this.applyDeferred.reject, new Error("Unable to find selector.")),
                t = {};
            d.shouldObserveChangesUntilTimeout() ? t = { timeout: r.partial(_.isTimedOut, this.startTime), onTimeout: e } : d.isEditor() && I.waitUntil(r.partial(_.isTimedOut, this.startTime)).then(e, e), this.unobserveSelector = I.observeSelector(this.change.selector, r.bind(this.maybeApplyToElement, this), t); var n = f.querySelectorAll(this.change.selector);
            r.each(n, r.bind(this.maybeApplyToElement, this)) } catch (e) { this.applyDeferred.reject(e) } return this.applyDeferred }, i.prototype.maybeApplyToElement = function(e) { var t = s.CHANGE_ID_ATTRIBUTE_PREFIX + this.change.id; if (e.hasAttribute(t)) return g.debug("AttributeChange not being applied. Element already changed, or is a child of an element that was changed by this AttributeChange. " + e), void this.applyDeferred.resolve();
        this.rateMeter && this.rateMeter.countCurrentTick(); var n = r.bind(this.applyOrReapplyToElement, this, e, t); if (n(), A) { var i = r.bind((function() { E.setTimeout(r.bind((function() { this.cancelled || v.observe(a, e, T) }), this)) }), this),
                a = v.create(r.bind((function() { this.rateMeter && this.rateMeter.countCurrentTick(), a.disconnect(), n(), i() }), this));
            i(), this.disconnectObserverQueue.push(r.bind(a.disconnect, a)) }
        this.applyDeferred.resolve() }, i.prototype.applyOrReapplyToElement = function(e, t) { var n = {};
        r.forOwn(this.change.attributes, (function(i, a) { switch (a) {
                case p.selectorChangeType.CLASS:
                    r.isUndefined(e.className) || (n[p.selectorChangeType.CLASS] = e.className, e.className = i); break;
                case p.selectorChangeType.HREF:
                    r.isUndefined(e.href) || (n[p.selectorChangeType.HREF] = e.href, e.href = i); break;
                case p.selectorChangeType.HTML:
                    r.isUndefined(e.innerHTML) || (n[p.selectorChangeType.HTML] = e.innerHTML, e.innerHTML = i, r.each(f.childrenOf(e), (function(e) { e.setAttribute(t, "") }))); break;
                case p.selectorChangeType.SRC:
                    r.isUndefined(e.src) || (n[p.selectorChangeType.SRC] = e.src, e.src = i); break;
                case p.selectorChangeType.STYLE:
                    break;
                case p.selectorChangeType.TEXT:
                    r.isUndefined(e.textContent) || (n[p.selectorChangeType.TEXT] = e.textContent, e.textContent = i); break;
                default:
                    throw new Error("Unrecognized attribute: " + a) } })); var i = y.createStylesFromChange(e.style.cssText, this.change);
        r.isString(i) && (n[p.selectorChangeType.STYLE] = e.style.cssText, e.style.cssText = i), e.setAttribute(t, ""), u.setData(e, this.change.id, this.identifier, n) }, i.prototype.cancel = function() { this.cancelled = !0, this.unobserveSelector && this.unobserveSelector(), r.each(this.disconnectObserverQueue, (function(e) { try { e() } catch (e) {} })) }, e.exports = function(e) { e.registerChangeApplier(p.changeType.ATTRIBUTE, i) } }), (function(e, t) { t.create = function(e) { return new MutationObserver(e) }, t.observe = function(e, t, n) { e.observe(t, n) } }), (function(e, t, n) { var i = n(2),
        r = n(230);
    t.transformVisibilityAttributesToCSS = function(e) { if (!e.attributes) return e; if (e.attributes[r.selectorChangeType.HIDE] || e.attributes[r.selectorChangeType.REMOVE]) { var t = i.extend({ css: {} }, i.cloneDeep(e)); return e.attributes[r.selectorChangeType.HIDE] && (t.css.visibility = "hidden", delete t.attributes[r.selectorChangeType.HIDE]), e.attributes[r.selectorChangeType.REMOVE] && (t.css.display = "none", delete t.attributes[r.selectorChangeType.REMOVE]), t } return e }, t.createStylesFromChange = function(e, t) { if (i.isEmpty(t.css)) return t.attributes.style; var n = "",
            r = t.attributes.style || ""; return i.each(t.css, (function(e, t) { var i = new RegExp(t + "\\s?:");
            i.test(r) || (n += t + ":" + e + ";") })), i.isUndefined(t.attributes.style) ? (e || "") + n : n + r } }), (function(e, t, n) {
    function i(e, t) { if (!r.isFunction(e.value)) throw new Error("Custom code must be a function");
        this.change = e } var r = n(2),
        a = n(229),
        o = n(230),
        s = n(146);
    i.prototype.apply = function() { var e = a(); try { s.apply(this.change.value), e.resolve() } catch (t) { e.reject(t) } return e }, e.exports = function(e) { e.registerChangeApplier(o.changeType.CUSTOM_CODE, i) } }), (function(e, t, n) {
    function i(e, t) { if (this.change = e, this.config = t, this.reasonShouldNotRedirect = this._checkForReasonNotToRedirect(), !this.reasonShouldNotRedirect) { if (!r.isString(this.change.dest) && !r.isFunction(this.change.dest)) throw new Error("Redirect destination is not a string or function"); if (r.isFunction(this.change.dest) && (this.change.dest = this.change.dest(), !r.isString(this.change.dest))) throw new Error("Redirect destination function did not evaluate to a string"); if (!this.config.action) throw new Error("Redirect changes require an action to be passed in the config.");
            d.dispatch(o.ANNOUNCE_PENDING_REDIRECT, { layerId: this.config.action.layerId }) } } var r = n(2),
        a = n(12).Promise,
        o = n(7),
        s = n(25),
        c = n(76),
        u = n(81),
        l = n(230),
        d = n(9),
        f = n(237),
        p = n(23),
        h = n(41),
        g = n(16),
        v = g.get("stores/action_data"),
        m = "COOKIE";
    i.prototype.apply = function() { if (this.reasonShouldNotRedirect) return p.warn("Redirect change is not being applied because: " + this.reasonShouldNotRedirect), a.resolve(l.changeState.IGNORED); if (this.reasonInvalid) return p.error("Redirect change cannot be applied because:", this.reasonInvalid), a.reject(this.reasonInvalid); try { this.tn(this.nn()) } catch (e) { return a.reject(e) } return a.resolve(l.changeState.APPLIED) }, i.prototype.nn = function() { var e = f.create(this.change.dest); if (this.change.preserveParameters) { var t = h.getLocation().search;
            t && (e.search ? e.search += "&" + t.substr(1) : e.search = t) } return e.toString() }, i.prototype.rn = function() { var e = u.createElement("style");
        e.setAttribute("type", "text/css"), e.innerHTML = "body{display:none;visibility:hidden;}", u.appendToHead(e) }, i.prototype.tn = function(e) { if (!m) { var t, n = v.getByChangeId(this.config.identifier);
            n && (t = n.variationId); var i = (t || "unknown variation") + "|" + u.getReferrer();
            p.debug("Change / Redirect / Setting redirect data:", i), c.set(s.COOKIES.REDIRECT, i, { maxAge: 5 }) } var r = !this.change.hasOwnProperty("hidePage") || this.change.hidePage;
        r && this.rn(), h.setLocation(e) }, i.prototype._checkForReasonNotToRedirect = function() { var e = c.get(s.COOKIES.REDIRECT); if (!this.change.allowAdditionalRedirect && !r.isUndefined(e)) return "Page was already redirected." }, e.exports = function(e) { e.registerChangeApplier(l.changeType.REDIRECT, i) } }), (function(e, t, n) { var i = n(2);
    t.create = function(e) { if (i.isEmpty(e)) return null; var t = document.createElement("a"); return t.href = e, t } }), (function(e, t, n) {
    function i(e, t) { this.change = r.extend({}, e), this.identifier = t.identifier } var r = n(2),
        a = n(229),
        o = n(230),
        s = n(106),
        c = "showWidget";
    i.prototype.apply = function() { return s.emit({ type: c, name: this.change.widget_id, data: this.change }), a().resolve(o.changeState.APPLIED) }, e.exports = function(e) { e.registerChangeApplier(o.changeType.WIDGET, i) } }), (function(e, t, n) {
    function i(e, t, n, i) { var l, d = e.decisionMetadata.experimentPriorities[i],
            f = function(n, i) { var r = c.getExperimentById(e, i.entityId); return c.isValidExperiment(t, r) && n.push(i.entityId), n },
            p = o.reduce(d, f, []); if (!o.isEmpty(p)) { var h, g = o.filter(d, { endOfRange: null }); if (g && g.length === d.length) h = r(p);
            else { if (0 !== g.length) throw new u("You must specify weights for all or none of the experiments in each priority layer.");
                h = a(p, d) } if (l = s.chooseWeightedCandidate(n, e.id, h)) { var v = c.getExperimentById(e, l); if (v) return v } } }

    function r(e) { for (var t = [], n = Math.round(1e4 / e.length), i = 0; i < e.length; i++) t.push({ entityId: e[i], endOfRange: n * (i + 1) }); return t[t.length - 1].endOfRange = 1e4, t }

    function a(e, t) { for (var n = [], i = [], r = 0, a = 0; a < t.length; a++) { var s = 0;
            e.indexOf(t[a].entityId) >= 0 && (s = 0 === a ? t[0].endOfRange : t[a].endOfRange - t[a - 1].endOfRange, r += s, i.push({ entityId: t[a].entityId, points: s })) } if (o.isEmpty(n))
            for (var c = 0, u = 0; u < i.length; u++) { var l = Math.round(1e4 * i[u].points / r) + c;
                n.push({ entityId: i[u].entityId, endOfRange: l }), c = l }
        return n[n.length - 1].endOfRange = 1e4, n } var o = n(2),
        s = n(142),
        c = n(143),
        u = n(144).DecisionError,
        l = "equal_priority",
        d = { decideLayer: function(e, t) { if (!e.decisionMetadata || !e.decisionMetadata.experimentPriorities) throw new u("No decisionMetadata.experimentPriorities on layer."); for (var n = e.decisionMetadata.experimentPriorities.length, r = 0; r < n; r++) { var a = i(e, t.audienceIds, t.bucketingId, r); if (a) { var o = c.selectVariation(a, t.audienceIds, t.bucketingId, t.activationId, t.preferredVariationMap); if (!e.decisionMetadata.offerConsistency || c.hasVariationActionsOnView(o, t.pageId)) return { experiment: a, variation: o } } } throw new u("No eligible experiment and variation found.") }, includePageIdInDecisionTicket: function(e) { return !(!e.decisionMetadata || !e.decisionMetadata.offerConsistency) } };
    e.exports = function(e) { e.registerDecider(l, d) } }), (function(e, t, n) { var i = n(143),
        r = n(144).DecisionError,
        a = "single_experiment",
        o = "multivariate",
        s = { selectExperiment: function(e, t, n) { if (e.experiments.length < 1) throw new r("Unable to find experiment to bucket user into"); var a = e.experiments[0]; if (!i.isValidExperiment(t, a)) throw new r('Audience conditions failed for experiment: "' + a.id + '".'); return a } };
    e.exports = function(e) { e.registerDecider(a, s), e.registerDecider(o, s) } })]);