window.addEventListener('load', function() {
    if (window.ga && ga.create) {
        console.log('Google Analytics is loaded');

        var img = document.createElement('img');
        img.setAttribute('style', 'display:none;');
        img.src = '/wp-content/themes/mintmobile/functions/adblock-tracker.php?tid=UA-81744027-1&ec=Allowing&ea=Google%20Analytics';
        document.body.appendChild(img);
    } else {
        console.log('Google Analytics is not loaded');

        var img = document.createElement('img');
        img.setAttribute('style', 'display:none;');
        img.src = '/wp-content/themes/mintmobile/functions/adblock-tracker.php?tid=UA-81744027-1&ec=Blocking&ea=Google%20Analytics';
        document.body.appendChild(img);
    }

    if (window.google_tag_manager) {
        console.log('Google Tag Manager is loaded');

        var img = document.createElement('img');
        img.setAttribute('style', 'display:none;');
        img.src = '/wp-content/themes/mintmobile/functions/adblock-tracker.php?tid=UA-81744027-1&ec=Allowing&ea=Google%20Tag%20Manager';
        document.body.appendChild(img);
    } else {
        console.log('Google Tag Manager is not loaded');

        var img = document.createElement('img');
        img.setAttribute('style', 'display:none;');
        img.src = '/wp-content/themes/mintmobile/functions/adblock-tracker.php?tid=UA-81744027-1&ec=Blocking&ea=Google%20Tag%20Manager';
        document.body.appendChild(img);
    }
    if (window.optimizely) {
        console.log('Optimizely is loaded');

        var img = document.createElement('img');
        img.setAttribute('style', 'display:none;');
        img.src = '/wp-content/themes/mintmobile/functions/adblock-tracker.php?tid=UA-81744027-1&ec=Allowing&ea=Optimizely';
        document.body.appendChild(img);
    } else {
        console.log('Optimizely is not loaded');

        var img = document.createElement('img');
        img.setAttribute('style', 'display:none;');
        img.src = '/wp-content/themes/mintmobile/functions/adblock-tracker.php?tid=UA-81744027-1&ec=Blocking&ea=Optimizely';
        document.body.appendChild(img);
    }
}, false);